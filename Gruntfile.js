'use strict';
module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Show grunt task time
    require('time-grunt')(grunt);

    // Configurable paths for the app
    var appConfig = {
        app: 'app',
        dist: 'dist'
    };

    // Grunt configuration
    grunt.initConfig({

        // Project settings
        homer: appConfig,
        pkg: grunt.file.readJSON('package.json'),
        // The grunt server settings
        connect: {
            options: {
                port: 3000,
                hostname: 'localhost',
                livereload: 35730
            },
            livereload: {
                options: {
                    open: true,
                    middleware: function (connect) {
                        return [
                            connect.static('.tmp'),
                            connect().use(
                                '/bower_components',
                                connect.static('./bower_components')
                            ),
                            connect.static(appConfig.app)
                        ];
                    }
                }
            },
            dist: {
                options: {
                    open: true,
                    base: '<%= homer.dist %>'
                }
            }
        },
        // Compile less to css
        less: {
            development: {
                options: {
                    compress: true,
                    optimization: 2
                },
                files: {
                    "app/styles/style.css": "app/less/style.less"
                }
            }
        },
        // Watch for changes in live edit
        watch: {
            styles: {
                files: ['app/less/**/*.less'],
                tasks: ['less', 'copy:styles'],
                options: {
                    nospawn: true,
                    livereload: '<%= connect.options.livereload %>'
                },
            },
            js: {
                files: ['<%= homer.app %>/scripts/{,*/}*.js'],
                options: {
                    livereload: '<%= connect.options.livereload %>'
                }
            },
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                    '<%= homer.app %>/**/*.html',
                    '.tmp/styles/{,*/}*.css',
                    '<%= homer.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                ]
            }
        },
        uglify: {
            options: {
                mangle: false
            }
        },
        // Clean dist folder
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%= homer.dist %>/{,*/}*',
                        '!<%= homer.dist %>/.git*'
                    ]
                }]
            },
            server: '.tmp'
        },
        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= homer.app %>',
                        dest: '<%= homer.dist %>',
                        src: [
                            '*.{ico,png,txt}',
                            '.htaccess',
                            '*.html',
                            'views/{,*/}*.html',
                            'styles/img/*.*',
                            'images/{,*/}*.*'
                        ]
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: 'bower_components/fontawesome',
                        src: ['fonts/*.*'],
                        dest: '<%= homer.dist %>'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: 'bower_components/footable/css/fonts',
                        src: ['*'],
                        dest: 'dist/styles/fonts/'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: 'img',
                        src: ['*'],
                        dest: 'dist/img'   
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: 'bower_components/bootstrap',
                        src: ['fonts/*.*'],
                        dest: '<%= homer.dist %>'
                    },
                  
                    {
                        expand: true,
                        dot: true,
                        cwd: 'app/fonts/pe-icon-7-stroke/',
                        src: ['fonts/*.*'],
                        dest: '<%= homer.dist %>'
                    },
                ]
            },
            styles: {
                expand: true,
                cwd: '<%= homer.app %>/styles',
                dest: '.tmp/styles/',
                src: '{,*/}*.css'
            }
        },
        'string-replace' : {
            dist: {
                options: {
                    replacements: [
                        {
                            pattern: /@build/g,
                            replacement: '<%= pkg.buildnum %>'
                        }
                    ]
                },
                files: [
                    {
                        expand: true,
                        src: [
                          '<%= homer.dist %>/index.html'
                        ]
                    }
                ]
            },
            distt: {
                options: {
                    replacements: [
                        {
                            pattern: /http:\/\/localhost:9000/g,
                            replacement: 'https://testapi.2docstore.com'
                        }
                    ]
                },
                files: [
                    {
                        expand: true,
                        src: [
                          '<%= homer.dist %>/scripts/**.js'
                        ]
                    }
                ]
            }

        },
          removelogging: {
            dist: {
              src: "<%= homer.dist %>/**/*.js" // Each file will be overwritten with the output! 
            }
          },

        // Renames files for browser caching purposes
        filerev: {
            dist: {
                src: [
                    '<%= homer.dist %>/scripts/{,*/}*.js',
                    '<%= homer.dist %>/styles/{,*/}*.css'
                ]
            }
        },
        buildnumber: {
            options: {
              field: 'buildnum',
            },
            files: ['package.json', 'bower.json']
        },
        htmlmin: {
            dist: {
                options: {
                    collapseWhitespace: true,
                    conservativeCollapse: true,
                    collapseBooleanAttributes: true,
                    removeCommentsFromCDATA: true,
                    removeOptionalTags: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= homer.dist %>',
                    src: ['*.html', 'views/{,*/}*.html'],
                    dest: '<%= homer.dist %>'
                }]
            }
        },
        useminPrepare: {
            html: 'app/index.html',
            options: {
                dest: 'dist'
            }
        },
        usemin: {
            html: ['dist/index.html']
        }
    });

    grunt.registerTask('live', [
        'clean:server',
        'copy:styles',
        'connect:livereload',
        'watch'
    ]);

    grunt.registerTask('server', [
        'build',
        'connect:dist:keepalive'
    ]);
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks("grunt-remove-logging");
    grunt.loadNpmTasks('grunt-build-number');
    grunt.registerTask('build', [
        'clean:dist',
        'less',
        'useminPrepare',
        'concat',
        'copy:dist',
        'cssmin',
        'uglify',
        'filerev',
        'usemin',
        //'removelogging',
        'htmlmin',
        'buildnumber',
        'string-replace'
    ]);

};