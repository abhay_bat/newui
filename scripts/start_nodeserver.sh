#!/bin/bash
cd /var/www/2docstore
sudo NODE_ENV=production PORT=9000 forever start --spinSleepTime 10000 server/app.js