/**
 * 15th Feb 2017
 * Directives for subscription plans
 * Chaithra
 * modified date : 
 */
angular.module('homer')
.directive('basicplan',basicplan)
.directive('standardplan',standardplan)
.directive('premiumplan',premiumplan)
.directive('prestigeplan',prestigeplan)

/*To display Basic Plan */

function basicplan(){
	return{
			templateUrl:'views/basicplan.html',
			controller:'subscriptionCtrl',
			restrict:'E',
			link:function($scope,$element,$attrs){

			}
	}
}

/*To display Standard Plan */
function standardplan(){
	return{
			templateUrl:'views/stdplan.html',
			controller:'subscriptionCtrl',
			restrict:'E',
			link:function($scope,$element,$attrs){

			}
	}
}

/*To Display Premium Plan*/
function premiumplan(){
	return{
		templateUrl:'views/premiumplan.html',
		controller:'subscriptionCtrl',
		restrict:'E',
		link:function($scope,$element,$attrs){

		}
	}
}

/*To display Prestige plan*/
function prestigeplan(){
	return{
		templateUrl:'views/prestigeplan.html',
		controller:'subscriptionCtrl',
		restrict:'E',
		link:function($scope,$element,$attrs){

		}
	}
}