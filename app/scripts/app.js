/**
 * HOMER - Responsive Admin Theme
 * version 1.8
 *
 */
(function () {
    angular.module('homer', [
        'ui.router',
        'ngCookies', 
        'ngResource',             // Angular flexible routing
        'ngSanitize',               // Angular-sanitize
        'ui.bootstrap',             // AngularJS native directives for Bootstrap
        'angular-flot',             // Flot chart
        'angles',                   // Chart.js
        'angular-peity',            // Peity (small) charts
        'cgNotify',                 // Angular notify
        'angles',                   // Angular ChartJS
        'ngAnimate',                // Angular animations
        'ui.map',                   // Ui Map for Google maps
        'ui.calendar',              // UI Calendar
        'summernote',               // Summernote plugin
        'ngGrid',                   // Angular ng Grid
        'ui.tree',                  // Angular ui Tree
        'bm.bsTour',                // Angular bootstrap tour
        'datatables',               // Angular datatables plugin
        'xeditable',                // Angular-xeditable
        'ui.select',                // AngularJS ui-select
        'ui.sortable',              // AngularJS ui-sortable
        'ui.footable',              // FooTable
        'angular-chartist',         // Chartist
        'gridshore.c3js.chart',     // C3 charts
        'datatables.buttons',       // Datatables Buttons
        'angular-ladda',            // Ladda - loading buttons
        'ui.codemirror',            // Ui Codemirror
        'angular-md5',              //md-5
        'angular-momentjs',         //moment js
        'ngFileUpload',           //file uplaod
        'rzModule',                 //angularjs-slider
        'btford.socket-io',
        'perfect_scrollbar',
        'ngMaterial',
        'infinite-scroll',
        'blockUI',
        'angularUtils.directives.dirPagination',
        'ngclipboard',
        'nvd3',
        'ngAnimate',
        'infinite-scroll'
        // 'ngScrollbar'        
        //'oc.lazyLoad'
        ])
})();

