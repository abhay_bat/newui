function TenantFactory($resource,$rootScope,url,GlobalService){
		var restcalls=GlobalService.getRestVar();
//AngularJS will instantiate a singleton by calling "new" on this function
	return $resource(url+'api/Domains/:id',  { id:  '@_id' },
    {	   	
	   	query:{
        method: restcalls.post,
        params:{
          id:'gettenant'
        }
       },
       remainingSize:{
        method:restcalls.post,
        params:{
          id:'remainingSize'
        }
       },
       createApplication:{
        method:restcalls.post,
        params:{
          id:'createApplication'
        }
       },
       updateapplication:{
        method:restcalls.post,
        params:{
          id:'updateapplication'
        }
       },
       applicationList:{
        isArray:true,
        method:restcalls.post,
        params:{
          id:'applicationList'
        }
       }

	});
}
angular
    .module('homer')
    .factory('TenantFactory', TenantFactory)

   
      