function clientFactory($resource,$rootScope,url,GlobalService){
		var restcalls=GlobalService.getRestVar();
	return $resource(url+'api/clients/:id/:count/:limit',  { id:  '@_id' },
    {	   	
	   	 query:{
	   	 	method: restcalls.get,
	   	 	isArray:true,
	   	 	params:{
	   	 		count:'@count',
	   	 		limit:'@limit'
	   	 	}
	   	 },
	   	 show:{
	   	 	isArray:true,
	   	 	method:restcalls.get
	   	 },
	   	 save:{
	   	 	method:restcalls.post
	   	 },
	   	 update:{
	   	 	method:restcalls.put
	   	 },
	   	 deleteClient:{
	   	 	method:restcalls.post,
	   	 	params:{
	   	 		id:'deleteClient'
	   	 	}
	   	 }
	   	
	});
}
angular
    .module('homer')
    .factory('clientFactory', clientFactory)