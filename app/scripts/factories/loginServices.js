function Auth($location, $rootScope, $http, $cookieStore,$q,url,User) {
   
   var currentUser = {};
   var tenantInfo;
    var currentDomain={};
    if($cookieStore.get('token')) {
      currentUser = User.get();
    }

    return {

      /**
       * Authenticate user and save token
       *
       * @param  {Object}   user     - login info
       * @param  {Function} callback - optional
       * @return {Promise}
       */
      login: function(user,secret, callback) {
       //console.log(user,secret,$rootScope.domainsInfo);
        var cb = callback || angular.noop;
        var deferred = $q.defer();
        var host = $location.host();
        console.log("host in login",host.split('.')[0],host);
          $http.post('https://revampapi.2docstore.com/oauth/token', {
          //$http.post('http://localhost:9000/oauth/token', {

          client_id:host.split('.')[0],
          client_secret:secret,
          username: user.username,
          password: user.password,
          grant_type : 'password'
          //redirecturl:  "http://tricons.2docstore.com:3000/#/dashboard"
        }).
        success(function(data) {
          $cookieStore.put('token', data.access_token);
          console.log("got token",data);
          currentUser = User.get(function(res){
            ////console.log(res);
            return cb(data);
          });
          deferred.resolve(data);
          
        }).
        error(function(err) {
          //////console.log(err);
          this.logout();
          deferred.reject(err);
          return cb(err);
        }.bind(this));

        return deferred.promise;
      },

      getTenant : function(cb)
      {
        var host = $location.host();
       console.log("host in login",host,host.split('.')[0]);
        $http.post(url+'api/Domains/gettenant', {

          clientId:host.split('.')[0]

        }).success(function(tenant){
          tenantInfo=tenant;
          cb(tenantInfo);
        })
      },

      getTenantSync: function(cb)
      {
        cb(tenantInfo);
      },

      /**
       * Delete access token and user info
       *
       * @param  {Function}
       */
      logout: function() {
        $cookieStore.remove('token');
        //$rootScope.defaultdoc=undefined;
       // console.log($cookieStore.get('token'));
        currentUser = {};
      },

      /**
       * Create a new user
       *
       * @param  {Object}   user     - user info
       * @param  {Function} callback - optional
       * @return {Promise}
       */
      createUser: function(user, callback) {
        var cb = callback || angular.noop;
        return User.save(user,
          function(data) {
            $cookieStore.put('token', data.token);
            currentUser = User.get();
            return cb(user);
          },
          function(err) {
            this.logout();
            return cb(err);
          }.bind(this)).$promise;
      },

     
      /**
       * Change password
       *
       * @param  {String}   oldPassword
       * @param  {String}   newPassword
       * @param  {Function} callback    - optional
       * @return {Promise}
       */
      changePassword: function(oldPassword, newPassword, callback) {
        var cb = callback || angular.noop;

        return User.changePassword({ id: currentUser._id }, {
          oldPassword: oldPassword,
          newPassword: newPassword
        }, function(user) {
          return cb(user);
        }, function(err) {
          return cb(err);
        }).$promise;
      },

      /**
       * Gets all available info on authenticated user
       *
       * @return {Object} user
       */
      getCurrentUser: function() {
      //  currentUser.$promise.then(function() {
          return currentUser;
//})        
      },

      /**
       * Check if a user is logged in
       *
       * @return {Boolean}
       */
      isLoggedIn: function() {

        return currentUser.hasOwnProperty('role');
      },

      /**
       * Waits for currentUser to resolve before checking if user is logged in
       */
      isLoggedInAsync: function(cb) {
       //  ////console.log("sdf");
          if(currentUser.hasOwnProperty('$promise')) {
          currentUser.$promise.then(function() {
           // ////console.log("currentUser");
            cb(true);
          }).catch(function() {
            cb(false);
          });
        } else if(currentUser.hasOwnProperty('role')) {
          cb(true);
        } else {
          cb(false);
        }
      },

      /**
       * Check if a user is an admin
       * @TODO Add comments on when it will throw error. 
       * @return {Boolean}
       */
      isAdmin: function(callback) {
        currentUser.$promise.then(function(data) {
          if(data && data.role){
            ////console.log("data",data.role);
            callback(data.role === 'admin');
          }    
        });
      },
      /* 
       checking if the user is ClientAdmin *** dev:(mamatha)
       * */
		isClientAdmin: function(callback) {
        currentUser.$promise.then(function(data) {
          if(data && data.role){
            ////console.log("data",data.role);
            callback(data.role === 'Client Admin');
          }    
        });
      },
      /**
       * Get auth token
       */
      getToken: function() {
        return $cookieStore.get('token');
      },
      checkDomain:function(callback){
        console.log("check domain function");
        var host = $location.host();
        console.log("host",host);
        currentDomain = User.checkDomain({"domain":host.split('.')[0]},function(res){
          console.log(res);
          $rootScope.domainsInfo=res; 
          console.log("domain info in service",$rootScope.domainsInfo);        
          // return res;
          });

         if(currentDomain.hasOwnProperty('$promise')) {
          currentDomain.$promise.then(function() {
            console.log("currentDomain",currentDomain);
            callback(currentDomain.status);
          });
        }else {
          callback(false);
        }
       console.log("currentDomain",currentDomain);
        $rootScope.domainsNames=currentDomain;
          console.log($rootScope.domainsNames);
      }
    };
};

/**
 * Pass function into module
 */
angular
    .module('homer')
    .factory('Auth', Auth)