function DocsFcrt(DefaultDocs,DocstoreService,GlobalService){
	var restcalls=GlobalService.getRestVar();
	var messagesList = {
		instructionMessage : "Documents related to clients/projects are organized below. For easy identification, associated logo has been displayed. To change or add logo, use option to upload logo or drag and drop logo at the cards below.",
		noClientMessage : "There are no clients/projects folder. <a href='client_management'>Click</a> here to add."
	};


	return {
		//Return Client instruction message
		instructionContent : function() {
			return messagesList.instructionMessage;
		},
		//Return No client or project message
		NoClientMessage : function() {
			return messagesList.noClientMessage;
		},
		checkLength : function(clients) {
			if (clients.length == 0)
				return true;
			else
				return false;
		},
		getAllsize : function(client) {
			//console.log(client,"to get size");
			DefaultDocs.getUsedSpace({
				"_id" : client.documentobj._id
			}, function(size) {
				//console.log("size",size,size.filesize);
				var used = size.filesize;
				var alloted =  client.allotedspace;
				var percentage = DocstoreService.findPercentage(used,alloted);
				// console.log(alloted  - used,used);
				//console.log(percentage,"percentage");
				if(percentage < 1){
					client.percentage = 0;	
				}
				else{
					client.percentage = percentage;
				}				
				client.usedSpace = used;
				client.alloted = alloted ;
				client.remaining = alloted  - used;

			});
			return client;	
		},		
		setProgressElements:function(){
			var progressElement={};
			progressElement.min=0;
			progressElement.low=50;
			progressElement.high=80;
			progressElement.max=100;
			progressElement.optimum=40;
			return progressElement;
		},
		sendWwarning:function(client){
			var warningLimit=85;
			if(client.percentage>=warningLimit)
			return true;
			else
			return false;
		},
		//check if you want to display on Iframe or colorbox
		checkFileType:function(fileName) {
			if ((/\.(gif|jpg|jpeg|tiff|png|ico)$/i).test(fileName)) {
			////console.log("this is a image");
			return false;
			} else {
				////console.log("this is not an image");
				return true;
			}
		}

	};
}

angular
    .module('homer')
    .factory('DocsFcrt', DocsFcrt)