/**
 * 13th Feb 2017
 * registrationFctry
 * Chaithra
 * modified date : 
 */
function registrationFctry($resource,url){
	return $resource(url+'api/tenantReg/:id',  { id:  '@_id' },
	{
		query:
		{
			method:'POST'
		}
	})
}
angular
    .module('homer')
    .factory('registrationFctry', registrationFctry)