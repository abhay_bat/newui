function userManagement($resource,$rootScope,url,GlobalService){
	var restcalls=GlobalService.getRestVar();
	return $resource(url+'api/users/:id',  { id:  '@_id' },
		{
       update: {

          method: restcalls.post,
          params:{
            id:'modify'
          }
        },

       query:{
        method: restcalls.get,
        isArray: true
       },
       save:{
        method: restcalls.post,
        params:{
          id:'createIamuser'
        }
       },
       verify:{
        method:restcalls.post,
        params: {
          id:'validatetoken'
        }
       },
      reverify:{
        method:restcalls.post,
        params: {
          id:'reverification'
        }
       },
       forgotPassword:{
        method:restcalls.post,
        params:{
          id:'forgotPassword'
        }
       },
       setPassword:{
        method:restcalls.post,
        params:{
          id:'setPassword'
        }
       }
  });
}

angular
    .module('homer')
    .factory('userManagement', userManagement)