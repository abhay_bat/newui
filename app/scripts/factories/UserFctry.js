function User($resource,$rootScope,url,GlobalService) {
  var restcalls=GlobalService.getRestVar();
    return $resource(url+'api/users/:id/:count/:limit', {id: '@_id'},
    {
      changePassword: {
        //isArray:true,
        method:restcalls.post,
        params: {
          id:'saveUser'
        }
      },
      get: {
        method: restcalls.get,
        params: {
          id:'me'
        }
      },
      show: {
        method: restcalls.post,
        params:{
          id:'getuser'
        }
  },
      checkDomain:{
        method:restcalls.post,
        params:{
          id:'domain'
        }
      },
      logout:{
        method:restcalls.post,
        params:{
          id:'logout'
        }
      },
      loginusers:{
        method:restcalls.post,
        isArray:true,
        params:{
          id:'loginusers'
        }
      },          
       update: {
          method: restcalls.post,
          params:{
            id:'modify'
          }
        },
       query:{
        method: restcalls.get,
        isArray: true,
        params:{
          count:'@count',
          limit:'@limit'
        }
       },
       save:{
        method: restcalls.post,
        params:{
          id:'createIamuser'
        }
       },
       verify:{
        method:restcalls.post,
        params: {
          id:'validatetoken'
        }
       },
      reverify:{
        method:restcalls.post,
        params: {
          id:'reverification'
        }
       },
       forgotPassword:{
        method:restcalls.post,
        params:{
          id:'forgotPassword'
        }
       },
       setPassword:{
        method:restcalls.post,
        params:{
          id:'setPassword'
        }
       },
       getLoggedInUsers:{
        method:restcalls.get,
        isArray: true,
        params:{
          id:'getLoggedInUsers'
        }
       }       
    });
}

angular
    .module('homer')
    .factory('User', User)