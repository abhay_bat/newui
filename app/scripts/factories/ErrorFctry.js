function ErrorFcrt($resource,$rootScope,url,GlobalService) {
	var restcalls=GlobalService.getRestVar();
	  return $resource(url+'api/errors/:id', {id: '@_id'},
    {
     query:{
        method: restcalls.post
       // isArray: true
       }
    });
}

angular
    .module('homer')
    .factory('ErrorFcrt', ErrorFcrt);

    