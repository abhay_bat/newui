
function globalSearchFcrt ($resource, $rootScope, url) {
	return $resource(url + 'api/globalsearchs/:id', {
		id : '@_id'
	}, {
		search : {
			method : 'POST',
			isArray : true
		}
	});
}
angular
    .module('homer')
    .factory('globalSearchFcrt', globalSearchFcrt)