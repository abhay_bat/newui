/**
 * 13th Feb 2017
 * PlansFactory
 * Chaithra
 * modified date : 
 */
 function plansFctry($resource,url,GlobalService){
 	var restcalls=GlobalService.getRestVar();
 	return  $resource(url+'api/plans/:id',  { id:  '@_id' },
 	{
 		query:{
 			method:restcalls.get,
 			isArray:true,
 			params:{
	          id:'getList'
	        }
 		}
 	})
 }

 angular
 .module('homer')
 .factory('plansFctry',plansFctry)