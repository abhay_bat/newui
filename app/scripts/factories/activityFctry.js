function activityFcrt($resource, $rootScope,url,GlobalService) {
	var restcalls=GlobalService.getRestVar();
	return $resource(url + 'api/activitylogs/:id', {id : '@_id'}, 
	{
		save : {
			method : restcalls.post
		},
		query : {
			isArray:true,
			method : restcalls.put,
			ignoreLoadingBar: true
		},
		FolderActivity :{
			isArray:true,
			method : restcalls.post,
			params:{
	   	 		id:'toGetFolderActivityLog'
	   	 	}
		}
	});
}

angular
    .module('homer')
    .factory('activityFcrt', activityFcrt)