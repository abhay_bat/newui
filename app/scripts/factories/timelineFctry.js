function timelineFctry($resource,$rootScope,url,GlobalService){
		var restcalls=GlobalService.getRestVar();
    return $resource(url + 'api/reminders/:id', {id : '@_id'}, 
		{
		save : {
			method : restcalls.post,
			params:{
	   			id:'createreminder'
	   		}
		},
		query : {
			method : restcalls.post,
			params:{
	   			id:'getreminder'
	   		}
		},
		update : {
			method : restcalls.post,
			params:{
	   			id:'modify'
	   		}
		}
		
		
	});
}
angular
    .module('homer')
    .factory('timelineFctry', timelineFctry)