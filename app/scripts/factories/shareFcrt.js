function ShareFctry($resource,$rootScope,url,Global2docstoreFcrt ){
 	var restcalls=Global2docstoreFcrt.getRestVar();
 	return $resource(url + 'api/sharedobjects/:id', {
		id : '@_id'
	}, {
		save : {
			method : restcalls.post
		},
		get:{
			isArray:true,
			method:restcalls.post,
			params:{
				id:'getshareobj'
			}
		},
		delete_policy:{
	   		method:restcalls.post,
	   		params:{
	   			id:'delete'
	   		}
	   	}
	});
 }
 angular
    .module('homer')
    .factory('ShareFctry', ShareFctry)