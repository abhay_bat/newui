/**
 * 13th Feb 2017
 * SubscriptionFactory
 * Chaithra
 * modified date : 
 */
function SubscriptionFactory($resource,url,GlobalService){
	var restcalls=GlobalService.getRestVar();
	return $resource(url+'api/tenants/:id',  { id:  '@_id' },
	{
		query:
		{
			method:restcalls.post
		}
	})
}
angular
    .module('homer')
    .factory('SubscriptionFactory', SubscriptionFactory)