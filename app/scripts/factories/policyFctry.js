function policyFactory($resource,$rootScope,url,GlobalService) {
var restcalls=GlobalService.getRestVar();
     //AngularJS will instantiate a singleton by calling "new" on this function
      return $resource(url+'api/policies/:id',  { id:  '@_id' },
    {
      
       query:{
        method: restcalls.get,
        isArray: true
       }
      
  })
}
angular
    .module('homer')
    .factory('policyFactory', policyFactory);