function reportFactory($resource,url,GlobalService){
	var restcalls=GlobalService.getRestVar();
	return $resource(url+'api/reports/:id/:count/:limit',  { id:  '@_id' },
    {	
    	query:{
    		method:restcalls.post,
    		isArray:true,
    		params:{
    			id:'tenantLoginLogoutReport'
    		}
    	},
    	tenantConsilidatedReport:{
    		method:restcalls.post,
    		isArray:true,
    		params:{
    			id:'tenantConsilidatedReport'
    		}
    	},
    	userReport:{
    		method:restcalls.post,
    		isArray:true,
    		params:{
    			id:'userReport'
    		}
    	},
        reportCount:{
            method:restcalls.post,
            params:{
                id:'count'
            }
        }

    });
}
angular
	.module ('homer')
	.factory('reportFactory',reportFactory)