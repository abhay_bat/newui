function ShareFctry($resource,$rootScope,url,GlobalService ){
 	var restcalls=GlobalService.getRestVar();
 	return $resource(url + 'api/sharedobjects/:id', {
		id : '@_id'
	}, {
		save : {
			method : restcalls.post
		},
		get:{
			isArray:true,
			method:restcalls.post,
			params:{
				id:'getshareobj'
			}
		},
		delete_policy:{
	   		method:restcalls.post,
	   		params:{
	   			id:'delete'
	   		}
	   	}
	   });
	}
	angular
    .module('homer')
    .factory('ShareFctry', ShareFctry)