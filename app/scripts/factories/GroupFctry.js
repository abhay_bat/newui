function Groups($resource,$rootScope,url,GlobalService) {
	var restcalls=GlobalService.getRestVar();
	  return $resource(url+'api/groups/:id/:count/:limit', {id: '@_id'},
    {
      create:{
        method: restcalls.post,
        params:{
          id:'creategroup'
        }
      },
      getAllGroups:{
        method: restcalls.get,
        isArray:true,
        params:
        {
          id:'listGroup',
          count:'@count',
          limit:'@limit'
        }  
      },
      addUser:{
        method: restcalls.post,
        params:{
          id:'addUserToGroup'
        }
      },
      Update:{
        method: restcalls.post,
        params:{
          id:'update'
        }
      } ,
      removeGroup:{
        method: restcalls.post,
        isArray:true,
        params:{
          id:'deleteGroup'
        }
      },
      removeUsers:{
        method: restcalls.post,
        //isArray:true,
        params:{
          id:'deleteUsersFromGroup'
        }
      },
      removeUser:{
        method: restcalls.post,
        params:{
          id:'deleteUserFromGroup'
        }
      },
       AddGroup:{
        method: restcalls.post,
        params:{
          id:'addDocumentToGroup'
        }
      },  
       RemoveGroupDoc:{
        method: restcalls.post,
        params:{
          id:'deleteDocument'
        }
      },
      getGroups:{
        method: restcalls.post,
        isArray:true,
        params:{
          id:'getGroups'
        }
      },
       removeDocGroups:{
        method: restcalls.post,
        isArray:true,
        params:{
          id:'deleteDocument'
        }
      },
       getdocument:{
        method: restcalls.post,
        isArray:true,
        params:{
          id:'getdocument'
        }
      }

    });
}

angular
    .module('homer')
    .factory('Groups', Groups)