angular.module('homer').controller('reportCntrl',
function($scope,$http,$filter,$state,url,$rootScope,Auth,User,
    UserService,GlobalService,$location,DefaultDocs,$location,ActivitylogService,
    activityFcrt,$moment,$timeout,$window,reportFactory,docsCtrlVariables,userManagement,UserService) {
    
    $scope.homerTemplate = 'views/notification/notify.html';
        
        User.getLoggedInUsers(function(res){
            console.log(res,"current");
        });
        
        reportFactory.reportCount(function(res){
            console.log(res);
            $scope.TotalActivity=res.TotalActivity;
            $scope.TotalLoginCount=res.TotalLoginCount;
            $scope.TotalUserCount=res.TotalUserCount;
        })

        var host = $location.host();

        var ioSocket = io(url, {
             query: "host="+host.split('.')[0],
            path: '/socket.io-client'
       });

        ioSocket.on('activitylog:save', function (item) {
            console.log(item);
            $scope.activitylog.unshift(item);    
        });

        $scope.imageExists = function(image, users) {
            // //console.log("before",image);
           var img = new Image();
               img.onload = function() {
               // //console.log("onload",image,users.avatarflag);
                 users.avatarflag = true;
                 $scope.$apply();
           }; 
               img.onerror = function() {
               // //console.log("onerror",image,users.avatarflag);
                 users.avatarflag=false;
                 $scope.$apply();
               };
           img.src = image;
        };


/* data query*/
            $scope.howMuch=0;
            // console.log($scope.howMuch);
            $scope.activityLogs=activityFcrt.query({'count':$scope.howMuch,'limit':20},function(data)
                     {
                          // console.log(data);
                        // console.log($scope.howMuch);
                      });
            var Counts=$scope.howMuch+20;
            // console.log(Counts);
            $scope.loadMore=function(){
                // console.log("here is load");
                var activityCount=Counts;
                // console.log(activityCount);
                var items=activityFcrt.query({'count':activityCount,'limit':20},function(data){
                // Counts=Counts+20;
                console.log(activityCount);
                for(var i=0;i<items.length;i++){
                    $scope.activityLogs.push(items[i]);
                    };
                    // console.log(activityCount,Counts);
                });
                Counts=Counts+20;
                // console.log(activityCount,Counts);
            };

            // To capitalize the first letter
            var firstToUpperCase = function(strng){
                // //console.log("this is strng",strng);
                return strng.substr(0, 1).toUpperCase() + strng.substr(1);
            }

            $scope.string_format=function(val){
            if(val == undefined){
                   return null;
               }
               var loc1 = val.substring(val.indexOf("/") + 1);
               var loc2 = loc1.substring(loc1.indexOf("/") + 1);
               return loc2;
              };

/**    *function name : get time, to give time in terms of "mins ago,days ago etc"
    @param: activity.when
    return : time in terms of moments ago
**/
    
            $scope.getTime=function(date){
                var DateFormat = $moment(date).fromNow();
                // //console.log(DateFormat)
                return DateFormat;
            };
            $scope.Capital_frst = function(firstLtr) {
                if (firstLtr == undefined)
                return;
                var res = firstLtr.toUpperCase();
                var res1 = res.slice(0, 1);
                return res1;
            };

/*for charts*/

           $scope.getCurrentUser = Auth.getCurrentUser;
            // console.log($scope.getCurrentUser,"$scope.getCurrentUser");
                /*To get logged in user info*/
    $scope.getCurrentUser().$promise.then(function() {
        $rootScope.loggedIn=$scope.getCurrentUser();
        docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);
        $scope.loggedIn = $scope.getCurrentUser()._id;
        $rootScope.loggedUser = $scope.loggedIn;
        console.log($rootScope.loggedIn,"$rootScope.loggedUser")
    });



$scope.showChart=false;
 $scope.valueArr=[];
  $scope.reportTitle="Reports";

$scope.loginChart=function(){
     $scope.reportTitle="Login Activities";
 $scope.isuseractivity=false;
//$scope.valueArr=[];
  /*to query for data*/
 reportFactory.query(function(data){
            console.log("reportdata",data);
            $scope.Logindata=data;
             angular.forEach($scope.Logindata,function(value,key){
                    console.log(value,key,"for loop");                   
                    var obj={};
                    obj.label=value._id.user;
                    obj.value=value.Login;
                    $scope.valueArr.push(obj);
                    $scope.showChart=true;

             })
             $scope.logindata=[{
                key:"Cumulative Return",
                values: $scope.valueArr
             }];
             $scope.activity_Data=$scope.logindata[0].values;
        console.log($scope.logindata[0].values,"$scope.logindata",$scope.activity_Data);
             
     })
}

   $scope.value2arr=[];
$scope.activityChart=function(){
     $scope.reportTitle="Activities ";   
     $scope.isuseractivity=false;
   // $scope.valueArr=[];
    reportFactory.tenantConsilidatedReport(function(data1){
        console.log("activity report",data1);
        $scope.activityData=data1;
         var keyNames = Object.keys( $scope.activityData[0]);  
                    console.log(keyNames,"keyNames")    
        angular.forEach(keyNames,function(value,key){
                    console.log(value,key,"for loop");
                     var obj={}; 
                    if(value!="_id"  && value != "Total"){
                        console.log(  value != "Total"," total")
                        obj.label=value;
                        obj.value=$scope.activityData[0][value];
                        console.log(obj,"obj label if condition");
                        $scope.value2arr.push(obj);
                         $scope.showChart=true;
                    }
             })

        $scope.logindata=[{
            key:"Cumulative Return",
            values: $scope.value2arr
        }];
        $scope.activity_Data=$scope.logindata[0].values;
        console.log($scope.logindata[0].values,"$scope.logindata",$scope.activity_Data);
    })
}


$scope.value3arr=[];
$scope.useractivityChart=function(){
     $scope.reportTitle="User Activities ";
    $scope.isuseractivity=true;
   // $scope.valueArr=[];
    console.log("useractivityChart");
    reportFactory.userReport(function(data3){
        console.log(data3,"data3");
        $scope.userActivity=data3;
        UserService.getUsers(function(res){
             console.log(res,"users",$rootScope.loggedIn);
             $scope.userList=res;
             $scope.selectedName=$rootScope.loggedIn._id;
             angular.forEach($scope.userActivity,function(value,key){
                if(value._id.user_id == $rootScope.loggedIn._id){
                     var keyNames = Object.keys(value);
                     console.log(value,"valueif",$rootScope.loggedIn,"true");                     
                     console.log(keyNames,"keyNames");
                     angular.forEach(keyNames,function(value1,key1){
                        console.log("keynames value",value1,key1);
                         if(value1!="_id" && value1 != "Total"){
                            console.log("keynames value1",value1 != "Total");
                            var obj={}; 
                            obj.label = value1;
                            obj.value = $scope.userActivity[key][value1];
                            console.log(obj,"obj.value",obj.label);
                            $scope.value3arr.push(obj);
                                $scope.showChart=true;
                            console.log( $scope.value3arr)
                        }
                     }) 
                }
             })
            
                $scope.logindata=[{
                    key:"Cumulative Return",
                    values: $scope.value3arr
                }];
                 $scope.activity_Data=$scope.logindata[0].values;
        console.log($scope.logindata[0].values,"$scope.logindata",$scope.activity_Data);
            })
    })
}
/*
*/
$scope.setNull=function(){
   // $scope.logindata=[];
    $scope.value4arr=[];
}
$scope.rc={};
$scope.value4arr=[];
$scope.selctedUser=function(user){
    //$scope.logindata=[];
    console.log($scope.value4arr);
    //$scope.value4arr=[];
                console.log("user",user);                
               // $scope.rc.api.clearElement();                
                 angular.forEach($scope.userActivity,function(value,key){
                   // console.log(value._id.user_id);
                if(value._id.user_id == user){
                     var keyNames = Object.keys(value);
                    // console.log(value,"valueif","true");                     
                    // console.log(keyNames,"keyNames");
                     angular.forEach(keyNames,function(value1,key1){

                       // console.log("keynames value",value1,key1);
                         if(value1!="_id"){
                           // console.log("keynames value1");
                            var obj={}; 
                            obj.label = value1;
                            obj.value = $scope.userActivity[key][value1];
                           // console.log(obj,"obj.value",obj.label);
                            $scope.value4arr.push(obj);
                           // console.log( $scope.value4arr);
                        }
                     }) 
                }
             })
            $scope.logindata=[{
                key:"Cumulative Return",
                values: $scope.value4arr
            }];
            $scope.activity_Data=$scope.logindata[0].values;
            console.log($scope.logindata[0].values,"$scope.logindata",$scope.activity_Data);

//                  setTimeout(function(){
//     console.log('scope api:', $scope.api);
//     $scope.api.refresh();
// },30000)
                 //console.log($scope.rc.api,"api");
               //  $scope.rc.api.update();
                // $scope.apply();
}

// $scope.data=[{
//                 key:"Cumulative Return",
//                 values: $scope.valueArr
//              }];

$scope.options = {
    chart: {
        type: 'discreteBarChart',
        height: 550,
        width: 950,

        margin : {
            top: 20,
            right: 20,
            bottom: 150,
            left: 55
        },
        x: function(d){ return d.label; },
        y: function(d){ return d.value; },
        showValues: true,
        valueFormat: function(d){
            return d3.format(',')(d);
        },
        transitionDuration: 500,
        xAxis: {
            axisLabel:'Activities' ,
            rotateLabels:-45
        },
        yAxis: {
            axisLabel: 'Count',
            axisLabelDistance: 30
        }
    }

};


/*to close the chart*/
    $scope.check=function(){
        console.log("to close chart");
        $scope.logindata=[];
        $scope.showChart=false;


    }



    // $scope.reportData=function(){

    // }
})