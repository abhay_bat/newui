angular.module('homer')

  .controller('profilecntrl', function ($scope,GlobalService,url,Upload,$state,$http,$rootScope,md5,Auth,User,notify,DefaultDocs,userManagement,url,ActivitylogService,UserService,docsCtrlVariables,NoteService,ErrorService) {
  	 $scope.homerTemplate = 'views/notification/notify.html';

var activity_type = GlobalService.getActivitytype();
$scope.getCurrentUser = Auth.getCurrentUser;
	$scope.getCurrentUser().$promise.then(function() {
		$scope.loggedIn = $scope.getCurrentUser();
		// console.log($scope.loggedIn ,$scope.loggedIn.contactnumber);
		$rootScope.loggedIn=$scope.loggedIn;
		 docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);
	});

  	Auth.getTenant(function(tenant){
    $scope.tenant=tenant; 
    $rootScope.tenantInfo=$scope.tenant;     
    });

    //userlist 
    UserService.getUsers(function(data){
		// console.log(data,data);
		userslist=data;
		// console.log(userslist.contactnumber)
	});

$scope.phoneNumbr = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{3,7}$/;
$scope.errorMessagephone="";
var notePhone="";
var Mobile= "";
var firstName=" ";

/*
validate phone number
*/
$scope.checkforPhoneError=function(phone){
	// //console.log($scope.phoneNumbr.test(phone))
	if(!$scope.phoneNumbr.test(phone)){		
		$scope.errorMessagephone=$scope.getMobile();
	}
	else{
		$scope.errorMessagephone=""
	}
}

$scope.phoneNumbr = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{3,7}$/;

NoteService.getUsermanagementNotes({module:'Note',code:'N_006'},function(data){
		// //console.log(data);
		 notePhone=data;
	});

ErrorService.getUsermanagementErrors({module:'User',code:'U_004'},function(data){
		 Mobile=data;
	});
ErrorService.getUsermanagementErrors({module:'User',code:'U_006'},function(data){
		 firstName=data;
	});
$scope.getnotePhone=function(){
		return notePhone;
	};
$scope.getMobile=function(){
		return Mobile;
	};
$scope.getfirstName=function(){
	return firstName;
}
/*$scope.getCurrentUser = Auth.getCurrentUser;*/
	$scope.user_profile=$scope.getCurrentUser();
	$scope.user_profile.$promise.then(function() {
		// //console.log("user",$scope.user_profile);
		$scope.profile_info = angular.copy($scope.user_profile);
		$scope.notify = angular.copy($scope.user_profile);

	})
/*
if image exists
*/
$scope.checkIfImageExists=function(user){
	// //console.log(user,$scope.profile_info);
	// user.name=$scope.profile_info.name
	docsCtrlVariables.checkForImage($scope.user_profile,$scope.user_profile.avatar);
	// //console.log($scope.user_profile);		
	}

	$scope.click = 0;
	$scope.formData = {};
	$scope.propic={};
	$scope.billing = {};
	$scope.errorText = {};
	$scope.list = [];
 	$scope.avatarTemp = "assets/images/users.png";
	$scope.whichFlag = "Profile Info";
	$scope.errorText = GlobalService.get_error();// error msg form Global2docstoreFcrt
	$scope.note = GlobalService.getNote();// note from Global2docstoreFcrt
	$scope.checkStatus=false;



 // to upload profile pic
  
	$scope.uploadAvatar = function(file,id) {
		// //console.log("upload avatar",file,id,$scope.profile_info.avatar);
		if(file){		
		 	Upload.upload({
	            url:  url + 'api/users',
	            data: {
	            	filename:file.name,
	            	id:id,
	            	file:file
	            }
	     	}).then(function (resp) {
	        	$scope.callReload();
	            window.location.reload();
	            // //console.log(id,"success");
	   			var profileLog={};
	            profileLog.activitytype=activity_type.PROFILE;
	            // profileLog.what='Profile_Pic';
	      		ActivitylogService.LogActivity(profileLog);//from activity service
	      		// //console.log(ActivitylogService);
	         	notify({ message: 'Profile Pic Uploaded successfully', classes: 'alert-success', templateUrl: $scope.homerTemplate});
	        }, function (resp) {
	            // //console.log('Error status: ' + resp.status);
	        }, function (evt) {
	        	$scope.progressPercentage = Math.min(97, parseInt(100.0 * evt.loaded / evt.total));
	            // console.log('progress: ' + $scope.progressPercentage  + '% ' + evt.config.data.file.name);
	        });
		}
		else{
			$scope.callReload();
		}	
	};

	// to set password strength
  $scope.strength=" ";
     $scope.passStrength = function(){ 
    return $scope.strength;   
  };

 
$scope.testStrength=function(pass){
  // //console.log(pass);
  var type;
  $scope.strength = 0;
  if(pass){
    if(pass.length<6){
      $scope.strength=0;
      // //console.log($scope.strength);
    } if( /[0-9]/.test( pass ) ){
      type = 'success';
    $scope.strength=$scope.strength+1;
    // //console.log("numbers",$scope.strength);    
  } if( /[a-z]/.test( pass ) )
    {
    $scope.strength=$scope.strength+1;
    // //console.log("alpha",$scope.strength);
  
  } if( /[A-Z]/.test( pass ) ){
    $scope.strength=$scope.strength+1;
    // //console.log("caps",$scope.strength);
  
  } if( /[^A-Z-0-9]/i.test( pass ) ){
    $scope.strength=$scope.strength+1;
    // //console.log("chars",$scope.strength);   
  }
  }
  else
   $scope.strength = -1;
  // return s;
};



//Form submission

  	$scope.submitMydata = function() {
  		
		if ($scope.whichFlag == "propicture") {				
				$scope.list.push(this.propic);
				userManagement.update($scope.propic,function(data) {
				});
		}

		if ($scope.whichFlag == "Profile_Info") {
			console.log(this.profile_info,this.profile_info.name,"profileinfo");

			if(this.profile_info.name){
				//this.profile_info.cell_number=$scope.loggedIn.contactnumber;

					if(this.profile_info.cell_number){
						if($scope.loggedIn.contactnumber){
							console.log("present");
							this.profile_info.cell_number=loggedIn.contactnumber;
							console.log(this.profile_info.cell_number,"cell number");			
							$scope.list.push(this.profile_info);
			            	$scope.profile_info.files=$scope.profile_info.avatar;		
							userManagement.update($scope.profile_info,function(data) {
								console.log("updated",$scope.profile_info);
								$scope.callSuccess();
								// PROFILEUPDATE
								var proupdateLog={};
				           		proupdateLog.activitytype=activity_type.PROFILEUPDATE;
				      			ActivitylogService.LogActivity(proupdateLog);//from activity service
							});
						}else{
							console.log("absent");
							console.log(this.profile_info.cell_number,"cell number");			
							$scope.list.push(this.profile_info);
			            	$scope.profile_info.files=$scope.profile_info.avatar;		
							userManagement.update($scope.profile_info,function(data) {
								console.log("updated",$scope.profile_info);
								$scope.callSuccess();
								// PROFILEUPDATE
								var proupdateLog={};
				           		proupdateLog.activitytype=activity_type.PROFILEUPDATE;
				      			ActivitylogService.LogActivity(proupdateLog);//from activity service
							});
						}
					}
			        else{
			        	notify({ message:'Please enter a valid contact number' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
			        }
			}
			else{
				notify({ message:'Please enter a valid name' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
			}

		} 
		else if ($scope.whichFlag == "Security") {
			$scope.list.push(this.formData);				
			if($scope.formData.old_password){
				if($scope.formData.password && $scope.formData.password_c){					
					if($scope.formData.password == $scope.formData.password_c ){
							if($scope.strength<=1){
								// //console.log("here pass",$scope.strength);
								notify({ message:'Password must be strong' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
								$scope.formData="";
								$scope.strength=" ";
							}
							else{
								// //console.log("updated");
								var userObj={ }; 
								userObj._id=$scope.user_profile._id;
								userObj.oldpassword=md5.createHash($scope.formData.old_password || '');
								userObj.newpassword=md5.createHash($scope.formData.password || '')								
								userManagement.setPassword(userObj,function(data) {
									$scope.callSuccess();
									var passLog={};
				            		passLog.activitytype=activity_type.PASSWORD;
				      				ActivitylogService.LogActivity(passLog);//from activity service
				      				// //console.log(ActivitylogService);
										$scope.formData="";
								},function(err){
									$scope.error=err.data;
									
									notify({ message: 'Warning -  Passwords do  not match', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
									
									$scope.formData="";
									$scope.strength=" ";
								});
							}											
							
					}//end of both match
					else{
							
							notify({ message: 'Warning -  Passwords do  not match', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
							$scope.formData="";
							$scope.strength=" ";

					}
				}
				else{	
					notify({ message: 'Warning -  Passwords cant be empty', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
					$scope.formData="";
					$scope.strength=" ";
				}

			}//end of old pass
			else{
			
					
					 notify({ message: 'Warning -  Old password cannot be empty', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
					$scope.formData="";
					$scope.strength=" ";
				}	
		
		} else if ($scope.whichFlag == "Notification") {
			$scope.list.push(this.notify);
			userManagement.update($scope.notify,function(data) {
				$scope.callSuccess();
				var proupdateLog={};
            	proupdateLog.activitytype=activity_type.PROFILEUPDATE;
      			ActivitylogService.LogActivity(proupdateLog);//from activity service
      			// //console.log(ActivitylogService);
				$scope.formData="";
				$scope.strength=" ";
			});		
		} 		
	};

//Cancel button function
	$scope.cancelFunc=function(){
	 window.location.reload();
	}

	//Returning to profile page
	$scope.callReload=function(){
	    $state.go('profile');
	}

// update success
	$scope.callSuccess=function(){	
		notify({ message: 'Profile-updated successfully', classes: 'alert-success', templateUrl: $scope.homerTemplate});
		$scope.strength="";
	};


//to set the flags
$scope.setFlag = function(flagSet) {	
		$scope.whichFlag = "";
		if (flagSet == "pro_info") {			
			$scope.whichFlag = "Profile_Info";			
		}
		if (flagSet == "secure") {			
			$scope.whichFlag = "Security";
		}
		if (flagSet == "notify") {			
			$scope.whichFlag = "Notification";
		}if (flagSet == "propic") {			
			$scope.whichFlag = "propicture";
		} 
		else if (flagSet == "bill") {
			$scope.whichFlag = "Billing";
		}
	};

	//checking if avatar is there, if not send true
	$scope.checkForAvatar = function() {
		if($scope.profile_info){
			if ($scope.profile_info.avatar){
				return false;
			}
			else{
				  $scope.profile_info.avatar= $scope.defaultfile;
			
				 return true;
			}
	 	}
	};

	// Initial step
    $scope.step = 1;
    
	$scope.profile =  {
        show: function(number) {
            $scope.step = number;
        },
        next: function() {
            $scope.step++ ;
        },
        prev: function() {
            $scope.step-- ;
        }
    };
  	
});