/**
 * 10th Feb 2017
 * SubscriptionController
 * Chaithra
 * modified date : 
 */
angular.module('homer')
.controller('subscriptionCtrl',function($scope,$state,$rootScope,PlanService,plansFctry){
	console.log("inside subscriptionCtrl");

	PlanService.getFreeplanObj(function(data){
		console.log("data",data);
		$scope.freeplanObj=data;
	});
	PlanService.getStandardObj(function(data1){
		console.log("data1",data1);
		$scope.stdplanObj=data1;
	});
	$scope.calcSpace=function(space){
		console.log("space",space);
		var calcspace = Math.floor(space/(1024*1024*1024));
		return calcspace;
	}
	$scope.Selected=function(){
		console.log("selected");
	}
	$scope.freePlan=function(){
		console.log("freeplan");
		$rootScope.selectedPlan="Free Trail Registration";
		$state.go('common.userregistration');
	}
	$scope.BasicPlan=function(){
		console.log("basic plan ");
		$rootScope.selectedPlan="Basic Plan Registration";
		$state.go('common.userregistration');
	}
	$scope.StandardPlan=function(){
		console.log("standard plan");
		$rootScope.selectedPlan="Standard Plan Registration";
		$state.go('common.userregistration');
	}
});
