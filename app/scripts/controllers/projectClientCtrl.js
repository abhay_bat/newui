angular.module('homer')
.controller('projectClientCtrl', function ($scope,$state,blockUI,$http,$rootScope,md5,Auth,
  User,notify,DefaultDocs,$moment,ShareFctry,sweetAlert,uploadService,$uibModal,
  GlobalService,ActivitylogService,activityFcrt,docsCtrlVariables,versionService,
  ClientService,DocsFcrt,UserService,$rootScope,clientFactory,sweetAlert) {
$rootScope.clientPage=true;
	
  UserService.getUsers(function(data){
		$scope.userslist=data;
    console.log($scope.userslist);
	});	

	$scope.clientList=[];
  $scope.getCurrentUser = Auth.getCurrentUser;
  $scope.getCurrentUser().$promise.then(function() {
    $rootScope.loggedIn = $scope.getCurrentUser();
    // $scope.loggedUser=$scope.getCurrentUser();
    ////console.log($scope.loggedIn,$scope.loggedIn.role);
    docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);

      });
  /*
    get the list of clients
    */ 
  ClientService.getClientsAsync(function(data){
  	$scope.clientLength=data.length;
   console.log($scope.clientLength);
  });


  /*to get limited client*/
  $scope.clientCount=0;

clientFactory.query({'count':$scope.clientCount,'limit':50},function(data){
      // console.log("getclientlist");
      $scope.clientList=data;
      console.log($scope.clientList);     
    });

$scope.loadclientMore=function(){   
    $scope.clientCount=$scope.clientCount+50;
    // console.log($scope.clientCount);
    clientFactory.query({'count':$scope.clientCount,'limit':50},function(data){
      for(var i=0;i<data.length;i++){
        $scope.clientList.push(data[i]);
      };
    });
  };

  // $scope.imageExists=function(users){
  // 	docsCtrlVariables.checkForImage(users,users.avatar);
  // 	// console.log(users)
  // }


   /*
   size calculations
   */

   $scope.getSize=function(client){
    var sizeSpecs = DocsFcrt.getAllsize(client);
   // console.log(client)
   }

/*
add clients
*/
$scope.addClient=function(){
	$state.go("clientManagement")
	}
  
$scope.checkIfImageExists=function(user,image){
	docsCtrlVariables.checkForImage(user,image);
};

/*
edit
*/
$scope.clickedOnView=function(document){
	$rootScope.projectViewDoc=document;
		docsCtrlVariables.setWhichDocument("ProjectView","clicked");
		$state.go("dashboard", {}, { reload: true });
}
/*
nav
*/
$scope.editClients=function(){
	$state.go('clientManagement')
}	

/*to delete client*/
  $scope.deleteClient=function(data){
    console.log("to delete client",data);
    sweetAlert.swal({
      title : "Warning",
      text : "Users, files and folders created will be moved to admin!!",
      type : "warning",
      showCancelButton : true,
      confirmButtonColor : "#62CB31",
      cancelButtonColor:"#F44336",
      confirmButtonText : "Delete",
      cancelButtonText : "Cancel",
      closeOnConfirm : true,
      closeOnCancel : true
    }, function(isConfirm) {
          if(isConfirm){
                var params={
                    client:data._id
                }
                clientFactory.deleteClient(params,function(res){
                  console.log("res",res);
                  angular.forEach($scope.clientList,function(value,key){
                    if(value==data){
                      $scope.clientList.splice(key,1);
                      sweetAlert.swal("Deleted!", "delete done.", "success");
                      blockUI.stop();
                    }
                  })
                })  
          }else{
            console.log("else");
            sweetAlert.swal("Cancelled", "client is safe :)", "error" );
          }
    })
  }

})


