/**
 * 10th Feb 2017
 * FreePlan controller
 * Chaithra
 * modified date : 
 */
angular.module('homer')
.controller('freeplanCtrl',function($scope,$rootScope,SubscriptionFactory,$state,notify,md5){
	initialize();
	unsetclasses();
	$scope.selPlan="Free Trial Registration";
	$scope.success = true;
	$scope.showcase = "xyz"
	$scope.submitLoading = false;
	console.log("free plan controller",$rootScope.selectedPlan);
	$scope.homerTemplate = 'views/notification/notify.html';	
	$scope.submit=function(data,plan){
		unsetclasses();
		if(data.username == ""){
			$scope.uclass = "uerror";
			$scope.ulabel = true;
		}else if(data.pwd == ""){
			$scope.plabel = true;
			$scope.pclass = "perror";
		}else if(matchpassword(data)){
			$scope.cplabel = true;
			$scope.pclass = "perror";
			$scope.cpclass = "cperror";
		}else if(data.useremail == ""){
			$scope.elabel = true;
			$scope.emclass = "emerror";
		}else if(validatemail(data)){
			$scope.vallabel = true;
			$scope.emclass = "emerror";
		}else if(data.organization == ""){
			$scope.olabel = true;
			$scope.oclass = "oerror";
		}else if(data.url == ""){
			$scope.urllabel = true;
			$scope.urlclass = "uerror";
		}
		else{
			var mdpassword = md5.createHash(data.pwd || ''); 
			var obj={
				first_name:data.username,
				company_name:data.url,
				url:data.url,
				primary_email:data.useremail,
				password:mdpassword,
				Subscription_Type:$scope.selPlan
			}
			console.log(obj);
			$scope.submitLoading = true;
			SubscriptionFactory.query(obj,function(res)
			{
				console.log("hitting api",res);
				$scope.success = false;
				$scope.submitLoading = false;
				//$state.go('common.login');
				//initialize();
			},
			function(err){
				$scope.submitLoading = false;
				$scope.sameurl = true;
				$scope.urlclass = "uerror";
			});
		}
	};

	$scope.populateurl = function(org){
		if(org == ""){
			$scope.showcase = "xyz";
			$scope.data.url = "";
		}else{
			var filtervalue = org.replace(/[^a-zA-Z0-9]/g, '-');
		    $scope.data.url = filtervalue.toLowerCase();
		    $scope.showcase = $scope.data.url;
		}
	};

	$scope.populateshowcase = function(url){
		if(url == ""){
			$scope.showcase = "xyz";
		}else{
			$scope.showcase = url;
		}
		
	}

	function unsetclasses(){
		$scope.uclass = "";
		$scope.pclass = "";
		$scope.cpclass = "";
		$scope.emclass = "";
		$scope.oclass = "";
		$scope.urlclass = "";
		$scope.ulabel = false;
		$scope.plabel = false;
		$scope.cplabel = false;
		$scope.elabel = false;
		$scope.vallabel = false;
		$scope.olabel = false;
		$scope.urllabel = false;
		$scope.sameurl = false;
	}

	function matchpassword(data){
		if(data.pwd != 0){
                if(data.pwd == data.cpwd){
                    return false;
                }else{
                    return true;
                } 
        }else{
        	return true;
        }
	}

	function validatemail(data){
		var regexp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(regexp.test(data.useremail)){         
            return false;
        }else{
            return true;
        }
	}

	function initialize(){
		$scope.data = {username:"",pwd:"",cpwd:"",useremail:"",organization:"",url:""};
	}
});