/** 2Docstore
 * page: admin (user group page)
 * dev: mamatha
 * created date : 16th  August 2016
 * modified date :  August 2016
 * Discription : creating the user group and listing of the groups 
 */
angular.module('homer')
.controller('usergroupctrl',function($scope,$http,$filter,$timeout,notify,$uibModal,$moment,$state,Groups,$rootScope,Auth,User,UserService,GlobalService,$location,DefaultDocs,ActivitylogService,ErrorService,NoteService,$window,docsCtrlVariables,GroupService) {
$scope.homerTemplate = 'views/notification/notify.html';
$scope.userarray=[];
$scope.grparray=[];
$scope.sortname=[];
$scope.BreadCrumbs=[];
var groupnote="";
	NoteService.getUsermanagementNotes({module:'Note',code:'N_019'},function(data){
		 groupnote=data;
	});
	$scope.notegroup=function(){
		return groupnote;
	};
var groupnote1="";
	NoteService.getUsermanagementNotes({module:'Note',code:'N_020'},function(data){
		 groupnote1=data;
	});
	$scope.notegroup1=function(){
		return groupnote1;
	};
var groupnameerr1="";
	ErrorService.getUsermanagementErrors({module:'Group',code:'G_001'},function(data){
		 groupnameerr1=data;
	});
	$scope.requiredErr=function(){
		return groupnameerr1;
	};
if($state.current.name=="InviteGroupUser"){
	if(!$rootScope.currentGroup){
		$state.go("GroupList");
	}
}
//To get current loggedin user
	$scope.getCurrentUser = Auth.getCurrentUser;
	var activity_type = GlobalService.getActivitytype();
	$scope.getCurrentUser().$promise.then(function(){
		$rootScope.loggedIn = $scope.getCurrentUser();
		docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);
	});
//list of Groups
	Groups.get(function(data){
		$scope.groupslist=data;
	});
//To make first letter as Uppercase and slice the 1st letter (groupservice.js)
	$scope.Capital_frst=function(grpname){
		return GroupService.First_UpperCase(grpname);
	};
//To make first letter as Uppercase(groupservice.js)
	$scope.firstToUpperCase = function(strng){
		return GroupService.firstToUpperCase(strng);
	};
//bread crumbs
	$scope.ManageGroup=function(){
		$scope.BreadCrumbs=[];
		var b1= "Manage Groups";
		$scope.BreadCrumbs.push(b1);
	};
//create group page
	$scope.addGroup1=function(){
		var b1= "Manage Groups";
		var b2="Add Group";
		$scope.BreadCrumbs.push(b1,b2);
	};
//add user to group
	$scope.addUserGroup=function(){
		var b1= "Manage Groups";
		var b2="Add Users";
		$scope.BreadCrumbs.push(b1,b2);
	};

//click on the bread crum to go to previous page
	$scope.clickOnPage=function(page){
		if(page =="Manage Groups"){
			$rootScope.currentGroup=undefined;
			$state.go("GroupList");
		}
	};
// listing logo
	$scope.imageExists = function(image,users){
	    var img = new Image();
	    img.onload = function() {
	      users.avatarflag = true;
	    }; 
	    img.onerror = function(){
	      users.avatarflag=false;
	    };
	    img.src = image;
	 };
// creating the group
	$scope.creatGroup=function(group,cb){
 	if( $scope.getCurrentUser().role=="Client Admin"){
    	group.organization=$scope.getCurrentUser().organization;
    }
    if($scope.getCurrentUser().role=="admin"){
    	group.organization=$scope.getCurrentUser().clientId;
    }
		if(!GroupService.DuplicateGroup(group.Name)){
	group.user=$scope.arrayOfuser;
		Groups.create(group,function(ata){
			var addgroup = {};
			addgroup.activitytype = activity_type.GROUPADD;
			addgroup.what = ata._id;
			addgroup.status = true;
			ActivitylogService.LogActivity(addgroup);
			cb();
			},function(err){ 
				cb();
			})
		}
		else{
			notify({ message: 'Duplicate Group name', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
		}
	};
//To add group
	 $scope.groupAdd=function(grp){
		$scope.creatGroup(grp,function(){
			notify({ message: 'Group is created', classes: 'alert-success', templateUrl: $scope.homerTemplate});
			$state.go('GroupList',{},{reload: true});
		});
	};
// To add another group
	 $scope.oneMoregroup=function(grp){
	 	$scope.creatGroup(grp,function(){
	 		notify({ message: 'Group is created', classes: 'alert-success', templateUrl: $scope.homerTemplate}); 
	 		$state.go($state.current, {}, {reload: true});
	 	});
	 };
//adding users to the group(view/manage users)
	$scope.viewGroup=function(group){
		$rootScope.currentGroup=group;
		$rootScope.users_list=$rootScope.currentGroup.user;
		$state.go('InviteGroupUser');
	};
//this is the 2nd screan in creat group page
	$scope.AddGroup=function(gp){
		$state.go('UserGroup');
	};
//open a modal to add users to the group
	$scope.openModal=function(){
		var modalInstance = $uibModal.open({
	        templateUrl: 'views/modal/modal_usergroup.html',
	        size:'lg',
	        controller:'userSelectCtrl'
	        });
	};
//disable edit options for loggin user
  $scope.checkdiableUser=function(data){
       if(data._id==$rootScope.loggedIn){
          return true;
         }else{
          return false;
         }
    };
//update group
    $scope.updateGroup = function(user){
	   Groups.Update(user,function(users){
	   		var groupUpdate = {};
			groupUpdate.activitytype = activity_type.GROUPUPDATE;
			groupUpdate.what = users._id;
			groupUpdate.status = true;
			ActivitylogService.LogActivity(groupUpdate);
	   	notify({ message: 'Group '+ users.Name +' is updated', classes: 'alert-success', templateUrl: $scope.homerTemplate}); 
	 		},function(err){
		notify({ message: 'Problem in updating', classes: 'alert-danger', templateUrl: $scope.homerTemplate}); 
	 		$state.go($state.current, {}, {reload: true});     
		});
   };
//indivisual group check to delete
	$scope.GrpCheck = function(grp,index){
	    if(grp.isChecked2){
	    	$scope.grparray.push(grp._id);
	    }else{
	    	$scope.selected=false;
	        var i=$scope.grparray.indexOf(grp._id);
	        $scope.grparray.splice(i,1);
	    }	        	
	};
//select multiple check 
$scope.Select_AllGrp = function(select){
	if(select){
		$scope.grparray=[];
		angular.forEach($scope.groupslist,function(val, k){
			val.isChecked2=true;
			$scope.grparray.push(val._id);
		})
	}else{
		angular.forEach($scope.groupslist, function(val, k){
			val.isChecked2=false;
			$scope.grparray=[];
		})
	}
};   
//both multiple and indivisul delete group
	$scope.DeleteGroup=function(){
		if($scope.grparray.length>=1){
		Groups.removeGroup($scope.grparray,function(res){
			console.log(res);
		// var groupdel = {};
		// 	groupdel.activitytype = activity_type.DELETE;
		// 	groupdel.what = val.Name;
		// 	groupdel.status = true;
		// 	ActivitylogService.LogActivity(groupdel);
		notify({ message: 'Group is deleted', classes: 'alert-success', templateUrl: $scope.homerTemplate});
		$state.go($state.current, {}, {reload: true});
			})
		}else{
			notify({ message: 'No group to delete', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
		}	
	};
//To search the group name and hide the multi checkbox
    $scope.$watch('searchLength', function(sm){
      $scope.searchResults = false;
        if(sm=="" || sm==undefined){
         	$scope.searchResults=false;
        }
        else{
            	$scope.searchResults=true;
        }
    });
//ng-init 
	var count=0;
	$scope.setCount=function(status){
		console.log(status);
		if(status)
		count=count+1;
	};
	$scope.checkLength=function(){
		if(count==0)
			return true;
		else
			return false;
	};

	//indivisual check to delete user
	$scope.UserRemoveInd = function (user,index){
	    if(user.isCheckedU){
	    	$scope.userarray.push(user._id);
	    }else{
	    	$scope.select=false;
	        var i=$scope.userarray.indexOf(user._id);
	        $scope.userarray.splice(i,1);
	    }      	
	};
//select multiple check to delete
$scope.UserRemoveAll = function(select){
	if(select){
		$scope.userarray=[];
		angular.forEach($rootScope.users_list, function(val, k){
			if(val.status==true){
				val.isCheckedU=true;
				$scope.userarray.push(val._id);
			}else{
			}
		})
	}else{
		angular.forEach($rootScope.users_list, function(val, k){
			val.isCheckedU=false;
		})
		$scope.userarray=[];
	}	
};

	// $scope.DeleteGroup=function(){
	// 	if($scope.grparray.length>=1){
	// 	Groups.removeGroup($scope.grparray,function(res){
	// 	notify({ message: 'Group is deleted', classes: 'alert-success', templateUrl: $scope.homerTemplate});
	// 	$state.go($state.current, {}, {reload: true});
	// 		})
	// 	}else{
	// 		notify({ message: 'No group to delete', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
	// 	}	
	// };

//remove users from the group
$scope.Removegrpuser=function(){
	var obj={
		user_id:$scope.userarray,
		group_id:$rootScope.currentGroup._id
		}
	if($scope.userarray.length>=1){
	Groups.removeUsers(obj,function(data){
		$rootScope.users_list=data.user;
		$scope.select=false;
	notify({ message: 'User deleted', classes: 'alert-success', templateUrl: $scope.homerTemplate});
		$state.go($state.current, {}, {reload: true});
		//var ind=$rootScope.users_list.map(function(e) { return e._id; }).indexOf(value._id);
	});
	}else{
		notify({ message: 'No users selected', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
	}
};

//remove group
$scope.removeGroup=function(rm){
	Groups.removeGroup(rm,function(val){
		// var groupdel = {};
		// 	groupdel.activitytype = activity_type.DELETE;
		// 	groupdel.what = val.Name;
		// 	groupdel.status = true;
		// 	ActivitylogService.LogActivity(groupdel);
		notify({ message: 'Group '+ val.Name +' is deleted', classes: 'alert-success', templateUrl: $scope.homerTemplate});
			$state.go($state.current, {}, {reload: true});
	});
};

//status and roles( need to move this in global factory)
  $scope.userstatus = [
    {value: true, text: 'Active'},
    {value:false, text: 'Disabled'},
  ];
  $scope.groups=[
    {_id:1, text:'admin'},
    {_id:2, text:'user'},
    {_id:3, text:'Client Admin'}
  ];
//To show status,if set show status else not set
	$scope.showStatus = function(user){
    	var selected = [];
    	selected = $filter('filter')($scope.userstatus, {value: user.status});
    	return selected.length ? selected[0].text : 'Not set';
  	};
//role, if set show role else not set
  $scope.showGroup = function(user){
    var selected = [];
    if(user.role) {
      selected = $filter('filter')($scope.groups, {text: user.role});
    }
    return selected.length ? selected[0].text : 'Not set';
  };
//time in moment.js
    $scope.sendTime=function(date){
		var DateFormat = $moment(date).fromNow();
		return DateFormat;
	};
   $scope.cancelGroup = function(group){
        	$scope.group={};
    };


})
//Another controller for adding users to the group(modal was not working in the same controller,so creating one more controller here)
.controller('userSelectCtrl',function($scope,$http,$filter,$uibModalInstance,$moment,$state,Groups,$rootScope,Auth,User,UserService,GlobalService,$location,DefaultDocs,ActivitylogService,activityFcrt,ErrorService,NoteService,$window,docsCtrlVariables,GroupService,notify) {
$scope.homerTemplate = 'views/notification/notify.html';
$scope.userarray=[];
//To get current loggedin user;
$scope.getCurrentUser = Auth.getCurrentUser;

	$scope.getCurrentUser().$promise.then(function() {
		 docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);
		});

	$scope.getUserslists=function(){
		return $scope.userslist;
	};

	$scope.Capital_frst=function(grpname){
		return GroupService.First_UpperCase(grpname);
	};
//list of users and splice the one who already present in the group
	UserService.getUsers(function(data){
		$scope.tempuser=data;
		$scope.userslist=angular.copy($scope.tempuser);
		 angular.forEach($scope.tempuser,function(value,key){
		 	angular.forEach($rootScope.users_list,function(val,k){
		 		if(String(value._id)==String(val._id)){
		 		 	var ind=$scope.userslist.map(function(e) { return e._id; }).indexOf(value._id)
		 		 		$scope.userslist.splice(ind,1);
		 		 	}
		 		 });
		 })
	});	
//select users from userslist
	$scope.selectuser = function (users,index){
	    if(users.isCheckedI){
	    	$scope.userarray.push(users._id);
	    }else{
	    	$scope.selected=false;
	        var i=$scope.userarray.indexOf(users._id);
	        $scope.userarray.splice(i,1);
	    }	        	
	};
	//$scope.searchResults11;
// watch function for multi select checkbox
	    $scope.$watch('searchLength11', function(sm){
            $scope.searchResults11 = false;
            if(sm=="" || sm==undefined){
            	$scope.searchResults11=false;
            }
            else{
            	$scope.searchResults11=true;
            }
        });
	/*
	function to check length of loop
	*/
var count=0;
$scope.setCount=function(status){
	if(status)
	count=count+1;
};
$scope.checkLength=function(){
	if(count==0)
		return true;
	else
		return false;
}
//multiselect in grouplist
$scope.selectAll = function(select){
	if(select){
		$scope.userarray=[];
		angular.forEach($scope.userslist, function(val, k){
			if(val.status==true){
				val.isCheckedI=true;
				$scope.userarray.push(val._id);
			}else{
			}
		})
	}else{
		$scope.userarray=[];
		angular.forEach($scope.userslist, function(val, k){
			val.isCheckedI=false;
		})
	}
};
//add(select) the users to the group
	$scope.saveuser=function(usr){
		angular.forEach($scope.userslist,function(value){
			angular.forEach($scope.userarray,function(val){
				if(String(value._id)==val){
					$rootScope.users_list.push(value);
				}
			})
		})
		var obj={
					id:$rootScope.currentGroup._id,
					user:$scope.userarray
				}
		if($scope.userarray.length>=1){
			Groups.addUser(obj,function(res){
		// var adduserG = {};
		// 	adduserG.activitytype = activity_type.DELETE;
		// 	adduserG.what = val.Name;
		// 	adduserG.status = true;
		// 	ActivitylogService.LogActivity(adduserG);
				notify({ message: 'Users added to the group', classes: 'alert-success', templateUrl: $scope.homerTemplate});
			})
			$uibModalInstance.close();
		}
		else{
				notify({ message: 'No Users to add', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
			}
	};

//check if imageExists
	$scope.imageExists = function(image, users){
    	var img = new Image();
    	img.onload = function(){
	      	users.avatarflag = true;
	      	$scope.$apply();
    	}; 
    	img.onerror = function(){
		    users.avatarflag=false;
		    $scope.$apply();
    	};
    	img.src = image;
 	};
// check for role and setting organization
 	$scope.checkGroupOrg=function(user){
    if( $scope.getCurrentUser().role=="Client Admin" || $scope.getCurrentUser().role=="user"){
    	user.organization=$scope.getCurrentUser().organization;
    	//return false;
    }
    if($scope.getCurrentUser().role=="admin" && user.role=="admin"){
    	user.organization=$scope.getCurrentUser().clientId;
    	//return false;
    }
    // else
    // 	return true;
   };
//status and roles( need to move this in global factory)
  $scope.userstatus = [
    {value: true, text: 'Active'},
    {value:false, text: 'Disabled'},
  ];
  $scope.groups=[
    {_id:1, text:'admin'},
    {_id:2, text:'user'},
    {_id:3, text:'Client Admin'}
  ];
//To show status,if set show status else not set
	$scope.showStatus = function(user){
    	var selected = [];
    	selected = $filter('filter')($scope.userstatus, {value: user.status});
    	return selected.length ? selected[0].text : 'Not set';
  	};
//role, if set show role else not set
  $scope.showGroup = function(user){
    var selected = [];
    if(user.role) {
      selected = $filter('filter')($scope.groups, {text: user.role});
    }
    return selected.length ? selected[0].text : 'Not set';
  };
//time in moment.js
    $scope.sendTime=function(date){
		var DateFormat = $moment(date).fromNow();
		return DateFormat;
	};
//cancel
   $scope.cancel = function(){
        $uibModalInstance.close();
    };
})
//filter to provide list of  groups belongs to logged in user organisation(admin will get all the groups including clientadmin's org)
.filter('groupOrg',function(){
    return function (items, group) {
    	var OrgGroup=[];
    	if(items){
    		angular.forEach(items,function(val){
    			if((group.role=="Client Admin" && val.organization==group.organization)|| (group.role=="user" && val.organization==group.organization)){
    					OrgGroup.push(val);
    			}else if(group.role=="admin"){
    				OrgGroup.push(val);
    			}
    		})
    	}
    	return OrgGroup;
       };
})

.filter('groupLength',function(){
    return function (items, len) {
    	var GroupLength=[];
    	if(items){
    		angular.forEach(items,function(val){
    		if((len.role=="Client Admin" && val.organization==len.organization)|| (len.role=="user" && val.organization==len.organization)){
    		 	GroupLength.push(val);
    		 }else if(len.role=="admin"){
    		 	GroupLength.push(val);
    		 }
    		})
    	}
    	return GroupLength.length;
       };
});