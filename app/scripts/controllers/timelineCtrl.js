/**
 *
 * Timeline controller
 *
 */

angular
    .module('homer')
    .controller('timelineCtrl', timelineCtrl)

function timelineCtrl($scope, $uibModalInstance,$rootScope,docsCtrlVariables,$moment,param,timelineService,Auth,notify) {
    // console.log("inside timelineCtrl",param.doc);
    $scope.timelineDoc=param.doc;
    $scope.getCurrentUser = Auth.getCurrentUser;
    $scope.repeated = timelineService.getRepeat();
    $scope.currentDate = new Date();
    $scope.updateflag=false;
$scope.timeFlag=false;
    timelineService.getTimeline(param.doc,function(value){
        // console.log(value);
    $scope.getCurrentUser = Auth.getCurrentUser;
    $scope.homerTemplate='views/notification/notify.html';
$scope.getCurrentUser().$promise.then(function() {
        $rootScope.loggedIn = $scope.getCurrentUser();
        ////////////console.log($scope.loggedIn);
    });

        if(value.when){
            // console.log(value);
            $scope.updateflag=true;
            $scope.timeline=value;
            $scope.reminder_msg=value.message;
            // console.log($scope.reminder_msg);
              if(value.repeat==2){
                // console.log(value)
                angular.forEach(value.weekDay, function(v, key){
                   angular.forEach($scope.weeks, function(val, k){
                       if(v==val.day){
                        val.check=true;
                       }
                   });
                });
                
            }
           $scope.timeline.when=new Date(value.when); 
           if(value.untilDate){
            $scope.timeline.untilDate=new Date(value.untilDate);
           }
        }
        else{
            $scope.reminder_msg='Enter the Timeline text / details here';
             $scope.timeline={};
            $scope.timeline.when=$scope.currentDate;
        }
        
    });

    $scope.weeks = [{day:'Sun',check:false},{day:'Mon',check:false},{day:'Tue',check:false},{day:'Wed',check:false},{day:'Thu',check:false},{day:'Fri',check:false},{day:'Sat',check:false}];
  
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };


    $scope.saveTimeline=function(timeline){
     console.log(timeline,"to set timeline");
       $scope.timeFlag=true;
       // console.log($scope.timeFlag);
        var arr=[];
        $scope.reminder = {
                user_id : $scope.getCurrentUser()._id,
                document_id : param.doc._id,
                message : $scope.reminder_msg,
                when : new Date(timeline.when),
                checkboxModel : timeline.checkboxModel,
                recurring : timeline.recurring,
                repeat : timeline.repeat,
                untilDate : String(timeline.untilDate),
                //weekDay : $scope.selected1
            };    
            if($scope.reminder.repeat==2){
                angular.forEach($scope.weeks, function(value, key){
                    if(value.check==true){
                        arr.push(value.day);
                    }
                });
                $scope.reminder.weekDay=arr;
            }
            // console.log($scope.reminder);    
        timelineService.saveTimeline($scope.reminder,function(cb){
            console.log("set rem",cb);
            $uibModalInstance.close();
            
        });
        
    }
        $scope.updateTimeline=function(timeline){
             console.log(timeline,"update timeline");
             console.log(timeline,timeline._id);
            $scope.timeFlag=true;
        // console.log(timeline,$scope.timeFlag);
        var arr=[];
        $scope.updatereminder = {
                _id:timeline._id,
                user_id : $scope.getCurrentUser()._id,
                document_id : param.doc._id,
                message : $scope.reminder_msg,
                when : new Date(timeline.when),
                checkboxModel : timeline.checkboxModel,
                recurring : timeline.recurring,
                repeat : timeline.repeat,
                untilDate : String(timeline.untilDate),
                //weekDay : $scope.selected1
            };  
            console.log($scope.updatereminder);
            if($scope.updatereminder.repeat==2){
                angular.forEach($scope.weeks, function(value, key){
                    if(value.check==true){
                        arr.push(value.day);
                    }
                });
                $scope.updatereminder.weekDay=arr;
            }
            else{
                $scope.updatereminder.weekDay=[];
            }
            // console.log($scope.updatereminder);    
        timelineService.updateTimeline($scope.updatereminder,function(cb){
            console.log(cb,"cb")
            $uibModalInstance.close();
        });
        // console.log($scope.updatereminder,"updated reminder");

    };    

    $scope.checkSwitch=function(timeline){
        timeline.recurring=!timeline.recurring;
    }
    $scope.cancel=function(){
        $uibModalInstance.close();
    }
    $scope.selectWeek=function(week){
        // console.log(week);
        week.check=!week.check;
    }
}