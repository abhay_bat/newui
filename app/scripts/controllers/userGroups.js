/** 2Docstore
 * page: admin (user group page)
 * dev: mamatha
 * created date : 16th  August 2016
 * modified date :  23-Sept-2016   Pradeep T,  Modify the code structure
 * Discription : creating the user group and listing of the groups 
 */
'use strict';
 angular.module('homer')
 .controller('usergroupctrl',function(
 	$scope,$state,notify,
 	Groups,GroupService,$uibModal,$rootScope,docsCtrlVariables,
 	$filter,$moment,Auth,GlobalService,ActivitylogService) 
 {
 	$scope.getCurrentUser = Auth.getCurrentUser;
  $scope.getCurrentUser().$promise.then(function() {
    $rootScope.loggedIn = $scope.getCurrentUser();
    // $scope.loggedUser=$scope.getCurrentUser();
    ////console.log($scope.loggedIn,$scope.loggedIn.role);
    docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);

      });
 	
 	//scope variables used in this controller in addition to inheritated variables
 	$scope.homerTemplate = 'views/notification/notify.html';
 	$scope.selectedList=[];
 	$scope.selectUserList=[];
 	$scope.BreadCrumbs=[];
 	//Manage Breadcrumbs
 	var _pgManageGroup= "Manage Groups";
 	var _pgAddGroup="Add Group";
 	var _pgAddUsers="Add Users";
	var _pgGroupList="GroupList";
	var activity_type = GlobalService.getActivitytype();
	
	//console.log($state.current.name, "group",$rootScope.currentGroup);
	// if($rootScope.currentGroup==undefined){
	// 	$state.go(_pgGroupList);
	// };

	if($state.current.name=="InviteGroupUser" && $rootScope.currentGroup===undefined){
		$scope.BreadCrumbs=[];
		$state.go(_pgGroupList);
	}
 	//bread crumbs
 	$scope.ManageGroup=function(){
 		$scope.BreadCrumbs.push(_pgManageGroup);
 	};
	//create group page
	$scope.addGroup1=function(){
		$scope.BreadCrumbs.push(_pgManageGroup,_pgAddGroup);
	};
	//add user to group
	$scope.addUserGroup=function(){
		$scope.BreadCrumbs.push(_pgManageGroup,_pgAddUsers);
	};    
//click on the bread crum to go to previous page
	$scope.clickOnPage=function(page){
		if(page ==_pgManageGroup){
			$rootScope.currentGroup=undefined;
			$state.go(_pgGroupList);
		}
	};

	//list of Groups 
	
	/*with load more option (actualone)*/
	// $scope.groupCount=0;
	// $scope.groupslist=Groups.getAllGroups({'count':$scope.groupCount,'limit':8},function(groups){
	// 	// =groups;
	// 	// console.log(groups);
	// });

	// var Count=$scope.groupCount+8;
	// $scope.loadgroupMore=function(){
	// 	var grouplistCount=Count;
	// 	var lists=Groups.getAllGroups({'count':grouplistCount,'limit':8},function(data){
			
	// 		for(var i=0;i<lists.length;i++){
	// 			$scope.groupslist.push(lists[i]);
	// 		};
	// 	});
	// 	Count=Count+8;
	// };
     
	
/*Demo code */
$scope.groupCount=0;
	$scope.groupslist=Groups.getAllGroups({'count':$scope.groupCount,'limit':100},function(){
		// =groups;
		 //console.log("groups",groups);
	});

	var Count=$scope.groupCount+100;
	$scope.loadgroupMore=function(){
		var grouplistCount=Count;
		var lists=Groups.getAllGroups({'count':grouplistCount,'limit':100},function(){
			
			for(var i=0;i<lists.length;i++){
				$scope.groupslist.push(lists[i]);
			}
		});
		Count=Count+100;
	};


	//To make first letter as Uppercase and slice the 1st letter (groupservice.js)
	$scope.Capital_frst=function(grpname){
		return GroupService.First_UpperCase(grpname);
	};


	//this is the 2nd screan in creat group page
	$scope.AddGroup=function(){
		$state.go('UserGroup');
	};

//select multiple check to delete
	$scope.checkAll = function(select ,filtered){
		//console.log(select,filtered);
		if(!filtered)
		//var group="group";
		GroupService.UserRemoveAll(select,filtered,"group");
	};

//select individual check to delete
	$scope.addOrRemove=function(users,index,isSelect){
		//console.log(users,index,isSelect);
		$scope.selGroupis=users;
		if(!isSelect)
			$scope.selected=false;
		GroupService.addOrRemoveUser(users,index,$scope.selectedList);
	};
	
	//both multiple and individual delete group
	$scope.DeleteGroup=function(){
	//	console.log("delete group");         
		var groupList=GroupService.getList();
		var ListOfGroups=GroupService.getGroupList();
		//console.log(groupList,ListOfGroups);
			if(groupList.length>=1){
				Groups.removeGroup(groupList,function(){
					angular.forEach(groupList,function(val,k){
						//console.log("forloop",val,k,res);
						var ind=$scope.groupslist.map(function(e) { return e._id; }).indexOf(val);
						var i=ListOfGroups.map(function(e) { return e._id; }).indexOf(val);
						$scope.groupslist.splice(ind,1);
						ListOfGroups.splice(i,1);
						 //console.log("else condition",i);
						 //console.log($scope.selGroupis._id==val,"condition",$scope.selGroupis.Name,$scope.selGroupis)
				 		var deleteGrouplog = {};
						deleteGrouplog.activitytype = activity_type.GROUPDELETE;
						deleteGrouplog.what = $scope.selGroupis.Name;
						deleteGrouplog.status = true;
						//console.log("activity",deleteGrouplog);
						ActivitylogService.LogActivity(deleteGrouplog);
					});
				notify({ 
					message: 'Group is deleted',
					classes: 'alert-success',
					templateUrl: $scope.homerTemplate
					});
				$state.go('GroupList',{},{reload: true});
				});
				GroupService.deleteList();
		}else {
			notify({ 
				message: 'No group is selected',
				classes: 'alert-danger',
				templateUrl: $scope.homerTemplate
				});
		}
	};
	
	//this is the 2nd screen in creat group page
	$scope.AddGroup=function(){
		$state.go('UserGroup');
	};
	//keypress function in search
	$scope.valueSearch=function(){
		$scope.selectedList=[];
		angular.forEach($scope.groupslist,function(val, k){
			val.isChecked=false;
		});
		GroupService.deleteList();
		$scope.selected=false;
	};

//To add group
	 $scope.groupAdd=function(grp){
	 	 console.log(grp)
	 	if ($scope.GroupForm.$valid){
	 		// console.log("valid");
	 	// console.log(GroupService.DuplicateGroup(grp.Name,$scope.groupslist));
	 		if(!GroupService.DuplicateGroup(grp.Name,$scope.groupslist)){
	 			GroupService.creatGroup(grp,function(){
					notify({ message: 'Group is created', classes: 'alert-success', templateUrl: $scope.homerTemplate});
					
					$state.go('GroupList',{},{reload: true});
				});
	 		}else{
	 		notify({ message: 'Duplicate Group name', classes: 'alert-danger', templateUrl: $scope.homerTemplate});	
	 		}
	 	}else{
	 		// console.log("not valid");
	 		$scope.GroupForm.submitted = true;
	 	}
	};

// To add another group
	 $scope.oneMoregroup=function(grp){

	 		if(!GroupService.DuplicateGroup(grp.Name,$scope.groupslist)){
	 	GroupService.creatGroup(grp,function(){
			notify({ message: 'Group is created', classes: 'alert-success', templateUrl: $scope.homerTemplate});
			$state.go($state.current, {}, {reload: true});
		});
		}else{
			notify({ message: 'Duplicate Group name', classes: 'alert-danger', templateUrl: $scope.homerTemplate});	
	 	}
	 };
	 
	 	// update group
	$scope.groupUpdate=function(grp){
		// console.log(grp);
		GroupService.updateGroup(grp,function(){
			notify({ message: 'Group is updated', classes: 'alert-success', templateUrl: $scope.homerTemplate});
					//$state.go('GroupList',{},{reload: true});
		});
	};

	$scope.viewGroup=function(group){
		// console.log(group);
		$rootScope.currentGroup=group;
		$rootScope.users_list=group.user;
		$state.go('InviteGroupUser');
	};

	$scope.addUsersToGroup=function(){
		var modalInstance = $uibModal.open({
	        templateUrl: 'views/modal/modal_usergroup.html',
	        size:'lg',
	        controller:'userSelectCtrl'
	        });
	};

	$scope.imageExists=function(users){
		docsCtrlVariables.checkForImage(users,users.avatar);
	};

  $scope.userstatus = [
    {value: true, text: 'Active'},
    {value:false, text: 'Disabled'}
  ];
  //To show status,if set show status else not set
	$scope.showStatus = function(user){
    	var selected = [];
    	selected = $filter('filter')($scope.userstatus, {value: user.status});
    	return selected.length ? selected[0].text : 'Not set';
  	};

//select multiple check to delete
	$scope.UserRemoveAll = function(select ,filtered){
		GroupService.UserRemoveAll(select,filtered,$scope.selectUserList);
	};

//select indivisual check to delete
	$scope.addOrRemoveUser=function(users,index,isSelect){
		if(!isSelect)
			$scope.select=false;
		GroupService.addOrRemoveUser(users,index,$scope.selectUserList);
	};
//both multiple and individual delete users from group
	$scope.DeleteUsers=function(){
			var ListOfUsers=GroupService.getList();
			var obj={
					user_id:ListOfUsers,
					group_id:$rootScope.currentGroup._id
				};
			if(ListOfUsers.length>=1){
				Groups.removeUsers(obj,function(){
					angular.forEach(ListOfUsers,function(val, k){
						var ind=$rootScope.users_list.map(function(e) { return e._id; }).indexOf(val);
						$rootScope.users_list.splice(ind,1);
					});
					$scope.select=false;
					notify({ message: 'Users deleted', classes: 'alert-success', templateUrl: $scope.homerTemplate});
					$state.go($state.current, {}, {reload: true});
				});	
				GroupService.deleteList();
			 }else{
				notify({ 
					message: 'No user is selected',
					classes: 'alert-danger',
					templateUrl: $scope.homerTemplate
					});
			}
	};

	$scope.sendTime=function(date){
		var DateFormat = $moment(date).fromNow();
		return DateFormat;
	};

$scope.cancelGroup=function(){
// console.log(data);
$state.go($state.current, {}, {reload: true});
};
	
});