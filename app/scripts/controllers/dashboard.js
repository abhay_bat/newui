angular.module('homer')
  .controller('dashBoardCtrl', function ($scope,$state,$http,$location,Upload,$rootScope,md5,Auth,User,notify,DefaultDocs,$moment,ShareFctry,sweetAlert,uploadService,$uibModal,GlobalService,ActivitylogService,activityFcrt,docsCtrlVariables,versionService,timelineService,Groups,GroupService,blockUI,url) {

    // $uibModalInstance.dismiss('cancel');

   $scope.homerTemplate = 'views/notification/notify.html';
	var activity_type = GlobalService.getActivitytype();

/*sunil here*/
//$scope.shareFlag=false;
$scope.share=function(doc,version){
	console.log("share",doc,version);
	doc.shareFlag=true;

	$rootScope.guestdoc=doc;
	if(version){
		$rootScope.guestVersion=version;
	}
	var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/guestuser_invite.html',
        controller: 'GuestInviteCtrl'
        //size:'sm'
      })
}

/*
Array of pages
*/
$scope.pages=[1,2,3,4,5]
/*
setting pages
*/

$scope.setPage=function(val){
/*  console.log("check",val);
*/  docsCtrlVariables.setSkipValues(val)
  $scope.getMeMyDocs();
}

/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

$scope.getCurrentUser = Auth.getCurrentUser;
  $scope.getCurrentUser().$promise.then(function() {
    $rootScope.loggedIn = $scope.getCurrentUser();
    // $scope.loggedUser=$scope.getCurrentUser();
    ////console.log($scope.loggedIn,$scope.loggedIn.role);
    docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);

      });

// $ocLazyLoad.load('dashboard.js');
/*
Scope Variables declaraions
*/
//$rootScope.allinvitegroups=[];
$scope.showAddRow=false;
$scope.cbSelected=[];
$scope.trashSelected=[];
$scope.disableAddFolder=false;
$scope.showversion=false;
$scope.showUploadRow=[];
$scope.versionuploading=false;
$scope.showNoFiles=true;
$scope.versionfilename="";
$scope.trashcontent=true;
$scope.permadel=false;


$scope.getCurrentUser = Auth.getCurrentUser;
var host = $location.host();
$scope.getCurrentUser().$promise.then(function(){
	$rootScope.loggedIn = $scope.getCurrentUser();
		var someobj={
			host:host.split('.')[0]
		};
	var ioSocket = io(url, {
	 query: someobj,
      path: '/socket.io-client'
    });	

    ioSocket.on('Document:save', function (item) {
	if(item.isFavourite.user_id){
		var ind=item.isFavourite.user_id.indexOf($rootScope.loggedIn._id);
		if(ind>-1){
			item.isFavourite.status=true;
		}
		else{
			item.isFavourite.status=false;
		}
	}
	
		docsCtrlVariables.addDocumentSocket(item,function(process){
	
			var location = item.s3_path;
		       location = location.substring(location.indexOf("/") + 1);
		       var location1 = location;
		       location1 = location1.substring(location1.indexOf("/") + 1);
		        location2 = location1;
		        location2 = location2.substring(0, location2.lastIndexOf("/"));
			if($rootScope.loggedIn._id!=item.createdBy._id && process =="create"){
		        	notify({ message: "A new "+item.type+ " has been added at location "+location2 , classes: 'alert-success', templateUrl: $scope.homerTemplate});

			}
			else if(process=="update"){
				if(item.modifiedBy){
					if(item.modifiedBy!=$rootScope.loggedIn._id){
						notify({ message: item.type+" "+item.documentName+ " has been updated at location "+location2 , classes: 'alert-success', templateUrl: $scope.homerTemplate});	
					}
					
				}
				
			}
			if(!$scope.$$phase) {
			  $scope.$apply();
			}
			
		});		


});
    ioSocket.on('Document:remove', function (item) {
	})
$scope.$on('$destroy', function() {

    ioSocket.removeAllListeners('Document:save');
  });
	
});

/*\
Allow download
*/
  Auth.isLoggedInAsync(function(loggedIn) {
      if(loggedIn){
       $state.go('dashboard');
      }
     });

$scope.showDownload=function(doc)
{
	if(doc.allowDownload && doc.type=='file')
		return true;
	else
		return false;
}

/*
	functionName: errorMessage
	@param:text
*/
$scope.errorMessages=function(text){
	notify({ message: text, classes: 'alert-danger', templateUrl: $scope.homerTemplate});
}

// $scope.getCurrentUser = Auth.getCurrentUser;
// 	$scope.getCurrentUser().$promise.then(function(){
// 		$rootScope.loggedIn = $scope.getCurrentUser();
// 	});


/*
if image doesn't exist
*/
	$scope.checkIfImageExists=function(user,image){
	user.name=user.user_id.name;
	docsCtrlVariables.checkForImage(user,image);
	};

	$scope.showTrash = true;
		 $scope.trashFunc = function() {
		 	/**/
		 	console.log($scope.actualLength,$scope.trashLength)
		 	if( ($scope.actualLength-$scope.trashLength)== 0 ){
		 		notify({ message: 'There are zero items in trash' ,classes: 'alert-danger', templateUrl: $scope.homerTemplate});
		 	}
		 	else{
		 	docsCtrlVariables.updateDocumentInfo();
		 	$scope.showTrash = !$scope.showTrash;	
		 	}
		 	
		};

		$scope.hidetrashFunc=function(){
			docsCtrlVariables.updateDocumentInfo();
		 	$scope.showTrash = !$scope.showTrash;	

		}

	$scope.restoreFunc=function(){
		var currentDoc=docsCtrlVariables.getCurrentDocument();
		if($scope.trashSelected.length){
			for (var j = 0; j < $scope.trashSelected.length; j++) {
						var obj = {
							id : $scope.trashSelected[j]
						};
						DefaultDocs.restore(obj, function(data) {
							$scope.cbSelected=[];
					$scope.trashSelected = [];
					$scope.trashSelected[j]=false;

					$scope.checkAllItems=false;
						var restorelog = {};
								restorelog.activitytype=activity_type.RESTORE;
								restorelog.location=data.requestObject.s3_path;
	 						restorelog.what = data.requestObject;			
	 						restorelog.status = true;
							 ActivitylogService.LogActivity(restorelog);
							 docsCtrlVariables.restoreDoc(data.requestObject._id);
							// DefaultDocs.show({

							// 		id : currentDoc._id
							// 	}, function(response) {
							// 		docsCtrlVariables.setDocumentInfo(response);
							// 		/*$scope.documentList = response;
							// 		$scope.list = $scope.documentInfo;*/
									
							// 	});

							sweetAlert.swal("Restored!", "restore done.", "success");
										 	docsCtrlVariables.updateDocumentInfo();
										 	$scope.cbSelected=[];
										 	$scope.trashSelected=[];

						});

					}

		// 			$scope.cbSelected=null;
		}
		else{
			sweetAlert.swal("Cancelled", "You have not selected any trashed folder/file", "error");
		}
		// $scope.cbSelected=null;
		
		};


	//details
  $scope.getDocs=DefaultDocs.query({
  
  },function(data){
    $rootScope.tenantDocs=data;
  })

/*
get the current document name
*/
$scope.checkCurrentDocument=function(param){
	var current = docsCtrlVariables.getCurrentDocument();
	$rootScope.currentAccess=current.access_list;
		if(param=="name")
			return current.documentName;
		else{
			return current.root;
 		}
};
	// To capitalize the first letter and display(first letter)
	$scope.Capital_frst = function(firstLtr) {
		if (firstLtr == undefined)
			return;
		var res = firstLtr.toUpperCase();
		var res1 = res.slice(0,1);
		return res1;
	};

	//remove group
	$scope.removeGroup=function(grp,index){
		var currentDoc=docsCtrlVariables.getCurrentDocument();
		$scope.arrayOfgrp=[currentDoc._id];
		console.log(grp,$rootScope.policyId);
		var obj={
				id:grp._id,
				policy:$rootScope.policyId._id,
				document:$scope.arrayOfgrp
				};
				angular.forEach($rootScope.allinvitegroups,function(value){		
					if(value._id==obj.id){
						angular.forEach(value.groupinvite,function(val){
							if(val.document.indexOf(currentDoc._id)>-1){
								obj.policy=val.policy._id;
							}
						})
					}
	    		});
		Groups.RemoveGroupDoc(obj,function(res){
				//var grpLog={};
				//grpLog.activitytype=activity_type.REMOVE;
				//grpLog.location=data.requestObject.s3_path;
		 		//grpLog.what = data.requestObject;			
		 		//grpLog.status = true;
				//ActivitylogService.LogActivity(grpLog);
		})
	    $rootScope.allinvitegroups.splice(index,1);
	};

$scope.ifMydocs=function(){
	 var whichDoc=docsCtrlVariables.getWhichDocument();
	var current = docsCtrlVariables.getCurrentDocument();
	var bread = docsCtrlVariables.getbreadcrumbs();
	if(bread.length>1)
		return true;
	else 
		return false;
}


/*
	checkaccess
*/

docsCtrlVariables.newbreadcrumbs();
/*
Query to get my documents

*/
var whichDoc=docsCtrlVariables.getWhichDocument();		
$scope.getMeMyDocs=function(){
	
	 docsCtrlVariables.getDocs(whichDoc,function(docName){
	 	if(whichDoc=="ProjectView"){
	 		$scope.checkForPermission(docsCtrlVariables.getCurrentDocument(),function(){
	 		})
	 	}
	 	whichDoc=docName;
	 });
}
$scope.getMeMyDocs(); 

$scope.checkIfNtLocked=function(document){

		//check if the document object is valid. if not unlock and return.
		if (document == undefined || document.isLocked == undefined || document.isLocked.status == undefined ||  document.isLocked.status == false) {
			
	
			return false;
		}
	if (document.isLocked.status && document.isLocked.user_id==$rootScope.loggedIn._id ) {
			return true;
			} else {
				return false;

			}
}

/*
function name : folderClick
@params: document obj and evernt
return: will populate document list based on the elements
*/
$rootScope.showInvite=false;
$scope.allowCreateFolder=true;
$scope.allowDelete=true;
$scope.allowUpload=true;
$rootScope.allowMoadlOpen=true;
$scope.trashedClick=false;
$scope.folderClick = function(doc, ev,version) {
console.log(doc);

if(version==undefined || version==null){
	// console.log("null",doc.version);
	 angular.forEach(doc.version, function(value, key){
  if(doc.version.length == value.number){
  	// console.log(value,value.number);
    $rootScope.commentVersion=value;
    // console.log($rootScope.commentVersion);
  }
 });

}else{
	// console.log(doc.version,doc,version);
	$rootScope.commentVersion=doc;
};

$scope.checkAllItems=false;
	$scope.checkForPermission(doc,function(){

	if(doc.type=='folder'){
		 $scope.allotPermission(doc);
			
	
if(doc.isLocked.user_id==$rootScope.loggedIn._id || !doc.isLocked.status)
		{		
		$scope.cbSelected=[];
$scope.trashSelected=[];
		if(doc.showInvite)
		//$rootScope.showInvite=true;
			$scope.showInvite=true;
		else
			$rootScope.showInvite=false;
		if(doc.allowRemovingContrib)
			$scope.canUserRemoveContrib=true;
		else
			$scope.canUserRemoveContrib=false;

	
		if(doc.trash  )

		{
			$scope.allowCreateFolder=false;
			$scope.allowUpload=false;
		}
		else{
			$scope.allowCreateFolder=doc.allowAddFolders;
			$scope.allowUpload=doc.allowUpload;
			$scope.allowDelete=doc.allowDelete;
		}
			
		//push the folder to breadcrumbs don't push if folder is
		//$rootScope.breadcrumbs.push(doc);
		docsCtrlVariables.folderClick(doc);	
		Groups.getGroups({"id":doc._id},function(groups){
			
			////console.log($rootScope.allinvitegroups);

			$rootScope.allinvitegroups=groups;
			angular.forEach($rootScope.allinvitegroups,function(value){
				angular.forEach(value.groupinvite,function(val){
					if(val.document.indexOf(doc._id)>-1){
						$rootScope.policyId= val.policy;
						value.policy=val.policy.policy_name;
					}
				})
			})

		});	
	}
	else{
		notify({ message: doc.documentName +' is Locked' ,classes: 'alert-danger', templateUrl: $scope.homerTemplate});

	}
}
	//else type= file
	else{
		console.log("breadcrumbs");
		
		docsCtrlVariables.setpropertyDoc(doc);
	//	//console.log(doc.isLocked.user_id,$rootScope.loggedIn._id, !doc.isLocked.status)
	blockUI.start();
	  blockUI.message('Opening' + doc.documentName.slice(0,10) + "..");
if(doc.isLocked.user_id==$rootScope.loggedIn._id || !doc.isLocked.status)
		{/*activity log for file view*/
		var viewLog = {};
						viewLog.activitytype = activity_type.VIEW;
						viewLog.what = doc._id;
						viewLog.location = doc.parent;
						viewLog.status = true;
						ActivitylogService.LogActivity(viewLog);
// console.log(viewLog,ActivitylogService,viewLog.location,doc.parent,doc);
						/*if a document open with gdocs*/
		
			
						var file_id = {
							id : doc._id
						};
						if(version){
							file_id.version=version;
							doc.url=doc.url+'?versionId='+version;
						}
						console.log("file_id",file_id);
						DefaultDocs.viewFile(file_id, function(result) {
							 console.log(result,result.url,doc.url);
							blockUI.start();

// console.log(doc);
			if($scope.checkIfImage(doc)){
		
			$rootScope.isImage=true;
			$rootScope.imageUrl=result.url;
			console.log($rootScope.imageUrl);
		}
		else
		{
			console.log(doc);
		$rootScope.isImage=false;	
		if(((/\.(pdf)$/i).test(doc.documentName))){
			console.log("pdf");
			$rootScope.viewurl=doc.url;
			console.log(doc.url)
		}else{
			console.log("not pdf and images",doc.url);
			$rootScope.viewurl="https://view.officeapps.live.com/op/view.aspx?src="+doc.url;
			console.log($rootScope.viewurl);
		};

		}						
								$rootScope.documentDetails=doc;
								$rootScope.documentHeading=doc.documentName;
								// console.log("doc",doc);
								$rootScope.documentHeadingId=doc;
								
								docsCtrlVariables.openFile();

								}, function(err) {
									blockUI.stop();
									$scope.error = "Documenet is locked by " + doc.isLockedtoName;
									
									//SSsweetAlert.swal("Error",$scope.error,"error");

								});		

		// else{
		// //	// //console.log("image",doc);
		// 	var file_id = {
		// 					id : doc._id
		// 				};
		// 	if(version){
		// 					file_id.version=version;
		// 				}
		// 				DefaultDocs.viewFile(file_id, function(result) {
		// 						$rootScope.viewurl = result.url;
		// 						$rootScope.documentHeading=doc.documentName;
		// 						docsCtrlVariables.openFile();
		// 						})

		// }
		}
		else{
			blockUI.stop();
				notify({ message: doc.documentName +' is Locked' ,classes: 'alert-danger', templateUrl: $scope.homerTemplate});

		}
	}	
	});
			
};

if(whichDoc=="Client Documents" && $rootScope.projectViewDoc){
	//$scope.folderClick($rootScope.projectViewDoc);
}

/*
For BreadCrumbs
*/
/*
check if first
*/
$scope.checkIfFirst=function(){
	var bread = docsCtrlVariables.getbreadcrumbs();
	if(bread.length>0 && whichDoc=="shared")
		return true;
	else
		return false;
}
/*
for hiding contribs
*/
$scope.checkIfSharedDoc=function(){
	var bread = docsCtrlVariables.getbreadcrumbs();
	if(bread.length==0 && whichDoc=="shared")
		return true
	else
		return false
}

$scope.checkIfClientDoc=function(){
	var bread = docsCtrlVariables.getbreadcrumbs();
	if(bread.length==1 && whichDoc=="Client Documents")
		return true
	else
		return false
}


/*
clicked on shared in breadCrumb
*/
$scope.clickOnshared=function(){
	$scope.showversion=false;
	docsCtrlVariables.newbreadcrumbs();
	 docsCtrlVariables.getDocs(whichDoc,function(){

	 });
}
/*


Check if shared
*/
$scope.checkIfShared=function(){
	if(whichDoc=="shared")
		return true;
	else
		return false;
}
/*
FUNCTION TO get breadcrumbs
*/
$scope.getBreads=function(){
	var bread = docsCtrlVariables.getbreadcrumbs();
	return bread;
}

/*
///End all bread Crumbs for share
*/
/*
Start Breads for Search
*/
$scope.checkIfSearch=function(){
	if(whichDoc=="search")
		return true;
	else
		return false;
}

$scope.clickOnsearch=function(){
	docsCtrlVariables.newbreadcrumbs();
	 docsCtrlVariables.getDocs(whichDoc);
}

$scope.checkIfFirstS=function(){
	var bread = docsCtrlVariables.getbreadcrumbs();
	if (bread.length>0 && whichDoc=="search")
		return true;
	else
		return false;

}
/*
Start BreadCrumbs if its Fav documents

*/
$scope.checkIfFav=function(){
	if(whichDoc=="fav")
		return true;
	else
		return false;
}
$scope.checkIfFirstF=function(){
	var bread = docsCtrlVariables.getbreadcrumbs();
	if (bread.length>0 && whichDoc=="fav")
		return true;
	else
		return false;
}
$scope.clickOnFav=function(){
	$scope.showversion=false;
	docsCtrlVariables.newbreadcrumbs();
	 docsCtrlVariables.getDocs(whichDoc);
}

$scope.checkIfFavDoc=function(){
	var bread = docsCtrlVariables.getbreadcrumbs();
	if(bread.length==0 && whichDoc=="fav")
		return true
	else
		return false
}

$scope.checkIfSearchDoc=function(){
var bread = docsCtrlVariables.getbreadcrumbs();
	if(bread.length==0 && whichDoc=="search")
		return true
	else
		return false
}
/*
End Fav breads
*/
$scope.getDocumentList=function(){
	var docs= docsCtrlVariables.getDocumentInfo();
	//console.log("document list",docs)
	return docs;
}
/*Function to convert time in moments
@Params: time to be converted
returns: Time in sentence
*/

$scope.myPagingFunction = function(){
	//console.log("scrolling");
}
$scope.sendTime=function(date){
		var DateFormat = $moment(date).fromNow();
		return DateFormat;
	}
/*
check length of all docs
*/
$scope.checkLengthDoc=function(){
	var documentLength=docsCtrlVariables.giveLength();
	$scope.actualLength= docsCtrlVariables.giveLength();
	return documentLength;
};
$scope.len1=docsCtrlVariables.giveLength();
$scope.len2=docsCtrlVariables.giveLengthtrash();

$scope.checkLengthDocTrash=function(){
	var documentLength=docsCtrlVariables.giveLengthtrash();
	$scope.trashLength =docsCtrlVariables.giveLengthtrash()
	return documentLength;
};
$scope.checkLengthoFdocs=function(){
	if($scope.showTrash==false){
		var documentLength=docsCtrlVariables.giveLength()
		console.log(documentLength);
	if(documentLength==0)
		return true;
	else
		return false;
	}
	else {
		var documentLength=docsCtrlVariables.giveLengthtrash()
			if(documentLength==0)
				return true;
			else
				return false;		
	}
	
};

/*Function to convert time in moments
@Params: time to be converted
returns: Time in sentence
*/

	$scope.sortByWhat = ['type', 'created'];
	$scope.isSort = true;

	$scope.callSort=function(){
		if ($scope.isSort) {
			$scope.sortByWhat = ['type', 'documentName'];
			$scope.isSort = false;
		} else {

			$scope.sortByWhat = ['type', '-documentName'];
			$scope.isSort = true;
		}
	}


/*
Function Name: CheckAll
for checkbox selectAll
Is called when select all is called, end of the function, all the documents being listed will be 
to pushed to an array
*/
//checkAll function
	$scope.checkAll = function() {

		var documentList=docsCtrlVariables.getDocumentInfo();
		$scope.cbSelected = [];
		$scope.trashSelected=[];
		angular.forEach(documentList, function(document, key) {
			//if(document.trash==false){
				document.check = $scope.checkAllItems;
			//}			
			if (document.check == 'true') {
						if(document.trash==false){
							$scope.cbSelected.push(document._id);	
						}
						else{
							$scope.trashSelected.push(document._id);
						}
			}
			
		});

	};

/*
Function Name: Checkbox
Is called when checkbox is clicked on, based on if selected or selected twice, doc id will be pushed or
spliced from cb selected
*/
		//Keep a list of all check boxes selected
	$scope.checkbox = function(id,trash) {
			var docLength=$scope.checkLengthDoc();
			if (trash && $scope.trashSelected.length==0) {
				
					$scope.trashSelected.push(id);
					if ($scope.cbSelected.length + $scope.trashSelected.length == docLength)
				$scope.checkAllItems = true;

			}
			else if(!trash && $scope.cbSelected.length==0){
				$scope.cbSelected.push(id);
				if ($scope.cbSelected.length + $scope.trashSelected.length == docLength)
				$scope.checkAllItems = true;
			}	
					
		 else {
			if ($scope.cbSelected.indexOf(id) == -1 && !trash) {
				
					
				
					$scope.cbSelected.push(id);
			}
			else if($scope.cbSelected.indexOf(id) >-1 && !trash){
				$scope.cbSelected.splice($scope.cbSelected.indexOf(id), 1);

			}
							
			else if($scope.trashSelected.indexOf(id) == -1 && trash){

				$scope.trashSelected.push(id);
			} else if($scope.trashSelected.indexOf(id) >-1 && trash){
					$scope.trashSelected.splice($scope.trashSelected.indexOf(id), 1);
				}
			}
		
		if($scope.cbSelected.length + $scope.trashSelected.length==docLength)
		{
			$scope.checkAllItems = true;
		}
		else
		{
			$scope.checkAllItems = false;
		}
		
		// $scope.trashcount=0;
		// $scope.untrashcount=0;
		// angular.forEach(docsCtrlVariables.getDocumentInfo(), function(value, key){
		// 	angular.forEach($scope.cbSelected, function(val, k){
		// 		if(value._id==val){
		// 			if(value.trash==true){
		// 				$scope.trashcount++;
		// 			}
		// 			else{
		// 				$scope.untrashcount++;
		// 			}
		// 		}
		// 	});
		// });		
		// angular.forEach(docsCtrlVariables.getDocumentInfo(), function(value, key){
		// 	angular.forEach($scope.trashSelected, function(val, k){
		// 		if(value._id==val){
		// 			if(value.trash==true){
		// 				$scope.trashcount++;
		// 			}
		// 			else{
		// 				$scope.untrashcount++;
		// 			}
		// 		}
		// 	});
		// });			

		// if($scope.trashcount>0 && $scope.untrashcount >0 ){
		// 	$scope.trashcontent=false;
		// }
		// else if($scope.trashcount==0 || $scope.untrashcount ==0){
		// 	$scope.trashcontent=true;
		// }

		// if($scope.trashcount>0 && $scope.untrashcount ==0){
		// 	$scope.permadel=true;

		// }
		// else{
		// 	$scope.permadel=false;
		// }


	};
/*
@Function name:allotPermission
@prams:document object.
This will in turn call check for permission with a promise
*/
	$scope.allotPermission=function(doc){
	$scope.checkForPermission(doc,function(){

	});
	}
$scope.checkForPermission = function(folder,cb) {
		//////\//console.log(folder.documentName);
		//blockUI.start();
		//$scope.allowFolderClick = false;
		var docIndex;
		var flag = true;

		folder.access_list = [];
		Groups.getdocument({
			"document" : folder._id
		}, function(response) {
			console.log("res is",response);
			if (response.length > 0) {
				angular.forEach(response, function(user, key) {
					console.log("user info",user,$rootScope.loggedIn);
					console.log("user",user.user);
					var obj = {};
					if(!user.groupId){
						obj.policy_id = user.policy;
							obj.user_id = user.user;
							folder.access_list.push(obj);
							$rootScope.showEmptyContribList = false;

					}
							if ($rootScope.loggedIn._id == user.user._id) {
								flag = false;
								folder.policyName = user.policy.policy_name;
								folder.allowDelete = $scope.givePermission(user.policy.permissions.delete)
								folder.allowDownload = $scope.givePermission(user.policy.permissions.download);
								folder.allowAddFolders = $scope.givePermission(user.policy.permissions.add_folders);
								folder.allowEdit = $scope.givePermission(user.policy.permissions.edit);
								folder.allowShare = $scope.givePermission(user.policy.permissions.share);
								folder.allowUpload = $scope.givePermission(user.policy.permissions.upload);
								folder.allowView = $scope.givePermission(user.policy.permissions.view);
								if (folder.policyName == "Owner" || folder.policyName == "Co-owner" || folder.policyName == "Publisher") {
									folder.allowLock = true;
									folder.allowRemovingContrib = true;
									folder.showInvite=true;
									$scope.showInvite=true;

								} else {
									folder.allowLock = false;
									folder.allowRemovingContrib = false;
									folder.showInvite=false;
									$scope.showInvite=false;
									
								}	
							}					
					// angular.forEach(user.policies, function(policy, key) {
					// 	if (policy.document.indexOf(folder._id) > -1) {
					// 		obj.policy_id = policy.policy;
					// 		obj.user_id = user.user;
					// 		folder.access_list.push(obj);
					// 		$rootScope.showEmptyContribList = false;
					// 		if ($rootScope.loggedIn._id == user.user._id) {
					// 			flag = false;
					// 			folder.policyName = policy.policy.policy_name;
					// 			folder.allowDelete = $scope.givePermission(policy.policy.permissions.delete)
					// 			folder.allowDownload = $scope.givePermission(policy.policy.permissions.download);
					// 			folder.allowAddFolders = $scope.givePermission(policy.policy.permissions.add_folders);
					// 			folder.allowEdit = $scope.givePermission(policy.policy.permissions.edit);
					// 			folder.allowShare = $scope.givePermission(policy.policy.permissions.share);
					// 			folder.allowUpload = $scope.givePermission(policy.policy.permissions.upload);
					// 			folder.allowView = $scope.givePermission(policy.policy.permissions.view);
					// 			if (folder.policyName == "Owner" || folder.policyName == "Co-owner" || folder.policyName == "Publisher") {
					// 				folder.allowLock = true;
					// 				folder.allowRemovingContrib = true;
					// 				folder.showInvite=true;
					// 				$scope.showInvite=true;

					// 			} else {
					// 				folder.allowLock = false;
					// 				folder.allowRemovingContrib = false;
					// 				folder.showInvite=false;
					// 				$scope.showInvite=false;
									
					// 			}	
					// 		}
					// 	}

					// })
				})
		//		////console.log(folder);
				if (flag == true) {
					//set as owner if admin
					folder.policyName = "Owner";
					//
					folder.allowDelete = true;
					folder.allowDownload = true;
					folder.allowAddFolders = true;
					folder.allowEdit = true;
					folder.allowShare = true;
					folder.allowUpload = true;
					folder.allowView = true;
					folder.allowLock = true;
					folder.allowRemovingContrib = true;
					folder.showInvite=true
					$scope.showInvite=true;
				}
			
			} else {
		//		////console.log(response.length);
				folder.policyName = "Owner";
				folder.allowDelete = true;
				folder.allowDownload = true;
				folder.allowAddFolders = true;
				folder.allowEdit = true;
				folder.allowShare = true;
				folder.allowUpload = true;
				folder.allowView = true;
				folder.allowLock = true;
				folder.showInvite=true;
				$scope.showInvite=true;
				folder.allowRemovingContrib = true;
			}
	//console.log(folder);
			cb();

		}, function(err) {
			//blockUI.stop();
		});
		$scope.allowFolderClick = true;
	};
/*
give permission
*/
//change allow or delete
	$scope.givePermission = function(value) {
		if (value == "deny")
			return false;
		else
			return true;
	};
/*
getLockerName
*/
$scope.getLockerName=function(document){

	var id=document.isLocked.user_id;
		User.show({
				"user_id" : id
			}, function(response) {
				//document.matchLockWithLocker = false;
				document.lockedName = response.name;
	if($rootScope.loggedIn._id==document.isLocked.user_id || document.lockedName == undefined)
	{
		document.lockerText="Unlock"
	}
	else{
		document.lockerText="Document is locked by " + document.lockedName ;
	}
			})
	
}
/*
function : initiate lock
*/
$scope.initiateLock=function(document){
	if(document.isLocked.status==true)
		document.lockImage=true;
	else 
		document.lockImage=false;

}

/*
function Name: checkIDtoLock
gets the the ID of the user to whom the document is locked to 
*/
	$scope.findlock = function(document) {
		////console.log(document.isLocked.status , document.isLocked.user_id,$rootScope.loggedIn._id)

		//check if the document object is valid. if not unlock and return.
$scope.checkForPermission(document,function(){

		if (document == undefined || document.isLocked == undefined || document.isLocked.status == undefined) {
			
	/*		document.isLocked.status=true;
			document.isLocked.user_id=$rootScope.loggedIn._id;*/
			document.isLocked.lockImage=true
			return;
		}

		if (document.allowLock){
			if (document.isLocked.status && document.isLocked.user_id==$rootScope.loggedIn._id ) {
				//modify doc to include islocked as true
				// var obj={
				// 	status:false
				// };
				document.isLocked.status = false;
			
				
				DefaultDocs.locked(document, function(data) {
					document.lockImage=false;
						var lockLog = {};
					lockLog.activitytype = activity_type.UNLOCK;
					lockLog.what = document._id;
					lockLog.location = document.s3_path;
					lockLog.status = true;
					ActivitylogService.LogActivity(lockLog);
					
				});
			} else {
				//modify doc to include islocked as false
				// var obj={
				// 	user_id:$scope.getCurrentUser()._id,
				// 	status:true
				// };

				document.isLocked.status = true;
				DefaultDocs.locked(document, function(res) {
					document.isLocked.status=true;
					document.isLocked.user_id=$rootScope.loggedIn._id;
					var unlockLog = {};
					unlockLog.activitytype = activity_type.LOCK;
					unlockLog.what = document._id;
					unlockLog.location = document.s3_path;
					unlockLog.status = true;
					ActivitylogService.LogActivity(unlockLog);
					document.lockImage=true;
					document.lockedName =$scope.loggedIn.name
				},function(err){
				notify({ message: 'Warning - Document Locked by ' +document.lockedName ,classes: 'alert-danger', templateUrl: $scope.homerTemplate});
				});
			}
		} else {
			var lockFile = document.documentName;
			//swal("You don't have persmission to lock " + lockFile);
			sweetAlert.swal("Error","You don't have persmission to lock/unlock " + lockFile,"error");
		}
	})
	};

$scope.initiateFav=function(document){
	if (document.isFavourite.status == true){
		document.favText=" Favorite"
			
		}else{
			document.favText=" Favorite"
			
		}
}


/*
function to set favourite
*/
$scope.setFavourite=function(document)
{

	if(document.isLocked.status == true && document.isLocked.user_id!=$rootScope.loggedIn._id){

        notify({ message: 'Warning - Document Locked ' ,classes: 'alert-danger', templateUrl: $scope.homerTemplate});
    }
	else{
		if (document == undefined || document.isFavourite == undefined || document.isFavourite.status == undefined) {
			document.isFavourite.status = true;
			return;
		}

		if (document.isFavourite.status) {
			//modify doc to include islocked as true
			var obj = {
				user_id : $scope.getCurrentUser()._id,
				status : false
			};
			document.isFavourite.status = false;
			DefaultDocs.favroite(document, function(data) {
				document.isFavourite.status = false;
				var unfavLog = {};
				unfavLog.activitytype = activity_type.UNFAV;
				unfavLog.what = document;
				unfavLog.location = document.s3_path;
				unfavLog.status = true;
				ActivitylogService.LogActivity(unfavLog);
				document.favImage = false
			})

			
		} else {
			//modify doc to include islocked as false
			var obj = {
				user_id : $scope.getCurrentUser()._id,
				status : true
			};
			document.isFavourite.status = true;
			DefaultDocs.favroite(document, function(res) {
				
				document.isFavourite.status = true;
				//from activity service
				var favLog = {};
				favLog.activitytype = activity_type.FAV;
				favLog.what = document;
				favLog.location = document.s3_path;
				favLog.status = true;
				ActivitylogService.LogActivity(favLog);
				document.favImage = true;
				document.favName = $scope.loggedIn.name
			});
			
		}
	}
	
}
/*FunctionName: Download
@Params:docuemnt object
Returns: URL to start download*/
$scope.download=function(document,version){
	//console.log("vers",version);
			$scope.checkForPermission(document,function(){
if(document.isLocked.user_id==$rootScope.loggedIn._id || !document.isLocked.status)
{

	if(document.allowDownload){
	var current = docsCtrlVariables.getCurrentDocument();	
	var file_id = {
				id : document._id
			};
			if (version){
				file_id.version=version;
			}
			DefaultDocs.download(file_id, function(result) {
				location.href = result.url;
			var downloadLog = {};
			downloadLog.activitytype = activity_type.DOWNLOAD;
			downloadLog.what =  document._id;
			downloadLog.location = current.parent + '/' + current.documentName;
			downloadLog.status = true;
			ActivitylogService.LogActivity(downloadLog);
			//from activity service
			$scope.downloadLog = activityFcrt.save(downloadLog, function(data) {
			});
			}, function(err) {
				notify({ message: 'Warning - Please Enter Valid Username or Password', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
       
			});
		}
		else{
			notify({ message: 'Error - You do not have permission to download '+document.documentName , classes: 'alert-danger', templateUrl: $scope.homerTemplate});

		}
	}
	else{
			notify({ message: 'Error - '+document.documentName +' is Locked' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
	}
  });
}

/*FunctionName: findIcon(document)
@Params:docuemnt object
Returns: Class for display of appropriate icon*/
	$scope.findIcon=function(document){
	/*
	first criteria if the type is a folder
	*/		var icon=docsCtrlVariables.findIcons(document)
		return icon;
			}
/*
FunctionName: multidel
will delete all ID's present in cbSeleceted
*/
$scope.multiDel = function(document,id) {
	// console.log(document);
	if(!$scope.showTrash && $scope.trashSelected.length>0){
		//two function calls max
		//$scope.setUpObject();
	var currentDoc=docsCtrlVariables.getCurrentDocument();
		var documentList=docsCtrlVariables.getDocumentInfo();
		if ($scope.cbSelected.length != 0 || $scope.trashSelected.length != 0 ) {
	sweetAlert.swal({
				title : "Warning",
				text : "Files in Trash will be deleted permanently, other files will be trashed!",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#62CB31",
				cancelButtonColor:"#F44336",
				confirmButtonText : "Delete",
				cancelButtonText : "Cancel",
				closeOnConfirm : true,
				closeOnCancel : true
			}, function(isConfirm) {
				$scope.confirmdelete = true;
				if (isConfirm) {
					$scope.checkAllItems = false;
					for (var j = 0; j < $scope.trashSelected.length; j++) {
						var obj = {
							id : $scope.trashSelected[j]
						};
						//
						DefaultDocs.permanentDel(obj, function(data) {
								//splice
								docsCtrlVariables.trashDoc(data.requestObject._id);
								var multiDelLog = {};
								multiDelLog.activitytype=activity_type.DELETE;
								multiDelLog.location=data.requestObject.s3_path;
	 						multiDelLog.what = data.requestObject;			
	 						multiDelLog.status = true;
							 ActivitylogService.LogActivity(multiDelLog);
								// DefaultDocs.show({
								// 	id : currentDoc._id
								// }, function(response) {
								// 	docsCtrlVariables.setDocumentInfo(response);
								// 	$scope.documentList = response;
								// 	$scope.list = $scope.documentInfo;
								// 	//sweetAlert.swal("Deleted!", "delete done.", "success");
								
								// });
						}, function(err) {
							/*swal(err.data);*/
						sweetAlert.swal("Error",err.data,"error");
							angular.forEach(documentList, function(document, key) {
								document.check = 'false';
							});
						});
					}
	$scope.trashSelected = [];
	// deleting normal files 
						for (var j = 0; j < $scope.cbSelected.length; j++) {
						var obj = {
							id : $scope.cbSelected[j]
						};
						//
						DefaultDocs.delete(obj, function(data) {
								var multiDelLog={};
								multiDelLog.activitytype="Trashed";
	 						multiDelLog.what = data.requestObject._id;
	 						multiDelLog.location=data.requestObject.s3_path;
	 						//multiDelLog.what = data.requestObject;			
	 						multiDelLog.status = true;
							 ActivitylogService.LogActivity(multiDelLog)
								//splice
								//docsCtrlVariables.spliceDoc(data.requestObject._id);
								// DefaultDocs.show({
								// 	id : currentDoc._id
								// }, function(response) {
								// 	docsCtrlVariables.setDocumentInfo(response);
								// 	$scope.documentList = response;
								// 	$scope.list = $scope.documentInfo;
								// 	//sweetAlert.swal("Deleted!", "delete done.", "success");
								// });
						}, function(err) {
							/*swal(err.data);*/
						sweetAlert.swal("Error",err.data,"error");
							angular.forEach(documentList, function(document, key) {
								document.check = 'false';
							});
						});
					}
					sweetAlert.swal("Deleted!", "delete done.", "success");
					$scope.cbSelected = [];
				} else {
					$scope.cbSelected = [];
					$scope.trashSelected = [];
					angular.forEach(documentList, function(document, key) {
						document.check = 'false';
					});
					sweetAlert.swal("Cancelled", "Your files/folder is safe :)", "error" );
					$scope.checkAllItems=false;
				}
			});
		} else {
			sweetAlert.swal("Cancelled", "You have not selected any folder/file", "error");

		}	
	}
	else{
		var currentDoc=docsCtrlVariables.getCurrentDocument();
		var documentList=docsCtrlVariables.getDocumentInfo();
		// console.log(currentDoc,documentList);
		$scope.trashDelete();
		};
	};

$scope.checkForWhoLocked = function(document) {
		if ($rootScope.loggedIn == document.isLocked.user_id || document.isLocked.user_id == null) {
			document.matchLockWithLocker = true;
		} else {
			User.show({
				"user_id" : document.isLocked.user_id
			}, function(response) {
				document.matchLockWithLocker = false;
				document.isLockedtoName = response.name;
			})
		}
	};

$scope.trashDelete=function(){
			var currentDoc=docsCtrlVariables.getCurrentDocument();
		var documentList=docsCtrlVariables.getDocumentInfo();
		// console.log($scope.cbSelected,currentDoc,documentList);

		if ($scope.cbSelected.length != 0) {
			// console.log($scope.lockDocName,$scope.cbSelected);
			
				// if($scope.lockDocName.isLocked.user_id==$rootScope.loggedIn._id || !$scope.lockDocName.isLocked.status){

						sweetAlert.swal({
				title : "Are you sure?",
				text : "Your files will be moved to trash!!",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#1ABC9C",
				confirmButtonText : "Delete",
				cancelButtonText : "Cancel",
				closeOnConfirm : true,
				closeOnCancel : true
			}, function(isConfirm) {

				$scope.confirmdelete = true;
				if (isConfirm) {
					$scope.checkAllItems = false;
					for (var j = 0; j < $scope.cbSelected.length; j++) {
						var obj = {
							id : $scope.cbSelected[j]
						};
						//

						DefaultDocs.delete(obj, function(data) {
								var multiDelLog={};
								multiDelLog.activitytype="Trashed";
	 						multiDelLog.what = data.requestObject._id;
	 						multiDelLog.location=data.requestObject.s3_path;
	 						//multiDelLog.what = data.requestObject;			
	 						multiDelLog.status = true;
							 ActivitylogService.LogActivity(multiDelLog)
								//splice
								docsCtrlVariables.spliceDoc(data.requestObject._id);
								// DefaultDocs.show({
								// 	id : currentDoc._id
								// }, function(response) {
								// 	docsCtrlVariables.setDocumentInfo(response);
								// 	$scope.documentList = response;
								// 	$scope.list = $scope.documentInfo;
								// 	//sweetAlert.swal("Deleted!", "delete done.", "success");
								// });
							
							

						}, function(err) {
							
							/*swal(err.data);*/
						sweetAlert.swal("Error",err.data,"error");
							angular.forEach(documentList, function(document, key) {
								// console.log(document);
								document.check = 'false';
							});
						});
					}
					sweetAlert.swal("Deleted!", "delete done.", "success");

	$scope.cbSelected = [];
				} else {
					$scope.cbSelected = [];
					angular.forEach(documentList, function(document, key) {
						// console.log(document);
						document.check = 'false';
					});
					sweetAlert.swal("Cancelled", "Your files/folder is safe :)", "error");
					$scope.checkAllItems=false;
				}
			});
							

		} else {
			sweetAlert.swal("Cancelled", "You have not selected any folder/file", "error");
		}
};

/*
functionName:deleteFolder
*/
$scope.deleteFolder=function(document){
//	// //console.log(document,document._id);
	$scope.cbSelected = [];
		$scope.cbSelected.push(document._id);
	//	// //console.log($scope.cbSelected);
		$scope.trashDelete();

};


/*
functionName: removePolicy
*/
$scope.removePolicy=function(doc){
	var current = docsCtrlVariables.getCurrentDocument();
	ShareFctry.delete_policy({
					"doc_id" : current._id,
					"policyid" : doc
				}, function(res) {
					var index = $rootScope.currentAccess.indexOf(doc);
					$rootScope.currentAccess.splice(index, 1);

				})
}
/*
function Name; createFolderRow
will set a variable true to show one row 
*/
$scope.createFolderRow=function(){
	$scope.showAddRow=true;
}
/*
funcrionName: addFolder
@Param : folder name 
return : error if folder already exists
*/

$scope.addFolder=function(newFoldername){
	blockUI.start();
	$scope.disableAddFolder=true;
	$scope.sortByWhat = ['type', 'created'];

	//$scope.currentDocForAdd=angular.copy($scope.currentDocument);
	 		 
	 		 var flags= docsCtrlVariables.addfolder(newFoldername)
	 		  $scope.disableAddFolder=flags.disableAddFolder;
	 		  $scope.showAddRow=flags.showAddRow;
	 		  $scope.folderName=flags.folderName;
	 		  
// console.log($scope.showNoFiles);
};


/*
functio to close the add folder row
*/
$scope.closeAddFolder=function(){
	$scope.showAddRow=false;
}

/*
Function name upload
@param: file names
*/
$scope.uploadFiles=function(files)//
{
	// console.log(files);
	 // blockUI.start();
	$scope.showNoFiles=false;
	if($scope.versionuploading ==true)
	{
		$scope.hiderow=files;
		$scope.uplaodwhatFile=$scope.hiderow;
	}
	else
	{
		$scope.showUploadRow=files;	
		$scope.uplaodwhatFile=$scope.showUploadRow;
	}
		docsCtrlVariables.uploadFiles($scope.uplaodwhatFile,$scope.showNoFiles,$scope.versionuploading);
		 // blockUI.stop();
		 // console.log($scope.showNoFiles);
	};
// console.log($scope.showNoFiles);


/*
Can he remove contrib
*/
$scope.canUserRemoveContrib=false;


/*
check if image is locked
*/
$scope.checkLockImage=function(doc){

	if(!doc.trash)
		{
	if(doc.isLocked.user_id==$rootScope.loggedIn._id || !doc.isLocked.status)
		return true;
	else
		return false;		
	}
	else 
	return false;
};

$scope.supplyDocumentUrl=function(doc){

	if(!doc.trash && doc.type=='file')
	{
		
			if(doc.isLocked.user_id==$rootScope.loggedIn._id || !doc.isLocked.status){
				////////console.log(doc);
				return doc.url;
			}	
		}
	};
	$scope.imageUrl=function(doc,version){
		// var file_id = {
		// 			id : doc._id
		// 		  };
		// 		if(version){
		// 			file_id.version=version;
		// 			}
		// 		DefaultDocs.viewFile(file_id, function(result) {
		// 			doc.url=result.url;
		// 		})
	};


/*
FunctionName:BreadClick
@params: bread object
will splice everything infront of it and 
*/
$scope.breadClick=function(fObject)
{
	blockUI.start();
	$scope.checkForPermission(fObject,function(){
	
	console.log(fObject,"breadcrumbs");
	$scope.checkAllItems=false;
	
	blockUI.message('Opening' + " " + fObject.documentName);
	$scope.cbSelected=[];
$scope.trashSelected=[];	
	if(fObject.root==true && fObject.documentName=="My Documents"){
		$scope.allowUpload=true;
		$scope.allowCreateFolder=true;
		$scope.allowDelete=true;
	}
	else{
		if(fObject.trash==true){
			$scope.allowUpload=false
			$scope.allowCreateFolder=false;
			
		}
		else{
		$scope.allowUpload=fObject.allowUpload;
		$scope.allowCreateFolder=fObject.allowAddFolders;
			
		}
	
	
}	$scope.showversion=false;	
	docsCtrlVariables.breadClick(fObject);
})
};

// $scope.getshowInvite=function(){
//// 	////console.log(docsCtrlVariables.getCurrentDocument().showInvite);
// 	return docsCtrlVariables.getCurrentDocument().showInvite;
// }

/*
*function to open timeline modal
*/
$scope.setTimeline=function(doc){
	timelineService.setTimeline(doc);
}


/*
*returns the present version of the file
*/
$scope.getVersion=function(doc){
	return(versionService.getVersion(doc));	
}

$scope.versionarray=[];
/*
*returns the present version of the file
*/
$scope.openVersion=function(doc){
	// console.log(doc);
	$scope.versionarray=[];
	var count=0;
	doc.version.forEach(function(val){
		// console.log(val);
		var verobj={
			version:val.number,
			versionId:val.versionId,
			created_at:val.created_at,
			documentName:doc.documentName,
			versionflag:true,
			url:doc.url,
			created_by:val.added_by,
			createdBy:val.added_by,
			s3_path:doc.s3_path,
			parent:doc.parent,
			description:doc.description,
			comments:doc.comments,
			isLocked:doc.isLocked,
			allowDownload:doc.allowDownload,
			thumbnail_version:val.thumbnail_version,
			_id:doc._id,
			document:doc
		};
		// console.log(doc.version,verobj);
		count++;
		if(count<doc.version.length){
		$scope.versionarray.push(verobj);	
		}
		
	})
	$scope.versionfilename=doc.documentName;
	$scope.showversion=true;
}

/*
*Function to make particular version as current version
*/
$scope.makeCurrentVersion=function(versionobj){
	var allowVersion=false;
if(versionobj.isLocked.status==false)
{
	allowVersion=true;
}
else if(versionobj.isLocked.user_id==$rootScope.loggedIn._id)
{
allowVersion=true;
}
else{
allowVersion=false;	
}
////console.log(versionobj.isLocked.user_id==$rootScope.loggedIn._id,allowVersion)
if(allowVersion){


	var tempobj={
		version:versionobj.version,
		id:versionobj._id
	}
		DefaultDocs.changeversion(tempobj,function(data){
				var viewLog = {};
						viewLog.activitytype = activity_type.CHANGEVERSION;
						viewLog.what = versionobj.document._id;
						viewLog.location = versionobj.document.parent;
						viewLog.status = true;
						ActivitylogService.LogActivity(viewLog);

		$scope.showversion=false;
		//$state.go("dashboard", {}, { reload: true });
		$scope.checkForPermission(docsCtrlVariables.getCurrentDocument(),function(){
			$scope.folderClick(docsCtrlVariables.getCurrentDocument());
		});
		 
		// Take it to the listing page
	},function(err){
		console.log(err);
		
	});
	}
	else{
				notify({ message: 'Document is locked '+ versionobj.isLockedtoName, classes: 'alert-danger', templateUrl: $scope.homerTemplate});

	}
};
/*
function name:checkIfImage
@param:checkIfImage(document)
*/
$scope.checkIfImage=function(document){
	if((/\.(gif|jpg|jpeg|tiff|png|ico)$/i).test(document.documentName))
		return true;
	else 
		return false;
}

/*
*function to get thumbnail url of version list
*/
$scope.getThumbnailVersion=function(doc){
	versionService.getThumbnailVersion(doc);
}


/*
*function : To get thumbnail url
*/
$scope.getThumbnail=function(doc){
	versionService.getThumbnail(doc);

}
/*
*function Add new version
*params:Doc object
*/
$scope.addNewVersion=function(file,doc){
var allowVersion=false;
if(doc.isLocked.status==false)
{
	allowVersion=true;
}
else if(doc.isLocked.user_id==$rootScope.loggedIn._id)
{
allowVersion=true;
}
else{
allowVersion=false;	
}
if(allowVersion){
if(doc.documentName==file[0].name){
	doc.versionuploadflag=true;
	$scope.versionuploading=true;
	$scope.uploadFiles(file);
}
else{
	notify({ message: 'A new Version can be uploaded only if it has same name', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
}
}
else{
		notify({ message: 'Document is locked '+ doc.isLockedtoName, classes: 'alert-danger', templateUrl: $scope.homerTemplate});

}
};
// properties
	$scope.openProperties=function(document){
		blockUI.start();
		docsCtrlVariables.setpropertyDoc(document);
		$scope.propertyDoc=document;
		var modalInstance = $uibModal.open({
            templateUrl: 'views/modal/modal_property.html',
            controller: propertyCtrl
          
                 });
	};

/*
functionName:openInviteModal
Calls the invite modal
*/
	$scope.openInviteModal=function(){
		blockUI.start();
		$rootScope.inviteforDocument=$scope.currentDocument;
		var modalInstance = $uibModal.open({
	            templateUrl: 'views/modal/modal_invite.html',
	            controller: inviteCtrl
	                 });
	};

	$scope.openModalhelp=function(){
		//blockUI.start();
				var modalInstance = $uibModal.open({
	            templateUrl: 'views/modal/modal-help.html',
	            controller: 'HelpCtrl'
	                 });
	}
/*
The invite Controller
This will open a modal
*/
function inviteCtrl ($scope,Groups,$uibModalInstance,Auth,$rootScope,policyFactory,ShareFctry,docsCtrlVariables,$moment,blockUI) {
	blockUI.stop();
	/*
	errorMessages
	*/
$scope.errorMessages=function(text){
	notify({ message: text, classes: 'alert-danger', templateUrl: $scope.homerTemplate});
};
$scope.grp=[];
$scope.G_list=[];
$scope.arrayOfgrp=[];
$scope.currentDocu=[];

  //list of alphabets
   $scope.alphabet=["ALL","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
     $scope.ok = function () {
        $uibModalInstance.close();
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.getCurrentUser = Auth.getCurrentUser;
$scope.getCurrentUser().$promise.then(function() {
		$rootScope.loggedIn = $scope.getCurrentUser();
	});

    $scope.getUsers = User.query({'count':'undefined','limit':'undefined'},function(users){
   	 users.$promise.then(function(){
      $scope.users = users;
  			});
	});
    // To get list of Groups
	Groups.getAllGroups({'count':'undefined','limit':'undefined'},function(data){
		$scope.G_list=data;
		Groups.getGroups({"id":docsCtrlVariables.getCurrentDocument()._id},function(groups){
			$scope.assignedGroups=groups;
			angular.forEach($scope.G_list,function(value){
				angular.forEach($scope.assignedGroups,function(val){
					if(val._id==value._id){
						value.ischeck=true;
						angular.forEach(val.groupinvite,function(v){
							if(v.document.indexOf(docsCtrlVariables.getCurrentDocument()._id)>-1){
								value.assignedPolicy=v.policy.policy_name;
							}
						})
					}
				})
			})

		})
	});


	// To capitalize the first letter and display(first letter)
	$scope.firstToUpperCase = function(strng) {
		var First_cap=strng.substr(0, 1).toUpperCase() + strng.substr(1);
			return First_cap;
	};

	$scope.Capital_frst = function(firstLtr) {
		if (firstLtr == undefined)
			return;
		var res = firstLtr.toUpperCase();
		var res1 = res.slice(0,1);
		return res1;
	};

//To Invite a group to contributor list 
	$scope.selectGroup= function(group,policy,index){
		//console.log("start of select group fun");
	var currentDoc=docsCtrlVariables.getCurrentDocument();
		$scope.arrayOfgrp=[currentDoc._id];
		var obj={
				id:group._id,
				policy:policy,
				document:$scope.arrayOfgrp
				};
		 if(group.ischeck==true){
		 	angular.forEach($scope.roles,function(role){
				if(role._id==policy){
					group.assignedPolicy=role.policy_name;
				}
			})		 	
			 Groups.AddGroup(obj,function(res){
			 	Groups.getGroups({"id":currentDoc._id},function(data){
			 		//console.log(data);
					$rootScope.allinvitegroups=data;
					angular.forEach($rootScope.allinvitegroups,function(value){
						angular.forEach(value.groupinvite,function(val){
							if(val.document.indexOf(currentDoc._id)>-1){
								value.policy=val.policy.policy_name;
							}
						})
					})
				});
				var grpAddLog={};
				grpAddLog.activitytype=activity_type.ADD;
	 			grpAddLog.what = res._id;			
	 			grpAddLog.status = true;
				ActivitylogService.LogActivity(grpAddLog);
			})
	    	} else{
	    		group.assignedPolicy="";
			angular.forEach($rootScope.allinvitegroups,function(value){		
					if(value._id==obj.id){
						angular.forEach(value.groupinvite,function(val){
							if(val.document.indexOf(currentDoc._id)>-1){
									obj.policy=val.policy._id;
							}
						})
					}
	    		});
		    Groups.RemoveGroupDoc(obj,function(res){
		    	Groups.getGroups({"id":currentDoc._id},function(data){
					$rootScope.allinvitegroups=data;
					angular.forEach($rootScope.allinvitegroups,function(value){
						angular.forEach(value.groupinvite,function(val){
							if(val.document.indexOf(currentDoc._id)>-1){
								value.policy=val.policy.policy_name;
								
							}
						})
					})
				});
		//var grpDelLog={};
	    //grpDelLog.activitytype=activity_type.ADD;
		//grpDelLog.location=data.requestObject.s3_path;
	 	//grpDelLog.what = data.requestObject;			
	 	//grpDelLog.status = true;
		//ActivitylogService.LogActivity(grpDelLog);
				})
	    	}	
	};
/*
show capital instead of image
*/
$scope.checkIfImageExists=function(user){
	docsCtrlVariables.checkForImage(user,user.avatar,function(){
		$scope.$apply();
	});
};
// To capitalize the first letter and display(first letter)
	$scope.Capital_frst=function(grpname){
		return GroupService.First_UpperCase(grpname);
	};
    /*
    function to search based on selection
    */
    $scope.searchReq=function(alpha){
            	if(alpha=='ALL')
            		$scope.letter=""
            	else
            		$scope.letter=alpha;
    	};
   	/*
		To get the roles
   	*/
  $scope.getRoles = policyFactory.query(function(policy) {
    $scope.roles = policy;
  	$scope.setPolicy();
  });

  /*init function for select*/
	$scope.setPolicy=function(){
		if($scope.roles != undefined){
			if($scope.roles.length > 0){
				$scope.policy = $scope.roles[0]._id;	
			}
		}
	};
  /*
	Function to get theuser and policy to be attached
  */
  $scope.changePolicy=function(user,role){
  	blockUI.start();
  	 blockUI.message("Processing..")
  	var role_id;
  	angular.forEach($scope.roles,function(value,key){
  		if(String(role)==String(value._id)){
  			role_id=value._id;
  		}
  	})
  		
  	var currentDoc=docsCtrlVariables.getCurrentDocument()
  	  	if(user.check==true){
  		if(role){
  		/*call query to add the policy to user*/
  		 var obj={
        		user_id:user._id,
       			policy_id:role,
        		document_id: currentDoc._id    
         };
         var objForRepeat={
         	/*user_id.avatar:user.avatar,
         	policy_id.policy_name:role.policy_name,
         	user_id.name:user.name */
				user_id:{},
				policy_id:{}
	         }
	    objForRepeat.user_id._id = user._id;    
        objForRepeat.user_id.name=user.name;
        objForRepeat.user_id.avatar=user.avatar;
        angular.forEach($scope.roles,function(roles,key){
        	if(roles._id==role)
        	objForRepeat.policy_id.policy_name=roles.policy_name;
        })
        /*
        add contributors
        */
        ShareFctry.save(obj,function(data){
              //push into access list here
              docsCtrlVariables.setCurrentDocument(currentDoc)
              user.assignedRole=objForRepeat.policy_id.policy_name;
              $rootScope.currentAccess.push(objForRepeat)
         
            blockUI.stop();
             // $rootScope.defaultdoc.access_list.push(createObjs)
              //$rootScope.showEmptyContribList=false;
            
          });
  		}
  		else{

  			   blockUI.stop();
  			notify({ message: "Please choose a policy to invite", classes: 'alert-danger', templateUrl: $scope.homerTemplate});
  	  	}
  	}
  	else
  	{
 		/*
 		construct the object
 		*/
 		var doc={}
 		angular.forEach($scope.roles,function(value,key){
  		if(user.assignedRole==value.policy_name){
  			role_id=value._id;
  		}
  	})
 		angular.forEach($rootScope.currentAccess,function(list,key){
						if(list.user_id._id==user._id){
							doc=list;
						}
					})
 			doc.policy_id._id=role_id;
 			ShareFctry.delete_policy({
					"doc_id" : currentDoc._id,
					"policyid" : doc
				}, function(res) {
					var spliceKey
					angular.forEach($rootScope.currentAccess,function(list,key){
						if(String(list.user_id._id)==String(user._id)){
							spliceKey=key;
						}
					})
					user.assignedRole=""
					var index = $rootScope.currentAccess.indexOf(spliceKey);
					$rootScope.currentAccess.splice(spliceKey, 1);
					  
					   blockUI.stop();
				}) 		
  	}
  };
/*
function to check if user is added in the contrib list
*/
$scope.checkIfContributor=function(user){
	angular.forEach($rootScope.currentAccess,function(access_list,key){
		if(access_list.user_id._id==user._id){
			user.check=true;
			user.assignedRole=access_list.policy_id.policy_name;
		}
	})
};

	$scope.sendTime=function(date){
		var DateFormat = $moment(date).fromNow();
		return DateFormat;
	};

}

  })
.directive('ngEnter', function(){
    return function(scope, element, attrs) {
        element.bind("keydown", function(e) {
            if(e.which === 13) {
                scope.$apply(function(){
                    scope.$eval(attrs.ngEnter, {'e': e});
                });
                e.preventDefault();
            }
        });
    };
})
.filter('startsWithLetter',function(){
    return function (items, letter) {
    	if(items){
    		var filtered = [];
        var letterMatch = new RegExp(letter, 'i');
        for (var i = 0; i < items.length; i++) {
            var item = items[i];
            if (letterMatch.test(item.name.substring(0, 1))) {
                filtered.push(item);
            }
        }
        return filtered;
    	}
       };
})
.filter('startsWithLetterN',function(){
    return function (items, letter) {
    	if(items){
    		var filtered = [];
        var letterMatch = new RegExp(letter, 'i');
        for (var i = 0; i < items.length; i++) {
            var item = items[i];
            if (letterMatch.test(item.Name.substring(0, 1))) {
                filtered.push(item);
            }
        }
        return filtered;
    	}
       };





});
