angular
    .module('homer')
    .controller('userSelectCtrl',
    function($scope,$http,$filter,$uibModalInstance,
    	UserService,notify,
    	docsCtrlVariables,GroupService,
    	$moment,$rootScope,Groups){
    		
	$scope.homerTemplate = 'views/notification/notify.html';
	$scope.UserList=[];
	$scope.getuserslist=[];
	GroupService.deleteList();
	
		// to get users
	UserService.getUsers(function(data){
		$scope.tempuser=data;
		$scope.getuserslist=angular.copy($scope.tempuser);
		 angular.forEach($scope.tempuser,function(value,key){
		 	angular.forEach($rootScope.users_list,function(val,k){
		 		if(String(value._id)==String(val._id)){
		 		 	var ind=$scope.getuserslist.map(function(e) { return e._id; }).indexOf(value._id)
		 		 		$scope.getuserslist.splice(ind,1);
		 		 	}
		 		 });
		 })
	});	
	
	// if image exists
	$scope.imageExists=function(users){
		// console.log(users);
		docsCtrlVariables.checkForImage(users,users.avatar );
	};
	//to make first letter as capital
	$scope.Capital_frst=function(grpname){
		return GroupService.First_UpperCase(grpname);
	};	
	//time in moment.js
    $scope.sendTime=function(date){
		var DateFormat = $moment(date).fromNow();
		return DateFormat;
	};
	//select multiple check to delete
	$scope.All_user = function(select ,filtered){
		// console.log(select ,filtered,$scope.UserList);
		GroupService.UserRemoveAll(select,filtered,$scope.UserList);
	};
	//select indivisual check to delete
	$scope.selectuser=function(users,index,isSelect){
		if(!isSelect)
			$scope.selected=false;
		GroupService.addOrRemoveUser(users,index,$scope.UserList);
	};
	//add users to the group
	$scope.saveuser=function(){
		var groupList=GroupService.getList();
				angular.forEach($scope.getuserslist,function(value){
					angular.forEach(groupList,function(val){
						// console.log(value._id,val);
							if(String(value._id)==val){
								// console.log("users getting pushed",value.name);
								$rootScope.users_list.push(value);
								value.isChecked=false;
							}
					})
				})
				GroupService.deleteList();
		var obj={
				id:$rootScope.currentGroup._id,
				user:groupList
				};
			if(groupList.length>=1){
				Groups.addUser(obj,function(res){
					notify({ message: 'Users added to the group', classes: 'alert-success', templateUrl: $scope.homerTemplate});
				})
				$scope.UserList=[];
				$uibModalInstance.close();
			}else{
				notify({ message: 'Please select an user ', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
			}
	};
	
	$scope.cancel=function(){
		$scope.UserList=[]
		$uibModalInstance.close();
	}
	
	
	
});