angular.module('homer')
.controller('commentCntrl' , function($scope,notify,GlobalService,ActivitylogService,$rootScope,docsCtrlVariables,DefaultDocs,$moment,notify){
	// console.log("inside comments control ");
	$scope.homerTemplate = 'views/notification/notify.html';
	var activity_type = GlobalService.getActivitytype();
console.log( $rootScope.documentHeadingId , $rootScope.newDoccom);


var IndivisualDoc=$rootScope.documentHeadingId;   
$scope.fileDec=IndivisualDoc.description;
$scope.commentInfo=IndivisualDoc.createdBy;
$scope.Comment=IndivisualDoc.comments;
$scope.description="";
console.log($scope.Comment);


/*for autosize our text area */
    $.fn.enlargeTextArea = function() {
        return this.each(function() {
            var el = $(this);
            var elH = el.outerHeight();
            el.css({
                overflow: "hidden"
            });

            function manageTextarea() {
                el.css({
                    height: elH,
                    overflow: "hidden"
                });
                var nH = el.get(0).scrollHeight;
                nH = nH > elH ? nH : elH;
                el.css({
                    height: nH,
                    overflow: "hidden"
                })
            }

            el.bind("keydown", function() {
                manageTextarea(this);
            }).trigger("keydown");

        })
    };

    $(function() {
        $("textarea").not("#not").enlargeTextArea()
    });



	/*comments submit*/

	//file open opertion 
    var comments=[];
    $scope.commentFlag=false;
    $scope.replyFlag=false;

   $scope.submitData=function(data){
          if(data==undefined || data==null || data.length==0){
           notify({ message: 'Please enter the comment', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
          }else{
              if(IndivisualDoc.type=="folder"){
                    var currentDoc=$rootScope.documentHeadingId;
                      console.log(currentDoc,currentDoc.version);
                    var obj = {
                         id :$rootScope.documentHeadingId._id,
                         text:data
                        };
      
                    DefaultDocs.addComment(obj,function(res){
                     console.log(res);
                    $scope.Comment=res.comments;
                    var commentLog = {};
                      commentLog.activitytype = activity_type.COMMENT;
                      commentLog.what = res._id;
                      commentLog.location = res.parent;
                      commentLog.status = true;
                      ActivitylogService.LogActivity(commentLog);
                        });
                    $scope.description="";
                     // console.log(data);
              }else{
                        var currentDoc=$rootScope.documentHeadingId;
                        console.log(currentDoc,currentDoc.version);
                       var obj = {
                          id :$rootScope.documentHeadingId._id,
                          text:data,
                          versionId:$rootScope.commentVersion.versionId,
                          versionObjectId:$rootScope.commentVersion._id
                          };
      
                      DefaultDocs.addComment(obj,function(res){
                      console.log(res);
                      $rootScope.documentHeadingId=res;
                      $rootScope.newDoccom=res;
                      docsCtrlVariables.addToDocumentInfo(res);
                      $scope.Comment=res.comments;
                      console.log($scope.Comment);
                      var commentLog = {};
            commentLog.activitytype = activity_type.COMMENT;
            commentLog.what = res._id;
            commentLog.location = res.parent;
            commentLog.status = true;
            ActivitylogService.LogActivity(commentLog);

              console.log(commentLog);

                           });
                      console.log($rootScope.documentHeadingId ,$rootScope.newDoccom);
                      $scope.description="";
                      
                    };

              };
   };



/*deletecomments*/

$scope.deleteComment=function(data2){
    var currentDoc=$rootScope.documentHeadingId;
    // console.log("delete",data2);
    var obj3={
        id:$rootScope.documentHeadingId._id,
        comment:data2._id
       }

    DefaultDocs.deleteComment(obj3,function(data){
        // console.log(data);
        var ind=$scope.Comment.map(function(e) { return e._id; }).indexOf(data2._id);
        $scope.Comment.splice(ind,1);
        // docsCtrlVariables.spliceDoc(data.requestObject._id);
          })
    };

 $scope.imageExists = function(image, users) {
  // console.log(image,users);
        var img = new Image();
        img.onload = function() {
         users.avatarflag = true;
         $scope.$apply();
     }; 
     img.onerror = function() {
        users.avatarflag=false;
        $scope.$apply();
    };
    img.src = image;
  };


$scope.Capital_frst = function(firstLtr) {
    if (firstLtr == undefined)
        return;
    var res = firstLtr.toUpperCase();
    var res1 = res.slice(0, 1);
    return res1;
};


$scope.checkIfImageExists=function(user,image){
        // console.log(user,image);
    // user.name=user.name;
    docsCtrlVariables.checkForImage(user,image);
};
$scope.modifiedDate = function(date){
  // console.log(date);
  var locdate1 = $moment(date).fromNow();
  // console.log(locdate1);
  return locdate1;
};


});