 angular.module('homer')

    .controller('GuestInviteCtrl', function ($scope,$uibModalInstance,$state,$http,$rootScope,DefaultDocs,notify,$location) {
    	//console.log("guestInviteCtrl");
        var homerTemplate=  'views/notification/notify.html';
        var host = $location.host();
        $scope.emailList=[];
        $scope.add=function(name){
            $scope.emailList.push(name);
            console.log($scope.emailList);
            $scope.name="";
        }
        $scope.delete=function(index){
            //console.log($scope.emailList,index);
            $scope.emailList.splice(index,1);
           // console.log($scope.emailList,index);
        }
    	$scope.share=function(){
            //console.log("name",name);
            var reqobj={
                "email":$scope.emailList,
                "file_id":$rootScope.guestdoc._id,
                "url":"https://"+host.split('.')[0]+".2docstore.com/#/common/guest",
            };
            if($rootScope.guestVersion){
                reqobj.versionId=$rootScope.guestVersion
            }
            console.log("req obj",reqobj);
            DefaultDocs.inviteGuest(reqobj,function(res){
                console.log(res);
                if(res.type!="error"){
                    notify({ message: 'Success- File has been shared through email', classes: 'alert-success', templateUrl: homerTemplate});
                    $uibModalInstance.dismiss('cancel');
                }
                else{
                    notify({ message: res.message, classes: 'alert-danger', templateUrl: homerTemplate});
                    $uibModalInstance.dismiss('cancel');
                }
            })
        }

        $scope.cancel = function () {
            console.log($uibModalInstance);
            $uibModalInstance.dismiss('cancel');
        };

    });