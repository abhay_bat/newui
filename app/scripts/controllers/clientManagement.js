/**
 * 14th july 2016
 * UserController
 * mamatha and chaithra
 * modified date : 25th oct 2016
 */
angular.module('homer')
.controller('clientManagementCtrl', function ($scope,$state,GlobalService,clientFactory,$moment,
												DocsFcrt,TenantSpace,Auth,docsCtrlVariables,User,
												$rootScope,ActivitylogService,notify,ClientService,
												ErrorService,NoteService,blockUI,Upload,
												url,TenantFactory,sweetAlert) {
$scope.homerTemplate = 'views/notification/notify.html';
	console.log(TenantSpace.size/(1024*1024*1024));
	// var tenantSpaceee = 
	Auth.isAdmin(function(isadmin) {
              console.log("isadmin",isadmin);

               if(!isadmin){
               	  $state.go('common.error_two');
           console.log("not admin");
               }             
            });

	 // webshims.setOptions('waitReady', false);
  // webshims.setOptions('forms-ext', {types: 'date'});
  // webshims.polyfill('forms forms-ext');

//Activitylog
var activity_type = GlobalService.getActivitytype();
	$scope.searchText;
	$scope.BreadCrumbs=[];
	$scope.getclientlist=[];
	$scope.clientCount=0;
	$scope.currentDate = new Date();
	// var multiplesaveFlag=true;
 	//Manage Breadcrumbs
 	var _pgManageClient= "Client Management";
 	var _pgAddClient="Add Client";
 	var _pgClientList="clientManagement";

 	//loged in user
	$scope.getCurrentUser = Auth.getCurrentUser;
	$scope.getCurrentUser().$promise.then(function() {
		$rootScope.loggedIn=$scope.getCurrentUser();
		docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);
		$scope.loggedIn = $scope.getCurrentUser()._id;
	});

 	//bread crumbs
 	$scope.clientManage=function(){
 		$scope.BreadCrumbs.push(_pgManageClient);
 	};

 	$scope.addClientcrumbs=function(){
 		$scope.BreadCrumbs.push(_pgManageClient,_pgAddClient);
	};
	//click on the bread crum to go to previous page
	$scope.clickOnPage=function(page){
		if(page ==_pgManageClient){
			$state.go(_pgClientList);
		}
	};
 	//To next page Client creation
 	$scope.AddClient = function(){
 		TenantFactory.remainingSize(function(data){
 			console.log(data.size/(1024*1024*1024));
 			if(data.size>(1024*1024*1024)){
 				$state.go('addClient');
 			}else{
 				notify({ message:'Not enough space remaining,please contact Admin' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
 		};

 		});
	};
/*to display the total number of clients */
	  ClientService.getClientsAsync(function(data){
  		$scope.clientLength=data.length;
   		console.log($scope.clientLength,data);
  		});

/*Modified for demo */

	clientFactory.query({'count':$scope.clientCount,'limit':50},function(data){
			// console.log("getclientlist");
			$scope.getclientlist=data;
		});

	$scope.loadclientMore=function(){		
		$scope.clientCount=$scope.clientCount+50;
		// console.log($scope.clientCount);
		clientFactory.query({'count':$scope.clientCount,'limit':50},function(data){
			for(var i=0;i<data.length;i++){
				$scope.getclientlist.push(data[i]);
			};
		});
	};

	$scope.sendTime=function(date){
		var DateFormat = $moment(date).fromNow();
		return DateFormat;
	};

	$scope.findSize = function(client){
       var sizeSpecs = DocsFcrt.getAllsize(client);
       	client.allotedspaceGB = sizeSpecs.allotedspace/(1024*1024*1024);
   	};

    $scope.changeSize = function(size) {
    	// console.log(size);
    	// console.log($scope.slider.value,"total",TenantSpace.size);
		$scope.remainingSize = TenantSpace.size - (size * 1024 * 1024 * 1024);
		// console.log($scope.remainingSize);
	};

	$scope.checkdiableUser=function(data){
       if(data._id==$scope.loggedIn){
          return true;
        }
       else{
          return false;
         }
    };

//If image exists
$scope.checkIfImageExists=function(user){
	docsCtrlVariables.checkForImage(user,user.logo,function(){
		$scope.$apply();
	});
};

$scope.valueToSave=function(value){
$scope.setValue=value;
}
// console.log($scope.getclientlist);
//edit and update option in listing
$scope.updateClients=function(clients){

// TenantFactory.remainingSize(function(data){
// 	console.log(data.size/(1024*1024*1024));

TenantFactory.remainingSize(function(data){
 			console.log(data.size/(1024*1024*1024));
 		});

		$scope.spaceClient=clients.alloted;
	clients.allotedspace=clients.allotedspaceGB*1024*1024*1024;
	console.log(((TenantSpace.size + parseInt($scope.spaceClient))-(clients.allotedspace))/(1024*1024*1024));
	if(clients.allotedspace > 0){
		if(((TenantSpace.size + parseInt($scope.spaceClient))-(clients.allotedspace))>0){
			
			console.log(((TenantSpace.size + parseInt($scope.spaceClient))-(clients.allotedspace))/(1024*1024*1024));
			// if((TenantSpace.size-clients.allotedspace)>0){
				clientFactory.update(clients,function(data){
					TenantFactory.remainingSize(function(dataa){
						console.log(dataa.size/(1024*1024*1024));
					})
					notify({ message:'Client is updated' , classes: 'alert-success', templateUrl: $scope.homerTemplate});
					var updateClientlog = {};
					updateClientlog.activitytype = activity_type.CLIENTUPDATE;
					updateClientlog.what = clients._id;
					updateClientlog.status = true;
					ActivitylogService.LogActivity(updateClientlog);
					$state.go('clientManagement' , {}, { reload: true } );					
				},function(err){ 
					$state.go('clientManagement' , {}, { reload: true });  
		 		});

		 		TenantSpace.size=((TenantSpace.size +parseInt($scope.spaceClient))-(clients.allotedspace));
		}
		else{
			notify({ message:'Not enough space remaining,please contact Admin' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
				 //ClientService.setclientList(function(){ });
				 console.log(TenantSpace.size/(1024*1024*1024),clients.allotedspace/(1024*1024*1024),$scope.setValue);
				 clients.allotedspaceGB=$scope.setValue;
				
		}
	}else{
		// console.log("false");
			notify({ message:'Alloted space should be greater than zero' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
	}
// });	
};

/*to delete client*/
$scope.deleteClient=function(data){
	sweetAlert.swal({
		title : "Warning",
		text : "Users, files and folders created will be moved to admin!!",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#62CB31",
		cancelButtonColor:"#F44336",
		confirmButtonText : "Delete",
		cancelButtonText : "Cancel",
		closeOnConfirm : true,
		closeOnCancel : true
	}, function(isConfirm) {
		if(isConfirm){
			//console.log("delet")
				blockUI.start();
			//console.log("to delete client",data);
			var params={
				client:data._id
			}
			console.log("params",params);
			clientFactory.deleteClient(params,function(res){
				//console.log("res",res);
				angular.forEach($scope.getclientlist,function(value,key){
					if(value==data){
						$scope.getclientlist.splice(key,1);
						sweetAlert.swal("Deleted!", "delete done.", "success");
					var deleteClientlog = {};
					deleteClientlog.activitytype = activity_type.CLIENTDELETE;
					deleteClientlog.what = params.client;
					deleteClientlog.status = true;
					ActivitylogService.LogActivity(deleteClientlog);	
					console.log("deleteclient",deleteClientlog);					
						blockUI.stop();
					}
				})		
			})
		}else{
				console.log("else");
				sweetAlert.swal("Cancelled", "client is safe :)", "error" );
			}
	})
}

//adding Client functions
	var noteClient= "";
	var noteEmail= "";
	var notePhone="";
	var StartDate="";
	var EndDate="";
	var Mobile= "";
	var Email="";
	var Space=""
	var noteFolder="";
	var noteSpace="";
	var notesubscriptionStarts="";
	var notesubscriptionEnds="";
	// var emptyPhoneNumber="";
	var duplicateEmail="";
	var duplicateFolder="";
	var emptyUserName="";
	// var emptyName="";
	// var emptyFolder="";
	// var emptysubscriptionStarts="";
	var emptysubscriptionEnds="";
	var minSpace="";
	var maxSpace="";
	var Client_name="";
	var Client_validname="";
	var Valid_Foldname="";
	$scope.sliderSpace=/^[0-9]+$/i;
	$scope.NamePtrn=/^[a-zA-Z1-9 _]{3,25}$/;
	$scope.EmailPtrn = /^[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})/;
	$scope.phoneNumbr = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{3,7}$/;
	$scope.fldrName=/^[\w. ]+$/;

	NoteService.getUsermanagementNotes({module:'Note',code:'N_005'},function(data){
		 noteClient=data;
	});
	NoteService.getUsermanagementNotes({module:'Note',code:'N_007'},function(data){
			 noteEmail=data;
	});
	NoteService.getUsermanagementNotes({module:'Note',code:'N_006'},function(data){
		 notePhone=data;
	});
	NoteService.getUsermanagementNotes({module:'Note',code:'N_008'},function(data){
		 noteFolder=data;
	});
	NoteService.getUsermanagementNotes({module:'Note',code:'N_018'},function(data){
		 notesubscriptionStarts=data;
	});
	NoteService.getUsermanagementNotes({module:'Note',code:'N_009'},function(data){
		 notesubscriptionEnds=data;
	});
	NoteService.getUsermanagementNotes({module:'Note',code:'N_017'},function(data){
		 noteSpace=data;
	});
	ErrorService.getUsermanagementErrors({module:'User',code:'U_008'},function(data){
		 Client_name=data;
	});
	ErrorService.getUsermanagementErrors({module:'User',code:'U_006'},function(data){
		 Client_validname=data;
	});
	
	ErrorService.getUsermanagementErrors({module:'User',code:'U_004'},function(data){
		 Mobile=data;
	});
	ErrorService.getUsermanagementErrors({module:'User',code:'U_007'},function(data){
		Email=data;
	});
	ErrorService.getUsermanagementErrors({module:'Client',code:'C_002'},function(data){
		Valid_Foldname=data;
	});
	ErrorService.getUsermanagementErrors({module:'Client',code:'C_001'},function(data){
		Space=data;
		// console.log(Space);
	});
	ErrorService.getUsermanagementErrors({module:'Client',code:'C_003'},function(data){
		minSpace=data;
	});
	ErrorService.getUsermanagementErrors({module:'Client',code:'C_005'},function(data){
		maxSpace=data;
	});
	$scope.getClient_name=function(){
		return Client_name;
	};
	$scope.getClient_validname=function(){
		return Client_validname;
	};
	$scope.getminSpace=function(){
		return minSpace;
	};
	$scope.getmaxSpace=function(){
		return maxSpace;
	};
	$scope.getnoteSpace=function(){
		return noteSpace;
	};
	$scope.getnotesubscriptionStarts=function(){
		return notesubscriptionStarts;
	};
	$scope.getnotesubscriptionEnds=function(){
		return notesubscriptionEnds;
	};
	$scope.getnoteFolder=function(){
		return noteFolder;
	};
	$scope.getnoteClient=function(){
		return noteClient;
	};
	$scope.getnoteEmail=function(){
		return noteEmail;
	};
	$scope.getnotePhone=function(){
		return notePhone;
	};
	$scope.getMobile=function(){
		return Mobile;
	};
	$scope.getEmail=function(){
		return Email;
	};
	$scope.getValid_Foldname=function(){
		return Valid_Foldname;
	};
	// $scope.duplicate_folder = function(){
 //    duplicateFolder="";
 //  	};
	$scope.getSpace=function(){
		return Space;
	};

// comparing start and end date
    $scope.checkErr = function(startDate,endDate){
    // console.log(startDate,endDate);
    	var curDate = new Date();
    	curDate.setHours(0,0,0,0);
        $scope.errMessage = '';
        if(new Date(startDate) >= new Date(endDate)){
        	console.log(new Date(startDate) > new Date(endDate));
        		ErrorService.getUsermanagementErrors({module:'User',code:'U_009'},function(data){
					StartDate=data;
				});
        } else{
        	StartDate="";
        }
        if(new Date(startDate) < curDate){
        	console.log(new Date(startDate) < curDate);
        	    ErrorService.getUsermanagementErrors({module:'User',code:'U_010'},function(data){
					 EndDate=data;
				});
        } else{
        	EndDate="";
        }
    };

	$scope.getStartDate=function(){
		// console.log("get start date",StartDate);
		$scope.sdate=true;
		return StartDate;
	};
	$scope.getEndDate=function(){
		// console.log("get enddate",EndDate);
		return EndDate;
	};

	//changesizee function
	$scope.sizeChange=function(){
		// console.log($scope.slider.value,"total",TenantSpace.size,TenantSpace.size/(1024 * 1024 * 1024));
		$scope.remainingSize = TenantSpace.size - ($scope.slider.value * 1024 * 1024 * 1024);
		// console.log($scope.remainingSize);
		$scope.remainSizee=TenantSpace.size/(1024 * 1024 * 1024);

		// console.log($scope.remainSizee);
	};

	//slider for space allotment
$scope.slider = {
    value: "",
    options: {
     showSelectionBar: true,
    getPointerColor: function(value) {
            if (value >= 1)
            return '#2AE02A';
        },
    getSelectionBarColor: function(value) {
          if (value >= 1)
          return '#2AE02A';
        }, 
    translate: function(value) {
      return value + ' GB';
    },
      floor: 0,
      onChange:$scope.sizeChange,
      ceil:(Math.floor(TenantSpace.size/(1024 * 1024 * 1024))),
      step: 1,
      minLimit: 1,
      maxLimit: $scope.remainingSize
    }
};

	//For validation
	$scope.isValidClient=function(client,callback){		
			//duplicate email
		ClientService.duplicate_email1(client.email,function(flag){
			// console.log(flag);
			if(flag){
				// console.log(flag);
				ErrorService.getUsermanagementErrors({module:'User',code:'U_017'},function(data){
					duplicateEmail=data;
		 			});
		 		callback(false);
			}else {
				//duplicate folder
					ClientService.duplicate_Folder(client.foldername,function(flag1){
						// console.log(flag1);
						if(flag1){
							ErrorService.getUsermanagementErrors({module:'Client',code:'C_008'},function(data){
								duplicateFolder=data;
					 		});
					 		callback(false);
						}
						else callback(true);
					});				
			}
		})
	};
 	$scope.getduplicateEmail=function(){
		return duplicateEmail;
	};
	$scope.getduplicateFolder=function(){
		return duplicateFolder;
	};
	$scope.makeEmptyEmail=function(){
	   duplicateEmail="";
	  };
	  $scope.makeEmptyfolder=function(){
	   duplicateFolder="";
	  };

//To upload client logo
$scope.uploadFiles = function(obj, cb,res){
		Upload.upload({
			url : url + 'api/clients/clientlogo',
			data : obj
		}).then(function(response) {
			blockUI.stop();
			// console.log(response,"response when logo present");
			// var addClientlog = {};
			// addClientlog.activitytype = activity_type.CLIENT;
			// addClientlog.what = response.name;
			// addClientlog.status = true;
			// ActivitylogService.LogActivity(addClientlog);
			// console.log("activitylogggggg");
		 	$scope.client={};
			$scope.slider.value="";
			cb();
		});
	};

$scope.Bflag=false;
	//creating the client
	$scope.ClientAdd=function(client){
		console.log("to add client",client);
		if ($scope.Bflag) {
        return;
    }
    $scope.Bflag=true;
		if ($scope.ClientForm.$valid){
			// $scope.ClientForm.submitted = false;
			// console.log(client);
			$scope.Bflag=true;
			$scope.saveClient(client,function(){
				$scope.Bflag=true;
				ClientService.setclientList(function(){
					$state.go('clientManagement',{},{reload: true});
				})

			});
			// $scope.Bflag=false;
		}else{
			
           // console.log("not valid",client);
            $scope.ClientForm.submitted = true;
            $scope.Bflag=false;
        }
	};

	 // $scope.ClientForm.submitted = false;
		// To add another user
		$scope.Bflag=false;
	 $scope.oneMore=function(client){

	 	if ($scope.Bflag) {
        return;
    }
	 	$scope.Bflag=true;
	 	if ($scope.ClientForm.$valid){
	 		$scope.Bflag=true;
	 		// $scope.ClientForm.submitted = false;
				$scope.saveClient(client,function(){
					ClientService.setclientList(function(){
						$state.go($state.current, {}, {reload: true});
					})
				});
		}else{
		           // console.log("not valid",client);
		            $scope.ClientForm.submitted = true;
		            $scope.Bflag=false;
		        }
	};	

$scope.dateSet=function(data){

		// console.log(date);
	return data;
};


$scope.saveClient=function(client,cb){
	console.log("save client",client,client.subscriptionStarts,client.subscriptionEnds);
	 blockUI.start();
	// var startDatee=$scope.dateSet(client.subscriptionStarts);
	// var endDatee=$scope.dateSet(client.subscriptionEnds);
	$scope.isValidClient(client,function(flag){


		console.log("dates valid",client.subscriptionEnds>client.subscriptionStarts);

		if(client.subscriptionEnds>client.subscriptionStarts){
			
			console.log("dates valid",client.subscriptionEnds>client.subscriptionStarts);
				
				client.allotedspace=$scope.slider.value*1024*1024*1024;
				$scope.remainn=($scope.remainingSize/(1024*1024*1024));
				console.log(TenantSpace.size/(1024*1024*1024));

			if (client.allotedspace >1 && (client.allotedspace <= TenantSpace.size)){	
				if(flag){
					$scope.Bflag=false;

					console.log("flag true",flag);
						clientFactory.save(client,function(res){
							// $scope.Bflag=false;
							// console.log("flag true",res);
							var obj={id:res._id,file:$scope.files};
								client.id=res._id;
								if($scope.files){ 
									// console.log("file present");
									$scope.uploadFiles(obj,cb,res);
									var addClientlog = {};
									addClientlog.activitytype = activity_type.CLIENTADD;
									addClientlog.what = res.name;
									addClientlog.status = true;
									ActivitylogService.LogActivity(addClientlog);
									// console.log(addClientlog,"activitylogggggg");
									// console.log(addClientlog);
								} else{
									blockUI.stop();
									// console.log("else condition");
							 		var addClientlog = {};
									addClientlog.activitytype = activity_type.CLIENTADD;
									addClientlog.what = res.name;
									addClientlog.status = true;
									ActivitylogService.LogActivity(addClientlog);
									// console.log(addClientlog);
							 		$scope.slider.value="";
							 		cb();
							 	}
							 	// console.log("blockui stop");
							
						},function(err){
							$scope.Bflag=false;
							console.log("error",err);
							 blockUI.stop();
						});
						// blockUI.stop();
				}else{
					$scope.Bflag=false;

					console.log("flag false");
					blockUI.stop();
				}

			}else{
				$scope.Bflag=false;
				if(client.allotedspace==0){
					$scope.ClientForm.submitted = true;
					console.log("space check",client.allotedspace,$scope.ClientForm.submitted);
					$scope.getSpace();
					  blockUI.stop();
				}else{
					$scope.Bflag=false;
					console.log("space check");
				$scope.getmaxSpace();
				 blockUI.stop();
				}
				
			}

		}else{
			$scope.Bflag=false;
			console.log("dates not valid");
			$scope.getStartDate();
			  // blockUI.stop();
		};			
	});
};

$scope.clearClient=function(data){
	if(data){
    	data={};
    	$scope.slider.value="";
    		$state.go($state.current, {}, {reload: true});
    	}
    	else{
    		$state.go($state.current, {}, {reload: true});
    	}
    };



    /*load more option*/
	// clientFactory.query({'count':$scope.clientCount,'limit':20},function(data){
	// 	// console.log("getclientlist");
	// 	$scope.getclientlist=data;
	// });
// console.log($scope.getclientlist);


/*Previous code (actual one)*/
// $scope.loadclientMore=function(){
	
// 	$scope.clientCount=$scope.clientCount+20;
// 	// console.log($scope.clientCount);
// 	clientFactory.query({'count':$scope.clientCount,'limit':20},function(data){
// 					for(var i=0;i<data.length;i++){
// 						$scope.getclientlist.push(data[i]);
// 					};
// 				});
// };

});