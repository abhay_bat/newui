angular.module('homer')

  .controller('navigationCtrl', function ($scope,$state,$http,$rootScope,md5,Auth,User,notify,DefaultDocs,$moment,ActivitylogService,GlobalService,docsCtrlVariables,UserService,$rootScope) {
$scope.shared=true;	
var activity_type = GlobalService.getActivitytype();
  	$scope.getCurrentUser = Auth.getCurrentUser;
  	//console.log($scope.getCurrentUser());
	$scope.getCurrentUser().$promise.then(function() {
		$scope.loggedIn = $scope.getCurrentUser();
		// $scope.loggedUser=$scope.getCurrentUser();
		//console.log($scope.loggedIn,$scope.loggedIn.role);
	});
	 var whatDoc=docsCtrlVariables.getWhichDocument();
	$scope.checkIfImageExists=function(user,image){
		//console.log(user);
	if(user){
		docsCtrlVariables.checkForImage(user,image);
	}
	

	//console.log(user)
	}

	$scope.mydocs=true;
	$scope.myDocuments=function(){
		if($rootScope.clientPage)
			return "something"
		if($scope.mydocs)
			return "activeClass"

		else{
		if(whatDoc=="My Documents")
			return "activeClass"
		}
	}	

	$scope.clientDocs=function(){
		
			if($rootScope.clientPage){
			
			$scope.mydocs=false;

			return "activeClass"	
			}
			
		
	}

$scope.admin=false;
	$scope.Admin=function(){
		if($scope.admin)
			// $scope.mydocs=false;
			return "activeClass"
	}

$scope.userManage=false;
$scope.userManagement=function(){
	if($scope.userManage)
		// $scope.mydocs=false;
		return "activeClass"
}

$scope.clientManage=false;
$scope.clientManagement=function(){
	if($scope.clientManage)
		// $scope.mydocs=false;
		return "activeClass"
}

$scope.redirectPage=false;
$scope.redirectPages=function(){
	if($scope.redirectPage)
		return "activeClass"		
}

$scope.report=false;
$scope.Report=function(){
	if($scope.report)
		return "activeClass"
}

	$scope.getUserListing=function(){
		UserService.setUserManagementBC('userListing');

		$state.go('userListing');
	}

	$scope.sharedDocs=function(){
		 var whatDoc=docsCtrlVariables.getWhichDocument();
		 if($scope.loggedIn){
		 			if(!whatDoc && $scope.loggedIn.role=="user")
		{
			return "activeClass";
		}
		 }

			
			if(whatDoc=="shared"){
			$scope.mydocs=false;
			return "activeClass"	
			}

		}
	


	$scope.favDocs=function(){
		if(whatDoc=="fav"){
			
			$scope.mydocs=false;

			return "activeClass"	
			}
	}


	$scope.callAdmin=function(){
		//console.log("call admin");
	};

	/*
	Function name: logout
	Logs out the user
	*/
$scope.logOut=function(){
	var logoutLog={};
	logoutLog.activitytype = "Logged-Out";
	ActivitylogService.AsyncLogActivity(logoutLog, function() {
		User.logout(function(data){
			console.log(data);
			Auth.logout()
		});
		 $state.go("common.login");
	});	
};
	$scope.clickedOnClients=function(){
		$scope.mydocs=false;
		docsCtrlVariables.setWhichDocument("My Documents","clicked");
		$state.go("clientProject", {}, { reload: true });
	};
	
	$scope.clickMyDocs=function(){
	docsCtrlVariables.setWhichDocument("My Documents","clicked");
	$rootScope.clientPage=false;
		$state.go("dashboard", {}, { reload: true });	
	};
	$scope.clickedOnShared=function(){
			docsCtrlVariables.setWhichDocument("shared","clicked");
				$rootScope.clientPage=false;
			$state.go("dashboard", {}, { reload: true });
	};
	$scope.clickedOnFav=function(){
		
			docsCtrlVariables.setWhichDocument("fav","clicked");
				$rootScope.clientPage=false;
			$state.go("dashboard", {}, { reload: true });
	};
	$scope.clickedonAdmin=function(){
		$scope.mydocs=false;
			$rootScope.clientPage=false;
		$scope.admin=true;
	};
	$scope.clickedonUser=function(){
		$scope.mydocs=false;
			$rootScope.clientPage=false;
		$scope.userManage=true;
	};
	$scope.clickedonClient=function(){
		$scope.mydocs=false;
			$rootScope.clientPage=false;
		$scope.clientManage=true;
	};
	$scope.clickedonReport=function(){
		$scope.mydocs=false;
			$rootScope.clientPage=false;
		$scope.report=true;
	};
	$scope.clickedonRedirect=function(){
		$scope.mydocs=false;
			$rootScope.clientPage=false;
		$scope.redirectPage=true;
	};

  });
	


  
  // .directive('loggedUser',function(){
  // 	return{
  // 	template:'role:{{loggedIn.role}}'
  // 	};
  // });
