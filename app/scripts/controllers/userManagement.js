/** 2Docstore
 * page: admin (user management page)
 * dev: mamatha
 * created date : 14th  july 2016
 * modified date : 20th  july 2016
 * Discription : creating the user and listing of the user 
 */
 angular.module('homer')
 .controller('userManagementCtrl', function($scope,$http,$filter,blockUI,$state,
 	$rootScope,Auth,notify,User,UserService,GlobalService,$location,DefaultDocs,
 	ActivitylogService,activityFcrt,ErrorService,NoteService,$window,$moment,
 	docsCtrlVariables,Groups,GroupService,ClientService) {
 	// console.log("inside user management")
 	/* To check logged in user is Admin or client admin*/
 	Auth.isAdmin(function(isadmin){
 		$scope.ClientUserFlag= true;
 		Auth.isClientAdmin(function(ClientAdmin){
 			if(ClientAdmin){
 				$scope.ClientUserFlag= false;
 			}
 			if (!isadmin && !ClientAdmin){
 				$state.go('common.error_two');
 			};
 		});
 	});
 	var activity_type = GlobalService.getActivitytype();
 	$scope.homerTemplate = 'views/notification/notify.html';
 	$scope.getCurrentUser = Auth.getCurrentUser;
 	$scope.EmailPtrn = /^[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})/;
 	$scope.phoneNumbr = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{3,7}$/;

 	/*To get logged in user info*/
 	$scope.getCurrentUser().$promise.then(function() {
 		$rootScope.loggedIn=$scope.getCurrentUser();
 		docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);
 		$scope.loggedIn = $scope.getCurrentUser()._id;
 		$rootScope.loggedUser = $scope.loggedIn;
 	});


  // Disabling loggedin to edit his info in user list
  $scope.checkdiableUser=function(data){
  	if(data._id==$scope.loggedIn){
  		return true;
  	} else{
  		return false;
  	}
  }; 
  /*To get userlist (existing users)*/
	// var userslist=[];
	// UserService.getUsers(function(data){
	// 	userslist=data;
	// });	
          // mamatha here          
          
	// $scope.getUserlists=function(){
	// 	return userslist;
	// };



	/*load more option actual one */
// $scope.userCount=0;
// $scope.getUserlists=User.query({'count':$scope.userCount,'limit':20},function(data){
// // console.log(data);
// });


// var Count=$scope.userCount+20;
// $scope.loaduserMore=function(){
// 	var usetlistCount=Count;
// 	var lists=User.query({'count':usetlistCount,'limit':20},function(data){
	
// 		for(var i=0;i<lists.length;i++){
// 			$scope.getUserlists.push(lists[i]);
// 		};
// 	});
// 	Count=Count+20;
// };


/*demo code */
$scope.userCount=0;
$scope.getUserlists=User.query({'count':$scope.userCount,'limit':100},function(data){
			// console.log(data);
		});

var Count=$scope.userCount+100;
$scope.loaduserMore=function(){
	var usetlistCount=Count;
	var lists=User.query({'count':usetlistCount,'limit':100},function(data){		
		for(var i=0;i<lists.length;i++){
			$scope.getUserlists.push(lists[i]);
		};
	});
	Count=Count+100;
};

/*display note in form (add user)*/
var noteuser= "";
var noteEmail= "";
var notePhone="";
var noteOrg="";
var noteAdmin="";
var noteClientAdmin="";


NoteService.getUsermanagementNotes({module:'Note',code:'N_001'},function(data){
	noteuser=data; /*Password note*/
});
$scope.getnoteuser=function(){
	return noteuser;
};
NoteService.getUsermanagementNotes({module:'Note',code:'N_002'},function(data){
	noteEmail=data; /*Enter minimum 3 alphanumeric characters.*/
});
$scope.getnoteEmail=function(){
	return noteEmail;
};
NoteService.getUsermanagementNotes({module:'Note',code:'N_006'},function(data){
	notePhone=data; /*Enter your valid contact number*/
});
$scope.getnotePhone=function(){
	return notePhone;
};
NoteService.getUsermanagementNotes({module:'Note',code:'N_016'},function(data){
	noteOrg=data; /*User will be mapped to the default organization listed*/
});
$scope.getnoteOrg=function(){
	return noteOrg;
};
NoteService.getUsermanagementNotes({module:'Note',code:'N_003'},function(data){
	noteAdmin=data; /*Administrator will have full access ...*/
});
$scope.getnoteAdmin=function(){
	return noteAdmin;
};
NoteService.getUsermanagementNotes({module:'Note',code:'N_004'},function(data){
	noteClientAdmin=data; /*Client admin has limited access, belongs to the organization*/
});
$scope.getnoteClientAdmin=function(){
	return noteClientAdmin;
};
/*display Error in form (add user)*/
var Mobile= "";
var Email="";
ErrorService.getUsermanagementErrors({module:'User',code:'U_004'},function(data){
	Mobile=data;  /*Must be a valid 9 to 13 digit contact number*/
});
$scope.getMobile=function(){
	return Mobile;
};
ErrorService.getUsermanagementErrors({module:'User',code:'U_007'},function(data){
	Email=data;  /*Enter a valid email address*/
}); 
$scope.getEmail=function(){
	return Email;
};
  // Through error message for the empty fields and disappear err after clicking on the fields
  var emptyPhoneNumber="";
  var emptyUserName="";
  var duplicateEmail="";
  var emptyName="";
  var errOrg="";
  $scope.makeEmptyEmail=function(){
  	emptyUserName="";
  };
  $scope.makeEmptyPhone=function(){
  	emptyPhoneNumber="";
  };
  $scope.duplicate_email = function(){
  	duplicateEmail="";
  };
  $scope.makeEmptyName=function(){
  	emptyName="";
  };
 // open the new page for user creation with clearing old data
 $scope.AddUser = function(){
 	$state.go('adduser');
 };
// breadcrumbs  
$scope.userBreadCrumbs=[];
$scope.userManage=function(){
	$scope.userBreadCrumbs=[];
	var b1= "User Management";
	$scope.userBreadCrumbs.push(b1);
};
// This is init function in add user page
$scope.addGroup1=function(){
		//$scope.whichPage=$state.current.name;
		var b1= "User Management";
		var b2="Add User";
		$scope.userBreadCrumbs.push(b1,b2);
	};
// click on bread to go to previous page
$scope.clickOnPage=function(page){
	if(page =="User Management")
		$state.go("userManagement");
};

// listing logo and set flag onloading and error
$scope.imageExists = function(users){
	docsCtrlVariables.checkForImage(users, users.avatar);
};

ClientService.getClientsAsync(function(data){
	
	console.log(data,"clients");
	$scope.clientOrg=data;

	var host = $location.host();
	var tenantdoc={
		name:host.split('.')[0]
	};
	$scope.hostName=host.split('.')[0];
	$scope.clientOrg.push(tenantdoc);
	console.log($scope.clientOrg);
});


// check for role and setting organization
$scope.checkRole=function(user){
 		//console.log("to set an organization",user);
 		if($scope.getCurrentUser().role=="Client Admin" || $scope.getCurrentUser().role=="user"){
 			user.organization=$scope.getCurrentUser().organization;
 			return false;
 		}
 		if($scope.getCurrentUser().role=="admin" && user.role=="admin"){
 			user.organization=$scope.getCurrentUser().clientId;
 			return false;
 		}  else
 		return true;
 	};



 	/*To set time format as (ie:- few min ago)*/
 	$scope.sendTime=function(date){
 		var DateFormat = $moment(date).fromNow();
 		return DateFormat;
 	};



 	/*to check organization*/
 	$scope.checkOrg=function(data){
 		if(data=="user"){
 			console.log("data",data,$scope.clientOrg);
 			var host = $location.host();
 			$scope.hostName=host.split('.')[0];
 			var tenantdoc={
 				name:host.split('.')[0]
 			};
 			$scope.clientOrg.push(tenantdoc);
 		}else{
 			angular.forEach($scope.clientOrg,function(value,key){

 				if($scope.hostName==value.name){
 					console.log(key)
 					$scope.clientOrg.splice(key,1);
 					console.log($scope.clientOrg);
 				}
 			})
 			
 		}
 		
 	}

//To get document folders(MyDocuments and ClientDocuments) ("clientDocs" will the org for client admin)
DefaultDocs.query(function(data){
		// console.log(data)
		$scope.defaultdocs = data;
		angular.forEach($scope.defaultdocs,function(value,key){
			// console.log(value,key);
			if(value.documentName=="Client Documents"){
		    	// console.log(value.documentName);
		    	$scope.getdefaultFolders = DefaultDocs.show({
		    		id : value._id
		    	},function(response){
		    		var host = $location.host();
		    		var tenantdoc={
		    			documentName:host.split('.')[0]
		    		};
		    		console.log(tenantdoc,response);
		    		$scope.clientDocs=response;
		    		$scope.clientDocs.push(tenantdoc);
		    		console.log($scope.clientDocs);
		    	});
		    }
		});
	});
// To capitalize the first letter and display(first letter)
$scope.Capital_frst=function(grpname){
	return GroupService.First_UpperCase(grpname);
}        
//send verificaiton mail
$scope.sendVerification=function(userObj){
	//$scope.sendVerificationnow=false;
	User.reverify(userObj,function(response){
	//$scope.sendVerificationnow=true;
	notify({ message: 'Email sent successfully', classes: 'alert-success', templateUrl: $scope.homerTemplate});
},function(err){
	// $scope.sendVerificationnow=true;
	notify({ message: 'Problem in sending email', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
}); 
};
//Check switch (toggle function)
$scope.checkSwitch=function(user){
	if(user.status==true){
		user.status=false;
		return; 
	}else{
		user.status=true;
		return;
	}
};

//status and roles( need to move this in global factory)
$scope.userstatus = [
{value: true, text: 'Active'},
{value:false, text: 'Disabled'},
];
 // $scope.userstatus=UserService.getStatus();
 $scope.groups=[
 {_id:1, text:'admin'},
 {_id:2, text:'user'},
 {_id:3, text:'Client Admin'}
 ];

 $scope.groups1=[ 
 {_id:1, text:'user'},
 {_id:2, text:'Client Admin'}
 ];
	//To show status,if set show status else not set
	$scope.showStatus = function(user) {
		var selected = [];
		selected = $filter('filter')($scope.userstatus, {value: user.status});
		return selected.length ? selected[0].text : 'Not set';
	};

  //role, if set show role else not set
  $scope.showGroup = function(user) {
  	var selected = [];
  	var grp = [];
  	if(user.role){
  		if($scope.getCurrentUser().role=='admin'){
  			selected = $filter('filter')($scope.groups, {text: user.role});
  			//console.log(selected,$scope.groups);
  		}
  		if($scope.getCurrentUser().role=='Client Admin'){
  			selected = $filter('filter')($scope.groups1, {text: user.role});
  		}	
  	}
  	return selected.length ? selected[0].text : 'Not set';
  };


  // Check if the email is duplicate
	// $scope.isNotDup=function(email){
	// 	var dup=false;
	// 	  angular.forEach(userslist,function(value,key){
	// 	     if(value.username==email){
	// 	    	dup=true;
	// 	    }else {
	// 	    	//dup=false;
	// 	    }
	// 	  });
	// 	  return dup;
	// };
	

//To check all the empty field
$scope.errorMessage=function(mod,codes){
	ErrorService.getUsermanagementErrors({module:mod,code:codes},function(data){
		if(codes=='U_012'){
			emptyName=data;
		}if(codes=='U_013'){
			emptyUserName=data;
		}if(codes=='U_005'){
			emptyPhoneNumber=data;
		}
	});	
};

	//For validation
	$scope.isValiduser=function(user){
		if(!user.name && !user.username && !user.cell_number){
			$scope.errorMessage('User','U_012');
			$scope.errorMessage('User','U_013');
			$scope.errorMessage('User','U_005');
		}
		if(user.name ==undefined){
			ErrorService.getUsermanagementErrors({module:'User',code:'U_012'},function(data){
				emptyName=data;
			});
			return false;
		}
		if(user.username == undefined){
			ErrorService.getUsermanagementErrors({module:'User',code:'U_007'},function(data){
				emptyUserName=data;
			});
			return false;
		}
		if(UserService.DuplicateUser(user.username,$scope.getUserlists)){  
			blockUI.stop(); 
			ErrorService.getUsermanagementErrors({module:'User',code:'U_017'},function(data){
				duplicateEmail=data;
				$scope.Bflag=false;
			});
			return false;
		}
		if(user.cell_number == undefined){
			ErrorService.getUsermanagementErrors({module:'User',code:'U_005'},function(data){			
				emptyPhoneNumber=data;
			});
			return false;
		}
		if(user.organization == undefined){
			ErrorService.getUsermanagementErrors({module:'User',code:'U_018'},function(data){			
				errOrg=data;
			});
			return false;
		}
		return true;
	};
	$scope.getduplicateEmail=function(){
		$scope.dupEmail=true;
		return duplicateEmail;
		blockUI.stop();
		$scope.Bflag=false;

	};
	$scope.getemptyPhoneNumber=function(){
		return emptyPhoneNumber;
	};
	$scope.getemptyUserName=function(){
		return emptyUserName;
	};
	$scope.getemptyName=function(){
		return emptyName;
	};
	$scope.errorOrg=function(){
		return errOrg;
	};

	$scope.Bflag=false;
//add user(this is the callback function for both save and add another user)
$scope.userAdd=function(user){
	console.log("add one user",user,$scope.clientDocs);
	if ($scope.Bflag) {
		return;
	}
	$scope.Bflag=true;
	if ($scope.UserForm.$valid) {
			// console.log("valid user");
			$scope.Bflag=true;
			if(user.role=="Client Admin"){
				console.log("role client",user.role,$scope.clientDocs);
				user.clientid=user.organization._id;
				user.organization=user.organization.name;
					//user.clientid=user.organization._id;
					console.log(user,user.organization,"if");
					
				}else if(user.role=="user")
				{
					user.organization=user.organization.name;
					console.log(user,user.organization,"else");
				}		
				console.log("request",user);
				$scope.submitUser(user,function(){
					$scope.Bflag=true;
					notify({ message: 'User '+ user.name +' is created', classes: 'alert-success', templateUrl: $scope.homerTemplate});
					$state.go('userManagement',{},{reload: true});
				});

			}else {
		          // console.log("not valid");
		          $scope.UserForm.submitted = true;
		          $scope.Bflag=false;
		      }
		      
		  };

		  $scope.Bflag=false;
// To add another user
$scope.oneMore=function(user){
	if ($scope.Bflag) {
		return;
	}
	$scope.Bflag=true;
		// console.log("one more user");
		if ($scope.UserForm.$valid) {
			console.log(user.organization);
			$scope.Bflag=true;
			if(user.role=="Client Admin"){
				console.log("role client",user.role,$scope.clientDocs);
				user.clientid=user.organization._id;
				user.organization=user.organization.documentName;
				console.log(user,user.organization,"if");
				
			}else if(user.role=="user")
			{
				user.organization=user.organization.documentName;
				console.log(user,user.organization,"else");
			}
			$scope.submitUser(user,function(){
				notify({ message: 'User '+ user.name +' is created', classes: 'alert-success', templateUrl: $scope.homerTemplate});
				$state.go($state.current, {}, {reload: true});
			});
		}else{
			 // console.log("not valid");
			 $scope.UserForm.submitted = true;
			 $scope.Bflag=false;
			};	 	
		};

//Creating the user
$scope.submitUser=function(user,cb){
	console.log("user ",user);
	blockUI.start();
	$scope.Bflag=true;
		// console.log("user submit",user);
		if($scope.isValiduser(user)){
			console.log("valid user",user);
	 	 //save the added user
	 	 $scope.Bflag=true;
	 	 User.save(user,function(response){
	 	 	blockUI.stop();
	 	 	var addlog = {};
	 	 	addlog.activitytype = activity_type.USERADD;
	 	 	addlog.what = user.name;
	 	 	addlog.status = true;
	 	 	ActivitylogService.LogActivity(addlog);
	 	 	cb();
	 	 },function(err){ 
	 	 	blockUI.stop();
	 	 	$scope.Bflag=false;
	 	 	cb();
	 	 });
	 	}                        	
	 };
	 
//Updating the user
$scope.updateUser = function(user){
	console.log(user,"update");
	User.update(user,function(users){
		console.log("res",users);
		if(users.type=="error"){
			console.log("error",users.message);
			notify({ message: users.message, classes: 'alert-danger', templateUrl: $scope.homerTemplate});
			$state.go('userManagement'); 
		}else{
			var updatelog = {};
			updatelog.activitytype = activity_type.USERUPDATE;
			updatelog.what = user._id;
			updatelog.status = true;
			ActivitylogService.LogActivity(updatelog);
			$state.go('userManagement');
			notify({ message: 'User '+ user.name +' Updated', classes: 'alert-success', templateUrl: $scope.homerTemplate});
		}
		
	},function(err){ 
		console.log("error",err);
		$state.go('userManagement');      
	});
};

//cancle for add user
$scope.cancel = function(user) {
    	// $scope.user.name= "";
    	// $scope.user.last_name = "";
    	// $scope.user.username = "";
    	// $scope.user.cell_number = "";
    	// $scope.user.organization = "";
    	$state.go($state.current, {}, {reload: true});
    };   
}).filter('filterOrg',function($location){
	//console.log("filterOrg");
	return function (items, letter) {
		var host = $location.host();
		var filtered = [];
		for (var i = 0; i < items.length; i++) {
			console.log("items",items);
			if(letter=="Client Admin" && items[i].documentName!=host.split('.')[0]){
				filtered.push(items[i]);
   					//console.log(filtered);
   				}
   				else if(letter=="admin" || letter=="user"){
   					filtered.push(items[i]);
   					//console.log(filtered);
   				}
   			}
   			return filtered;   			
   		};
   	}); 
