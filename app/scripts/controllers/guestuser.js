 angular.module('homer')

    .controller('GuestCtrl', function ($scope,$state,$http,$rootScope,docsCtrlVariables,DefaultDocs) {
    	console.log("GuestCtrl",$state.params.id);
    	
        DefaultDocs.getGuestDocs({"token":$state.params.id},function(res){
            $rootScope.documentHeadingId=res.fileobj;
            $rootScope.documentHeading=res.fileobj.documentName;
            $rootScope.isguestuser=true;
            console.log("res",res);
            if($scope.checkIfImage(res.fileobj)){

                $rootScope.isImage=true;
                $rootScope.imageUrl=res.url;
                console.log($rootScope.imageUrl);
            }
            else
            {
               if(((/\.(pdf)$/i).test(res.fileobj.documentName))){
                console.log("pdf");
                $rootScope.viewurl=res.url;
                console.log(res.url)
                }else{
                    console.log("not pdf and images",res.url);
                    $rootScope.viewurl="https://view.officeapps.live.com/op/view.aspx?src="+res.url;
                    console.log($rootScope.viewurl);
                };
            }
                   
            docsCtrlVariables.guestopenFile();

        })

        $scope.checkIfImage=function(document){
            if((/\.(gif|jpg|jpeg|tiff|png|ico)$/i).test(document.documentName))
                return true;
            else 
                return false;
        }

        
    	

    });