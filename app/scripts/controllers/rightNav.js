'use strict';
angular.module('homer').controller('rightNavCtrl', function($scope,
	$state, url, $window, $http, $q, $rootScope,$location, 
	activityFcrt,$moment,GlobalService,User,DefaultDocs,$filter,Auth,docsCtrlVariables) {
	// console.log("inside rightnav");

	// $scope.activities = new Activities();
	// $scope.activities.emptyCount();
	// $scope.activities.howMuch=0;
var howMuch=0;
//var howMuch=10;
$scope.activitylog=[];
// console.log($scope.activitylog.length,$scope.activitylog);
	$scope.activityLog=activityFcrt.query({'count':howMuch,'limit':10},function(data)
	 {
	 	  console.log(data);
		// console.log(howMuch);
  	});
	$scope.loadMore=function(){
		console.log("loading",$scope.activityLog,$scope.activityLog.length);
		var Count=10;
		var items=activityFcrt.query({'count':Count,'limit':40},function(data){
			// console.log(data,Count,data.length,items);
		for(var i=0; i<items.length; i++){
			$scope.activityLog.push(items[i]);
		// console.log($scope.activityLog,$scope.activityLog.length);
		}

		});
	};
$scope.reportPage=function(){
	// console.log("reports");
	$state.go("report");
};

var host = $location.host();
User.loginusers(function(users){
	// console.log(users);
	$scope.loggedinusers=users;
});


var ioSocket = io(url, {
	 query: "host="+host.split('.')[0],
      path: '/socket.io-client'
    });

/*
logged IN
*/
$scope.checkIfloggedIn=function(activity){
	if($scope.loggedinusers){
		var ind=$scope.loggedinusers.map(function(e) { return e.userId; }).indexOf(activity.who.user_id);
		//console.log(ind);
		if(ind>-1){
			return "LoggedIn";
		}
		else{
			return "LoggedOut";
		}		
	}
};

ioSocket.on('activitylog:save', function (item,test) {
	//console.log(item,test);
	$scope.activityLog.unshift(item);		
});

$scope.$on('$destroy', function() {
    ioSocket.removeAllListeners('activitylog:save');
  });

// var ioSocket = io('http://motomoney.2docstore.com:9000', {
// 	query: "host="+host.split('.')[0],
//       path: '/socket.io-client'
//     });


ioSocket.on('thing:save', function (item) {
	// console.log(item);
	$scope.loggedinusers.push(item);
				if(!$scope.$$phase) {
			  //$digest or $apply
			  $scope.$apply();
			}
});

ioSocket.on('thing:remove', function (item) {
	//console.log(item);
	var ind=$scope.loggedinusers.map(function(e) { return e.userId; }).indexOf(item.userId);
	$scope.loggedinusers.splice(ind,1);
				if(!$scope.$$phase) {
			  //$digest or $apply
			  $scope.$apply();
			}
});

// ioSocket.on('thing:remove', function (item) {
// 	console.log(item);

// })	

/*
Query to get the documents listed under tenant
*/
$scope.spaceInfo=function(cb){
	console.log("check");
	Auth.getTenant(function(tenant){
		//console.log("Auth",tenant) ;
	    $rootScope.tenantInfo=tenant;
	    $scope.space=0;
	   // console.log($rootScope.tenantDocs,"$rootScope.tenantDocs")
	    if($rootScope.tenantDocs==undefined){
	    	//console.log("undefined",$rootScope.tenantDocs);
	    	DefaultDocs.query(function(data){
	    		//console.log("data",data);
	    		$rootScope.tenantDocs=data;
	    		 // console.log($rootScope.tenantDocs,"tenantdocs");
				angular.forEach($rootScope.tenantDocs,function(value,key){
					//console.log(value,"value");
					$scope.space=$scope.space+value.size;
					//console.log($scope.space,"nav");
					cb($rootScope.tenantInfo,$scope.space);
				});
	   		 })

	    }else{
	    	console.log("defined",$rootScope.tenantDocs);
	    	angular.forEach($rootScope.tenantDocs,function(value,key){
				//console.log(value,"value");
				$scope.space=$scope.space+value.size;
				//console.log($scope.space,"nav");
				cb($rootScope.tenantInfo,$scope.space);
			});
	    }   
		$scope.allottedspace=$rootScope.tenantInfo.subscription.space;	
	})
}


$scope.giveWidth=function(){
	console.log("giveWidthe");
	$scope.spaceInfo(function(tenentInfo,spacevalue){
		//console.log(tenentInfo,spacevalue);
		$scope.width=($scope.space/$rootScope.tenantInfo.subscription.space)*100;
		console.log("%width",$scope.width);
	})
			
};

/*
*/
$scope.checkIfImageExists=function(user,image){
	// console.log(user,image);
	user.name=user.user_name;
	docsCtrlVariables.checkForImage(user,image);
};		
/*
Query to get the activity
*/
	var Activity = GlobalService.getActivitytype();
	console.log("activity type",Activity);

$scope.setActivity=function(){
		/*
Query to get the activity
*/

	$scope.activitylog = activityFcrt.query({
		
	}, 
	function(data) {
		$scope.ActivityLog=data;
		$scope.activitylogger = data;
		$scope.activityLength = $scope.activitylogger.length;
	});

};
		/*
		location in tooltip
		*/
		$scope.locationString=function(location)
		{

			if (location.location) {
				var str = location.location;
				var doc = str.indexOf("/");
				var n1 = str.substring(0, doc);
				var n2 = str.substring(doc);
				var n3 = n2.substring(1);
				var str1 = n3;
				var doc1 = str1.indexOf("/");
				var n4 = str1.substring(0, doc1);
				var n5 = str1.substring(doc1);
				var n6 = n5.substring(1);
				location.short=n6;
			}
		};
	/**
	*function name : get time, to give time in terms of "mins ago,days ago etc"
	@param: activity.when
	return : time in terms of moments ago
	**/
	$scope.getTime=function(date){
		var DateFormat = $moment(date).fromNow();
		return DateFormat;
	};

		/**
	*function name : getActivityType"
	@param: activity obj
	return : Activity in a senetence
	**/
	$scope.getActivitytype = function(activityMsg) {
		//console.log("activityMsg",activityMsg);
		if (activityMsg) {
			if (activityMsg.location) {
				var str = activityMsg.location;
				var doc = str.indexOf("/");
				var n1 = str.substring(0, doc);
				var n2 = str.substring(doc);
				var n3 = n2.substring(1);
				var str1 = n3;
				var doc1 = str1.indexOf("/");
				var n4 = str1.substring(0, doc1);
				var n5 = str1.substring(doc1);
				var n6 = n5.substring(1);
			}

			// To capitalize the first letter
			var firstToUpperCase = function(strng) {
				return strng.substr(0, 1).toUpperCase() + strng.substr(1);
			}
			if (activityMsg.who) {
				var strng = activityMsg.who.user_name;
				var User_name = firstToUpperCase(strng);
			} else {
				var User_name = "";
			}
			var text;
			switch (activityMsg.activitytype) {
			case Activity.LOGIN:
			case Activity.LOGOUT:
				text =  Activity.HAS + activityMsg.activitytype.bold();
				break;
			case Activity.SHARE:
			case Activity.DOWNLOAD:
			case Activity.VIEW:
			case Activity.CHANGEVERSION:
			text = activityMsg.activitytype.bold() + " " + activityMsg.what.docu_type + " " + activityMsg.what.document_name + Activity.FROM + n6;
				break;
			case Activity.UPLOAD:
			text = activityMsg.activitytype.bold() + " " + activityMsg.what.document_name.link("http://motomoneydemo.2docstore.com") + Activity.TO + n6;
				break;
			case Activity.CREATE:
				text =  activityMsg.activitytype.bold() + " " + activityMsg.what.docu_type + " " + activityMsg.what.document_name + Activity.IN + n6;
				break;
			case Activity.DELETE:
			case Activity.TRASH:
			case Activity.RESTORE:
				text = Activity.HAS + activityMsg.activitytype.bold() + " " + activityMsg.what.document_name + Activity.IN + n6;
				break;
			case Activity.ADD:
			case Activity.UPDATE:
				text =  Activity.HAS + activityMsg.activitytype.bold() + " " + activityMsg.newUser;
				break;			
			case Activity.REMIND:
				text =  Activity.HAS + activityMsg.activitytype.bold() + " " + activityMsg.what + Activity.IN + n6;
				break;
			case Activity.USERADD:
			case Activity.CLIENTUPDATE:
			case Activity.CLIENTADD:
			case Activity.USERUPDATE:
			//case Activity.CLIENTDELETE:
			text = Activity.HAS + activityMsg.activitytype.bold() + " " + activityMsg.what.document_name ; 
			 	break;
			 	case Activity.CLIENTDELETE:
			text = Activity.HAS + activityMsg.activitytype.bold() + " " + activityMsg.what ; 
			 	break;
			case Activity.LOCK:
			case Activity.UNLOCK:
			case Activity.FAV:
			case Activity.UNFAV:
				text =  Activity.HAS + activityMsg.activitytype.bold() + " " + activityMsg.what.docu_type + Activity.IN + n6;
				break;
			case Activity.PASSWORD:			
			case Activity.PROFILE:
			case Activity.PROFILEUPDATE:
				text = Activity.HAS + activityMsg.activitytype.bold() ; 
				break;
			case Activity.GROUPADD:
			case Activity.GROUPUPDATE:			
				text = Activity.HAS + activityMsg.activitytype.bold() +" " + activityMsg.what.document_name;
				break;
			case Activity.GROUPDELETE:	
				text = Activity.HAS + activityMsg.activitytype.bold() +" " + activityMsg.what;
				break;
			case Activity.COMMENT:
				text = Activity.HAS + activityMsg.activitytype.bold() + " " + activityMsg.what.docu_type + " " + activityMsg.what.document_name + Activity.IN + n6;
				break;
			case Activity.DESCRIPTION:
			 	text = Activity.HAS + activityMsg.activitytype.bold() + Activity.TO + activityMsg.what.document_name + Activity.IN + n6;
          		break; 
			default:
				text = "default text"; 
			}
			return text;
		}
	};

}).filter('bytes', function() {
	return function(bytes, precision) {
		if (bytes == 0)
			return '0KB';
		if (isNaN(parseFloat(bytes)) || !isFinite(bytes))
			return '-';
		if ( typeof precision === 'undefined')
			precision = 1;
		var units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB'],
		    number = Math.floor(Math.log(bytes) / Math.log(1024));
		return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + ' ' + units[number];
	};
}).filter('limitHtml', function() {
        return function(text, limit) {

            var changedString = String(text).replace(/<[^>]+>/gm, '');
            var length = changedString.length;

            return length > limit ? changedString.substr(0, limit - 1) : changedString;
        };
    });
