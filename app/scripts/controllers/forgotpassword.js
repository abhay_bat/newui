  angular.module('homer')

    .controller('forgotpasswordCntrl', function ($scope,$state,$http,$rootScope,md5,Auth,User,notify,DefaultDocs,userManagement) {
       $scope.homerTemplate = 'views/notification/notify.html';


       // console.log("inside forgotpass");
       Auth.getTenant(function(tenant){
      $scope.tenant=tenant;
       $rootScope.tenantInfo=$scope.tenant;
        // console.log($rootScope.tenantInfo)
      });


  /* Verifying for password match */
  var userObj = {};
       $scope.forgotpass=function(user){

        if($scope.username && $scope.username2){
          if($scope.username == $scope.username2){
                // console.log(" forgotpass ", $scope.username , $scope.username2,user);  
          var userList={ };
          userList.username=$scope.username;
              
       userManagement.forgotPassword(userList,function(response){
      var ename=$scope.username;

    //sending the mail to user 
  notify({ message: 'Email Sent - We have sent the Verification Mail to'+" " +ename, classes: 'alert-success', templateUrl: $scope.homerTemplate});
  // console.log("success");
  $state.go('common.error_two');
  $scope.username="";
   $scope.username2="";
   
  },function(err){
   // console.log(err);
   notify({ message: 'Warning - No such User!! Please check the Email', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
   user="";
  });


  }
  else{
    // console.log("error");
    notify({message:'Email do not match',classes:'alert-danger',templateUrl:$scope.homerTemplate});
    $scope.username="";
   $scope.username2="";
  }
}

       };
       
      });