/**
 *
 * modalCtrl
 *
 */

 angular
 .module('homer')
 .controller('modalCtrl', modalCtrl)

 function modalCtrl($scope,$uibModal) {

/*getting the url from dashboard controller to here

*/

$scope.open = function () {

    var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modal_example.html',
        controller: ModalInstanceCtrl,
    });
};

$scope.open1 = function () {
    var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modal_example1.html',
        controller: ModalInstanceCtrl
    });
};

$scope.open3 = function (size) {
    var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modal_example3.html',
        size: size,
        controller: ModalInstanceCtrl,
    });
};

$scope.open2 = function () {
    var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modal_example2.html',
        controller: ModalInstanceCtrl,
        windowClass: "hmodal-info"
    });
};

$scope.open4 = function () {
    var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modal_example2.html',
        controller: ModalInstanceCtrl,
        windowClass: "hmodal-warning"
    });
};

$scope.open5 = function () {
    var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modal_example2.html',
        controller: ModalInstanceCtrl,
        windowClass: "hmodal-success"
    });
};

$scope.open6 = function () {
    var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modal_example2.html',
        controller: ModalInstanceCtrl,
        windowClass: "hmodal-danger"
    });
};
};

function ModalInstanceCtrl ($scope, $uibModalInstance,$rootScope,docsCtrlVariables,$moment,
    Auth,DefaultDocs,notify,blockUI,GlobalService,ActivitylogService,Groups,timelineService,
    activityFcrt,sweetAlert,$uibModalInstance,$uibModal) {
    blockUI.stop();
    console.log("hi");
     $scope.homerTemplate = 'views/notification/notify.html';
    var activity_type = GlobalService.getActivitytype();
    console.log($rootScope.documentDetails);
    $rootScope.allowMoadlOpen=true;

    $scope.check = function () {
        $uibModalInstance.dismiss('cancel');
    };

$scope.showcomments=false;
/*chaithra here*/
$scope.navigate=function(data){
    if(data==true){
        $scope.showcomments=false;

    }else{
        $scope.showcomments=true;

    }
    
}

$scope.checkComments=function(data){
    if(data==false){
        return "col-md-12 col-sm-12 col-xs-12 col-lg-12"
    
        
    }else{
        return "col-md-9 col-sm-9 col-xs-9 col-lg-9"
    }
}

    // $scope.embedUrl="https://docs.google.com/viewer?embedded=true&url=" + $rootScope.viewurl;
    // $scope.embedUrl="https://docs.google.com/viewer?embedded=true&url=" + $rootScope.viewurl;
    

var IndivisualDoc=$rootScope.documentHeadingId; 
$scope.fileDec=IndivisualDoc.description;
$scope.commentInfo=IndivisualDoc.createdBy;
$scope.Comment=IndivisualDoc.comments;

/*To get current user*/
if(!$rootScope.isguestuser)
{
    $scope.getCurrentUser = Auth.getCurrentUser;
$scope.getCurrentUser().$promise.then(function() {
        $rootScope.loggedIn = $scope.getCurrentUser();
    });
}

    

$scope.checkifImageExists=function(user,image){
        console.log(user,image);
    user.name=user.name;
    docsCtrlVariables.checkForImage(user,image);
};  

$scope.ok = function () {
    $uibModalInstance.close();
};

$scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
};

/*
function to set favourite
*/
$scope.setFavourite=function(document)
{
    console.log("to set fav",$rootScope.documentDetails,document);
    if($rootScope.documentDetails.documentName==document){    
        
        if($rootScope.documentDetails.isLocked.status == true && $rootScope.documentDetails.isLocked.user_id!=$rootScope.loggedIn._id){

            notify({ message: 'Warning - Document Locked ' ,classes: 'alert-danger', templateUrl: $scope.homerTemplate});
        }
        else{
            if ($rootScope.documentDetails == undefined || $rootScope.documentDetails.isFavourite == undefined || $rootScope.documentDetails.isFavourite.status == undefined) {
                $rootScope.documentDetails.isFavourite.status = true;
                return;
            }

            if ($rootScope.documentDetails.isFavourite.status) {
                //modify doc to include islocked as true
                var obj = {
                    user_id : $scope.getCurrentUser()._id,
                    status : false
                };
                $rootScope.documentDetails.isFavourite.status = false;
                DefaultDocs.favroite($rootScope.documentDetails, function(data) {
                    $rootScope.documentDetails.isFavourite.status = false;
                    var unfavLog = {};
                    unfavLog.activitytype = activity_type.UNFAV;
                    unfavLog.what = $rootScope.documentDetails;
                    unfavLog.location = $rootScope.documentDetails.s3_path;
                    unfavLog.status = true;
                    ActivitylogService.LogActivity(unfavLog);
                    $rootScope.documentDetails.favImage = false
                })

                
            } else {
                //modify doc to include islocked as false
                var obj = {
                    user_id : $scope.getCurrentUser()._id,
                    status : true
                };
                $rootScope.documentDetails.isFavourite.status = true;
                DefaultDocs.favroite($rootScope.documentDetails, function(res) {
                    
                    $rootScope.documentDetails.isFavourite.status = true;
                    //from activity service
                    var favLog = {};
                    favLog.activitytype = activity_type.FAV;
                    favLog.what = $rootScope.documentDetails;
                    favLog.location = $rootScope.documentDetails.s3_path;
                    favLog.status = true;
                    ActivitylogService.LogActivity(favLog);
                    $rootScope.documentDetails.favImage = true;
                    $rootScope.documentDetails.favName = $scope.loggedIn.name
                });
                
            }
        }
    }else{
        console.log("diff docs");
    }
    
}

/*to set lock/unlock*/

$scope.findlock = function(document) {
        console.log($rootScope.documentDetails.isLocked.status , $rootScope.documentDetails.isLocked.user_id,$rootScope.loggedIn._id)
    if($rootScope.documentDetails.documentName==document){  
        //check if the document object is valid. if not unlock and return.
        // $scope.checkForPermission($rootScope.documentDetails,function(){

                if ($rootScope.documentDetails == undefined || $rootScope.documentDetails.isLocked == undefined || $rootScope.documentDetails.isLocked.status == undefined) {
                    
            /*      document.isLocked.status=true;
                    document.isLocked.user_id=$rootScope.loggedIn._id;*/
                    $rootScope.documentDetails.isLocked.lockImage=true
                    return;
                }

                if ($rootScope.documentDetails.allowLock){
                    if ($rootScope.documentDetails.isLocked.status && $rootScope.documentDetails.isLocked.user_id==$rootScope.loggedIn._id ) {
                        //modify doc to include islocked as true
                        // var obj={
                        //  status:false
                        // };
                        $rootScope.documentDetails.isLocked.status = false;
                    
                        
                        DefaultDocs.locked($rootScope.documentDetails, function(data) {
                            $rootScope.documentDetails.lockImage=false;
                                var lockLog = {};
                            lockLog.activitytype = activity_type.UNLOCK;
                            lockLog.what = $rootScope.documentDetails._id;
                            lockLog.location = $rootScope.documentDetails.s3_path;
                            lockLog.status = true;
                            ActivitylogService.LogActivity(lockLog);
                            
                        });
                    } else {
                        //modify doc to include islocked as false
                        // var obj={
                        //  user_id:$scope.getCurrentUser()._id,
                        //  status:true
                        // };

                        $rootScope.documentDetails.isLocked.status = true;
                        DefaultDocs.locked($rootScope.documentDetails, function(res) {
                            $rootScope.documentDetails.isLocked.status=true;
                            $rootScope.documentDetails.isLocked.user_id=$rootScope.loggedIn._id;
                            var unlockLog = {};
                            unlockLog.activitytype = activity_type.LOCK;
                            unlockLog.what = $rootScope.documentDetails._id;
                            unlockLog.location = $rootScope.documentDetails.s3_path;
                            unlockLog.status = true;
                            ActivitylogService.LogActivity(unlockLog);
                            $rootScope.documentDetails.lockImage=true;
                            $rootScope.documentDetails.lockedName =$scope.loggedIn.name
                        },function(err){
                        notify({ message: 'Warning - Document Locked by ' +$rootScope.documentDetails.lockedName ,classes: 'alert-danger', templateUrl: $scope.homerTemplate});
                        });
                    }
                } else {
                    var lockFile = $rootScope.documentDetails.documentName;
                    //swal("You don't have persmission to lock " + lockFile);
                    sweetAlert.swal("Error","You don't have persmission to lock/unlock " + lockFile,"error");
                }
            // })
    }else{
        console.log("diff doc");
        };
}


/*to give permission*/    
    $scope.givePermission = function(value) {
        if (value == "deny")
            return false;
        else
            return true;
    };
/*
*function to open timeline modal
*/
$scope.setTimeline=function(doc){
  if($rootScope.documentDetails.documentName==doc){      
    timelineService.setTimeline($rootScope.documentDetails);
    }else{
        console.log("diff docs");
    }
}   

/*
for hiding contribs
*/
var whichDoc=docsCtrlVariables.getWhichDocument();

$scope.checkIfSharedDoc=function(){
    var bread = docsCtrlVariables.getbreadcrumbs();
    if(bread.length==0 && whichDoc=="shared")
        return true
    else
        return false
}

$scope.checkIfFavDoc=function(){
    var bread = docsCtrlVariables.getbreadcrumbs();
    if(bread.length==0 && whichDoc=="fav")
        return true
    else
        return false
}
$scope.checkIfSearchDoc=function(){
var bread = docsCtrlVariables.getbreadcrumbs();
    if(bread.length==0 && whichDoc=="search")
        return true
    else
        return false
}
$scope.checkIfClientDoc=function(){
    var bread = docsCtrlVariables.getbreadcrumbs();
    if(bread.length==1 && whichDoc=="Client Documents")
        return true
    else
        return false
} 
/*
*function Add new version
*params:Doc object
*/
$scope.addNewVersion=function(file,doc){
    if($rootScope.documentDetails.documentName==doc){  
        var allowVersion=false;
        if($rootScope.documentDetails.isLocked.status==false)
        {
            allowVersion=true;
        }
        else if($rootScope.documentDetails.isLocked.user_id==$rootScope.loggedIn._id)
        {
        allowVersion=true;
        }
        else{
        allowVersion=false; 
        }
        if(allowVersion){
        if($rootScope.documentDetails.documentName==file[0].name){
            $rootScope.documentDetails.versionuploadflag=true;
            $scope.versionuploading=true;
            $scope.uploadFiles(file);
        }
        else{
            notify({ message: 'A new Version can be uploaded only if it has same name', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
        }
        }
        else{
                notify({ message: 'Document is locked '+ $rootScope.documentDetails.isLockedtoName, classes: 'alert-danger', templateUrl: $scope.homerTemplate});

        }
    }else{
        console.log("not matched");
    }
};

/*
Function name upload
@param: file names
*/
$scope.uploadFiles=function(files)//
{
    // console.log(files);
     // blockUI.start();
    $scope.showNoFiles=false;
    if($scope.versionuploading ==true)
    {
        $scope.hiderow=files;
        $scope.uplaodwhatFile=$scope.hiderow;
    }
    else
    {
        $scope.showUploadRow=files; 
        $scope.uplaodwhatFile=$scope.showUploadRow;
    }
        docsCtrlVariables.uploadFiles($scope.uplaodwhatFile,$scope.showNoFiles,$scope.versionuploading);
         // blockUI.stop();
         // console.log($scope.showNoFiles);
    };


 /*FunctionName: Download
@Params:docuemnt object
Returns: URL to start download*/
$scope.download=function(document,version){
    //console.log("vers",version);
    if($rootScope.documentDetails.documentName==document){
        // $scope.checkForPermission($rootScope.documentDetails,function(){
                if($rootScope.documentDetails.isLocked.user_id==$rootScope.loggedIn._id || !$rootScope.documentDetails.isLocked.status)
                {

                    if($rootScope.documentDetails.allowDownload){
                    var current = docsCtrlVariables.getCurrentDocument();   
                    var file_id = {
                                id : $rootScope.documentDetails._id
                            };
                            if (version){
                                file_id.version=version;
                            }
                            DefaultDocs.download(file_id, function(result) {
                                location.href = result.url;
                            var downloadLog = {};
                            downloadLog.activitytype = activity_type.DOWNLOAD;
                            downloadLog.what =  $rootScope.documentDetails._id;
                            downloadLog.location = current.parent + '/' + current.documentName;
                            downloadLog.status = true;
                            ActivitylogService.LogActivity(downloadLog);
                            //from activity service
                            $scope.downloadLog = activityFcrt.save(downloadLog, function(data) {
                            });
                            }, function(err) {
                                notify({ message: 'Warning - Please Enter Valid Username or Password', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
                       
                            });
                        }
                        else{
                            notify({ message: 'Error - You do not have permission to download '+$rootScope.documentDetails.documentName , classes: 'alert-danger', templateUrl: $scope.homerTemplate});

                        }
                    }
                    else{
                            notify({ message: 'Error - '+$rootScope.documentDetails.documentName +' is Locked' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
                    }
         // });
     }else{
            console.log("diff docs");
        }
}
/*
functionName:deleteFolder
*/
$scope.deleteFolder=function(document){
    if($rootScope.documentDetails.documentName==document){
//  // //console.log(document,document._id);
    $scope.cbSelected = [];
        $scope.cbSelected.push($rootScope.documentDetails._id);
    //  // //console.log($scope.cbSelected);
        $scope.trashDelete();
    }else{
        console.log("diff docs");
    }
};

$scope.trashDelete=function(){

            var currentDoc=docsCtrlVariables.getCurrentDocument();
        var documentList=docsCtrlVariables.getDocumentInfo();

        // console.log($scope.cbSelected,currentDoc,documentList);


        if ($scope.cbSelected.length != 0) {
            // console.log($scope.lockDocName,$scope.cbSelected);
            
                // if($scope.lockDocName.isLocked.user_id==$rootScope.loggedIn._id || !$scope.lockDocName.isLocked.status){

                        sweetAlert.swal({
                title : "Are you sure?",
                text : "Your files will be moved to trash!!",
                type : "warning",
                showCancelButton : true,
                confirmButtonColor : "#1ABC9C",
                confirmButtonText : "Delete",
                cancelButtonText : "Cancel",
                closeOnConfirm : true,
                closeOnCancel : true
            }, function(isConfirm) {
                $scope.check();
                $scope.confirmdelete = true;
                if (isConfirm) {
                    $scope.checkAllItems = false;
                    for (var j = 0; j < $scope.cbSelected.length; j++) {
                        var obj = {
                            id : $scope.cbSelected[j]
                        };
                        //

                        DefaultDocs.delete(obj, function(data) {
                                var multiDelLog={};
                                multiDelLog.activitytype="Trashed";
                            multiDelLog.what = data.requestObject._id;
                            multiDelLog.location=data.requestObject.s3_path;
                            //multiDelLog.what = data.requestObject;            
                            multiDelLog.status = true;
                             ActivitylogService.LogActivity(multiDelLog)
                                //splice
                                docsCtrlVariables.spliceDoc(data.requestObject._id);
                                // DefaultDocs.show({
                                //  id : currentDoc._id
                                // }, function(response) {
                                //  docsCtrlVariables.setDocumentInfo(response);
                                //  $scope.documentList = response;
                                //  $scope.list = $scope.documentInfo;
                                //  //sweetAlert.swal("Deleted!", "delete done.", "success");
                                // });
                            
                            

                        }, function(err) {
                            
                            /*swal(err.data);*/
                        sweetAlert.swal("Error",err.data,"error");
                            angular.forEach(documentList, function(document, key) {
                                // console.log(document);
                                document.check = 'false';
                            });
                        });
                    }
                    sweetAlert.swal("Deleted!", "delete done.", "success");

    $scope.cbSelected = [];
                } else {
                    $scope.cbSelected = [];
                    angular.forEach(documentList, function(document, key) {
                        // console.log(document);
                        document.check = 'false';
                    });
                    sweetAlert.swal("Cancelled", "Your files/folder is safe :)", "error");
                    $scope.checkAllItems=false;
                }
            });
                            

        } else {
            sweetAlert.swal("Cancelled", "You have not selected any folder/file", "error");
        }
};

// properties
    $scope.openProperties=function(document){
        console.log("open properties");
        if($rootScope.documentDetails.documentName==document){
            console.log("true");
            blockUI.start();
            docsCtrlVariables.setpropertyDoc($rootScope.documentDetails);
            $scope.propertyDoc=$rootScope.documentDetails;
            var modalInstance = $uibModal.open({
                templateUrl: 'views/modal/modal_property.html',
                controller: propertyCtrl
              
             });
        }
        
    };

};



//     /*for image viewer*/
//     (function($){
//         $.fn.gdocsViewer = function(options) {
//             console.log(options);
//             var settings = {
//                 width  : '700',
//                 height : '750'
//             };

//             if (options) { 
//                 $.extend(settings, options);
//             }

//             return this.each(function() {
//                 var file = $(this).attr('href');
//             // int SCHEMA = 2, DOMAIN = 3, PORT = 5, PATH = 6, FILE = 8, QUERYSTRING = 9, HASH = 12
//             var extregex = /^((https?|ftp):\/)?\/?([^:\/\s]+)(:([^\/]*))?((\/[\w/-]+)*\/)([\w\-\.]+[^#?\s]+)(\?([^#]*))?(#(.*))?$/i;
//             var ext = file.match(extregex)[8].split(".").pop();
//             if (/^(tiff|pdf|ppt|pps|doc|docx|txt)$/.test(ext)) {
//                 $(this).after(function () {
//                     var id = $(this).attr('id');
//                     var gdvId = (typeof id !== 'undefined' && id !== false) ? id + '-gdocsviewer' : '';
//                     return '<div id="' + gdvId + '" class="gdocsviewer"><iframe src="https://docs.google.com/viewer?embedded=true&url=' + encodeURIComponent(file) + '" width="' + settings.width + '" height="' + settings.height + '" style="border: none;margin : 0 auto; display : block;"></iframe></div>';
//                 })
//             }
//         });
// };
// })( jQuery );

// /*<![CDATA[*/
// $(document).ready(function () {
//     $('a.embed').gdocsViewer({
//         width: 800,
//         height: 570
//     });
//     $('#embedURL').gdocsViewer();
// });
