angular.module('homer')
.controller('redirectpageCtrl', function($scope,$state,TenantFactory,notify,$moment,docsCtrlVariables,Auth,$rootScope,$window){
   $scope.homerTemplate = 'views/notification/notify.html';
   console.log("inside redirectpage controller");
   $scope.getCurrentUser = Auth.getCurrentUser;

   /*To get logged in user info*/
   $scope.getCurrentUser().$promise.then(function() {
      $rootScope.loggedIn=$scope.getCurrentUser();
      docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);
      $scope.loggedIn = $scope.getCurrentUser()._id;
      $rootScope.loggedUser = $scope.loggedIn;
   });

   //breadcrumbs
   $scope.userBreadCrumbs=[];
   $scope.redirectPage=function(){
      $scope.userBreadCrumbs=[];
      var b1= "Applications";
      $scope.userBreadCrumbs.push(b1);
   }

   $scope.NamePtrn=/^[a-zA-Z1-9 _]{3,25}$/;
   $scope.EmailPtrn = /^[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})/;

   $scope.infoFlag=false;
   TenantFactory.applicationList(function(data){
      console.log(data);
      $scope.domains=data;
      for(var i=0;i<data.length;i++){
         // console.log(data[i].application,"data loop");
         $scope.appList=data[i].application;
      }
      // console.log($scope.appList);
   });

   $scope.nameOfUrls=[];
   $scope.addAppflag=false;
   $scope.addApp=function(){
     $scope.addAppflag=true;
     $scope.editAppflag=false;
      // console.log("app add page");

   };

   $scope.editApp=function(data){
    $scope.addAppflag=false;
    for(var i=0;i<$scope.domains.length;i++){
      // console.log($scope.domains[i],"data loop");
      $scope.appdomain=$scope.domains[i];
   }
   $scope.editAppflag=true;
   $scope.editApplication=data;
   // for(var i =0;i<data.redirectUri.length;i++){
   //    console.log(data.redirectUri[i]);
   $scope.Uri=data.redirectUri;
   angular.forEach($scope.Uri,function(link,key){
      $scope.nameOfUrls[key]=link;
   })
   // }
   // $scope.addAppflag=true;
   // console.log($scope.editApplication);
};

$scope.updateApp=function(data){
   console.log(data,"update App",$scope.editApplication, $scope.appdomain);
   var editObj={
      id:$scope.appdomain._id,
      appId:$scope.editApplication._id,
      redirectUri:data
   }
   TenantFactory.updateapplication(editObj,function(res){
      console.log("updateduri result",res.application);
      angular.forEach(res.application,function(val){
         console.log("val",val);
         if(val._id==$scope.editApplication._id){
            // $rootScope.loginObj=val;
            console.log( val,$scope.appdomain);
            console.log(val.redirectUri[val.redirectUri.length-1]);
            var url=val.redirectUri[val.redirectUri.length-1];
             $window.location.href = "http://application.2docstore.com:3000";
         }
      })
         })
};


      $scope.formateDate = function(date){
            var locdate1 = $moment(date).fromNow();
            return locdate1;
         };

         $scope.addInfo=function(data1){
            console.log( $scope.domains);
            if($scope.dataForm.$valid){
            console.log("submit here",data1,"valid",$scope.domains);

                         angular.forEach($scope.domains,function(value){
                        console.log(value);
                        $scope.infoFlag=true;
         // $scope.information=data1;
                     var object={
                        id:value._id,
                        name:data1.name,
                        // emailId:data1.emailId,
                        description:data1.description
                     }
                     console.log(object);
         TenantFactory.createApplication(object,function(res){
            console.log(res,"result here");
            if(res.type=="error"){
               // console.log("error type");
               $scope.infoFlag=false;
               notify({ message:'Domain already exists' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
               // $scope.information
               data1={};
            }else{
               console.log(res.application[res.application.length-1],"no error",$scope.appList);
               $scope.appList=res.application;
               $scope.result=res._id;
               angular.forEach(res,function(key,value){
                  // console.log(key,value);
               })
               $scope.information = (res.application[res.application.length-1]);
               // console.log($scope.information,$scope.result);
               // $state.go("updatedPage");
            }
            
         },function(err){
            // console.log("error");
            notify({ message:'Domain already exists' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
         })

                         })
              
}else{
   // console.log("not valid");
   $scope.dataForm.submitted=true;
}   		
};

$scope.updateInfo=function(data2){
   console.log(data2,"update",$scope.information,$scope.domains);
            angular.forEach($scope.domains,function(value){
               console.log(value);
                   var obj={
                           id:'58356521b91de8d7d6c67b24',
                           appId:$scope.information._id,
                           redirectUri:data2.url
                        }
                  TenantFactory.updateapplication(obj,function(res){
                     console.log(res,"update result",data2);
                     $scope.addAppflag=false;

                      angular.forEach(res.application,function(values){
                        console.log("val",values,data2);
                        if(values.name==data2.name){
                           $rootScope.loginObj=values;
                           console.log(values.redirectUri[values.redirectUri.length-1]);
                           var url=values.redirectUri[values.redirectUri.length-1];                           
                           // $window.location.href = url;
                        }
                     })

                     $state.go($state.current, {}, {reload: true});
                           // $state.go('/login');
                        })
            })
  
   };

$scope.clearInfo=function(data1){

 // console.log("cancel");
 $state.go($state.current, {}, {reload: true});
};

});