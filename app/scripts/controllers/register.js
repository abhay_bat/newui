angular.module('homer')

  .controller('registerCntrl', function ($scope,$state,ActivitylogService,GlobalService,$http,$rootScope,md5,Auth,User,notify,DefaultDocs,userManagement,docsCtrlVariables) {
     $scope.homerTemplate = 'views/notification/notify.html';
     console.log("inside register page");
     var activity_type = GlobalService.getActivitytype();

Auth.getTenant(function(tenant){
    $scope.tenant=tenant;
  //$rootScope.domainDetails=tenant;
    $rootScope.tenantInfo=$scope.tenant;
    });


$scope.user = {};
  // $scope.servicePassword = "";



  //to verify user 
var verifyUser={
  token:$state.params.id
  };

  // to set password strength
$scope.strength="";

$scope.passStrength = function(){
    return $scope.strength;
  };

 
$scope.testStrength=function(pass){
  // console.log(pass);
  var type;
  $scope.strength = 0;
  if(pass){
    if(pass.length<6){
      $scope.strength=0;
      } if( /[0-9]/.test( pass ) ){
        type = 'success';
        $scope.strength=$scope.strength+1;
        // console.log("numbers");
      } if( /[a-z]/.test( pass ) )
          {
            $scope.strength=$scope.strength+1;
            // console.log("alpha");  
           } if( /[A-Z]/.test( pass ) ){
               $scope.strength=$scope.strength+1;
              // console.log("caps");
             } if( /[^A-Z-0-9]/i.test( pass ) ){
                  $scope.strength=$scope.strength+1;
                  // console.log("chars");
                }
  }
  else{
    $scope.strength = -1;
  // return s;
  }
};


userManagement.verify(verifyUser,function(response){
  console.log(response.email,response,"response");
$scope.username=response.email;

},function(err){
  // console.log("error");
  $state.go('common.error_one');
});



$scope.register=function(){

  if($scope.strength==1 && $scope.password1 !== undefined && $scope.password2 !== undefined){
  
 notify({ message: 'Warning -  Passwords must be strong ', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
$scope.strength="";
$scope.password1 = "";
 $scope.password2 = "";
}else{
  if ($scope.password1 !== undefined && $scope.password2 !== undefined) {
          // console.log("inside condition");
 if ($scope.password1.length != 0 && $scope.password1.length != 0 && $scope.strength>=2) {
if ($scope.password1 == $scope.password2){
$scope.user.password1 = md5.createHash($scope.password1 || '');
 $scope.user.email=$scope.username;
User.changePassword($scope.user,function(data){
  var loginobj={
                  username:$scope.username,
                  password:$scope.user.password1
                };
var userObj = {};
 Auth.login(loginobj,$scope.tenant.clientSecret)
 .then(function(userr){

      $scope.email = "";
      $scope.oldPassword = "";
      $scope.password1 = "";
      $scope.password2 = "";  
      $scope.strength=""; 
 Auth.isLoggedInAsync(function(loggedIn) {
            $scope.loginButton=true;
            var loginLog={};
            loginLog.activitytype=activity_type.LOGIN;
            ActivitylogService.LogActivity(loginLog);//from activity service
          });
      $scope.getCurrentUser = Auth.getCurrentUser;
      $scope.getCurrentUser().$promise.then(function() {
    $rootScope.loggedIn = $scope.getCurrentUser();
    // console.log($rootScope.loggedIn);
    docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);
    $state.go('dashboard');
  });
notify({ message: 'success - successfully logged in  ', classes: 'alert-success', templateUrl: $scope.homerTemplate});
            var passLog={};
            passLog.activitytype=activity_type.PASSWORD;
            ActivitylogService.LogActivity(passLog);//from activity service
            // console.log(passLog);
 },
 function(err) {
        notify({ message: 'Warning - Please Enter Valid Username ', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
        });

})

}else{
  $scope.errorMessage="";
              notify({ message: 'Warning - Passwords donot match', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
              //alertify.error("New passwords don't match");
              $scope.email = "";
              $scope.oldPassword = "";
              $scope.password1 = "";
              $scope.password2 = "";
              $scope.strength="";
}
 }else{
   $scope.errorMessage=""
              notify({ message: 'Warning - Please enter the password', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
         
          $scope.email = "";
          $scope.oldPassword = "";
          $scope.password1 = "";
          $scope.password2 = "";
          $scope.strength="";
        };
}else{
$scope.errorMessage=""
              notify({ message: 'Warning - Passwords Fields can not be empty', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
         
          $scope.email = "";
          $scope.oldPassword = "";
          $scope.password1 = "";
          $scope.password2 = "";
          $scope.strength="";
}
};
};

$scope.cancel=function(){
  // console.log("cancelled");
  $scope.password1 = "";
  $scope.password2 = "";
};



   });