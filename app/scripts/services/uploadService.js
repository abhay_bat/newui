angular.module('homer')
  .service('uploadService', function (Upload,url,DefaultDocs,ActivitylogService,GlobalService) {
    var activity_type = GlobalService.getActivitytype();
  	this.file_upload=function(file1,currentDoc,callback) {
                console.log("parameter",file1);
                    file1.upload = Upload.upload({
                            url : url + 'api/Documents',
                            data : {
                                filename : file1.name,
                                s3_path : currentDoc.s3_path,
                                parent : currentDoc.parent,
                                id:currentDoc._id,
                                foldername : currentDoc.documentName,
                                //as_user:"582d565ea2339920d3ca5cb6",
                                size:file1.size,
                                file : file1
                            }
                        });
                        file1.upload.then(function(response) {
                                console.log("parameter",response);
                                callback(response.data);
                                var uploadLog = {};
                                    var uploadLog = {};
                                file1.progress=100;
                                    uploadLog.activitytype = activity_type.UPLOAD;
                                uploadLog.what = response.data._id;
                                // console.log(uploadLog.what);
                                uploadLog.location = currentDoc.parent + '/' + currentDoc.documentName;
                                uploadLog.status = true;
                                ActivitylogService.LogActivity(uploadLog);
                                // console.log(file1.progress);
                               // console.log(docsCtrlVariables.getWhichDocument());
                            // var getdocs = DefaultDocs.show({
                            //     id : currentDoc._id
                            // }, function(res) {
                            //     // console.log("parameter",response);
                            //     var uploadLog = {};
                            //     file1.progress=100;
                            //     uploadLog.activitytype = activity_type.UPLOAD;
                            //     uploadLog.what = response.data._id;
                            //     // console.log(uploadLog.what);
                            //     uploadLog.location = currentDoc.parent + '/' + currentDoc.documentName;
                            //     uploadLog.status = true;
                            //     ActivitylogService.LogActivity(uploadLog);
                            //     callback(res);
                            // });
                            file1.result = response.data;
                            },function(response) {
                                // console.log("parameter1",response);
                                callback(response);
                            }, function(evt) {
                                // console.log("event",evt);
                                file1.progress = Math.min(97, parseInt(100.0 * evt.loaded / evt.total));
                            // console.log(file1.progress);
                            });
            };

            // this.cancel_upload=function(file1){
            //     file1.upload.abort(); 
            // };
  })