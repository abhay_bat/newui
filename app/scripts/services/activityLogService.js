'use strict';

angular.module('homer')
  .service('ActivitylogService', function ($http,$rootScope,Auth,activityFcrt) {
 	var getCurrentUser=Auth.getCurrentUser;
 
  this.LogActivity = function(obj){
  	    //console.log("This is activity log service",obj);    
    var reminderObject = {
			who		: getCurrentUser()._id,
			/*browser	: deviceDetector.browser,
			device  : deviceDetector.device,*/
			level 	: 1
		};
		
    obj.who=getCurrentUser()._id;
    activityFcrt.save(obj, function(data) {
    	obj={};
		//console.log(data);	
		//callback();
	});
	
  };
  
  this.AsyncLogActivity = function(obj,callback){  	
   // console.log("This is activity log service",obj);
    obj.who=getCurrentUser()._id;
    //console.log("activity log service",obj.who);
   /* obj.browser=deviceDetector.browser;
    obj.device=deviceDetector.device;
    */
    var reminderObject = {
			who		: getCurrentUser()._id,
			/*browser	: deviceDetector.browser,
			device  : deviceDetector.device,
*/			level 	: 1
		};
		
    activityFcrt.save(obj, function(data) {
    	obj={};
		//console.log(data);	
		callback();
	});
	
  };
  
});
  
  
