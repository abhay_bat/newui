/* global io */
'use strict';

angular.module('homer')
  .factory('socket', function() {

    // socket.io now auto-configures its connection when we ommit a connection url
    //var ioSocket = io.connect('http://tricons.2docstore.com:9000/socket.io-client');



    // var host = $location.host();
    // var ioSocket = io('https://mamatha.2docstore.com:9000', {
    //  //   // Send auth token on connection, you will need to DI the Auth service above
    // 'query': 'token=' + host.split('.')[0],

    // 'reconnection': true,
    // 'reconnectionDelay': 1000,
    // 'reconnectionDelayMax' : 5000,
    // 'reconnectionAttempts': 5,
    //  path: '/socket.io-client'

    // });

    // var socket = socketFactory({
    //   ioSocket: ioSocket
    // });

 return {
      // socket: socket,

      // /**
      //  * Register listeners to sync an array with updates on a model
      //  *
      //  * Takes the array we want to sync, the model name that socket updates are sent from,
      //  * and an optional callback function after new items are updated.
      //  *
      //  * @param {String} modelName
      //  * @param {Array} array
      //  * @param {Function} cb
      //  */
      // syncUpdates: function (modelName, array, cb) {
      //   cb = cb || angular.noop;


      //   /**
      //    * Syncs item creation/updates on 'model:save'
      //    */
      //   socket.on(modelName + ':save', function (item) {
      //    console.log("item here",item,array);
      //     var oldItem = _.find(array, {_id: item._id});
      //     var index = array.indexOf(oldItem);
      //     var event = 'created';
      //     // replace oldItem if it exists
      //     // otherwise just add item to the collection
      //     if (oldItem) {
      //       array.splice(index, 1, item);
      //       event = 'updated';
      //     } else {
      //      array.push(item);
      //    }

      //     cb(event, item, array);
      //   });

      //   /**
      //    * Syncs removed items on 'model:remove'
      //    */
      //   socket.on(modelName + ':remove', function (item) {
      //     console.log(item);
      //     var event = 'deleted';
      //     _.remove(array, {_id: item._id});
      //     cb(event, item, array);
      //   });

      //   socket.on(modelName + ':update', function (item) {
      //    console.log(item);
      //   });
      // },

      // /**
      //  * Removes listeners for a models updates on the socket
      //  *
      //  * @param modelName
      //  */
      // unsyncUpdates: function (modelName) {
      //   socket.removeAllListeners(modelName + ':save');
      //   socket.removeAllListeners(modelName + ':remove');
      // }
    };
  });
