'use strict';

angular.module('homer')
  .service('GlobalService', function (Upload,url) {
 	   
   
 	var space = {MIN:'Value cant be less than 5',INT:'Please enter integers only',MAX:'Value can not be more than 1000 GB'}; 
	var no_data = "No Data to display";
    var footer_text = "<span>Copyright <i class='fa fa-copyright'></i> 2016 2Docstore All rights reserved.</span>";
    var restCalls = { get:'GET', put:'PUT', post:'POST', delete:'DELETE' };
    var note = {NOTE_PW:'Password should contain Alphanumeric with atleast one UPPERCASE,one lowercase and special character(ex:Trinetra@1)',
			    USER:'Minimum 3 alphanumeric characters.',
			    NOTE_ORG:'User will be mapped to the default organization listed.',
			    NOTE_EMAIL:'Enter valid email ID for user authentication.',
			    REG_USER:'Administrator will have full access whereas User will be having limited access.',
			    CLIENTADMIN:'Client Admin has limited access,belongs to the organisation',
			    CLIENT:'Minimum 3 alphanumeric characters allowed for client name.',
			    MOBILE:'Enter 10 digit mobile number.',
			    C_EMAIL:'Enter valid email ID for client authentication.',
			    FOLDER:'Enter meaningful folder name for creating folder in client documents.',
			    SUB_UNTIL:"Chose the Subscription end date.",
			    REMINDER_NOTE:'Enter meaningful text to be remind.',
			    WHEN:'Chose the start date for the Timeline.',
			    REPEAT:'Please select how often you want be remind.',
                NEVER:'There is no end date for never.',
                UNTIL:'Chose the end date for the Timeline.',
                message: 'Enter the Timeline text / details here'};
                
    // var regex = {PHONE:'/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/'};
			    
    var error_type ={PW_ERR_PTRN:'Must contain one lower case,uppercase letter and one special character.',
		   			 PW_MIN_LNG:'Password must be between 6 and 20 characters.',PW_NOMATCH:'Passwords do not match.',
		   			 UN_SHRT:'Name should contains more than 3 characters',UN_LNG:'Name should contains less than 20 characters',
		   			 MOBILE:'Must be a valid 10 digit Contact number',VALID_NUM:'Enter a valid phone number',
		   			 VALID_NAME:'Please enter valid name' ,VALID_EMAIL:'Enter a valid email.',VALID_DATE:'Enter a valid date.',
		   			 REQ:'This field is required.',E_DATE:'End Date should be greater than start date',S_DATE:'Start Date should be greater than current date.'};

    var activity_type = {LOGIN:'Logged-In',LOGOUT:'Logged-Out',CREATE:'Created',
        SHARE:'Shared',USERADD:'Added user',USERUPDATE:'Updated user',
   		UPLOAD:'Uploaded',DOWNLOAD:'Downloaded',DELETE:'Deleted',MUL_DELETE:'Deleted',
        RESTORE:'Restored',TRASH:'Trashed',VIEW:'Viewed',CHANGEVERSION:'Changed version',
   		ADD:'Added',CLIENTADD:'Added client',CLIENTUPDATE:'Updated client',IS:' is ',
        HAS:' has ',FROM:' from ',IN:' in ',TO:' to ',
  		REMIND:'set Timeline',LOCK:'locked',UNLOCK:'unlocked',FAV:'favorited',
        UNFAV:'removed favourite',UPDATE:'updated',PASSWORD:'Changed Password',
        PROFILE:'Uploaded Profile_Pic',PROFILEUPDATE:'Updated Profile',
        GROUPADD:'Created group',GROUPUPDATE:'Updated group',THIS:' this ',
        DESCRIPTION :' updated description',COMMENT : 'Added Comment'
        ,GROUPDELETE:'Deleted group',CLIENTDELETE:'Deleted client'};
	var action ={DOC1:'My Documents',DOC2:'Client Documents',ACTIVE:'Active',
        DISABLE:'Disable',ADMIN:'Admin',
		CLIENT_ADMIN:'Client Admin',NOT_SET:'Not set'};
                
    this.getText = function(){    
        return footer_text;
    };

    this.get_error = function(){    
        return error_type;
    };
    
    this.getNote = function(){    
        return note;
    };

    this.getspace = function(){    
        return space;
    };

    this.getRestVar=function(){
    	return restCalls;
    };
       
    this.getActivitytype = function(){
        return activity_type;
 	 };
 	 
 	this.getNodata = function(){   
        return no_data;
    };
    
    this.getRegex = function(){    
        return regex;
    };
    this.getAction = function(){    
        return action;
    };
   

});

  
