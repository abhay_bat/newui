angular.module('homer')
  .service('timelineService', function (DefaultDocs,$uibModal,timelineFctry) {


  	    var repeatlist = [
    	{ "id": 1, "repeat": " Daily " },
        { "id": 2, "repeat": " Weekly " },
        { "id": 3, "repeat": " Monthly " },
        { "id": 4, "repeat": " Yearly " }
    ];

this.setTimeline=function(doc){

    var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/timeline.html',
        controller: timelineCtrl,
        size:'md',
        resolve:{
        	param:function(){
        		return{'doc':doc};
        	}
        }          
    })
}

this.getTimeline=function(doc,cb){
	var obj = {
			"_id" : doc._id
		};
		timelineFctry.query(obj, function(value) {
			// console.log(value);
				cb(value);
		});
        // console.log(obj);
}

this.saveTimeline=function(timeline,cb){
     console.log(timeline);
	timelineFctry.save(timeline,function(res){
		console.log(res,"set timeline servs");
		cb(res);
	})
}
this.updateTimeline=function(timeline,cb){
     console.log(timeline,"update timeline service");
	timelineFctry.update(timeline,function(res){
		console.log(res,"rsult");
		cb(res);
	})
}

this.validateTimeline=function(timeline){

}

this.getRepeat=function(){
	return repeatlist;
}

  });