'use strict';

angular.module('homer')
.service('docsCtrlVariables', function ($http,$rootScope,Auth,activityFcrt,DefaultDocs,notify,$uibModal,uploadService,GlobalService,ShareFctry,ActivitylogService,$moment,globalSearchFcrt,versionService,socket,blockUI,Groups) {


 var activity_type = GlobalService.getActivitytype();
 var homerTemplate=  'views/notification/notify.html';
 var documentInfo=[];
 var currentdocument={};
 var breadcrumbs=[];
 var access_list={};
 var whichDocument;
 var clicks="";
 var propertyDoc={};
 var skip=0;
 var limit=10;

 this.resetCount=function(){
  this.skip=0;
 }
 this.setSkipValues=function(value){
  skip=(value * limit)-limit ;
 }
 this.getSkipValue=function(){
  return skip;
 }
 this.setpropertyDoc=function(doc){
   propertyDoc=doc;
 }

 this.getpropertyDoc=function(){
  return propertyDoc;
}

this.initWhichDocument=function(){
  whichDocument="";
}


this.spliceDoc=function(docid){
  var ind=documentInfo.map(function(e) { return e._id; }).indexOf(docid);
  documentInfo[ind].trash=true;
  documentInfo[ind].check=false;
      ////console.log(documentInfo);
    }

    this.restoreDoc=function(docid){
      var ind=documentInfo.map(function(e) { return e._id; }).indexOf(docid);
      documentInfo[ind].trash=false;
      documentInfo[ind].check=false;
    }    

    this.trashDoc=function(docid){
      var ind=documentInfo.map(function(e) { return e._id; }).indexOf(docid);
      documentInfo.splice(ind,1);
    }


    this.checkForImage=function(user,image,cb){
      // console.log(user,image);
      cb= cb || angular.noop;
      if(user){
        if(user.name){
          var res1 = user.name.slice(0, 1);
      var res = res1.toUpperCase();
      user.capital=res;
      /*
      image load
      */
      var img = new Image ;
      img.onload = function(){
        user.avatarflag = true;
        cb();
      //$scope.$apply();
    }; 
    img.onerror = function() {
     user.avatarflag=false;
     cb();
     // $scope.$apply();
   };
   img.src = image;
        }  
 }
}

this.setWhichDocument=function(document,click){
  skip=0;
  whichDocument=document;
  clicks=click;
};

this.addToDocumentInfo=function(document){
  console.log("start of addToDocumentInfo",document,documentInfo);
  var ind = documentInfo.map(function(e) { return e._id; }).indexOf(document._id);
  console.log("start",documentInfo);
  console.log(ind);
  if(ind>-1){
console.log("before splice",documentInfo);
    documentInfo.splice(ind,1);
    console.log("after splice",documentInfo);
    documentInfo.push(document);
        console.log("after push addToDocumentInfo",documentInfo);
  }
  else{
    console.log("after addToDocumentInfo",documentInfo);
    documentInfo.push(document);
  }
  
};

    this.addDocumentSocket=function(document,cb){
      console.log(currentdocument.s3_path,document.parent);
      if(currentdocument.s3_path==document.parent){
        checkForPermission(document,function(){
          var ind=documentInfo.map(function(e) { return e.s3_path; }).indexOf(document.s3_path);
         // console.log(ind);
          if(ind>-1){
            //documentInfo[ind]=document;
            documentInfo.splice(ind,1);
            documentInfo.push(document);
            console.log("update",ind);
            cb("update");
          }
          else{
            documentInfo.push(document);
            //console.log(documentInfo);
            cb("create");
          }
                
        })
      }      
    };

    this.delDocumentSocket=function(document,cb){
      if(currentdocument.s3_path==document.parent){
        var ind=documentInfo.map(function(e) { return e.s3_path; }).indexOf(document.s3_path);
        documentInfo.splice(ind,1);
        cb();
      }  
    };

    this.getWhichDocument=function(){
      return whichDocument;
    };

this.getbreadcrumbs=function(){
  return breadcrumbs;

};
this.addAccessList=function(value){
  //    currentdocument.access_list.push(value);
};
this.newbreadcrumbs=function(){
  breadcrumbs=[];
};
this.getAccessList=function(){
  access_list=currentdocument.access_list;
  return access_list;
}
this.getDocumentInfo=function(){
  return documentInfo;

}
this.setDocumentInfo=function(document){
  documentInfo=document;
}
this.updateDocumentInfo=function(){
  angular.forEach(documentInfo,function(doc,key){
    doc.check=false;
  })
}
this.setCurrentDocument=function(document){
// console.log(document);
  currentdocument=document;
};
  	/*
	get the current document
 */
 this.getCurrentDocument=function(document){
  // console.log(document)
  return currentdocument;
}
  	/*
		query to get the documents(MY docs or client)

   */
   this.getDocs = function(docName,cb){
     // var start = $moment().get('millisecond');
     blockUI.start()
     Auth.isAdmin(function(isadmin) {
      var role=isadmin;
      if(clicks !="clicked"){
        if(role)
        {
          docName="My Documents";
        } 
        else
        {
          docName="shared";
        }
      }

      if(docName=="shared"){
        documentInfo=[];
        var getshared = DefaultDocs.getshareddocs(function(shared) {
         documentInfo  = shared;
         angular.forEach(documentInfo,function(doc,key){

           checkForPermission(doc,function(){
                        //documentInfo.push(res);
                      })
         })
          // var end = $moment().get('millisecond');
          blockUI.stop();
          cb(docName);
        });


      }
      else if(docName=="fav"){
        documentInfo=[];
        var getfav = DefaultDocs.listFavs(function(favs) {
                var end = $moment().get('millisecond');
           documentInfo = favs;  
           blockUI.stop();
              /* angular.forEach(documentInfo,function(doc,key){
                 checkForPermission(doc,function(){
                        //documentInfo.push(res);
                      })
                    })*/


                  });
      }
      else if(docName=="search"){
        documentInfo=[];
        var getsearch = globalSearchFcrt.search({
          val : $rootScope.globalSearch
        }, function(res) {

            //var end = $moment().get('millisecond');

            documentInfo = res;
            blockUI.stop();
         /*        angular.forEach(documentInfo,function(doc,key){
                 checkForPermission(doc,function(){
                        //documentInfo.push(res);
                      })
                    })*/
                  })
      }
      else if(docName=="ProjectView"){
        documentInfo=[];
        breadcrumbs=[];
        currentdocument={};
        DefaultDocs.query(function(data) {

          var defaultdocs = data;
          angular.forEach(defaultdocs,function(documents,key){
            if(documents.documentName=="Client Documents"){
              console.log("Client Documents",documents);
              breadcrumbs.push(documents);
                  //currentdocument=documents;

                  DefaultDocs.show({
                    id : documents._id
                  }, function(response) {
                    //$scope.documentList=response;
                    DefaultDocs.show({
                      id : $rootScope.projectViewDoc._id
                    }, function(resp) {
                      breadcrumbs.push($rootScope.projectViewDoc);
                    //$scope.documentList=response;
                    currentdocument=$rootScope.projectViewDoc;
                    documentInfo=resp;
                    blockUI.stop();
                           //    var end = $moment().get('millisecond');

             /* angular.forEach(documentInfo,function(doc,key){
                 checkForPermission(doc,function(){

                       //console.log(doc);
                        //documentInfo.push(res);
                      })
            })
            */
            cb();
          });
                  });

                }
              })
        })
      }

      else{
        blockUI.message('Opening My Documents..'); 
        documentInfo=[];
        breadcrumbs=[];
        currentdocument={};
        var response1 = DefaultDocs.query(function(data) {

          var defaultdocs = data;
          angular.forEach(defaultdocs,function(documents,key){
            if(documents.documentName==docName){


              /*set Breadcrumbs here*/
              //$rootScope.breadcrumbs.push($scope.defaultdocs[i]);
              breadcrumbs.push(documents);
              //$scope.currentDocument=$scope.defaultdocs[i];
              currentdocument=documents;

              var response2=DefaultDocs.show({
                id : documents._id,
                skip:skip,
                limit:limit
              }, function(response) {

                   // $scope.documentList=response;
                   documentInfo=response;
                   blockUI.stop();
            //          angular.forEach(documentInfo,function(doc,key){
               /*  checkForPermission(doc,function(){
                     
                        //documentInfo.push(res);
                      })*/
            // })
                    // socket.syncUpdates('Document', documentInfo,function(event,item,array){

                    // });
                    var end = $moment().get('millisecond');
                    // //console.log("Time taken :",end-start);
                    //socket.syncUpdates('Document', documentInfo);
                  });
            }
          })

        });

      }
    });

}
  	 /*
  	 for length of documentInfo
      */
    this.giveLengthtrash=function(){
      var count =0
      angular.forEach(documentInfo, function(value, key){
        if(value.trash!=true){
          count++
        }
      });
      var lengthDocs=documentInfo.length;
      return count;
    }
         /*
     for length of documentInfo
       */
     this.giveLength=function(){
      var lengthDocs=documentInfo.length;
      return lengthDocs;
    }
  	/*
  	folder click

  	*/
  	this.folderClick=function(document){
      var start = $moment().get('millisecond');
      // //console.log(document,"image in service");

      //
      checkForPermission(document,function(){
                //  //console.log(doc)
                       // //console.log(res);
                        //documentInfo.push(res);
                      })
      blockUI.start();
      blockUI.message('Opening '+ ' ' + document.documentName.slice(0, 10) + '..');
      DefaultDocs.show({
        id : document._id,
        skip:skip,
        limit:limit
      }, function(response) {

       blockUI.stop();
       while(documentInfo.length > 0){
        documentInfo.pop();
      }
      breadcrumbs.push(document);				
      currentdocument = document;
      documentInfo = response;
      angular.forEach(documentInfo,function(doc,key){
       checkForPermission(doc,function(){
                //  //console.log(doc)
                       // //console.log(res);
                        //documentInfo.push(res);
                      })

     })

          //  //console.log(documentInfo);
          var end = $moment().get('millisecond');


        }, function(err) {
            console.log(err)
            blockUI.stop();
            notify({ message: 'Error - Folder locked', classes: 'alert-danger', templateUrl: homerTemplate});
          });

    }
    this.openFile=function(){
      blockUI.stop();
              //open modal
      //        $rootScope.allowMoadlOpen=false;
      var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modal_comment.html',
        controller: ModalInstanceCtrl,
        size:'lg'
      })
    }

    this.guestopenFile=function(){
      blockUI.stop();
              //open modal
      //        $rootScope.allowMoadlOpen=false;
      var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modal_comment.html',
        controller: ModalInstanceCtrl,
         backdrop  : 'static',
   keyboard  : false,
        size:'lg'
      })
    }
    

    this.uploadFiles=function(files,showNoFiles,versionuploading){
          
      angular.forEach(files, function(file) {
        uploadService.file_upload(file, currentdocument, function(response) {
        //documentInfo.push(response);

        checkForPermission(response,function(){
          var ind=documentInfo.map(function(e) { return e._id; }).indexOf(response._id);

          if(ind>-1){
            documentInfo[ind]=response; 
            var getdocs = DefaultDocs.show({
              id : currentdocument._id
            }, function(res) {
              console.log(res);
              documentInfo=res;
            })
          }
          else{
            //documentInfo.push(response);
           // versionService.getThumbnail(response);
         }
       })


        // if (response.status == 500) {
        //  files = [];
        // /*error here*/
        // } else {
        //   //$scope.documentList= response;
        //   documentInfo=response;
        //   //$scope.mylength = $scope.isEmptyData(response.length);
        //   file.progress = 100;
        //   versionuploading=false;
        //   showNoFiles=false;
        // }
      });
      });
    };


// to cancel upload
// this.cancel=function(file){
// uploadService.cancel_upload();
// };
    /*
    to set bread and views
    */
    this.breadClick=function(document){
      var indexval = breadcrumbs.indexOf(document);
      breadcrumbs.splice(indexval + 1);
      documentInfo=[];
      DefaultDocs.show({
        id : document._id
      }, function(response){
        documentInfo = response;

        currentdocument = document;
        blockUI.stop();
    /*    angular.forEach(documentInfo,function(doc,key){
         checkForPermission(doc,function(){
                       //console.log(doc);
                        //documentInfo.push(res);
                      })
       })*/
      });
    };

  /*
  Adding a new folder
  */
  this.addfolder=function(newFoldername,showAddRow,disableAddFolder){

    var flagsObj={};
    var regex = /^[\w. ]+$/;
    var notExists = this.docDuplicates(newFoldername);
    if (newFoldername != undefined && regex.test(newFoldername) && newFoldername.length <= 25) {
      if (notExists) {
       blockUI.stop();

      //  notify({ message: 'Success - Please wait while the Folder is being created', classes: 'alert-success', templateUrl: homerTemplate});
          /* flagsObj.showAddRow=true;
             flagsObj.disableAddFolder=true;
             newFoldername=newFoldername;*/
console.log(newFoldername);
             currentdocument.name = newFoldername;
             blockUI.start();
             blockUI.message('Creating Folder...');
             DefaultDocs.save({
              "id" : currentdocument._id,
              "s3_path" : currentdocument.s3_path,
              "parent" : currentdocument.parent,
              "documentName" : currentdocument.documentName,
              "name" : newFoldername
            }, function(res) {
      console.log(res);
          var addfolderlog = {};
          var id = res._id;
          var addfolderlog = {};
          var id = res._id;
          addfolderlog.activitytype = activity_type.CREATE;
          addfolderlog.what = id;
          addfolderlog.location = currentdocument.parent + '/' + currentdocument.documentName;
          addfolderlog.status = true;
          ActivitylogService.LogActivity(addfolderlog);
          //console.log(documentInfo);
          // checkForPermission(res,function(){
          //              // console.log(res);
          //               documentInfo.push(res);

          //             })
          //documentInfo.push(res);
          // socket.syncUpdates('Document',documentInfo,function(event,item,array){

          //             console.log(event,item,array);
          //             //documentInfo=array;
          //             checkForPermission(item,function(){
          //              // console.log(res);
          //               //documentInfo.push(res);
          //             })
          // });

         // console.log(documentInfo);
         showAddRow=false;
         disableAddFolder=false;
         newFoldername="";
         flagsObj.disableAddFolder=true;
         flagsObj.showAddRow=false;
         blockUI.stop();
         notify({ message: 'Success- Folder successfully created', classes: 'alert-success', templateUrl: homerTemplate});          

          // var getdocs = DefaultDocs.show({
          //   id : currentdocument._id
          // }, function(response) {
          // documentInfo=response
          //   showAddRow=false;
          //   disableAddFolder=false;
          //   newFoldername="";
          //     flagsObj.disableAddFolder=true;
          //     flagsObj.showAddRow=false;
          //      notify({ message: 'Success- Folder successfully created', classes: 'alert-success', templateUrl: homerTemplate});
          // });
        });

           } else {
            blockUI.stop();
            notify({ message: 'Error - Folder name already exists', classes: 'alert-danger', templateUrl: homerTemplate});
            flagsObj.showAddRow="true"
            flagsObj.newFoldername="";
            flagsObj.disableAddFolder=false; 
          }
        } else {
          blockUI.stop();
          notify({ message: 'Enter valid folder name', classes: 'alert-danger', templateUrl: homerTemplate});
          flagsObj.showAddRow="true"
          flagsObj.newFoldername="";
          flagsObj.disableAddFolder=false;
        } 

        return flagsObj; 
      }
      
      this.docDuplicates = function(newFolder) {

        var notExists = true;
        angular.forEach(documentInfo,function(document,key){
         if (document.type == "folder" && document.documentName == newFolder) {
          notExists = false;
        }
      })


        return notExists;
      };

      this.findIcons=function(document){
        if(document.type=="folder"){
          return "pe-7s-folder"
        }
        else if((/\.(gif|jpg|jpeg|tiff|png|ico)$/i).test(document.documentName)){
          return "fa fa-file-picture-o";
        }
        else if ((/\.(docx|doc)$/i).test(document.documentName)) {

          return "fa fa-file-word-o";
        } else if ((/\.(xlsx|xls|csv)$/i).test(document.documentName)) {

          return "fa fa-file-excel-o";;
        } else if ((/\.(ppt|pptx)$/i).test(document.documentName)) {

          return "fa fa-file-powerpoint-o";
        }
        else if ((/\.(pdf)$/i).test(document.documentName)) {
          return "fa fa-file-pdf-o"
        }
        else {
          return "pe-7s-file";
        }

      }

      var checkForPermission = function(folder,cb) {
    //blockUI.start();

    //$scope.allowFolderClick = false;
    var docIndex;
    var flag = true;

    folder.access_list = [];
    Groups.getdocument({
      "document" : folder
    }, function(response) {
      if (response.length > 0) {
        angular.forEach(response, function(user, key) {
          var obj = {};
          angular.forEach(user.policies, function(policy, key) {
            if (policy.document.indexOf(folder._id) > -1) {
              obj.policy_id = policy.policy;
              obj.user_id = user.user;
              folder.access_list.push(obj);
              $rootScope.showEmptyContribList = false;
              if ($rootScope.loggedIn._id == user.user._id) {
                flag = false;
                folder.policyName = policy.policy.policy_name;
                folder.allowDelete = givePermission(policy.policy.permissions.delete)
                folder.allowDownload = givePermission(policy.policy.permissions.download);
                folder.allowAddFolders = givePermission(policy.policy.permissions.add_folders);
                folder.allowEdit = givePermission(policy.policy.permissions.edit);
                folder.allowShare = givePermission(policy.policy.permissions.share);
                folder.allowUpload = givePermission(policy.policy.permissions.upload);
                folder.allowView = givePermission(policy.policy.permissions.view);
                if (folder.policyName == "Owner" || folder.policyName == "Co-owner" || folder.policyName == "Publisher") {
                  folder.allowLock = true;
                  folder.allowRemovingContrib = true;
                  folder.showInvite=true;
                  $rootScope.showInvite=true;

                } else {
                  folder.allowLock = false;
                  folder.allowRemovingContrib = false;
                  folder.showInvite=false;
                  $rootScope.showInvite=false;
                  
                } 
              }
            }

          })
        })
        if (flag == true) {
          //set as owner if admin
          folder.policyName = "Owner";
          //
          folder.allowDelete = true;
          folder.allowDownload = true;
          folder.allowAddFolders = true;
          folder.allowEdit = true;
          folder.allowShare = true;
          folder.allowUpload = true;
          folder.allowView = true;
          folder.allowLock = true;
          folder.allowRemovingContrib = true;
          folder.showInvite=true
          $rootScope.showInvite=true;
        }

      } else {
        folder.policyName = "Owner";
        folder.allowDelete = true;
        folder.allowDownload = true;
        folder.allowAddFolders = true;
        folder.allowEdit = true;
        folder.allowShare = true;
        folder.allowUpload = true;
        folder.allowView = true;
        folder.allowLock = true;
        folder.showInvite=true;
        $rootScope.showInvite=true;
        folder.allowRemovingContrib = true;
      }
      cb();
    }, function(err) {
      //blockUI.stop();
    });
    //console.log(folder)
    $rootScope.allowFolderClick = true;
  };

  var givePermission = function(value) {
    if (value == "deny")
      return false;
    else
      return true;
  }
})
