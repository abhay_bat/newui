angular.module('homer')
  .service('GroupService', function ($http,$rootScope,Groups,ActivitylogService,GlobalService,ClientService) {
  	var groupslist=Groups.getAllGroups();
	var activity_type = GlobalService.getActivitytype();
  	// listing logo
	this.imageExists = function(image,users){
	    var img = new Image();
	    img.onload = function() {
	      users.avatarflag = true;
	    }; 
	    img.onerror = function(){
	      users.avatarflag=false;
	    };
	    img.src = image;
	 };	

//To avoid the duplicate groups to be created
	this.DuplicateGroup=function(grp,list){
		console.log(grp,list);
		var dup=false;
		  angular.forEach(list,function(value,key){
		     if(value.Name==grp){
		    	dup=true;
		    }else {
		    }
		  });
		  return dup;
	};
//To make first letter as Uppercase and slice the 1st letter
	this.First_UpperCase = function(firstLtr) {
		if (firstLtr == undefined)
			return;
		var res = firstLtr.toUpperCase();
		var res1 = res.slice(0,1);
		return res1;
	};
		
/*
Select All
*/
var list=[];
		this.UserRemoveAll = function(select ,filtered,value){
			console.log(select ,filtered);
			if(select){
				list=[];
				angular.forEach(filtered, function(val, k){
					val.isChecked=true;
					console.log(val.isChecked);
					list.push(val._id);
				})
			}else{
				angular.forEach(filtered, function(val, k){
					val.isChecked=false;
					list=[];
				})	
			}		
		};
		

// indivisual check
	this.addOrRemoveUser = function(user,index){
	    if(user.isChecked){
	    	list.push(user._id);
	    }else{
	    	user.select=false;
	        var i=list.indexOf(user._id);
	        list.splice(i,1); 
	    }   	
	};
	
	// to delete
	this.deleteList=function(){
		list=[];
	}
	
	this.getList=function(){
		return list;
	}

	this.getGroupList=function(){
		return groupslist;
	}
	
//To make first letter as Uppercase in a group name 
	this.firstToUpperCase = function(strng){
	    var First_cap=strng.substr(0, 1).toUpperCase() + strng.substr(1);
	      return First_cap;
	};
	
	// creating the group
	this.creatGroup=function(group,cb){
		console.log("group",group,$rootScope.loggedIn);
		ClientService.getClientsAsync(function(data){
			//console.log("clients",data);
			console.log(data,$rootScope.loggedIn.organization);
			if($rootScope.loggedIn.role=="admin"){
				//group.clientid=value._id;
					//console.log("groupif",group);
					Groups.create(group,function(data){
						//console.log(data);
						var addgroup = {};
						addgroup.activitytype = activity_type.GROUPADD;
						addgroup.what = data._id;
						addgroup.status = true;
						ActivitylogService.LogActivity(addgroup);
						console.log(addgroup);
						cb();
					},function(err){ 
						cb();
					})
			}
			else{
				angular.forEach(data,function(value,key){
				if(value.name==$rootScope.loggedIn.organization)
				{	
					//console.log("Match")
					group.clientid=value._id;
					//console.log("group else",group);
					Groups.create(group,function(data){
						//console.log(data);
						var addgroup = {};
						addgroup.activitytype = activity_type.GROUPADD;
						addgroup.what = data._id;
						addgroup.status = true;
						ActivitylogService.LogActivity(addgroup);
						console.log(addgroup);
						cb();
					},function(err){ 
						cb();
					})
				}

			})
			}
			
		})
		
	};

		
	//update group
   this.updateGroup = function(user,cb){
	   Groups.Update(user,function(users){
	   		var groupUpdate = {};
			groupUpdate.activitytype = activity_type.GROUPUPDATE;
			groupUpdate.what = users._id;
			groupUpdate.status = true;
			ActivitylogService.LogActivity(groupUpdate);
			console.log(groupUpdate);
			cb();
		   	},function(err){
					cb();    
			});
   };

	
	
	
  })