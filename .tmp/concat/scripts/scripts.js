/**
 * HOMER - Responsive Admin Theme
 * version 1.8
 *
 */

$(document).ready(function () {

    // Set minimal height of #wrapper to fit the window
    fixWrapperHeight();

    // Add special class to minimalize page elements when screen is less than 768px
    setBodySmall();

});

$(window).bind("load", function () {

    // Remove splash screen after load
    $('.splash').css('display', 'none')
});

$(window).bind("resize click", function () {

    // Add special class to minimalize page elements when screen is less than 768px
    setBodySmall();

    // Waint until metsiMenu, collapse and other effect finish and set wrapper height
    setTimeout(function () {
        fixWrapperHeight();
    }, 300);
});

function fixWrapperHeight() {

    // Get and set current height
    var headerH = 62;
    var navigationH = $("#navigation").height();
    var contentH = $(".content").height();

    // Set new height when contnet height is less then navigation
    if (contentH < navigationH) {
        $("#wrapper").css("min-height", navigationH + 'px');
    }

    // Set new height when contnet height is less then navigation and navigation is less then window
    if (contentH < navigationH && navigationH < $(window).height()) {
        $("#wrapper").css("min-height", $(window).height() - headerH  + 'px');
    }

    // Set new height when contnet is higher then navigation but less then window
    if (contentH > navigationH && contentH < $(window).height()) {
        $("#wrapper").css("min-height", $(window).height() - headerH + 'px');
    }
}


function setBodySmall() {
    if ($(this).width() < 769) {
        $('body').addClass('page-small');
    } else {
        $('body').removeClass('page-small');
        $('body').removeClass('show-sidebar');
    }
}
/**
 * HOMER - Responsive Admin Theme
 * version 1.8
 *
 */
(function () {
    angular.module('homer', [
        'ui.router',
        'ngCookies', 
        'ngResource',             // Angular flexible routing
        'ngSanitize',               // Angular-sanitize
        'ui.bootstrap',             // AngularJS native directives for Bootstrap
        'angular-flot',             // Flot chart
        'angles',                   // Chart.js
        'angular-peity',            // Peity (small) charts
        'cgNotify',                 // Angular notify
        'angles',                   // Angular ChartJS
        'ngAnimate',                // Angular animations
        'ui.map',                   // Ui Map for Google maps
        'ui.calendar',              // UI Calendar
        'summernote',               // Summernote plugin
        'ngGrid',                   // Angular ng Grid
        'ui.tree',                  // Angular ui Tree
        'bm.bsTour',                // Angular bootstrap tour
        'datatables',               // Angular datatables plugin
        'xeditable',                // Angular-xeditable
        'ui.select',                // AngularJS ui-select
        'ui.sortable',              // AngularJS ui-sortable
        'ui.footable',              // FooTable
        'angular-chartist',         // Chartist
        'gridshore.c3js.chart',     // C3 charts
        'datatables.buttons',       // Datatables Buttons
        'angular-ladda',            // Ladda - loading buttons
        'ui.codemirror',            // Ui Codemirror
        'angular-md5',              //md-5
        'angular-momentjs',         //moment js
        'ngFileUpload',           //file uplaod
        'rzModule',                 //angularjs-slider
        'btford.socket-io',
        'perfect_scrollbar',
        'ngMaterial',
        'infinite-scroll',
        'blockUI',
        'angularUtils.directives.dirPagination',
        'ngclipboard',
        'nvd3',
        'ngAnimate',
        'infinite-scroll'
        // 'ngScrollbar'        
        //'oc.lazyLoad'
        ])
})();



/**
 * HOMER - Responsive Admin Theme
 * version 1.8
 *
 */

function configState($stateProvider, $urlRouterProvider,$httpProvider,$sceDelegateProvider,blockUIConfig) {

    // Optimize load start with remove binding information inside the DOM element
   // $compileProvider.debugInfoEnabled(true);
   //$httpProvider.defaults.withCredentials = false;
   
   blockUIConfig.autoBlock = false;
      $httpProvider.interceptors.push('authInterceptor');
      $httpProvider.defaults.headers.post['Content-Type'] = 'application/json';

 $sceDelegateProvider.resourceUrlWhitelist([
   'self',
   "https://docs.google.com/**",
   "https://2docstore1.s3-us-west-2.amazonaws.com/**",
   "https://s3-us-west-2.amazonaws.com/**",
    "https://view.officeapps.live.com/**"
  ]);
    // Set default state

//        $locationProvider.html5Mode({
//   enabled: true,
//   requireBase: false
// })

  $urlRouterProvider.otherwise("/common/login");
    $stateProvider

        // Landing page
        .state('landing', {
            url: "/landing_page",
            templateUrl: "views/landing_page.html",
            data: {
                pageTitle: 'Landing page',
                specialClass: 'landing-page'
            }
        })

        // Dashboard - Main page
        .state('dashboard', {
            url: "/dashboard",
            templateUrl: "views/dashboard.html",
            data: {
                pageTitle: 'Dashboard'
            }
            
       
        })

        // clientProject
          .state('clientProject', {
           url: "/clientProject",
           templateUrl: "views/clientProject.html",
           data: {
               pageTitle: 'Projects'
           }
       })
        
        // Analytics
        .state('analytics', {
            url: "/analytics",
            templateUrl: "views/analytics.html",
            data: {
                pageTitle: 'Analytics'
            }
        })

        // Widgets
        .state('widgets', {
            url: "/widgets",
            templateUrl: "views/widgets.html",
            data: {
                pageTitle: 'Widgets'
            }
        })

        // Widgets
        .state('options', {
            url: "/options",
            templateUrl: "views/options.html",
            data: {
                pageTitle: 'Options',
                pageDesc: 'Example small header for demo purpose.'
            }
        })

        // Interface
        .state('interface', {
            abstract: true,
            url: "/interface",
            templateUrl: "views/common/content.html",
            data: {
                pageTitle: 'Interface'
            }
        })
        // .state('right_sidebar',{
        //      abstract: true,
        //     url:"/right_sidebar",
        //     templateUrl:"views/common/right_sidebar.html",
        //     data:{
        //         pageTitle:'right_sidebar'
        //     }
        // })
        .state('interface.buttons', {
            url: "/buttons",
            templateUrl: "views/interface/buttons.html",
            data: {
                pageTitle: 'Colors and Buttons',
                pageDesc: 'The basic color palette'
            }
        })
        .state('interface.typography', {
            url: "/typography",
            templateUrl: "views/interface/typography.html",
            data: {
                pageTitle: 'Typography',
                pageDesc: 'The basic elements of typography'
            }
        })
        .state('interface.components', {
            url: "/components",
            templateUrl: "views/interface/components.html",
            data: {
                pageTitle: 'Components',
                pageDesc: 'Tabs, according, collapse and other UI components'
            }
        })
        .state('interface.icons', {
            url: "/icons",
            templateUrl: "views/interface/icons.html",
            data: {
                pageTitle: 'Icons',
                pageDesc: 'Two great icon libraries. Pe-icon-7-stroke and Font Awesome'
            }
        })
        .state('interface.panels', {
            url: "/panels",
            templateUrl: "views/interface/panels.html",
            data: {
                pageTitle: 'Panels',
                pageDesc: 'Two great icon libraries. Pe-icon-7-stroke and Font Awesome'
            }
        })
        .state('interface.alerts', {
            url: "/alerts",
            templateUrl: "views/interface/alerts.html",
            data: {
                pageTitle: 'Alerts',
                pageDesc: 'Notification and custom alerts'
            }
        })
        .state('interface.modals', {
            url: "/modals",
            templateUrl: "views/interface/modals.html",
            data: {
                pageTitle: 'Modals',
                pageDesc: 'Modal window examples'
            }
        })
        .state('interface.loading_buttons', {
            url: "/loading_buttons",
            templateUrl: "views/interface/loading_buttons.html",
            data: {
                pageTitle: 'Ladda',
                pageDesc: 'Loading buttons'
            }
        })
        .state('interface.list', {
            url: "/list",
            templateUrl: "views/interface/list.html",
            data: {
                pageTitle: 'Nestable list',
                pageDesc: 'Nestable - Drag & drop hierarchical list.'
            }
        })
        .state('interface.tour', {
            url: "/tour",
            templateUrl: "views/interface/tour.html",
            data: {
                pageTitle: 'Tour',
                pageDesc: 'The easiest way to show people how to use your website.'
            }
        })
        .state('interface.draggable_panels', {
            url: "/draggable_panels",
            templateUrl: "views/interface/draggable_panels.html",
            data: {
                pageTitle: 'Draggable panels',
                pageDesc: 'Example page for draggable panels'
            }
        })
        .state('interface.code_editor', {
            url: "/code_editor",
            templateUrl: "views/interface/code_editor.html",
            data: {
                pageTitle: 'Code editor',
                pageDesc: 'Versatile text editor implemented in JavaScript for the browser.'
            }
        })

        // App views
        .state('app_views', {
            abstract: true,
            url: "/app_views",
            templateUrl: "views/common/content.html",
            data: {
                pageTitle: 'App Views'
            }
        })
        .state('app_views.timeline', {
            url: "/timeline",
            templateUrl: "views/app_views/timeline.html",
            data: {
                pageTitle: 'Timeline',
                pageDesc: 'Present your events in timeline style.'
            }
        })
        .state('app_views.contacts', {
            url: "/contacts",
            templateUrl: "views/app_views/contacts.html",
            data: {
                pageTitle: 'Contacts',
                pageDesc: 'Show users list in nice and color panels'
            }
        })
        // .state('app_views.profile', {
        //     url: "/profile",
        //     templateUrl: "views/app_views/profile.html",
        //     data: {
        //         pageTitle: 'Profile'
        //         // pageDesc: 'Show user data in clear profile design'
        //     }
        // })
        .state('app_views.calendar', {
            url: "/calendar",
            templateUrl: "views/app_views/calendar.html",
            data: {
                pageTitle: 'Calendar',
                pageDesc: 'Full-sized, drag & drop event calendar.'
            }
        })
        .state('app_views.projects', {
            url: "/projects",
            templateUrl: "views/app_views/projects.html",
            data: {
                pageTitle: 'Projects',
                pageDesc: 'List of projects.'
            }
        })
        .state('app_views.project_detail', {
            url: "/project_detail",
            templateUrl: "views/app_views/project_detail.html",
            data: {
                pageTitle: 'Project detail',
                pageDesc: 'Special page for project detail.'
            }
        })
        .state('app_views.app_plans', {
            url: "/app_plans",
            templateUrl: "views/app_views/app_plans.html",
            data: {
                pageTitle: 'App plans',
                pageDesc: 'Present pricing option for your app'
            }
        })
        .state('app_views.social_board', {
            url: "/social_board",
            templateUrl: "views/app_views/social_board.html",
            data: {
                pageTitle: 'Social board',
                pageDesc: 'Message board for social interactions.'
            }
        })
        .state('app_views.blog', {
            url: "/blog",
            templateUrl: "views/app_views/blog.html",
            data: {
                pageTitle: 'Blog',
                pageDesc: 'Article board for blog page.'
            }
        })
        .state('blog_details', {
            url: "/blog_details",
            templateUrl: "views/app_views/blog_details.html",
            data: {
                pageTitle: 'Article',
                pageDesc: 'Article blog page.'
            }
        })
        .state('app_views.forum', {
            url: "/forum",
            templateUrl: "views/app_views/forum.html",
            data: {
                pageTitle: 'Forum',
                pageDesc: 'Topics board for forum page.'
            }
        })
        .state('app_views.forum_details', {
            url: "/forum_details",
            templateUrl: "views/app_views/forum_details.html",
            data: {
                pageTitle: 'Topic',
                pageDesc: 'Topic for forum page.'
            }
        })
        .state('app_views.gallery', {
            url: "/gallery",
            templateUrl: "views/app_views/gallery.html",
            data: {
                pageTitle: 'Galery',
                pageDesc: 'Touch-enabled, responsive and customizable image & video gallery.'
            }
        })
        .state('app_views.notes', {
            url: "/notes",
            templateUrl: "views/app_views/notes.html",
            data: {
                pageTitle: 'Notes',
                pageDesc: 'Build notebook functionality in your app'
            }
        })
        .state('app_views.mailbox', {
            url: "/mailbox",
            templateUrl: "views/app_views/mailbox.html",
            data: {
                pageTitle: 'Mailbox',
                pageDesc: 'Mailbox - Email list.'
            }
        })
        .state('app_views.faq', {
            url: "/faq",
            templateUrl: "views/app_views/faq.html",
            data: {
                pageTitle: 'FAQ',
                pageDesc: 'FAQ page - build faq/support page for your app'
            }
        })
        .state('app_views.email_compose', {
            url: "/email_compose",
            templateUrl: "views/app_views/email_compose.html",
            data: {
                pageTitle: 'Mailbox',
                pageDesc: 'Mailbox - Email compose.'
            }
        })
        .state('app_views.email_view', {
            url: "/email_view",
            templateUrl: "views/app_views/email_view.html",
            data: {
                pageTitle: 'Mailbox',
                pageDesc: 'Mailbox - Email view.'
            }
        })
        .state('app_views.invoice', {
            url: "/invoice",
            templateUrl: "views/app_views/invoice.html",
            data: {
                pageTitle: 'Invoice',
                pageDesc: 'Clean invoice template.'
            }
        })
        .state('app_views.file_manager', {
            url: "/mailbox",
            templateUrl: "views/app_views/file_manager.html",
            data: {
                pageTitle: 'File manager',
                pageDesc: 'Show you files in a nica manager design.'
            }
        })
        .state('app_views.search', {
            url: "/search",
            templateUrl: "views/app_views/search.html",
            data: {
                pageTitle: 'Search view',
                pageDesc: 'Use search view to show search functionality.'
            }
        })
        .state('app_views.chat_view', {
            url: "/chat_view",
            templateUrl: "views/app_views/chat_view.html",
            data: {
                pageTitle: 'Chat view',
                pageDesc: 'Create a chat room in your app'
            }
        })
        // Transitions
        .state('transitions', {
            abstract: true,
            url: "/transitions",
            templateUrl: "views/common/content_blank.html",
            data: {
                pageTitle: 'Transitions'
            }
        })
        .state('transitions.overview', {
            url: "/overview",
            templateUrl: "views/transitions/overview.html",
            data: {
                pageTitle: 'Overview of transitions Effect'
            }
        })
        .state('transitions.transition_one', {
            url: "/transition_one",
            templateUrl: "views/transitions/transition_one.html"
        })
        .state('transitions.transition_two', {
            url: "/transition_two",
            templateUrl: "views/transitions/transition_two.html"
        })
        .state('transitions.transition_three', {
            url: "/transition_three",
            templateUrl: "views/transitions/transition_three.html"
        })
        .state('transitions.transition_four', {
            url: "/transition_four",
            templateUrl: "views/transitions/transition_four.html"
        })
        .state('transitions.transition_five', {
            url: "/transition_five",
            templateUrl: "views/transitions/transition_five.html"
        })
        // Charts
        .state('charts', {
            abstract: true,
            url: "/charts",
            templateUrl: "views/common/content.html",
            data: {
                pageTitle: 'Charts'
            }
        })
        .state('charts.flot', {
            url: "/flot",
            templateUrl: "views/charts/flot.html",
            data: {
                pageTitle: 'Flot chart',
                pageDesc: 'Flot is a pure JavaScript plotting library for jQuery, with a focus on simple usage, attractive looks and interactive features.'
            }
        })
        .state('charts.chartjs', {
            url: "/chartjs",
            templateUrl: "views/charts/chartjs.html",
            data: {
                pageTitle: 'ChartJS',
                pageDesc: 'Simple HTML5 Charts using the canvas element'
            }
        })
        .state('charts.inline', {
            url: "/inline",
            templateUrl: "views/charts/inline.html",
            data: {
                pageTitle: 'Inline charts',
                pageDesc: 'Small inline charts directly in the browser using data supplied in the controller.'
            }
        })
        .state('charts.chartist', {
            url: "/chartist",
            templateUrl: "views/charts/chartist.html",
            data: {
                pageTitle: 'Chartist',
                pageDesc: 'Chartist.js is a simple responsive charting library built with SVG.'
            }
        })
        .state('charts.c3Charts', {
            url: "/c3Charts",
            templateUrl: "views/charts/c3Charts.html",
            data: {
                pageTitle: 'C3 Charts',
                pageDesc: 'D3-based reusable chart library'
            }
        })
        // Common views
        .state('common', {
            abstract: true,
            url: "/common",
            templateUrl: "views/common/content_empty.html",
            data: {
                pageTitle: 'Common'
            }
        })
        .state('common.login', {
            url: "/login",
            templateUrl: "views/common_app/login.html",
            data: {
                pageTitle: 'Login page',
                specialClass: 'blank'
            }
        })
        //subscription page
        .state('common.subscription',{
            url:"/subscription",
            templateUrl:"views/common_app/subscription.html",
            data:{
                pageTitle:'Subscription page',
                specialClass: 'blank'
            }
        })
        .state('common.userregistration',{
            url:"/userregistration",
            templateUrl:"views/common_app/userregistration.html",
            data:{
                pageTitle:'FreePlan',
                specialClass:'blank'
            }
        })
        .state('basicplan',{
            url:"/basicplan",
            templateUrl:"views/basicplan.html",
            data:{
                pageTitle:'BasicPlan'
            }
        })
        .state('stdplan',{
            url:"/stdplan",
            templateUrl:"views/stdplan.html",
            data:{
                pageTitle:'StandardPlan'
            }
        })
        .state('premiumplan',{
            url:"/premiumplan",
            templateUrl:"views/premiumplan.html",
            data:{
                pageTitle:'PremiumPlan'
            }
        })
        .state('prestigeplan',{
            url:"/prestigeplan",
            templateUrl:"views/prestigeplan.html",
            data:{
                pageTitle:"PrestigePlan"
            }
        })
         // for redirect page
        .state('redirectPage',{
            url:"/redirectPage",
            templateUrl:"views/redirectPage.html",
            data:{
                pageTitle:'Redirect Page'
               
            }
        }) 
        .state('updatedPage',{
            url:"/updatedPage",
            templateUrl:"views/updatedPage.html",
            data:{
                pageTitle:'Update Page',
                specialClass: 'blank'
            }
        }) 
        .state('common.register', {
            url: "/register/:id",
            templateUrl: "views/common_app/register.html",
            
            data: {
                pageTitle: 'Register page',
                specialClass: 'blank'
            }
        })
        .state('common.guest', {
            url: "/guest/:id",
            templateUrl: "views/common_app/guest.html",
            
            data: {
                pageTitle: 'Guest page',
                specialClass: 'blank'
            }
        })
        .state('common.forgotpassword',{
            url:"/forgotpassword",
            templateUrl:"views/common_app/forgotpassword.html",
            data:{
                pageTitle: 'Forgotpassword page',
                specialClass: 'blank'
            }
        })
        .state('common.error_one', {
            url: "/error_one",
            templateUrl: "views/common_app/error_one.html",
            data: {
                pageTitle: 'Error 404',
                specialClass: 'blank'
            }
        })
        .state('common.error_two', {
            url: "/error_two",
            templateUrl: "views/common_app/error_two.html",
            data: {
                pageTitle: 'Error 505',
                specialClass: 'blank'
            }
        })
        .state('common.lock', {
            url: "/lock",
            templateUrl: "views/common_app/lock.html",
            data: {
                pageTitle: 'Lock page',
                specialClass: 'blank'
            }
        })
        .state('common.password_recovery', {
            url: "/password_recovery",
            templateUrl: "views/common_app/password_recovery.html",
            data: {
                pageTitle: 'Password recovery',
                specialClass: 'blank'
            }
        })
        // Tables views
        .state('tables', {
            abstract: true,
            url: "/tables",
            templateUrl: "views/common/content.html",
            data: {
                pageTitle: 'Tables'
            }
        })
        .state('tables.tables_design', {
            url: "/tables_design",
            templateUrl: "views/tables/tables_design.html",
            data: {
                pageTitle: 'Tables design',
                pageDesc: 'Examples of various designs of tables.'
            }
        })
        .state('tables.ng_grid', {
            url: "/ng_grid",
            templateUrl: "views/tables/ng_grid.html",
            data: {
                pageTitle: 'ngGgrid',
                pageDesc: 'Examples of various designs of tables.'
            }
        })
        .state('tables.datatables', {
            url: "/datatables",
            templateUrl: "views/tables/datatables.html",
            data: {
                pageTitle: 'DataTables',
                pageDesc: 'Advanced interaction controls to any HTML table'
            }
        })
        .state('tables.footable', {
            url: "/footable",
            templateUrl: "views/tables/footable.html",
            data: {
                pageTitle: 'FooTable',
                pageDesc: 'Advanced interaction controls to any HTML table'
            }
        })
        // Forms views
        .state('forms', {
            abstract: true,
            url: "/forms",
            templateUrl: "views/common/content_small.html",
            data: {
                pageTitle: 'Forms'
            }
        })
        .state('forms.forms_elements', {
            url: "/forms_elements",
            templateUrl: "views/forms/forms_elements.html",
            data: {
                pageTitle: 'Forms elements',
                pageDesc: 'Examples of various form controls.'
            }
        })
        .state('forms.forms_extended', {
            url: "/forms_extended",
            templateUrl: "views/forms/forms_extended.html",
            data: {
                pageTitle: 'Forms extended',
                pageDesc: 'Examples of various extended form controls.'
            }
        })
        .state('forms.text_editor', {
            url: "/text_editor",
            templateUrl: "views/forms/text_editor.html",
            data: {
                pageTitle: 'Text editor',
                pageDesc: 'Examples of text editor.'
            }
        })
        .state('forms.wizard', {
            url: "/wizard",
            templateUrl: "views/forms/wizard.html",
            data: {
                pageTitle: 'Wizard',
                pageDesc: 'Build a form with wizard functionality.'
            }
        })
        .state('forms.validation', {
            url: "/validation",
            templateUrl: "views/forms/validation.html",
            data: {
                pageTitle: 'Validation',
                pageDesc: 'Build a form with validation functionality.'
            }
        })
        // Grid system
        .state('grid_system', {
            url: "/grid_system",
            templateUrl: "views/grid_system.html",
            data: {
                pageTitle: 'Grid system'
            }
        })
        //Abhay added this for landing page   
        .state('userManagement', {
            url: "/userManagement",
            templateUrl: "views/userManagement.html",
            data: {
                pageTitle: 'User Listing'
            }
        })
          .state('apiDocumentation', {
            url: "/apiDocumentation",
            templateUrl: "views/apiDocumentation.html",
            data: {
                pageTitle: 'Api apiDocumentation'
            },
            controller:'apiDocumentationCtrl'
        })
      // mamatha added this for Add User
        .state('adduser', {
            url: "/adduser",
            templateUrl: "views/adduser.html",
            data: {
                pageTitle: 'Add User'
            }
        })
    // mamatha added this for client listing
          .state('clientManagement', {
            url: "/clientManagement",
            templateUrl: "views/clientManagement.html",
            data: {
                pageTitle: 'Client Management'
            },
            controller:'clientManagementCtrl',
            resolve:{
                TenantSpace:function(TenantFactory){
                    return TenantFactory.remainingSize(function(data){
                        return data;
                    }).$promise;
                }
            }

        })
          // mamatha added this for Add client
          .state('addClient', {
            url: "/addClient",
            templateUrl: "views/addClient.html",
            data: {
                pageTitle: 'Add Client'
            },
            controller:'clientManagementCtrl',
            resolve:{
                TenantSpace:function(TenantFactory){
                    return TenantFactory.remainingSize(function(data){
                        console.log(data);
                        return data;
                    }).$promise;
                }
            }
        })

        .state('GroupList', {
            url: "/GroupList",
            templateUrl: "views/GroupList.html",
            data: {
                pageTitle: 'Group Listing'
            }
        })
        .state('UserGroup', {
            url: "/UserGroup",
            templateUrl: "views/UserGroup.html",
            data: {
                pageTitle: 'User Group'
            }
        })
        .state('InviteGroupUser', {
            url: "/InviteGroupUser",
            templateUrl: "views/InviteGroupUser.html",
            data: {
                pageTitle: 'Add User To Group'
            }
        })
        // for report
        .state('report',{
            url:"/report",
            templateUrl:"views/report.html",
            data:{
                pageTitle:'Reports'
            }
        })



// for redit
        .state('redit',{
            url:"/redit",
            templateUrl:"views/redit.html",
            data:{
                pageTitle:'Reports'
            }
        })


        .state('profile', {
            url: "/profile",
            templateUrl: "views/profile.html",
            data: {
                pageTitle: 'Profile'
                // pageDesc: 'Show user data in clear profile design'
            }
        })

        .state('versionForm',{
            url:"/versionForm",
            templateUrl:"views/versionForm.html",
            data:{
                pageTitle:'Version'
            }
        })        

}

angular
    .module('homer')
    .config(configState)
    .run(function($rootScope, $state, editableOptions,Auth,$location) {

        $rootScope.$state = $state;
        editableOptions.theme = 'bs3';
          //function to validate login status and domain validity   
       $rootScope.$on('$stateChangeStart', function (event, next) {

      Auth.isLoggedInAsync(function(loggedIn) {
        Auth.checkDomain(function(validDomain){
            var host = $location.host();
          if ( !loggedIn && validDomain) {
            event.preventDefault();
          if($state.current.name!="common.forgotpassword" && $state.current.name!="common.register" && $state.current.name!="common.guest" && $state.current.name!="apiDocumentation" ){
           $location.path('common.login');
          }
        // else  if($state.current.name!="common.register"){     
        //     $location.path('common.login');
        //   }
          
        }else if( !loggedIn && !validDomain){
            console.log("not valid domain",host[0]);
            //to register a new domain
            if(host.split('.')[0] == "registration"){
                event.preventDefault();
                $location.path('/common/userregistration');
            }else{
               event.preventDefault();
               $location.path('/common/error_two');
            }
            
          //$state.go('common.error_two');
        }
        });
        
      });
    });
    }).value('url', 'https://revampapi.2docstore.com/');

    //}).value('url', 'http://localhost:9000/');

/**
 *
 * propsFilter
 *
 */

angular
    .module('homer')
    .filter('propsFilter', propsFilter)

function propsFilter(){
    return function(items, props) {
        var out = [];

        if (angular.isArray(items)) {
            items.forEach(function(item) {
                var itemMatches = false;

                var keys = Object.keys(props);
                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    }
}
/**
 * HOMER - Responsive Admin Theme
 * Copyright 2015 Webapplayers.com
 *
 */

angular
    .module('homer')
    .directive('pageTitle', pageTitle)
    .directive('sideNavigation', sideNavigation)
    .directive('minimalizaMenu', minimalizaMenu)
    .directive('sparkline', sparkline)
    .directive('icheck', icheck)
    .directive('panelTools', panelTools)
    .directive('panelToolsFullscreen', panelToolsFullscreen)
    .directive('smallHeader', smallHeader)
    .directive('animatePanel', animatePanel)
    .directive('landingScrollspy', landingScrollspy)
    .directive('clockPicker', clockPicker)
    .directive('dateTimePicker', dateTimePicker)
    .directive('noItems',noItems)


/**
 * pageTitle - Directive for set Page title - mata title
 */
function pageTitle($rootScope, $timeout) {
    return {
        link: function(scope, element) {
            var listener = function(event, toState, toParams, fromState, fromParams) {
                // Default title
                var title = '2DocStore | Safe and Secure';
                // Create your own title pattern
                if (toState.data && toState.data.pageTitle) title = '2DocStore | ' + toState.data.pageTitle;
                $timeout(function() {
                    element.text(title);
                });
            };
            $rootScope.$on('$stateChangeStart', listener);
        }
    }
};

/**
 * sideNavigation - Directive for run metsiMenu on sidebar navigation
 */
function sideNavigation($timeout){
    return {
        restrict: 'A',
        controller:'navigationCtrl',
        link: function(scope, element) {
            // Call the metsiMenu plugin and plug it to sidebar navigation
            element.metisMenu();

            // Colapse menu in mobile mode after click on element
            var menuElement = $('#side-menu a:not([href$="\\#"])');
            menuElement.click(function(){

                if ($(window).width() < 769) {
                    $("body").toggleClass("show-sidebar");
                }
            });
        }
    };
};

/**
 * minimalizaSidebar - Directive for minimalize sidebar
 */
function minimalizaMenu($rootScope) {
    return {
        restrict: 'EA',
        template: '<div class="header-link hide-menu" ng-click="minimalize()"><i class="fa fa-bars"></i></div>',
        controller: function ($scope, $element) {

            $scope.minimalize = function () {
            if ($(window).width() < 769) {
                    $("body").toggleClass("show-sidebar");
                } else {
                    $("body").toggleClass("hide-sidebar");
                }
            }
        }
    };
};

/** * minimalizaSidebar - Directive for minimalize sidebar
 */
function noItems() {
    return {
        restrict: 'EA',
        scope: {
             title: '@'
        },
        template: '<h2>No {{title}} to display, please click on Add {{title}}</h2>',
        //templateUrl: 'views/common/noResult.html',
        controller: function ($scope, $element) {
            console.log($scope.title)
        }
    };
};

/**
 * sparkline - Directive for Sparkline chart
 */
function sparkline() {
    return {
        restrict: 'A',
        scope: {
            sparkData: '=',
            sparkOptions: '=',
        },
        link: function (scope, element, attrs) {
            scope.$watch(scope.sparkData, function () {
                render();
            });
            scope.$watch(scope.sparkOptions, function(){
                render();
            });
            var render = function () {
                $(element).sparkline(scope.sparkData, scope.sparkOptions);
            };
        }
    }
};

/**
 * icheck - Directive for custom checkbox icheck
 */
function icheck($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function($scope, element, $attrs, ngModel) {
            return $timeout(function() {
                var value;
                value = $attrs['value'];

                $scope.$watch($attrs['ngModel'], function(newValue){
                    $(element).iCheck('update');
                })

                return $(element).iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green'

                }).on('ifChanged', function(event) {
                        if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                            $scope.$apply(function() {
                                return ngModel.$setViewValue(event.target.checked);
                            });
                        }
                        if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                            return $scope.$apply(function() {
                                return ngModel.$setViewValue(value);
                            });
                        }
                    });
            });
        }
    };
}


/**
 * panelTools - Directive for panel tools elements in right corner of panel
 */
function panelTools($timeout) {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'views/common/panel_tools.html',
        controller: function ($scope, $element) {
            // Function for collapse ibox
            $scope.showhide = function () {
                var hpanel = $element.closest('div.hpanel');
                var icon = $element.find('i:first');
                var body = hpanel.find('div.panel-body');
                var footer = hpanel.find('div.panel-footer');
                body.slideToggle(300);
                footer.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                hpanel.toggleClass('').toggleClass('panel-collapse');
                $timeout(function () {
                    hpanel.resize();
                    hpanel.find('[id^=map-]').resize();
                }, 50);
            },

            // Function for close ibox
            $scope.closebox = function () {
                var hpanel = $element.closest('div.hpanel');
                hpanel.remove();
            }

        }
    };
};

/**
 * panelToolsFullscreen - Directive for panel tools elements in right corner of panel with fullscreen option
 */
function panelToolsFullscreen($timeout) {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'views/common/panel_tools_fullscreen.html',
        controller: function ($scope, $element) {
            // Function for collapse ibox
            $scope.showhide = function () {
                var hpanel = $element.closest('div.hpanel');
                var icon = $element.find('i:first');
                var body = hpanel.find('div.panel-body');
                var footer = hpanel.find('div.panel-footer');
                body.slideToggle(300);
                footer.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                hpanel.toggleClass('').toggleClass('panel-collapse');
                $timeout(function () {
                    hpanel.resize();
                    hpanel.find('[id^=map-]').resize();
                }, 50);
            };

            // Function for close ibox
            $scope.closebox = function () {
                var hpanel = $element.closest('div.hpanel');
                hpanel.remove();
                if($('body').hasClass('fullscreen-panel-mode')) { $('body').removeClass('fullscreen-panel-mode');}
            };

            // Function for fullscreen
            $scope.fullscreen = function () {
                var hpanel = $element.closest('div.hpanel');
                var icon = $element.find('i:first');
                $('body').toggleClass('fullscreen-panel-mode');
                icon.toggleClass('fa-expand').toggleClass('fa-compress');
                hpanel.toggleClass('fullscreen');
                setTimeout(function() {
                    $(window).trigger('resize');
                }, 100);
            }

        }
    };
};

/**
 * smallHeader - Directive for page title panel
 */
function smallHeader() {
    return {
        restrict: 'A',
        scope:true,
        controller: function ($scope, $element) {
            $scope.small = function() {
                var icon = $element.find('i:first');
                var breadcrumb  = $element.find('#hbreadcrumb');
                $element.toggleClass('small-header');
                breadcrumb.toggleClass('m-t-lg');
                icon.toggleClass('fa-arrow-up').toggleClass('fa-arrow-down');
            }
        }
    }
}

function animatePanel($timeout,$state) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            //Set defaul values for start animation and delay
            var startAnimation = 0;
            var delay = 0.06;   // secunds
            var start = Math.abs(delay) + startAnimation;

            // Store current state where directive was start
            var currentState = $state.current.name;

            // Set default values for attrs
            if(!attrs.effect) { attrs.effect = 'zoomIn'};
            if(attrs.delay) { delay = attrs.delay / 10 } else { delay = 0.06 };
            if(!attrs.child) { attrs.child = '.row > div'} else {attrs.child = "." + attrs.child};

            // Get all visible element and set opactiy to 0
            var panel = element.find(attrs.child);
            panel.addClass('opacity-0');

            // Count render time
            var renderTime = panel.length * delay * 1000 + 700;

            // Wrap to $timeout to execute after ng-repeat
            $timeout(function(){

                // Get all elements and add effect class
                panel = element.find(attrs.child);
                panel.addClass('stagger').addClass('animated-panel').addClass(attrs.effect);

                var panelsCount = panel.length + 10;
                var animateTime = (panelsCount * delay * 10000) / 10;

                // Add delay for each child elements
                panel.each(function (i, elm) {
                    start += delay;
                    var rounded = Math.round(start * 10) / 10;
                    $(elm).css('animation-delay', rounded + 's');
                    // Remove opacity 0 after finish
                    $(elm).removeClass('opacity-0');
                });

                // Clear animation after finish
                $timeout(function(){
                    $('.stagger').css('animation', '');
                    $('.stagger').removeClass(attrs.effect).removeClass('animated-panel').removeClass('stagger');
                    panel.resize();
                }, animateTime)

            });



        }
    }
}

function landingScrollspy(){
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.scrollspy({
                target: '.navbar-fixed-top',
                offset: 80
            });
        }
    }
}

/**
 * clockPicker - Directive for clock picker plugin
 */
function clockPicker() {
    return {
        restrict: 'A',
        link: function(scope, element) {
            element.clockpicker();
        }
    };
};

function dateTimePicker(){
    return {
        require: '?ngModel',
        restrict: 'AE',
        scope: {
            pick12HourFormat: '@',
            language: '@',
            useCurrent: '@',
            location: '@'
        },
        link: function (scope, elem, attrs) {
            elem.datetimepicker({
                pick12HourFormat: scope.pick12HourFormat,
                language: scope.language,
                useCurrent: scope.useCurrent
            })

            //Local event change
            elem.on('blur', function () {

                // For test
                //console.info('this', this);
                //console.info('scope', scope);
                //console.info('attrs', attrs);


                /*// returns moments.js format object
                 scope.dateTime = new Date(elem.data("DateTimePicker").getDate().format());
                 // Global change propagation
                 $rootScope.$broadcast("emit:dateTimePicker", {
                 location: scope.location,
                 action: 'changed',
                 dateTime: scope.dateTime,
                 example: scope.useCurrent
                 });
                 scope.$apply();*/
            })
        }
    };
}


//version directive




/**
 * 15th Feb 2017
 * Directives for subscription plans
 * Chaithra
 * modified date : 
 */
angular.module('homer')
.directive('basicplan',basicplan)
.directive('standardplan',standardplan)
.directive('premiumplan',premiumplan)
.directive('prestigeplan',prestigeplan)

/*To display Basic Plan */

function basicplan(){
	return{
			templateUrl:'views/basicplan.html',
			controller:'subscriptionCtrl',
			restrict:'E',
			link:function($scope,$element,$attrs){

			}
	}
}

/*To display Standard Plan */
function standardplan(){
	return{
			templateUrl:'views/stdplan.html',
			controller:'subscriptionCtrl',
			restrict:'E',
			link:function($scope,$element,$attrs){

			}
	}
}

/*To Display Premium Plan*/
function premiumplan(){
	return{
		templateUrl:'views/premiumplan.html',
		controller:'subscriptionCtrl',
		restrict:'E',
		link:function($scope,$element,$attrs){

		}
	}
}

/*To display Prestige plan*/
function prestigeplan(){
	return{
		templateUrl:'views/prestigeplan.html',
		controller:'subscriptionCtrl',
		restrict:'E',
		link:function($scope,$element,$attrs){

		}
	}
}
/**
 *
 * appCtrl
 *
 */

angular
    .module('homer')
    .controller('appCtrl', appCtrl);

function appCtrl($http, $scope, $timeout) {

    // For iCheck purpose only
    $scope.checkOne = true;

    /**
     * Sparkline bar chart data and options used in under Profile image on left navigation panel
     */

    $scope.barProfileData = [5, 6, 7, 2, 0, 4, 2, 4, 5, 7, 2, 4, 12, 11, 4];
    $scope.barProfileOptions = {
        type: 'bar',
        barWidth: 7,
        height: '30px',
        barColor: '#62cb31',
        negBarColor: '#53ac2a'
    };
    $scope.chartIncomeData = [
        {
            label: "line",
            data: [ [1, 10], [2, 26], [3, 16], [4, 36], [5, 32], [6, 51] ]
        }
    ];

    $scope.chartIncomeOptions = {
        series: {
            lines: {
                show: true,
                lineWidth: 0,
                fill: true,
                fillColor: "#64cc34"

            }
        },
        colors: ["#62cb31"],
        grid: {
            show: false
        },
        legend: {
            show: false
        }
    };

    /**
     * Tooltips and Popover - used for tooltips in components view
     */
    $scope.dynamicTooltip = 'Hello, World!';
    $scope.htmlTooltip = "I\'ve been made <b>bold</b>!";
    $scope.dynamicTooltipText  = 'Dynamic';
    $scope.dynamicPopover = 'Hello, World!';
    $scope.dynamicPopoverTitle = 'Title';

    /**
     * Pagination - used for pagination in components view
     */
    $scope.totalItems = 64;
    $scope.currentPage = 4;

    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    /**
     * Typehead - used for typehead in components view
     */
    $scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Dakota', 'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];
    // Any function returning a promise object can be used to load values asynchronously
    $scope.getLocation = function(val) {
        return $http.get('http://maps.googleapis.com/maps/api/geocode/json', {
            params: {
                address: val,
                sensor: false
            }
        }).then(function(response){
                return response.data.results.map(function(item){
                    return item.formatted_address;
                });
            });
    };

    /**
     * Rating - used for rating in components view
     */
    $scope.rate = 7;
    $scope.max = 10;

    $scope.hoveringOver = function(value) {
        $scope.overStar = value;
        $scope.percent = 100 * (value / this.max);
    };

    /**
     * groups - used for Collapse panels in Tabs and Panels view
     */
    $scope.groups = [
        {
            title: 'Dynamic Group Header - 1',
            content: 'A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. '
        },
        {
            title: 'Dynamic Group Header - 2',
            content: 'A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. '
        }
    ];

    $scope.oneAtATime = true;

    /**
     * Some Flot chart data and options used in Dashboard
     */

    var data1 = [ [0, 55], [1, 48], [2, 40], [3, 36], [4, 40], [5, 60], [6, 50], [7, 51] ];
    var data2 = [ [0, 56], [1, 49], [2, 41], [3, 38], [4, 46], [5, 67], [6, 57], [7, 59] ];

    $scope.chartUsersData = [data1, data2];
    $scope.chartUsersOptions = {
        series: {
            splines: {
                show: true,
                tension: 0.4,
                lineWidth: 1,
                fill: 0.4
            },
        },
        grid: {
            tickColor: "#f0f0f0",
            borderWidth: 1,
            borderColor: 'f0f0f0',
            color: '#6a6c6f'
        },
        colors: [ "#62cb31", "#efefef"],
    };


    /**
     * Some Pie chart data and options
     */

    $scope.PieChart = {
        data: [1, 5],
        options: {
            fill: ["#62cb31", "#edf0f5"]
        }
    };

    $scope.PieChart2 = {
        data: [226, 360],
        options: {
            fill: ["#62cb31", "#edf0f5"]
        }
    };
    $scope.PieChart3 = {
        data: [0.52, 1.561],
        options: {
            fill: ["#62cb31", "#edf0f5"]
        }
    };
    $scope.PieChart4 = {
        data: [1, 4],
        options: {
            fill: ["#62cb31", "#edf0f5"]
        }
    };
    $scope.PieChart5 = {
        data: [226, 134],
        options: {
            fill: ["#62cb31", "#edf0f5"]
        }
    };
    $scope.PieChart6 = {
        data: [0.52, 1.041],
        options: {
            fill: ["#62cb31", "#edf0f5"]
        }
    };

    $scope.BarChart = {
        data: [5, 3, 9, 6, 5, 9, 7, 3, 5, 2],
        options: {
            fill: ["#dbdbdb", "#62cb31"],
        }
    };

    $scope.LineChart = {
        data: [5, 9, 7, 3, 5, 2, 5, 3, 9, 6, 5, 9, 4, 7, 3, 2, 9, 8, 7, 4, 5, 1, 2, 9, 5, 4, 7],
        options: {
            fill: '#62cb31',
            stroke: '#62cb31',
            width: 64
        }
    };


    $scope.stanimation = 'bounceIn';
    $scope.runIt = true;
    $scope.runAnimation = function(){

        $scope.runIt = false;
        $timeout(function(){
            $scope.runIt = true;
        }, 100)

    };

}

/**
 * HOMER - Responsive Admin Theme
 * Copyright 2015 Webapplayers.com
 *
 * Sweet Alert Directive
 * Official plugin - http://tristanedwards.me/sweetalert
 * Angular implementation inspiring by https://github.com/oitozero/ngSweetAlert
 */


function sweetAlert($timeout, $window) {
    var swal = $window.swal;
    return {
        swal: function (arg1, arg2, arg3) {
            $timeout(function () {
                if (typeof(arg2) === 'function') {
                    swal(arg1, function (isConfirm) {
                        $timeout(function () {
                            arg2(isConfirm);
                        });
                    }, arg3);
                } else {
                    swal(arg1, arg2, arg3);
                }
            }, 200);
        },
        success: function (title, message) {
            $timeout(function () {
                swal(title, message, 'success');
            }, 200);
        },
        error: function (title, message) {
            $timeout(function () {
                swal(title, message, 'error');
            }, 200);
        },
        warning: function (title, message) {
            $timeout(function () {
                swal(title, message, 'warning');
            }, 200);
        },
        info: function (title, message) {
            $timeout(function () {
                swal(title, message, 'info');
            }, 200);
        }

    };
};

/**
 * Pass function into module
 */
angular
    .module('homer')
    .factory('sweetAlert', sweetAlert)

/**
 *
 * alertsCtrl
 *
 */

angular
    .module('homer')
    .controller('alertsCtrl', alertsCtrl)

function alertsCtrl($scope, sweetAlert, notify) {

    $scope.demo1 = function () {
        sweetAlert.swal({
            title: "Welcome in Alerts",
            text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
        });
    }

    $scope.demo2 = function () {
        sweetAlert.swal({
            title: "Good job!",
            text: "You clicked the button!",
            type: "success"
        });
    }

    $scope.demo3 = function () {
        sweetAlert.swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!"
            },
            function () {
                sweetAlert.swal("Booyah!");
            });
    }

    $scope.demo4 = function () {
        sweetAlert.swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: false },
            function (isConfirm) {
                if (isConfirm) {
                    sweetAlert.swal("Deleted!", "Your imaginary file has been deleted.", "success");
                } else {
                    sweetAlert.swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
    }

    $scope.msg = 'Hello! This is a sample message!';
    $scope.demo = function () {
        notify({
            message: $scope.msg,
            classes: $scope.classes,
            templateUrl: $scope.template,
        });
    };
    $scope.closeAll = function () {
        notify.closeAll();
    };

    $scope.homerTemplate = 'views/notification/notify.html';
    $scope.homerDemo1 = function(){
        notify({ message: 'Info - This is a Homer info notification', classes: 'alert-info', templateUrl: $scope.homerTemplate});
    }
    $scope.homerDemo2 = function(){
        notify({ message: 'Success - This is a Homer success notification', classes: 'alert-success', templateUrl: $scope.homerTemplate});
    }
    $scope.homerDemo3 = function(){
        notify({ message: 'Warning - This is a Homer warning notification', classes: 'alert-warning', templateUrl: $scope.homerTemplate});
    }
    $scope.homerDemo4 = function(){
        notify({ message: 'Danger - This is a Homer danger notification', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
    }

}
/**
 *
 * modalCtrl
 *
 */

 angular
 .module('homer')
 .controller('modalCtrl', modalCtrl)

 function modalCtrl($scope,$uibModal) {

/*getting the url from dashboard controller to here

*/

$scope.open = function () {

    var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modal_example.html',
        controller: ModalInstanceCtrl,
    });
};

$scope.open1 = function () {
    var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modal_example1.html',
        controller: ModalInstanceCtrl
    });
};

$scope.open3 = function (size) {
    var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modal_example3.html',
        size: size,
        controller: ModalInstanceCtrl,
    });
};

$scope.open2 = function () {
    var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modal_example2.html',
        controller: ModalInstanceCtrl,
        windowClass: "hmodal-info"
    });
};

$scope.open4 = function () {
    var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modal_example2.html',
        controller: ModalInstanceCtrl,
        windowClass: "hmodal-warning"
    });
};

$scope.open5 = function () {
    var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modal_example2.html',
        controller: ModalInstanceCtrl,
        windowClass: "hmodal-success"
    });
};

$scope.open6 = function () {
    var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modal_example2.html',
        controller: ModalInstanceCtrl,
        windowClass: "hmodal-danger"
    });
};
};

function ModalInstanceCtrl ($scope, $uibModalInstance,$rootScope,docsCtrlVariables,$moment,
    Auth,DefaultDocs,notify,blockUI,GlobalService,ActivitylogService,Groups,timelineService,
    activityFcrt,sweetAlert,$uibModalInstance,$uibModal) {
    blockUI.stop();
    console.log("hi");
     $scope.homerTemplate = 'views/notification/notify.html';
    var activity_type = GlobalService.getActivitytype();
    console.log($rootScope.documentDetails);
    $rootScope.allowMoadlOpen=true;

    $scope.check = function () {
        $uibModalInstance.dismiss('cancel');
    };

$scope.showcomments=false;
/*chaithra here*/
$scope.navigate=function(data){
    if(data==true){
        $scope.showcomments=false;

    }else{
        $scope.showcomments=true;

    }
    
}

$scope.checkComments=function(data){
    if(data==false){
        return "col-md-12 col-sm-12 col-xs-12 col-lg-12"
    
        
    }else{
        return "col-md-9 col-sm-9 col-xs-9 col-lg-9"
    }
}

    // $scope.embedUrl="https://docs.google.com/viewer?embedded=true&url=" + $rootScope.viewurl;
    // $scope.embedUrl="https://docs.google.com/viewer?embedded=true&url=" + $rootScope.viewurl;
    

var IndivisualDoc=$rootScope.documentHeadingId; 
$scope.fileDec=IndivisualDoc.description;
$scope.commentInfo=IndivisualDoc.createdBy;
$scope.Comment=IndivisualDoc.comments;

/*To get current user*/
if(!$rootScope.isguestuser)
{
    $scope.getCurrentUser = Auth.getCurrentUser;
$scope.getCurrentUser().$promise.then(function() {
        $rootScope.loggedIn = $scope.getCurrentUser();
    });
}

    

$scope.checkifImageExists=function(user,image){
        console.log(user,image);
    user.name=user.name;
    docsCtrlVariables.checkForImage(user,image);
};  

$scope.ok = function () {
    $uibModalInstance.close();
};

$scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
};

/*
function to set favourite
*/
$scope.setFavourite=function(document)
{
    console.log("to set fav",$rootScope.documentDetails,document);
    if($rootScope.documentDetails.documentName==document){    
        
        if($rootScope.documentDetails.isLocked.status == true && $rootScope.documentDetails.isLocked.user_id!=$rootScope.loggedIn._id){

            notify({ message: 'Warning - Document Locked ' ,classes: 'alert-danger', templateUrl: $scope.homerTemplate});
        }
        else{
            if ($rootScope.documentDetails == undefined || $rootScope.documentDetails.isFavourite == undefined || $rootScope.documentDetails.isFavourite.status == undefined) {
                $rootScope.documentDetails.isFavourite.status = true;
                return;
            }

            if ($rootScope.documentDetails.isFavourite.status) {
                //modify doc to include islocked as true
                var obj = {
                    user_id : $scope.getCurrentUser()._id,
                    status : false
                };
                $rootScope.documentDetails.isFavourite.status = false;
                DefaultDocs.favroite($rootScope.documentDetails, function(data) {
                    $rootScope.documentDetails.isFavourite.status = false;
                    var unfavLog = {};
                    unfavLog.activitytype = activity_type.UNFAV;
                    unfavLog.what = $rootScope.documentDetails;
                    unfavLog.location = $rootScope.documentDetails.s3_path;
                    unfavLog.status = true;
                    ActivitylogService.LogActivity(unfavLog);
                    $rootScope.documentDetails.favImage = false
                })

                
            } else {
                //modify doc to include islocked as false
                var obj = {
                    user_id : $scope.getCurrentUser()._id,
                    status : true
                };
                $rootScope.documentDetails.isFavourite.status = true;
                DefaultDocs.favroite($rootScope.documentDetails, function(res) {
                    
                    $rootScope.documentDetails.isFavourite.status = true;
                    //from activity service
                    var favLog = {};
                    favLog.activitytype = activity_type.FAV;
                    favLog.what = $rootScope.documentDetails;
                    favLog.location = $rootScope.documentDetails.s3_path;
                    favLog.status = true;
                    ActivitylogService.LogActivity(favLog);
                    $rootScope.documentDetails.favImage = true;
                    $rootScope.documentDetails.favName = $scope.loggedIn.name
                });
                
            }
        }
    }else{
        console.log("diff docs");
    }
    
}

/*to set lock/unlock*/

$scope.findlock = function(document) {
        console.log($rootScope.documentDetails.isLocked.status , $rootScope.documentDetails.isLocked.user_id,$rootScope.loggedIn._id)
    if($rootScope.documentDetails.documentName==document){  
        //check if the document object is valid. if not unlock and return.
        // $scope.checkForPermission($rootScope.documentDetails,function(){

                if ($rootScope.documentDetails == undefined || $rootScope.documentDetails.isLocked == undefined || $rootScope.documentDetails.isLocked.status == undefined) {
                    
            /*      document.isLocked.status=true;
                    document.isLocked.user_id=$rootScope.loggedIn._id;*/
                    $rootScope.documentDetails.isLocked.lockImage=true
                    return;
                }

                if ($rootScope.documentDetails.allowLock){
                    if ($rootScope.documentDetails.isLocked.status && $rootScope.documentDetails.isLocked.user_id==$rootScope.loggedIn._id ) {
                        //modify doc to include islocked as true
                        // var obj={
                        //  status:false
                        // };
                        $rootScope.documentDetails.isLocked.status = false;
                    
                        
                        DefaultDocs.locked($rootScope.documentDetails, function(data) {
                            $rootScope.documentDetails.lockImage=false;
                                var lockLog = {};
                            lockLog.activitytype = activity_type.UNLOCK;
                            lockLog.what = $rootScope.documentDetails._id;
                            lockLog.location = $rootScope.documentDetails.s3_path;
                            lockLog.status = true;
                            ActivitylogService.LogActivity(lockLog);
                            
                        });
                    } else {
                        //modify doc to include islocked as false
                        // var obj={
                        //  user_id:$scope.getCurrentUser()._id,
                        //  status:true
                        // };

                        $rootScope.documentDetails.isLocked.status = true;
                        DefaultDocs.locked($rootScope.documentDetails, function(res) {
                            $rootScope.documentDetails.isLocked.status=true;
                            $rootScope.documentDetails.isLocked.user_id=$rootScope.loggedIn._id;
                            var unlockLog = {};
                            unlockLog.activitytype = activity_type.LOCK;
                            unlockLog.what = $rootScope.documentDetails._id;
                            unlockLog.location = $rootScope.documentDetails.s3_path;
                            unlockLog.status = true;
                            ActivitylogService.LogActivity(unlockLog);
                            $rootScope.documentDetails.lockImage=true;
                            $rootScope.documentDetails.lockedName =$scope.loggedIn.name
                        },function(err){
                        notify({ message: 'Warning - Document Locked by ' +$rootScope.documentDetails.lockedName ,classes: 'alert-danger', templateUrl: $scope.homerTemplate});
                        });
                    }
                } else {
                    var lockFile = $rootScope.documentDetails.documentName;
                    //swal("You don't have persmission to lock " + lockFile);
                    sweetAlert.swal("Error","You don't have persmission to lock/unlock " + lockFile,"error");
                }
            // })
    }else{
        console.log("diff doc");
        };
}


/*to give permission*/    
    $scope.givePermission = function(value) {
        if (value == "deny")
            return false;
        else
            return true;
    };
/*
*function to open timeline modal
*/
$scope.setTimeline=function(doc){
  if($rootScope.documentDetails.documentName==doc){      
    timelineService.setTimeline($rootScope.documentDetails);
    }else{
        console.log("diff docs");
    }
}   

/*
for hiding contribs
*/
var whichDoc=docsCtrlVariables.getWhichDocument();

$scope.checkIfSharedDoc=function(){
    var bread = docsCtrlVariables.getbreadcrumbs();
    if(bread.length==0 && whichDoc=="shared")
        return true
    else
        return false
}

$scope.checkIfFavDoc=function(){
    var bread = docsCtrlVariables.getbreadcrumbs();
    if(bread.length==0 && whichDoc=="fav")
        return true
    else
        return false
}
$scope.checkIfSearchDoc=function(){
var bread = docsCtrlVariables.getbreadcrumbs();
    if(bread.length==0 && whichDoc=="search")
        return true
    else
        return false
}
$scope.checkIfClientDoc=function(){
    var bread = docsCtrlVariables.getbreadcrumbs();
    if(bread.length==1 && whichDoc=="Client Documents")
        return true
    else
        return false
} 
/*
*function Add new version
*params:Doc object
*/
$scope.addNewVersion=function(file,doc){
    if($rootScope.documentDetails.documentName==doc){  
        var allowVersion=false;
        if($rootScope.documentDetails.isLocked.status==false)
        {
            allowVersion=true;
        }
        else if($rootScope.documentDetails.isLocked.user_id==$rootScope.loggedIn._id)
        {
        allowVersion=true;
        }
        else{
        allowVersion=false; 
        }
        if(allowVersion){
        if($rootScope.documentDetails.documentName==file[0].name){
            $rootScope.documentDetails.versionuploadflag=true;
            $scope.versionuploading=true;
            $scope.uploadFiles(file);
        }
        else{
            notify({ message: 'A new Version can be uploaded only if it has same name', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
        }
        }
        else{
                notify({ message: 'Document is locked '+ $rootScope.documentDetails.isLockedtoName, classes: 'alert-danger', templateUrl: $scope.homerTemplate});

        }
    }else{
        console.log("not matched");
    }
};

/*
Function name upload
@param: file names
*/
$scope.uploadFiles=function(files)//
{
    // console.log(files);
     // blockUI.start();
    $scope.showNoFiles=false;
    if($scope.versionuploading ==true)
    {
        $scope.hiderow=files;
        $scope.uplaodwhatFile=$scope.hiderow;
    }
    else
    {
        $scope.showUploadRow=files; 
        $scope.uplaodwhatFile=$scope.showUploadRow;
    }
        docsCtrlVariables.uploadFiles($scope.uplaodwhatFile,$scope.showNoFiles,$scope.versionuploading);
         // blockUI.stop();
         // console.log($scope.showNoFiles);
    };


 /*FunctionName: Download
@Params:docuemnt object
Returns: URL to start download*/
$scope.download=function(document,version){
    //console.log("vers",version);
    if($rootScope.documentDetails.documentName==document){
        // $scope.checkForPermission($rootScope.documentDetails,function(){
                if($rootScope.documentDetails.isLocked.user_id==$rootScope.loggedIn._id || !$rootScope.documentDetails.isLocked.status)
                {

                    if($rootScope.documentDetails.allowDownload){
                    var current = docsCtrlVariables.getCurrentDocument();   
                    var file_id = {
                                id : $rootScope.documentDetails._id
                            };
                            if (version){
                                file_id.version=version;
                            }
                            DefaultDocs.download(file_id, function(result) {
                                location.href = result.url;
                            var downloadLog = {};
                            downloadLog.activitytype = activity_type.DOWNLOAD;
                            downloadLog.what =  $rootScope.documentDetails._id;
                            downloadLog.location = current.parent + '/' + current.documentName;
                            downloadLog.status = true;
                            ActivitylogService.LogActivity(downloadLog);
                            //from activity service
                            $scope.downloadLog = activityFcrt.save(downloadLog, function(data) {
                            });
                            }, function(err) {
                                notify({ message: 'Warning - Please Enter Valid Username or Password', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
                       
                            });
                        }
                        else{
                            notify({ message: 'Error - You do not have permission to download '+$rootScope.documentDetails.documentName , classes: 'alert-danger', templateUrl: $scope.homerTemplate});

                        }
                    }
                    else{
                            notify({ message: 'Error - '+$rootScope.documentDetails.documentName +' is Locked' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
                    }
         // });
     }else{
            console.log("diff docs");
        }
}
/*
functionName:deleteFolder
*/
$scope.deleteFolder=function(document){
    if($rootScope.documentDetails.documentName==document){
//  // //console.log(document,document._id);
    $scope.cbSelected = [];
        $scope.cbSelected.push($rootScope.documentDetails._id);
    //  // //console.log($scope.cbSelected);
        $scope.trashDelete();
    }else{
        console.log("diff docs");
    }
};

$scope.trashDelete=function(){

            var currentDoc=docsCtrlVariables.getCurrentDocument();
        var documentList=docsCtrlVariables.getDocumentInfo();

        // console.log($scope.cbSelected,currentDoc,documentList);


        if ($scope.cbSelected.length != 0) {
            // console.log($scope.lockDocName,$scope.cbSelected);
            
                // if($scope.lockDocName.isLocked.user_id==$rootScope.loggedIn._id || !$scope.lockDocName.isLocked.status){

                        sweetAlert.swal({
                title : "Are you sure?",
                text : "Your files will be moved to trash!!",
                type : "warning",
                showCancelButton : true,
                confirmButtonColor : "#1ABC9C",
                confirmButtonText : "Delete",
                cancelButtonText : "Cancel",
                closeOnConfirm : true,
                closeOnCancel : true
            }, function(isConfirm) {
                $scope.check();
                $scope.confirmdelete = true;
                if (isConfirm) {
                    $scope.checkAllItems = false;
                    for (var j = 0; j < $scope.cbSelected.length; j++) {
                        var obj = {
                            id : $scope.cbSelected[j]
                        };
                        //

                        DefaultDocs.delete(obj, function(data) {
                                var multiDelLog={};
                                multiDelLog.activitytype="Trashed";
                            multiDelLog.what = data.requestObject._id;
                            multiDelLog.location=data.requestObject.s3_path;
                            //multiDelLog.what = data.requestObject;            
                            multiDelLog.status = true;
                             ActivitylogService.LogActivity(multiDelLog)
                                //splice
                                docsCtrlVariables.spliceDoc(data.requestObject._id);
                                // DefaultDocs.show({
                                //  id : currentDoc._id
                                // }, function(response) {
                                //  docsCtrlVariables.setDocumentInfo(response);
                                //  $scope.documentList = response;
                                //  $scope.list = $scope.documentInfo;
                                //  //sweetAlert.swal("Deleted!", "delete done.", "success");
                                // });
                            
                            

                        }, function(err) {
                            
                            /*swal(err.data);*/
                        sweetAlert.swal("Error",err.data,"error");
                            angular.forEach(documentList, function(document, key) {
                                // console.log(document);
                                document.check = 'false';
                            });
                        });
                    }
                    sweetAlert.swal("Deleted!", "delete done.", "success");

    $scope.cbSelected = [];
                } else {
                    $scope.cbSelected = [];
                    angular.forEach(documentList, function(document, key) {
                        // console.log(document);
                        document.check = 'false';
                    });
                    sweetAlert.swal("Cancelled", "Your files/folder is safe :)", "error");
                    $scope.checkAllItems=false;
                }
            });
                            

        } else {
            sweetAlert.swal("Cancelled", "You have not selected any folder/file", "error");
        }
};

// properties
    $scope.openProperties=function(document){
        console.log("open properties");
        if($rootScope.documentDetails.documentName==document){
            console.log("true");
            blockUI.start();
            docsCtrlVariables.setpropertyDoc($rootScope.documentDetails);
            $scope.propertyDoc=$rootScope.documentDetails;
            var modalInstance = $uibModal.open({
                templateUrl: 'views/modal/modal_property.html',
                controller: propertyCtrl
              
             });
        }
        
    };

};



//     /*for image viewer*/
//     (function($){
//         $.fn.gdocsViewer = function(options) {
//             console.log(options);
//             var settings = {
//                 width  : '700',
//                 height : '750'
//             };

//             if (options) { 
//                 $.extend(settings, options);
//             }

//             return this.each(function() {
//                 var file = $(this).attr('href');
//             // int SCHEMA = 2, DOMAIN = 3, PORT = 5, PATH = 6, FILE = 8, QUERYSTRING = 9, HASH = 12
//             var extregex = /^((https?|ftp):\/)?\/?([^:\/\s]+)(:([^\/]*))?((\/[\w/-]+)*\/)([\w\-\.]+[^#?\s]+)(\?([^#]*))?(#(.*))?$/i;
//             var ext = file.match(extregex)[8].split(".").pop();
//             if (/^(tiff|pdf|ppt|pps|doc|docx|txt)$/.test(ext)) {
//                 $(this).after(function () {
//                     var id = $(this).attr('id');
//                     var gdvId = (typeof id !== 'undefined' && id !== false) ? id + '-gdocsviewer' : '';
//                     return '<div id="' + gdvId + '" class="gdocsviewer"><iframe src="https://docs.google.com/viewer?embedded=true&url=' + encodeURIComponent(file) + '" width="' + settings.width + '" height="' + settings.height + '" style="border: none;margin : 0 auto; display : block;"></iframe></div>';
//                 })
//             }
//         });
// };
// })( jQuery );

// /*<![CDATA[*/
// $(document).ready(function () {
//     $('a.embed').gdocsViewer({
//         width: 800,
//         height: 570
//     });
//     $('#embedURL').gdocsViewer();
// });

/**
 *
 * charts_flotCtrl
 *
 */

angular
    .module('homer')
    .controller('charts_flotCtrl', charts_flotCtrl)

function charts_flotCtrl($scope) {

    /**
     * Bar Chart Options
     */
    $scope.flotBarOptions = {
        series: {
            bars: {
                show: true,
                barWidth: 0.8,
                fill: true,
                fillColor: {
                    colors: [ { opacity: 0.6 }, { opacity: 0.6 } ]
                },
                lineWidth: 1
            }
        },
        xaxis: {
            tickDecimals: 0
        },
        colors: ["#62cb31"],
        grid: {
            color: "#e4e5e7",
            hoverable: true,
            clickable: true,
            tickColor: "#D4D4D4",
            borderWidth: 0,
            borderColor: 'e4e5e7',
        },
        legend: {
            show: false
        },
        tooltip: true,
        tooltipOpts: {
            content: "x: %x, y: %y"
        }
    };

    /**
     * Bar Chart Options for Analytics
     */
    $scope.flotBarOptionsDas = {
        series: {
            bars: {
                show: true,
                barWidth: 0.8,
                fill: true,
                fillColor: {
                    colors: [ { opacity: 1 }, { opacity: 1 } ]
                },
                lineWidth: 1
            }
        },
        xaxis: {
            tickDecimals: 0
        },
        colors: ["#62cb31"],
        grid: {
            show: false
        },
        legend: {
            show: false
        }
    };

    /**
     * Bar Chart Options for Widget
     */
    $scope.flotBarOptionsWid = {
        series: {
            bars: {
                show: true,
                barWidth: 0.8,
                fill: true,
                fillColor: {
                    colors: [ { opacity: 1 }, { opacity: 1 } ]
                },
                lineWidth: 1
            }
        },
        xaxis: {
            tickDecimals: 0
        },
        colors: ["#3498db"],
        grid: {
            show: false
        },
        legend: {
            show: false
        }
    };

    /**
     * Bar Chart data
     */
    $scope.flotChartData = [
        {
            label: "bar",
            data: [ [1, 12], [2, 14], [3, 18], [4, 24], [5, 32], [6, 22] ]
        }
    ];

    /**
     * Line Chart Data
     */
    $scope.flotLineAreaData = [
        {
            label: "line",
            data: [ [1, 34], [2, 22], [3, 19], [4, 12], [5, 32], [6, 54], [7, 23], [8, 57], [9, 12], [10, 24], [11, 44], [12, 64], [13, 21] ]
        }
    ]

    var data1 = [ [0, 26], [1, 24], [2, 29], [3, 26], [4, 33], [5, 26], [6, 36], [7, 28] ];

    $scope.chartUsersData = [data1];
    $scope.chartUsersOptions = {
        series: {
            splines: {
                show: true,
                tension: 0.4,
                lineWidth: 1,
                fill: 0.5
            },
        },
        grid: {
            tickColor: "#e4e5e7",
            borderWidth: 1,
            borderColor: '#e4e5e7',
            color: '#6a6c6f'
        },
        colors: [ "#62cb31", "#efefef"],
    };

    /**
     * Pie Chart Data
     */
    $scope.pieChartData = [
        { label: "Data 1", data: 16, color: "#84c465", },
        { label: "Data 2", data: 6, color: "#8dd76a", },
        { label: "Data 3", data: 22, color: "#a2c98f", },
        { label: "Data 4", data: 32, color: "#c7eeb4", }
    ];

    $scope.pieChartDataDas = [
        { label: "Data 1", data: 16, color: "#62cb31", },
        { label: "Data 2", data: 6, color: "#A4E585", },
        { label: "Data 3", data: 22, color: "#368410", },
        { label: "Data 4", data: 32, color: "#8DE563", }
    ];

    $scope.pieChartDataWid = [
        { label: "Data 1", data: 16, color: "#fad57c", },
        { label: "Data 2", data: 6, color: "#fde5ad", },
        { label: "Data 3", data: 22, color: "#fcc43c", },
        { label: "Data 4", data: 32, color: "#ffb606", }
    ];

    /**
     * Pie Chart Options
     */
    $scope.pieChartOptions = {
        series: {
            pie: {
                show: true
            }
        },
        grid: {
            hoverable: true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
            shifts: {
                x: 20,
                y: 0
            },
            defaultTheme: false
        }
    };

    $scope.lineChartData = [
        {
            label: "line",
            data: [ [1, 24], [2, 15], [3, 29], [4, 34], [5, 30], [6, 40], [7, 23], [8, 27], [9, 40] ]
        }
    ];

    /**
     * Line Chart Options
     */
    $scope.lineChartOptions = {
        series: {
            lines: {
                show: true,
                lineWidth: 1,
                fill: true,
                fillColor: {
                    colors: [ { opacity: 0.5 }, { opacity: 0.5 }
                    ]
                }
            }
        },
        xaxis: {
            tickDecimals: 0
        },
        colors: ["#62cb31"],
        grid: {
            tickColor: "#e4e5e7",
            borderWidth: 1,
            borderColor: '#e4e5e7',
            color: '#6a6c6f'
        },
        legend: {
            show: false
        },
        tooltip: true,
        tooltipOpts: {
            content: "x: %x, y: %y"
        }
    };

    /**
     * Line Chart Options for Dashboard
     */
    $scope.lineChartOptionsDas = {
        series: {
            lines: {
                show: true,
                lineWidth: 1,
                fill: true,
                fillColor: {
                    colors: [ { opacity: 1 }, { opacity: 1}
                    ]
                }
            }
        },
        xaxis: {
            tickDecimals: 0
        },
        colors: ["#62cb31"],
        grid: {
            tickColor: "#e4e5e7",
            borderWidth: 1,
            borderColor: '#e4e5e7',
            color: '#6a6c6f'
        },
        legend: {
            show: false
        },
        tooltip: true,
        tooltipOpts: {
            content: "x: %x, y: %y"
        }
    };

    /**
     * Sin cos Chart Options
     */

    var sin = [],
        cos = [];
    for (var i = 0; i < 14; i += 0.5) {
        sin.push([i, Math.sin(i)]);
        cos.push([i, Math.cos(i)]);
    }

    $scope.sinCosChartData =
        [
            { data: sin, label: "sin(x)"},
            { data: cos, label: "cos(x)"}
        ];
    $scope.sinCosChartOptions = {
        series: {
            lines: {
                show: true
            },
            points: {
                show: true
            }
        },
        grid: {
            tickColor: "#e4e5e7",
            borderWidth: 1,
            borderColor: '#e4e5e7',
            color: '#6a6c6f'
        },
        yaxis: {
            min: -1.2,
            max: 1.2
        },
        colors: [ "#62cb31", "#efefef"],
    }
}
/**
 *
 * chartjsCtrl
 *
 */

angular
    .module('homer')
    .controller('chartjsCtrl', chartjsCtrl)

function chartjsCtrl($scope) {

    /**
     * Data for Polar chart
     */
    $scope.polarData = [
        {
            value: 120,
            color:"#62cb31",
            highlight: "#57b32c",
            label: "Homer"
        },
        {
            value: 140,
            color: "#80dd55",
            highlight: "#57b32c",
            label: "Inspinia"
        },
        {
            value: 100,
            color: "#a3e186",
            highlight: "#57b32c",
            label: "Luna"
        }
    ];

    /**
     * Options for Polar chart
     */
    $scope.polarOptions = {
        scaleShowLabelBackdrop : true,
        scaleBackdropColor : "rgba(255,255,255,0.75)",
        scaleBeginAtZero : true,
        scaleBackdropPaddingY : 1,
        scaleBackdropPaddingX : 1,
        scaleShowLine : true,
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 1,
        animationSteps : 100,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
    };

    /**
     * Data for Doughnut chart
     */
    $scope.doughnutData = [
        {
            value: 20,
            color:"#62cb31",
            highlight: "#57b32c",
            label: "App"
        },
        {
            value: 120,
            color: "#91dc6e",
            highlight: "#57b32c",
            label: "Software"
        },
        {
            value: 100,
            color: "#a3e186",
            highlight: "#57b32c",
            label: "Laptop"
        }
    ];

    /**
     * Options for Doughnut chart
     */
    $scope.doughnutOptions = {
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 1,
        percentageInnerCutout : 45, // This is 0 for Pie charts
        animationSteps : 100,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
    };

    /**
     * Data for Line chart
     */
    $scope.lineData = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
            {
                label: "Example dataset",
                fillColor: "rgba(220,220,220,0.5)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [22, 44, 67, 43, 76, 45, 12]
            },
            {
                label: "Example dataset",
                fillColor: "rgba(98,203,49,0.5)",
                strokeColor: "rgba(98,203,49,0.7)",
                pointColor: "rgba(98,203,49,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(26,179,148,1)",
                data: [16, 32, 18, 26, 42, 33, 44]
            }
        ]
    };

    /**
     * Options for Line chart
     */
    $scope.lineOptions = {
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.05)",
        scaleGridLineWidth : 1,
        bezierCurve : true,
        bezierCurveTension : 0.4,
        pointDot : true,
        pointDotRadius : 4,
        pointDotStrokeWidth : 1,
        pointHitDetectionRadius : 20,
        datasetStroke : true,
        datasetStrokeWidth : 1,
        datasetFill : true,
    };

    /**
     * Data for Sharp Line chart
     */
    $scope.sharpLineData = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
            {
                label: "Example dataset",
                fillColor: "rgba(98,203,49,0.5)",
                strokeColor: "rgba(98,203,49,0.7)",
                pointColor: "rgba(98,203,49,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(98,203,49,1)",
                data: [33, 48, 40, 19, 54, 27, 54]
            }
        ]
    };

    /**
     * Options for Sharp Line chart
     */
    $scope.sharpLineOptions = {
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.05)",
        scaleGridLineWidth : 1,
        bezierCurve : false,
        pointDot : true,
        pointDotRadius : 4,
        pointDotStrokeWidth : 1,
        pointHitDetectionRadius : 20,
        datasetStroke : true,
        datasetStrokeWidth : 1,
        datasetFill : true,
    };

    /**
     * Options for Bar chart
     */
    $scope.barOptions = {
        scaleBeginAtZero : true,
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.05)",
        scaleGridLineWidth : 1,
        barShowStroke : true,
        barStrokeWidth : 1,
        barValueSpacing : 5,
        barDatasetSpacing : 1,
    };

    /**
     * Data for Bar chart
     */
    $scope.barData = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
            {
                label: "My First dataset",
                fillColor: "rgba(220,220,220,0.5)",
                strokeColor: "rgba(220,220,220,0.8)",
                highlightFill: "rgba(220,220,220,0.75)",
                highlightStroke: "rgba(220,220,220,1)",
                data: [65, 59, 80, 81, 56, 55, 40]
            },
            {
                label: "My Second dataset",
                fillColor: "rgba(98,203,49,0.5)",
                strokeColor: "rgba(98,203,49,0.8)",
                highlightFill: "rgba(98,203,49,0.75)",
                highlightStroke: "rgba(98,203,49,1)",
                data: [28, 48, 40, 19, 86, 27, 90]
            }
        ]
    };

    /**
     * Options for Single Bar chart
     */
    $scope.singleBarOptions = {
        scaleBeginAtZero : true,
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.05)",
        scaleGridLineWidth : 1,
        barShowStroke : true,
        barStrokeWidth : 1,
        barValueSpacing : 5,
        barDatasetSpacing : 1,
    };

    /**
     * Data for Single Bar chart
     */
    $scope.singleBarData = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
            {
                label: "My Second dataset",
                fillColor: "rgba(98,203,49,0.5)",
                strokeColor: "rgba(98,203,49,0.8)",
                highlightFill: "rgba(98,203,49,0.75)",
                highlightStroke: "rgba(98,203,49,1)",
                data: [10, 20, 30, 40, 30, 50, 60]
            }
        ]
    };

    /**
     * Data for Radar chart
     */
    $scope.radarData = {
        labels: ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"],
        datasets: [
            {
                label: "My First dataset",
                fillColor: "rgba(98,203,49,0.2)",
                strokeColor: "rgba(98,203,49,1)",
                pointColor: "rgba(98,203,49,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "#62cb31",
                data: [65, 59, 66, 45, 56, 55, 40]
            },
            {
                label: "My Second dataset",
                fillColor: "rgba(98,203,49,0.4)",
                strokeColor: "rgba(98,203,49,1)",
                pointColor: "rgba(98,203,49,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "#62cb31",
                data: [28, 12, 40, 19, 63, 27, 87]
            }
        ]
    };

    /**
     * Options for Radar chart
     */
    $scope.radarOptions = {
        scaleShowLine : true,
        angleShowLineOut : true,
        scaleShowLabels : false,
        scaleBeginAtZero : true,
        angleLineColor : "rgba(0,0,0,.1)",
        angleLineWidth : 1,
        pointLabelFontFamily : "'Arial'",
        pointLabelFontStyle : "normal",
        pointLabelFontSize : 10,
        pointLabelFontColor : "#666",
        pointDot : true,
        pointDotRadius : 2,
        pointDotStrokeWidth : 1,
        pointHitDetectionRadius : 20,
        datasetStroke : true,
        datasetStrokeWidth : 1,
        datasetFill : true,
    };

    /**
     * Data for Line chart
     */
    $scope.lineProjectData = {
        labels: ["January", "February", "March", "April"],
        datasets: [
            {
                label: "Example dataset",
                fillColor: "rgba(98,203,49,0.5)",
                strokeColor: "rgba(98,203,49,0.7)",
                pointColor: "rgba(98,203,49,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(26,179,148,1)",
                data: [17,21,19,24]
            }
        ]
    };

    /**
     * Options for Line chart
     */
    $scope.lineProjectOptions = {
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.05)",
        scaleGridLineWidth : 1,
        bezierCurve : false,
        pointDot : true,
        pointDotRadius : 3,
        pointDotStrokeWidth : 1,
        pointHitDetectionRadius : 20,
        datasetStroke : false,
        datasetStrokeWidth : 1,
        datasetFill : true,
        responsive: true,
        tooltipTemplate: "<%= value %>",
        showTooltips: true,
        onAnimationComplete: function()
        {
            this.showTooltip(this.datasets[0].points, true);
        },
        tooltipEvents: []
    };
}
/**
 *
 * inlineChartsCtrl
 *
 */

angular
    .module('homer')
    .controller('inlineChartsCtrl', inlineChartsCtrl)

function inlineChartsCtrl($scope) {

    /**
     * Inline chart
     */
    $scope.inlineData = [34, 43, 43, 35, 44, 32, 44, 52, 25];
    $scope.inlineOptions = {
        type: 'line',
        lineColor: '#54ab2c',
        fillColor: '#62cb31',
    };

    /**
     * Bar chart
     */
    $scope.barSmallData = [5, 6, 7, 2, 0, -4, -2, 4];
    $scope.barSmallOptions = {
        type: 'bar',
        barColor: '#62cb31',
        negBarColor: '#c6c6c6'
    };

    /**
     * Pie chart
     */
    $scope.smallPieData = [1, 1, 2];
    $scope.smallPieOptions = {
        type: 'pie',
        sliceColors: ['#62cb31', '#b3b3b3', '#e4f0fb']
    };

    /**
     * Long line chart
     */
    $scope.longLineData = [34, 43, 43, 35, 44, 32, 15, 22, 46, 33, 86, 54, 73, 53, 12, 53, 23, 65, 23, 63, 53, 42, 34, 56, 76, 15, 54, 23, 44];
    $scope.longLineOptions = {
        type: 'line',
        lineColor: '#62cb31',
        fillColor: '#ffffff'
    };

    /**
     * Tristate chart
     */
    $scope.tristateData = [1, 1, 0, 1, -1, -1, 1, -1, 0, 0, 1, 1];
    $scope.tristateOptions = {
        type: 'tristate',
        posBarColor: '#62cb31',
        negBarColor: '#bfbfbf'
    };

    /**
     * Discrate chart
     */
    $scope.discreteData = [4, 6, 7, 7, 4, 3, 2, 1, 4, 4, 5, 6, 3, 4, 5, 8, 7, 6, 9, 3, 2, 4, 1, 5, 6, 4, 3, 7, ];
    $scope.discreteOptions = {
        type: 'discrete',
        lineColor: '#62cb31'
    };

    /**
     * Pie chart
     */
    $scope.pieCustomData = [52, 12, 44];
    $scope.pieCustomOptions = {
        type: 'pie',
        height: '150px',
        sliceColors: ['#1ab394', '#b3b3b3', '#e4f0fb']
    };

    /**
     * Bar chart
     */
    $scope.barCustomData = [5, 6, 7, 2, 0, 4, 2, 4, 5, 7, 2, 4, 12, 14, 4, 2, 14, 12, 7];
    $scope.barCustomOptions = {
        type: 'bar',
        barWidth: 8,
        height: '150px',
        barColor: '#1ab394',
        negBarColor: '#c6c6c6'
    };

    /**
     * Line chart
     */
    $scope.lineCustomData = [34, 43, 43, 35, 44, 32, 15, 22, 46, 33, 86, 54, 73, 53, 12, 53, 23, 65, 23, 63, 53, 42, 34, 56, 76, 15, 54, 23, 44];
    $scope.lineCustomOptions = {
        type: 'line',
        lineWidth: 1,
        height: '150px',
        lineColor: '#17997f',
        fillColor: '#ffffff',
    };
}
/**
 *
 * clockCtrl
 *
 */

angular
    .module('homer')
    .controller('clockCtrl', clockCtrl)

function clockCtrl($scope, $timeout) {
    $scope.tickInterval = 1000 //ms

    var tick = function() {
        $scope.clock = Date.now() // get the current time
        $timeout(tick, $scope.tickInterval); // reset the timer
    }

    // Start the timer
    $timeout(tick, $scope.tickInterval);
}
/**
 *
 * timelineCtrl
 *
 */

angular
    .module('homer')
    .controller('timelineCtrl', timelineCtrl)

function timelineCtrl($scope) {


    $scope.timelineItems = [
        {
            type: "The standard chunk of Lorem Ipsum",
            content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to ",
            date: 1423063721,
            info: "It is a long established fact that"
        },
        {
            type: "There are many variations",
            content: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here",
            date: 1423063721,
            info: "It is a long established fact that"
        },
        {
            type: "Contrary to popular belief",
            content: " If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
            date: 1423063721,
            info: "It is a long established fact that"
        },
        {
            type: "Lorem Ipsum",
            content: "All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words.",
            date: 1423063721,
            info: "It is a long established fact that"
        },
        {
            type: "The generated Lorem Ipsum",
            content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to ",
            date: 1423063721,
            info: "It is a long established fact that"
        },
        {
            type: "The standard chunk",
            content: "Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.",
            date: 1423063721,
            info: "It is a long established fact that"
        }
    ];

}
/**
 *
 * googleMapCtrl
 *
 */

angular
    .module('homer')
    .controller('googleMapCtrl', googleMapCtrl)

function googleMapCtrl($scope, $timeout) {
    $scope.mapOptions = {
        zoom: 14,
        center: new google.maps.LatLng(40.6700, -73.9400),
        // Style for Google Maps
        styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}],
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
}
/**
 *
 * calendarCtrl
 *
 */

angular
    .module('homer')
    .controller('calendarCtrl', calendarCtrl)

function calendarCtrl($scope) {

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $scope.alertMessage = "Report all events from UI calendar.";

    // Events
    $scope.events = [
        {title: 'All Day Event',start: new Date(y, m, 1)},
        {id: 999,title: 'Repeating Event',start: new Date(y, m, d - 3, 16, 0),allDay: false},
        {id: 999,title: 'Repeating Event',start: new Date(y, m, d + 4, 16, 0),allDay: false},
        {title: 'Birthday Party',start: new Date(y, m, d + 1, 19, 0),end: new Date(y, m, d + 1, 22, 30),allDay: false},
        {title: 'Homer task',start: new Date(y, m, d + 5, 19, 0),end: new Date(y, m, d + 6, 22, 30),allDay: false, backgroundColor :"#62cb31", borderColor :"#62cb31"},
        {title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
    ];


    /* message on eventClick */
    $scope.alertOnEventClick = function( event, allDay, jsEvent, view ){
        $scope.alertMessage = (event.title + ': Clicked ');
    };
    /* message on Drop */
    $scope.alertOnDrop = function(event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view){
        $scope.alertMessage = (event.title +': Droped to make dayDelta ' + dayDelta);
    };
    /* message on Resize */
    $scope.alertOnResize = function(event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view ){
        $scope.alertMessage = (event.title +': Resized to make dayDelta ' + minuteDelta);
    };

    /* config object */
    $scope.uiConfig = {
        calendar:{
            height: 550,
            editable: true,
            header: {
                left: 'prev,next',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            eventClick: $scope.alertOnEventClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize
        }
    };

    /* Event sources array */
    $scope.eventSources = [$scope.events];

}
/**
 *
 * editorCtrl
 *
 */

angular
    .module('homer')
    .controller('editorCtrl', editorCtrl)

function editorCtrl($scope) {

    $scope.summernoteText = ['<h3>Hello Jonathan! </h3>',
        '<p>dummy text of the printing and typesetting industry. <strong>Lorem Ipsum has been the dustrys</strong> standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more',
        '<br/><br/>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.',
        'recently with.</p><p>Mark Smith</p>'].join('');

    $scope.summernoteTextTwo = ['<h4>It is a long established fact</h4>',
        '<p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)',
        '<p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)',
        'with.</p>'].join('');

    $scope.summernoteOpt = {
        toolbar: [
            ['headline', ['style']],
            ['style', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
            ['textsize', ['fontsize']],
            ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
        ]
    };

}
/**
 *
 * nggridCtrl
 *
 */

angular
    .module('homer')
    .controller('nggridCtrl', nggridCtrl)

function nggridCtrl($scope) {

    $scope.exampleData = [ { "Name": "Jakeem", "Email": "imperdiet@vulputatevelit.com", "Company": "Laoreet Lectus Corporation", "City": "Vaux-sur-Sure", "Country": "Papua New Guinea" }, { "Name": "Kadeem", "Email": "sit.amet.risus@scelerisquenequesed.net", "Company": "Mi Felis Adipiscing Institute", "City": "Fauglia", "Country": "Bouvet Island" }, { "Name": "Paula", "Email": "venenatis.lacus@milorem.net", "Company": "Libero LLP", "City": "Tirupati", "Country": "Antigua and Barbuda" }, { "Name": "Bree", "Email": "adipiscing.non.luctus@loremutaliquam.edu", "Company": "Vitae Purus Gravida Institute", "City": "Chatteris", "Country": "Poland" }, { "Name": "Quinn", "Email": "Nunc@ac.com", "Company": "Dui Lectus Rutrum Consulting", "City": "Wolverhampton", "Country": "Venezuela" }, { "Name": "Magee", "Email": "pretium.aliquet.metus@venenatislacus.co.uk", "Company": "Dui Associates", "City": "Stokrooie", "Country": "Japan" }, { "Name": "Rowan", "Email": "mus@rutrum.net", "Company": "Diam Pellentesque Habitant Institute", "City": "Ashburton", "Country": "Taiwan" }, { "Name": "Nina", "Email": "lobortis.augue@feugiatnec.org", "Company": "Auctor Velit Eget Consulting", "City": "Stevenage", "Country": "Denmark" }, { "Name": "Chava", "Email": "nec@ipsumSuspendissesagittis.com", "Company": "Egestas Company", "City": "Aulnay-sous-Bois", "Country": "Togo" }, { "Name": "Uma", "Email": "tincidunt.nunc@vestibulumneque.net", "Company": "Sem Semper Corp.", "City": "Dalkeith", "Country": "Nigeria" }, { "Name": "Amal", "Email": "laoreet.posuere@eu.net", "Company": "Non Massa PC", "City": "Stafford", "Country": "South Sudan" }, { "Name": "Dana", "Email": "Nulla.dignissim@mattisornarelectus.co.uk", "Company": "Laoreet PC", "City": "Gentinnes", "Country": "Korea, South" }, { "Name": "Iris", "Email": "nostra.per.inceptos@magnamalesuada.co.uk", "Company": "Diam Vel LLC", "City": "Oudekapelle", "Country": "Dominican Republic" }, { "Name": "Joshua", "Email": "Duis@enimgravidasit.com", "Company": "Magna Foundation", "City": "San Francisco", "Country": "Guinea-Bissau" }, { "Name": "Rosalyn", "Email": "egestas.ligula.Nullam@auctorullamcorpernisl.ca", "Company": "Sodales Mauris LLC", "City": "Seydişehir", "Country": "Sudan" }, { "Name": "Hilary", "Email": "et.pede.Nunc@accumsanneque.co.uk", "Company": "Et Rutrum Corp.", "City": "Broechem", "Country": "Bulgaria" }, { "Name": "Amena", "Email": "nisl.Maecenas.malesuada@vitaeorci.edu", "Company": "Quis LLC", "City": "Joliet", "Country": "Saint Lucia" }, { "Name": "Rashad", "Email": "Pellentesque.tincidunt@euneque.org", "Company": "Suspendisse Tristique Neque Industries", "City": "Amlwch", "Country": "Timor-Leste" }, { "Name": "Sharon", "Email": "ornare.sagittis@vitaeeratvel.ca", "Company": "Tellus Foundation", "City": "Woodstock", "Country": "Chile" } ];

    $scope.gridOptions = {
        data: 'exampleData'
    };
//
    $scope.gridOptionsTwo = {
        data: 'exampleData',
        showGroupPanel: true,
        jqueryUIDraggable: true
    }
}
/**
 *
 * nestableCtrl
 *
 */

angular
    .module('homer')
    .controller('nestableCtrl', nestableCtrl)

function nestableCtrl($scope) {

    // Handle actions
    $scope.remove = function(scope) {
        scope.remove();
    };
    $scope.toggle = function(scope) {
        scope.toggle();
    };
    $scope.moveLastToTheBeginning = function () {
        var a = $scope.data.pop();
        $scope.data.splice(0,0, a);
    };
    $scope.newSubItem = function(scope) {
        var nodeData = scope.$modelValue;
        nodeData.nodes.push({
            id: nodeData.id * 10 + nodeData.nodes.length,
            title: nodeData.title + '.' + (nodeData.nodes.length + 1),
            nodes: []
        });
    };
    $scope.collapseAll = function() {
        $scope.$broadcast('collapseAll');
    };
    $scope.expandAll = function() {
        $scope.$broadcast('expandAll');
    };

    // Nestable list example data
    $scope.data = [{
        "id": 1,
        "title": "Sem fringilla",
        "nodes": [
            {
                "id": 11,
                "title": "Nisl lacus et, ultricies",
                "nodes": [
                    {
                        "id": 111,
                        "title": "Congue hac",
                        "nodes": []
                    }
                ]
            },
            {
                "id": 12,
                "title": "Consectetuer orci mollis",
                "nodes": []
            }
        ],
    }, {
        "id": 2,
        "title": "Gravida morbi non",
        "nodes": [
            {
                "id": 21,
                "title": "Lorem aliquam",
                "nodes": []
            },
            {
                "id": 22,
                "title": "Inceptos nibh",
                "nodes": []
            }
        ],
    }, {
        "id": 3,
        "title": "Pede hymenaeos",
        "nodes": [
            {
                "id": 31,
                "title": "Magnis morbi orci",
                "nodes": []
            },
            {
                "id": 32,
                "title": "Ad tortor, auctor dui",
                "nodes": []
            },
            {
                "id": 33,
                "title": "Orci magnis, mauris",
                "nodes": []
            }
        ],
    }];
}
/**
 *
 * tourCtrl
 *
 */

angular
    .module('homer')
    .controller('tourCtrl', tourCtrl)

function tourCtrl($scope) {





}
/**
 *
 * datatablesCtrl
 *
 */

angular
    .module('homer')
    .controller('datatablesCtrl', datatablesCtrl)

function datatablesCtrl($scope, DTOptionsBuilder, DTColumnBuilder) {

    // See all possibility of fetch data with example code at:
    // http://l-lin.github.io/angular-datatables/#/withAjax

    // Please note that api file is not included to grunt build process
    // As it will be probably replacement to some REST service we only add it for demo purpose

    $scope.dtOptions = DTOptionsBuilder.fromSource('api/datatables.json')
        .withDOM("<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp")
        .withButtons([
            {extend: 'copy',className: 'btn-sm'},
            {extend: 'csv',title: 'ExampleFile', className: 'btn-sm'},
            {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm'},
            {extend: 'print',className: 'btn-sm'}
        ]);
    $scope.dtColumns = [
        DTColumnBuilder.newColumn('Name').withTitle('Name'),
        DTColumnBuilder.newColumn('Position').withTitle('Position'),
        DTColumnBuilder.newColumn('Office').withTitle('Office'),
        DTColumnBuilder.newColumn('Age').withTitle('Age'),
        DTColumnBuilder.newColumn('Start_date').withTitle('Start_date'),
        DTColumnBuilder.newColumn('Salary').withTitle('Salary')
    ];
}
/**
 *
 * wizardCtrl
 *
 */

angular
    .module('homer')
    .controller('wizardOneCtrl', wizardOneCtrl)

function wizardOneCtrl($scope, notify, sweetAlert) {

    // Initial user
    $scope.user = {
        username: 'Mark Smith',
        email: 'mark@company.com',
        password: 'secret_password',
        approve: false
    }

    // Initial step
    $scope.step = 1;

    // Wizard functions
    $scope.wizard =  {
        show: function(number) {
            $scope.step = number;
        },
        next: function() {
            $scope.step++ ;
        },
        prev: function() {
            $scope.step-- ;
        }
    };

    $scope.submit = function()
    {
        // Show notification
        sweetAlert.swal({
            title: "Thank you!",
            text: "You approved our example form!",
            type: "success"
        });

        // 'Redirect' to step 1
        $scope.step = 1;

    }

}


/**
 *
 * formsCtrl
 *
 */

angular
    .module('homer')
    .controller('formsCtrl', formsCtrl)
    .controller('SelectLocalCtrl', SelectLocalCtrl)
    .controller('RadiolistCtrl', RadiolistCtrl)
    .controller('Html5InputsCtrl', Html5InputsCtrl)
    .controller('DatepickerDemoCtrl', DatepickerDemoCtrl)
    .controller('spinCtrl', spinCtrl)

function formsCtrl($scope) {

    $scope.user = {
        name: 'awesome user',
        desc: 'Awesome user \ndescription!',
        remember: true
    };


    $scope.person = {};
    $scope.people = [
        { name: 'Adam',      email: 'adam@email.com',      age: 10 },
        { name: 'Amalie',    email: 'amalie@email.com',    age: 12 },
        { name: 'Wladimir',  email: 'wladimir@email.com',  age: 30 },
        { name: 'Samantha',  email: 'samantha@email.com',  age: 31 },
        { name: 'Estefanía', email: 'estefanía@email.com', age: 16 },
        { name: 'Natasha',   email: 'natasha@email.com',   age: 54 },
        { name: 'Nicole',    email: 'nicole@email.com',    age: 43 },
        { name: 'Adrian',    email: 'adrian@email.com',    age: 21 }
    ];

    $scope.availableColors = ['Red','Green','Blue','Yellow','Magenta','Maroon','Umbra','Turquoise'];
    $scope.multipleDemo = {};
    $scope.multipleDemo.colors = ['Blue','Red'];

    $scope.availableTags = ['Branding','Website','Design','Ilustration','New','Important','External'];
    $scope.multipleTags = {};
    $scope.multipleTags.tags = ['Branding','Website','Design','Ilustration','New'];

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };
}

function SelectLocalCtrl($scope, $filter) {
    $scope.user = {
        status: 2
    };

    $scope.statuses = [
        {value: 1, text: 'status1'},
        {value: 2, text: 'status2'},
        {value: 3, text: 'status3'},
        {value: 4, text: 'status4'}
    ];

    $scope.showStatus = function() {
        var selected = $filter('filter')($scope.statuses, {value: $scope.user.status});
        return ($scope.user.status && selected.length) ? selected[0].text : 'Not set';
    };
}

function RadiolistCtrl($scope, $filter){
    $scope.user = {
        status: 2
    };

    $scope.statuses = [
        {value: 1, text: 'status1'},
        {value: 2, text: 'status2'}
    ];

    $scope.showStatus = function() {
        var selected = $filter('filter')($scope.statuses, {value: $scope.user.status});
        return ($scope.user.status && selected.length) ? selected[0].text : 'Not set';
    };
}

function Html5InputsCtrl($scope) {
    $scope.user = {
        email: 'email@example.com',
        tel: '123-45-67',
        number: 29,
        range: 10,
        url: 'http://example.com',
        search: 'blabla',
        color: '#6a4415',
        date: null,
        time: '12:30',
        datetime: null,
        month: null,
        week: null
    };
}

function DatepickerDemoCtrl($scope) {
    $scope.today = function() {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
        return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
}

function spinCtrl($scope){

    $scope.inputteresxcs = 55;
    $scope.spinOption1 = {
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
    };

    $scope.spinOption2 = {
        verticalbuttons: true
    }

    $scope.spinOption3 = {
        postfix: '%'
    }

    $scope.spinOption4 = {
        postfix: "a button",
        postfix_extraclass: "btn btn-default"
    }

}
/**
 *
 * draggableCtrl
 *
 */

angular
    .module('homer')
    .controller('draggableCtrl', draggableCtrl)

function draggableCtrl($scope) {

    $scope.sortableOptions = {
        connectWith: ".connectPanels",
        handler: ".panel-body"
    };

}
/**
 *
 * validationCtrl
 *
 */

angular
    .module('homer')
    .controller('validationCtrl', validationCtrl)

function validationCtrl($scope) {

    $scope.signupForm = function() {

        if ($scope.signup_form.$valid) {
            // Submit as normal
        } else {
            $scope.signup_form.submitted = true;
        }
    }

}

/**
 *
 * chartistCtrl
 *
 */

angular
    .module('homer')
    .controller('chartistCtrl', chartistCtrl);

function chartistCtrl($scope) {


    $scope.lineData = {
        labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],
        series: [
            [12, 9, 7, 8, 5],
            [2, 1, 3.5, 7, 3],
            [1, 3, 4, 5, 6]
        ]
    };

    $scope.lineOptions = {
        fullWidth: true,
        chartPadding: {
            right: 40
        }
    };

    $scope.pieData = {
        series: [10, 5, 5]
    };


    $scope.guageData = {
        series: [25, 25, 25, 25]
    };

    $scope.guageOptions = {
        donut: true,
        donutWidth: 60,
        startAngle: 270,
        total: 200,
        showLabel: false
    };


    $scope.hbarData = {
        labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
        series: [
            [2, 3, 4, 5, 6, 7, 8],
            [3, 4, 5, 6, 7, 8, 9]
        ]
    };

    $scope.hbarOptions = {
        seriesBarDistance: 10,
        reverseData: true,
        horizontalBars: true,
        axisY: {
            offset: 70
        }
    };

    $scope.sbarData = {
        labels: ['Q1', 'Q2', 'Q3', 'Q4'],
        series: [
            [800000, 1200000, 1400000, 1200000],
            [200000, 400000, 500000, 300000],
            [300000, 300000, 400000, 600000]
        ]
    };

    $scope.sbarOptions = {
        stackBars: true,
        axisY: {
            labelInterpolationFnc: function(value) {
                return (value / 1000) + 'k';
            }
        }
    };

    $scope.areaData = {
        labels: [1, 2, 3, 4, 5, 6, 7, 8],
        series: [[5, 9, 7, 8, 5, 3, 5, 4]
        ]
    };

    $scope.areaOptions = {
        low: 0,
        showArea: true
    };

}
/**
 *
 * codeEditorCtrl
 *
 */

angular
    .module('homer')
    .controller('codeEditorCtrl', codeEditorCtrl)

function codeEditorCtrl($scope) {

    $scope.editorOptions = {
        lineNumbers: true,
        matchBrackets: true,
        styleActiveLine: true
    };

}
/**
 *
 * loadingCtrl
 *
 */

angular
    .module('homer')
    .controller('loadingCtrl', loadingCtrl)

function loadingCtrl($scope, $timeout) {

    $scope.runLoading = function() {
        // start loading
        $scope.loading = true;

        $timeout(function(){
            // Simulate some service
            $scope.loading = false;
        },2000)
    };


    // Demo purpose actions
    $scope.runLoading1 = function () {
        // start loading
        $scope.loading1 = true;

        $timeout(function () {
            // Simulate some service
            $scope.loading1 = false;
        }, 2000)
    };
    $scope.runLoading2 = function () {
        // start loading
        $scope.loading2 = true;

        $timeout(function () {
            // Simulate some service
            $scope.loading2 = false;
        }, 2000)
    };
    $scope.runLoading3 = function () {
        // start loading
        $scope.loading3 = true;

        $timeout(function () {
            // Simulate some service
            $scope.loading3 = false;
        }, 2000)
    };
    $scope.runLoading4 = function () {
        // start loading
        $scope.loading4 = true;

        $timeout(function () {
            // Simulate some service
            $scope.loading4 = false;
        }, 2000)
    };
    $scope.runLoading5 = function () {
        // start loading
        $scope.loading5 = true;

        $timeout(function () {
            // Simulate some service
            $scope.loading5 = false;
        }, 2000)
    };
    $scope.runLoading6 = function () {
        // start loading
        $scope.loading6 = true;

        $timeout(function () {
            // Simulate some service
            $scope.loading6 = false;
        }, 2000)
    };
    $scope.runLoading7 = function () {
        // start loading
        $scope.loading7 = true;

        $timeout(function () {
            // Simulate some service
            $scope.loading7 = false;
        }, 2000)
    };
    $scope.runLoading8 = function () {
        // start loading
        $scope.loading8 = true;

        $timeout(function () {
            // Simulate some service
            $scope.loading8 = false;
        }, 2000)
    };
    $scope.runLoading9 = function () {
        // start loading
        $scope.loading9 = true;

        $timeout(function () {
            // Simulate some service
            $scope.loading9 = false;
        }, 2000)
    };
    $scope.runLoading10 = function () {
        // start loading
        $scope.loading10 = true;

        $timeout(function () {
            // Simulate some service
            $scope.loading10 = false;
        }, 2000)
    };
    $scope.runLoading11 = function () {
        // start loading
        $timeout(function() {
            $scope.loading11 = 0.1;
        }, 500);
        $timeout(function() {
            $scope.loading11 += 0.2;
        }, 1000);
        $timeout(function() {
            $scope.loading11 += 0.3;
        }, 1500);
        $timeout(function() {
            $scope.loading11 = false;
        }, 2000);

    };
    $scope.runLoading12 = function () {
        // start loading
        $timeout(function() {
            $scope.loading12 = 0.1;
        }, 500);
        $timeout(function() {
            $scope.loading12 += 0.2;
        }, 1000);
        $timeout(function() {
            $scope.loading12 += 0.3;
        }, 1500);
        $timeout(function() {
            $scope.loading12 = false;
        }, 2000);

    };

    $scope.runLoadingDemo = function() {
        // start loading
        $scope.loadingDemo = true;

        $timeout(function(){
            // Simulate some service
            $scope.loadingDemo = false;
        },2000)
    };


}
angular.module('homer')
  .controller('loginCtrl', function ($scope,$state,$http,$rootScope,$location,md5,Auth,User,notify,DefaultDocs,GlobalService,ActivitylogService,docsCtrlVariables,blockUI,$window) {

     blockUI.stop();
     $scope.homerTemplate = 'views/notification/notify.html';
     
     console.log($rootScope.domainsInfo,"domaininfo");
     console.log("root domains",$rootScope.domainsNames,$rootScope.domainsNames.domain,$rootScope.domainsNames.grant_type);

// Auth.isLoggedInAsync(function(loggedIn) {
//   console.log(loggedIn);
//   if(loggedIn){
//      $state.go('dashboard');$rootScope.domainsInfo.grant_type
//   }
// });

console.log($state.currentpage,$rootScope.loginObj);
     var activity_type = GlobalService.getActivitytype();
    $scope.loginButton=false;
     Auth.getTenant(function(tenant){
      console.log("hello",tenant);
    $scope.tenant=tenant;
    console.log($scope.tenant,"tenant info");
  //$rootScope.domainDetails=tenant;
    $rootScope.tenantInfo=$scope.tenant;
      console.log($rootScope.tenantInfo,$rootScope.tenantInfo.clientId,"tenant");
    });
   

/*
 Function Name:  login
 Validate User 
 */  
  $scope.login=function(){
  // console.log( $rootScope.domainsInfo,$rootScope.domainsInfo.grant_type,client_crendential);
//   if($rootScope.domainsInfo.grant_type=="client_crendential"){

//     $scope.grant_Type=$rootScope.domainsInfo.grant_type;

//     for(var i=0;i< $rootScope.tenantInfo.application.length;i++){
//       console.log($rootScope.tenantInfo.application[i]);
//       if($rootScope.domainsNames.domain==$rootScope.tenantInfo.application[i].name){
//         console.log($rootScope.tenantInfo.application[i].name,"appssss");
//         $scope.clientSeecrett=$rootScope.tenantInfo.application[i].clientSecret;
//       }
//     }
//   }else{
//     $scope.grant_Type=$rootScope.domainsInfo.grant_type;
//     $scope.clientSeecrett=$scope.tenant.clientSecret;
//   }
// console.log($scope.grant_Type,$scope.clientSeecrett,"details");
            var set=""
    docsCtrlVariables.setWhichDocument(set);
      // ////console.log($scope.username,$scope.password);
      if($scope.username && $scope.password){
        $scope.passwordHash=md5.createHash($scope.password || '');
        //construct object for authentication
          var userobject={
          username:$scope.username,
          password:$scope.passwordHash
        };
       // console.log(userobject,$scope.tenant.clientSecret);
          //Authenticate if user exists or not
          Auth.login(userobject,$scope.tenant.clientSecret)
        .then( function(tenant) {
           console.log(tenant.redirecturl);
          // Logged in, redirect to home
           $scope.loginButton=true;
           Auth.isLoggedInAsync(function(loggedIn) {
            $scope.loginButton=true;
            var loginLog={};
            loginLog.activitytype=activity_type.LOGIN;
            ActivitylogService.LogActivity(loginLog);//from activity service
          });

  $scope.getCurrentUser = Auth.getCurrentUser;
  $scope.getCurrentUser().$promise.then(function() {
    $rootScope.loggedIn = $scope.getCurrentUser();


    if($rootScope.loggedIn.clientId=="retrax" || $rootScope.loggedIn.clientId=="Retrax"){
    console.log("loggedIn",$rootScope.loggedIn);  
               console.log(tenant);

    }
    
    // $scope.loggedUser=$scope.getCurrentUser();
    ////console.log($scope.loggedIn,$scope.loggedIn.role);
    docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);  

  });

       // $state.go('dashboard');
          
  if(tenant.redirecturl)
  {
       // $state.go('dashboard')
        var gotTo=tenant.redirecturl;
        location.href=gotTo;
  }
  else
  {
    $state.go('dashboard')
  }      
        })
        .catch( function(err) {
         // $scope.loginForm.empty = true;
         $scope.username="";
        $scope.password = "";
        notify({ message: 'Warning - Please Enter Valid Username or Password', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
        });

      }

      
      //////console.log($scope.passwordHash);
      
        // } else {
        //   // console.log("not valid");
        //     $scope.loginForm.submitted = true;
        // }

      
    }
/*
  Function Name:
*/
$scope.fPassword=function(){
  // ////console.log("set password");
  $state.go('common.forgotpassword');
};

  })
  angular.module('homer')

    .controller('forgotpasswordCntrl', function ($scope,$state,$http,$rootScope,md5,Auth,User,notify,DefaultDocs,userManagement) {
       $scope.homerTemplate = 'views/notification/notify.html';


       // console.log("inside forgotpass");
       Auth.getTenant(function(tenant){
      $scope.tenant=tenant;
       $rootScope.tenantInfo=$scope.tenant;
        // console.log($rootScope.tenantInfo)
      });


  /* Verifying for password match */
  var userObj = {};
       $scope.forgotpass=function(user){

        if($scope.username && $scope.username2){
          if($scope.username == $scope.username2){
                // console.log(" forgotpass ", $scope.username , $scope.username2,user);  
          var userList={ };
          userList.username=$scope.username;
              
       userManagement.forgotPassword(userList,function(response){
      var ename=$scope.username;

    //sending the mail to user 
  notify({ message: 'Email Sent - We have sent the Verification Mail to'+" " +ename, classes: 'alert-success', templateUrl: $scope.homerTemplate});
  // console.log("success");
  $state.go('common.error_two');
  $scope.username="";
   $scope.username2="";
   
  },function(err){
   // console.log(err);
   notify({ message: 'Warning - No such User!! Please check the Email', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
   user="";
  });


  }
  else{
    // console.log("error");
    notify({message:'Email do not match',classes:'alert-danger',templateUrl:$scope.homerTemplate});
    $scope.username="";
   $scope.username2="";
  }
}

       };
       
      });
angular.module('homer')
  .controller('dashBoardCtrl', function ($scope,$state,$http,$location,Upload,$rootScope,md5,Auth,User,notify,DefaultDocs,$moment,ShareFctry,sweetAlert,uploadService,$uibModal,GlobalService,ActivitylogService,activityFcrt,docsCtrlVariables,versionService,timelineService,Groups,GroupService,blockUI,url) {

    // $uibModalInstance.dismiss('cancel');

   $scope.homerTemplate = 'views/notification/notify.html';
	var activity_type = GlobalService.getActivitytype();

/*sunil here*/
//$scope.shareFlag=false;
$scope.share=function(doc,version){
	console.log("share",doc,version);
	doc.shareFlag=true;

	$rootScope.guestdoc=doc;
	if(version){
		$rootScope.guestVersion=version;
	}
	var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/guestuser_invite.html',
        controller: 'GuestInviteCtrl'
        //size:'sm'
      })
}

/*
Array of pages
*/
$scope.pages=[1,2,3,4,5]
/*
setting pages
*/

$scope.setPage=function(val){
/*  console.log("check",val);
*/  docsCtrlVariables.setSkipValues(val)
  $scope.getMeMyDocs();
}

/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

$scope.getCurrentUser = Auth.getCurrentUser;
  $scope.getCurrentUser().$promise.then(function() {
    $rootScope.loggedIn = $scope.getCurrentUser();
    // $scope.loggedUser=$scope.getCurrentUser();
    ////console.log($scope.loggedIn,$scope.loggedIn.role);
    docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);

      });

// $ocLazyLoad.load('dashboard.js');
/*
Scope Variables declaraions
*/
//$rootScope.allinvitegroups=[];
$scope.showAddRow=false;
$scope.cbSelected=[];
$scope.trashSelected=[];
$scope.disableAddFolder=false;
$scope.showversion=false;
$scope.showUploadRow=[];
$scope.versionuploading=false;
$scope.showNoFiles=true;
$scope.versionfilename="";
$scope.trashcontent=true;
$scope.permadel=false;


$scope.getCurrentUser = Auth.getCurrentUser;
var host = $location.host();
$scope.getCurrentUser().$promise.then(function(){
	$rootScope.loggedIn = $scope.getCurrentUser();
		var someobj={
			host:host.split('.')[0]
		};
	var ioSocket = io(url, {
	 query: someobj,
      path: '/socket.io-client'
    });	

    ioSocket.on('Document:save', function (item) {
	if(item.isFavourite.user_id){
		var ind=item.isFavourite.user_id.indexOf($rootScope.loggedIn._id);
		if(ind>-1){
			item.isFavourite.status=true;
		}
		else{
			item.isFavourite.status=false;
		}
	}
	
		docsCtrlVariables.addDocumentSocket(item,function(process){
	
			var location = item.s3_path;
		       location = location.substring(location.indexOf("/") + 1);
		       var location1 = location;
		       location1 = location1.substring(location1.indexOf("/") + 1);
		        location2 = location1;
		        location2 = location2.substring(0, location2.lastIndexOf("/"));
			if($rootScope.loggedIn._id!=item.createdBy._id && process =="create"){
		        	notify({ message: "A new "+item.type+ " has been added at location "+location2 , classes: 'alert-success', templateUrl: $scope.homerTemplate});

			}
			else if(process=="update"){
				if(item.modifiedBy){
					if(item.modifiedBy!=$rootScope.loggedIn._id){
						notify({ message: item.type+" "+item.documentName+ " has been updated at location "+location2 , classes: 'alert-success', templateUrl: $scope.homerTemplate});	
					}
					
				}
				
			}
			if(!$scope.$$phase) {
			  $scope.$apply();
			}
			
		});		


});
    ioSocket.on('Document:remove', function (item) {
	})
$scope.$on('$destroy', function() {

    ioSocket.removeAllListeners('Document:save');
  });
	
});

/*\
Allow download
*/
  Auth.isLoggedInAsync(function(loggedIn) {
      if(loggedIn){
       $state.go('dashboard');
      }
     });

$scope.showDownload=function(doc)
{
	if(doc.allowDownload && doc.type=='file')
		return true;
	else
		return false;
}

/*
	functionName: errorMessage
	@param:text
*/
$scope.errorMessages=function(text){
	notify({ message: text, classes: 'alert-danger', templateUrl: $scope.homerTemplate});
}

// $scope.getCurrentUser = Auth.getCurrentUser;
// 	$scope.getCurrentUser().$promise.then(function(){
// 		$rootScope.loggedIn = $scope.getCurrentUser();
// 	});


/*
if image doesn't exist
*/
	$scope.checkIfImageExists=function(user,image){
	user.name=user.user_id.name;
	docsCtrlVariables.checkForImage(user,image);
	};

	$scope.showTrash = true;
		 $scope.trashFunc = function() {
		 	/**/
		 	console.log($scope.actualLength,$scope.trashLength)
		 	if( ($scope.actualLength-$scope.trashLength)== 0 ){
		 		notify({ message: 'There are zero items in trash' ,classes: 'alert-danger', templateUrl: $scope.homerTemplate});
		 	}
		 	else{
		 	docsCtrlVariables.updateDocumentInfo();
		 	$scope.showTrash = !$scope.showTrash;	
		 	}
		 	
		};

		$scope.hidetrashFunc=function(){
			docsCtrlVariables.updateDocumentInfo();
		 	$scope.showTrash = !$scope.showTrash;	

		}

	$scope.restoreFunc=function(){
		var currentDoc=docsCtrlVariables.getCurrentDocument();
		if($scope.trashSelected.length){
			for (var j = 0; j < $scope.trashSelected.length; j++) {
						var obj = {
							id : $scope.trashSelected[j]
						};
						DefaultDocs.restore(obj, function(data) {
							$scope.cbSelected=[];
					$scope.trashSelected = [];
					$scope.trashSelected[j]=false;

					$scope.checkAllItems=false;
						var restorelog = {};
								restorelog.activitytype=activity_type.RESTORE;
								restorelog.location=data.requestObject.s3_path;
	 						restorelog.what = data.requestObject;			
	 						restorelog.status = true;
							 ActivitylogService.LogActivity(restorelog);
							 docsCtrlVariables.restoreDoc(data.requestObject._id);
							// DefaultDocs.show({

							// 		id : currentDoc._id
							// 	}, function(response) {
							// 		docsCtrlVariables.setDocumentInfo(response);
							// 		/*$scope.documentList = response;
							// 		$scope.list = $scope.documentInfo;*/
									
							// 	});

							sweetAlert.swal("Restored!", "restore done.", "success");
										 	docsCtrlVariables.updateDocumentInfo();
										 	$scope.cbSelected=[];
										 	$scope.trashSelected=[];

						});

					}

		// 			$scope.cbSelected=null;
		}
		else{
			sweetAlert.swal("Cancelled", "You have not selected any trashed folder/file", "error");
		}
		// $scope.cbSelected=null;
		
		};


	//details
  $scope.getDocs=DefaultDocs.query({
  
  },function(data){
    $rootScope.tenantDocs=data;
  })

/*
get the current document name
*/
$scope.checkCurrentDocument=function(param){
	var current = docsCtrlVariables.getCurrentDocument();
	$rootScope.currentAccess=current.access_list;
		if(param=="name")
			return current.documentName;
		else{
			return current.root;
 		}
};
	// To capitalize the first letter and display(first letter)
	$scope.Capital_frst = function(firstLtr) {
		if (firstLtr == undefined)
			return;
		var res = firstLtr.toUpperCase();
		var res1 = res.slice(0,1);
		return res1;
	};

	//remove group
	$scope.removeGroup=function(grp,index){
		var currentDoc=docsCtrlVariables.getCurrentDocument();
		$scope.arrayOfgrp=[currentDoc._id];
		console.log(grp,$rootScope.policyId);
		var obj={
				id:grp._id,
				policy:$rootScope.policyId._id,
				document:$scope.arrayOfgrp
				};
				angular.forEach($rootScope.allinvitegroups,function(value){		
					if(value._id==obj.id){
						angular.forEach(value.groupinvite,function(val){
							if(val.document.indexOf(currentDoc._id)>-1){
								obj.policy=val.policy._id;
							}
						})
					}
	    		});
		Groups.RemoveGroupDoc(obj,function(res){
				//var grpLog={};
				//grpLog.activitytype=activity_type.REMOVE;
				//grpLog.location=data.requestObject.s3_path;
		 		//grpLog.what = data.requestObject;			
		 		//grpLog.status = true;
				//ActivitylogService.LogActivity(grpLog);
		})
	    $rootScope.allinvitegroups.splice(index,1);
	};

$scope.ifMydocs=function(){
	 var whichDoc=docsCtrlVariables.getWhichDocument();
	var current = docsCtrlVariables.getCurrentDocument();
	var bread = docsCtrlVariables.getbreadcrumbs();
	if(bread.length>1)
		return true;
	else 
		return false;
}


/*
	checkaccess
*/

docsCtrlVariables.newbreadcrumbs();
/*
Query to get my documents

*/
var whichDoc=docsCtrlVariables.getWhichDocument();		
$scope.getMeMyDocs=function(){
	
	 docsCtrlVariables.getDocs(whichDoc,function(docName){
	 	if(whichDoc=="ProjectView"){
	 		$scope.checkForPermission(docsCtrlVariables.getCurrentDocument(),function(){
	 		})
	 	}
	 	whichDoc=docName;
	 });
}
$scope.getMeMyDocs(); 

$scope.checkIfNtLocked=function(document){

		//check if the document object is valid. if not unlock and return.
		if (document == undefined || document.isLocked == undefined || document.isLocked.status == undefined ||  document.isLocked.status == false) {
			
	
			return false;
		}
	if (document.isLocked.status && document.isLocked.user_id==$rootScope.loggedIn._id ) {
			return true;
			} else {
				return false;

			}
}

/*
function name : folderClick
@params: document obj and evernt
return: will populate document list based on the elements
*/
$rootScope.showInvite=false;
$scope.allowCreateFolder=true;
$scope.allowDelete=true;
$scope.allowUpload=true;
$rootScope.allowMoadlOpen=true;
$scope.trashedClick=false;
$scope.folderClick = function(doc, ev,version) {
console.log(doc);

if(version==undefined || version==null){
	// console.log("null",doc.version);
	 angular.forEach(doc.version, function(value, key){
  if(doc.version.length == value.number){
  	// console.log(value,value.number);
    $rootScope.commentVersion=value;
    // console.log($rootScope.commentVersion);
  }
 });

}else{
	// console.log(doc.version,doc,version);
	$rootScope.commentVersion=doc;
};

$scope.checkAllItems=false;
	$scope.checkForPermission(doc,function(){

	if(doc.type=='folder'){
		 $scope.allotPermission(doc);
			
	
if(doc.isLocked.user_id==$rootScope.loggedIn._id || !doc.isLocked.status)
		{		
		$scope.cbSelected=[];
$scope.trashSelected=[];
		if(doc.showInvite)
		//$rootScope.showInvite=true;
			$scope.showInvite=true;
		else
			$rootScope.showInvite=false;
		if(doc.allowRemovingContrib)
			$scope.canUserRemoveContrib=true;
		else
			$scope.canUserRemoveContrib=false;

	
		if(doc.trash  )

		{
			$scope.allowCreateFolder=false;
			$scope.allowUpload=false;
		}
		else{
			$scope.allowCreateFolder=doc.allowAddFolders;
			$scope.allowUpload=doc.allowUpload;
			$scope.allowDelete=doc.allowDelete;
		}
			
		//push the folder to breadcrumbs don't push if folder is
		//$rootScope.breadcrumbs.push(doc);
		docsCtrlVariables.folderClick(doc);	
		Groups.getGroups({"id":doc._id},function(groups){
			
			////console.log($rootScope.allinvitegroups);

			$rootScope.allinvitegroups=groups;
			angular.forEach($rootScope.allinvitegroups,function(value){
				angular.forEach(value.groupinvite,function(val){
					if(val.document.indexOf(doc._id)>-1){
						$rootScope.policyId= val.policy;
						value.policy=val.policy.policy_name;
					}
				})
			})

		});	
	}
	else{
		notify({ message: doc.documentName +' is Locked' ,classes: 'alert-danger', templateUrl: $scope.homerTemplate});

	}
}
	//else type= file
	else{
		console.log("breadcrumbs");
		
		docsCtrlVariables.setpropertyDoc(doc);
	//	//console.log(doc.isLocked.user_id,$rootScope.loggedIn._id, !doc.isLocked.status)
	blockUI.start();
	  blockUI.message('Opening' + doc.documentName.slice(0,10) + "..");
if(doc.isLocked.user_id==$rootScope.loggedIn._id || !doc.isLocked.status)
		{/*activity log for file view*/
		var viewLog = {};
						viewLog.activitytype = activity_type.VIEW;
						viewLog.what = doc._id;
						viewLog.location = doc.parent;
						viewLog.status = true;
						ActivitylogService.LogActivity(viewLog);
// console.log(viewLog,ActivitylogService,viewLog.location,doc.parent,doc);
						/*if a document open with gdocs*/
		
			
						var file_id = {
							id : doc._id
						};
						if(version){
							file_id.version=version;
							doc.url=doc.url+'?versionId='+version;
						}
						console.log("file_id",file_id);
						DefaultDocs.viewFile(file_id, function(result) {
							 console.log(result,result.url,doc.url);
							blockUI.start();

// console.log(doc);
			if($scope.checkIfImage(doc)){
		
			$rootScope.isImage=true;
			$rootScope.imageUrl=result.url;
			console.log($rootScope.imageUrl);
		}
		else
		{
			console.log(doc);
		$rootScope.isImage=false;	
		if(((/\.(pdf)$/i).test(doc.documentName))){
			console.log("pdf");
			$rootScope.viewurl=doc.url;
			console.log(doc.url)
		}else{
			console.log("not pdf and images",doc.url);
			$rootScope.viewurl="https://view.officeapps.live.com/op/view.aspx?src="+doc.url;
			console.log($rootScope.viewurl);
		};

		}						
								$rootScope.documentDetails=doc;
								$rootScope.documentHeading=doc.documentName;
								// console.log("doc",doc);
								$rootScope.documentHeadingId=doc;
								
								docsCtrlVariables.openFile();

								}, function(err) {
									blockUI.stop();
									$scope.error = "Documenet is locked by " + doc.isLockedtoName;
									
									//SSsweetAlert.swal("Error",$scope.error,"error");

								});		

		// else{
		// //	// //console.log("image",doc);
		// 	var file_id = {
		// 					id : doc._id
		// 				};
		// 	if(version){
		// 					file_id.version=version;
		// 				}
		// 				DefaultDocs.viewFile(file_id, function(result) {
		// 						$rootScope.viewurl = result.url;
		// 						$rootScope.documentHeading=doc.documentName;
		// 						docsCtrlVariables.openFile();
		// 						})

		// }
		}
		else{
			blockUI.stop();
				notify({ message: doc.documentName +' is Locked' ,classes: 'alert-danger', templateUrl: $scope.homerTemplate});

		}
	}	
	});
			
};

if(whichDoc=="Client Documents" && $rootScope.projectViewDoc){
	//$scope.folderClick($rootScope.projectViewDoc);
}

/*
For BreadCrumbs
*/
/*
check if first
*/
$scope.checkIfFirst=function(){
	var bread = docsCtrlVariables.getbreadcrumbs();
	if(bread.length>0 && whichDoc=="shared")
		return true;
	else
		return false;
}
/*
for hiding contribs
*/
$scope.checkIfSharedDoc=function(){
	var bread = docsCtrlVariables.getbreadcrumbs();
	if(bread.length==0 && whichDoc=="shared")
		return true
	else
		return false
}

$scope.checkIfClientDoc=function(){
	var bread = docsCtrlVariables.getbreadcrumbs();
	if(bread.length==1 && whichDoc=="Client Documents")
		return true
	else
		return false
}


/*
clicked on shared in breadCrumb
*/
$scope.clickOnshared=function(){
	$scope.showversion=false;
	docsCtrlVariables.newbreadcrumbs();
	 docsCtrlVariables.getDocs(whichDoc,function(){

	 });
}
/*


Check if shared
*/
$scope.checkIfShared=function(){
	if(whichDoc=="shared")
		return true;
	else
		return false;
}
/*
FUNCTION TO get breadcrumbs
*/
$scope.getBreads=function(){
	var bread = docsCtrlVariables.getbreadcrumbs();
	return bread;
}

/*
///End all bread Crumbs for share
*/
/*
Start Breads for Search
*/
$scope.checkIfSearch=function(){
	if(whichDoc=="search")
		return true;
	else
		return false;
}

$scope.clickOnsearch=function(){
	docsCtrlVariables.newbreadcrumbs();
	 docsCtrlVariables.getDocs(whichDoc);
}

$scope.checkIfFirstS=function(){
	var bread = docsCtrlVariables.getbreadcrumbs();
	if (bread.length>0 && whichDoc=="search")
		return true;
	else
		return false;

}
/*
Start BreadCrumbs if its Fav documents

*/
$scope.checkIfFav=function(){
	if(whichDoc=="fav")
		return true;
	else
		return false;
}
$scope.checkIfFirstF=function(){
	var bread = docsCtrlVariables.getbreadcrumbs();
	if (bread.length>0 && whichDoc=="fav")
		return true;
	else
		return false;
}
$scope.clickOnFav=function(){
	$scope.showversion=false;
	docsCtrlVariables.newbreadcrumbs();
	 docsCtrlVariables.getDocs(whichDoc);
}

$scope.checkIfFavDoc=function(){
	var bread = docsCtrlVariables.getbreadcrumbs();
	if(bread.length==0 && whichDoc=="fav")
		return true
	else
		return false
}

$scope.checkIfSearchDoc=function(){
var bread = docsCtrlVariables.getbreadcrumbs();
	if(bread.length==0 && whichDoc=="search")
		return true
	else
		return false
}
/*
End Fav breads
*/
$scope.getDocumentList=function(){
	var docs= docsCtrlVariables.getDocumentInfo();
	//console.log("document list",docs)
	return docs;
}
/*Function to convert time in moments
@Params: time to be converted
returns: Time in sentence
*/

$scope.myPagingFunction = function(){
	//console.log("scrolling");
}
$scope.sendTime=function(date){
		var DateFormat = $moment(date).fromNow();
		return DateFormat;
	}
/*
check length of all docs
*/
$scope.checkLengthDoc=function(){
	var documentLength=docsCtrlVariables.giveLength();
	$scope.actualLength= docsCtrlVariables.giveLength();
	return documentLength;
};
$scope.len1=docsCtrlVariables.giveLength();
$scope.len2=docsCtrlVariables.giveLengthtrash();

$scope.checkLengthDocTrash=function(){
	var documentLength=docsCtrlVariables.giveLengthtrash();
	$scope.trashLength =docsCtrlVariables.giveLengthtrash()
	return documentLength;
};
$scope.checkLengthoFdocs=function(){
	if($scope.showTrash==false){
		var documentLength=docsCtrlVariables.giveLength()
		console.log(documentLength);
	if(documentLength==0)
		return true;
	else
		return false;
	}
	else {
		var documentLength=docsCtrlVariables.giveLengthtrash()
			if(documentLength==0)
				return true;
			else
				return false;		
	}
	
};

/*Function to convert time in moments
@Params: time to be converted
returns: Time in sentence
*/

	$scope.sortByWhat = ['type', 'created'];
	$scope.isSort = true;

	$scope.callSort=function(){
		if ($scope.isSort) {
			$scope.sortByWhat = ['type', 'documentName'];
			$scope.isSort = false;
		} else {

			$scope.sortByWhat = ['type', '-documentName'];
			$scope.isSort = true;
		}
	}


/*
Function Name: CheckAll
for checkbox selectAll
Is called when select all is called, end of the function, all the documents being listed will be 
to pushed to an array
*/
//checkAll function
	$scope.checkAll = function() {

		var documentList=docsCtrlVariables.getDocumentInfo();
		$scope.cbSelected = [];
		$scope.trashSelected=[];
		angular.forEach(documentList, function(document, key) {
			//if(document.trash==false){
				document.check = $scope.checkAllItems;
			//}			
			if (document.check == 'true') {
						if(document.trash==false){
							$scope.cbSelected.push(document._id);	
						}
						else{
							$scope.trashSelected.push(document._id);
						}
			}
			
		});

	};

/*
Function Name: Checkbox
Is called when checkbox is clicked on, based on if selected or selected twice, doc id will be pushed or
spliced from cb selected
*/
		//Keep a list of all check boxes selected
	$scope.checkbox = function(id,trash) {
			var docLength=$scope.checkLengthDoc();
			if (trash && $scope.trashSelected.length==0) {
				
					$scope.trashSelected.push(id);
					if ($scope.cbSelected.length + $scope.trashSelected.length == docLength)
				$scope.checkAllItems = true;

			}
			else if(!trash && $scope.cbSelected.length==0){
				$scope.cbSelected.push(id);
				if ($scope.cbSelected.length + $scope.trashSelected.length == docLength)
				$scope.checkAllItems = true;
			}	
					
		 else {
			if ($scope.cbSelected.indexOf(id) == -1 && !trash) {
				
					
				
					$scope.cbSelected.push(id);
			}
			else if($scope.cbSelected.indexOf(id) >-1 && !trash){
				$scope.cbSelected.splice($scope.cbSelected.indexOf(id), 1);

			}
							
			else if($scope.trashSelected.indexOf(id) == -1 && trash){

				$scope.trashSelected.push(id);
			} else if($scope.trashSelected.indexOf(id) >-1 && trash){
					$scope.trashSelected.splice($scope.trashSelected.indexOf(id), 1);
				}
			}
		
		if($scope.cbSelected.length + $scope.trashSelected.length==docLength)
		{
			$scope.checkAllItems = true;
		}
		else
		{
			$scope.checkAllItems = false;
		}
		
		// $scope.trashcount=0;
		// $scope.untrashcount=0;
		// angular.forEach(docsCtrlVariables.getDocumentInfo(), function(value, key){
		// 	angular.forEach($scope.cbSelected, function(val, k){
		// 		if(value._id==val){
		// 			if(value.trash==true){
		// 				$scope.trashcount++;
		// 			}
		// 			else{
		// 				$scope.untrashcount++;
		// 			}
		// 		}
		// 	});
		// });		
		// angular.forEach(docsCtrlVariables.getDocumentInfo(), function(value, key){
		// 	angular.forEach($scope.trashSelected, function(val, k){
		// 		if(value._id==val){
		// 			if(value.trash==true){
		// 				$scope.trashcount++;
		// 			}
		// 			else{
		// 				$scope.untrashcount++;
		// 			}
		// 		}
		// 	});
		// });			

		// if($scope.trashcount>0 && $scope.untrashcount >0 ){
		// 	$scope.trashcontent=false;
		// }
		// else if($scope.trashcount==0 || $scope.untrashcount ==0){
		// 	$scope.trashcontent=true;
		// }

		// if($scope.trashcount>0 && $scope.untrashcount ==0){
		// 	$scope.permadel=true;

		// }
		// else{
		// 	$scope.permadel=false;
		// }


	};
/*
@Function name:allotPermission
@prams:document object.
This will in turn call check for permission with a promise
*/
	$scope.allotPermission=function(doc){
	$scope.checkForPermission(doc,function(){

	});
	}
$scope.checkForPermission = function(folder,cb) {
		//////\//console.log(folder.documentName);
		//blockUI.start();
		//$scope.allowFolderClick = false;
		var docIndex;
		var flag = true;

		folder.access_list = [];
		Groups.getdocument({
			"document" : folder._id
		}, function(response) {
			console.log("res is",response);
			if (response.length > 0) {
				angular.forEach(response, function(user, key) {
					console.log("user info",user,$rootScope.loggedIn);
					console.log("user",user.user);
					var obj = {};
					if(!user.groupId){
						obj.policy_id = user.policy;
							obj.user_id = user.user;
							folder.access_list.push(obj);
							$rootScope.showEmptyContribList = false;

					}
							if ($rootScope.loggedIn._id == user.user._id) {
								flag = false;
								folder.policyName = user.policy.policy_name;
								folder.allowDelete = $scope.givePermission(user.policy.permissions.delete)
								folder.allowDownload = $scope.givePermission(user.policy.permissions.download);
								folder.allowAddFolders = $scope.givePermission(user.policy.permissions.add_folders);
								folder.allowEdit = $scope.givePermission(user.policy.permissions.edit);
								folder.allowShare = $scope.givePermission(user.policy.permissions.share);
								folder.allowUpload = $scope.givePermission(user.policy.permissions.upload);
								folder.allowView = $scope.givePermission(user.policy.permissions.view);
								if (folder.policyName == "Owner" || folder.policyName == "Co-owner" || folder.policyName == "Publisher") {
									folder.allowLock = true;
									folder.allowRemovingContrib = true;
									folder.showInvite=true;
									$scope.showInvite=true;

								} else {
									folder.allowLock = false;
									folder.allowRemovingContrib = false;
									folder.showInvite=false;
									$scope.showInvite=false;
									
								}	
							}					
					// angular.forEach(user.policies, function(policy, key) {
					// 	if (policy.document.indexOf(folder._id) > -1) {
					// 		obj.policy_id = policy.policy;
					// 		obj.user_id = user.user;
					// 		folder.access_list.push(obj);
					// 		$rootScope.showEmptyContribList = false;
					// 		if ($rootScope.loggedIn._id == user.user._id) {
					// 			flag = false;
					// 			folder.policyName = policy.policy.policy_name;
					// 			folder.allowDelete = $scope.givePermission(policy.policy.permissions.delete)
					// 			folder.allowDownload = $scope.givePermission(policy.policy.permissions.download);
					// 			folder.allowAddFolders = $scope.givePermission(policy.policy.permissions.add_folders);
					// 			folder.allowEdit = $scope.givePermission(policy.policy.permissions.edit);
					// 			folder.allowShare = $scope.givePermission(policy.policy.permissions.share);
					// 			folder.allowUpload = $scope.givePermission(policy.policy.permissions.upload);
					// 			folder.allowView = $scope.givePermission(policy.policy.permissions.view);
					// 			if (folder.policyName == "Owner" || folder.policyName == "Co-owner" || folder.policyName == "Publisher") {
					// 				folder.allowLock = true;
					// 				folder.allowRemovingContrib = true;
					// 				folder.showInvite=true;
					// 				$scope.showInvite=true;

					// 			} else {
					// 				folder.allowLock = false;
					// 				folder.allowRemovingContrib = false;
					// 				folder.showInvite=false;
					// 				$scope.showInvite=false;
									
					// 			}	
					// 		}
					// 	}

					// })
				})
		//		////console.log(folder);
				if (flag == true) {
					//set as owner if admin
					folder.policyName = "Owner";
					//
					folder.allowDelete = true;
					folder.allowDownload = true;
					folder.allowAddFolders = true;
					folder.allowEdit = true;
					folder.allowShare = true;
					folder.allowUpload = true;
					folder.allowView = true;
					folder.allowLock = true;
					folder.allowRemovingContrib = true;
					folder.showInvite=true
					$scope.showInvite=true;
				}
			
			} else {
		//		////console.log(response.length);
				folder.policyName = "Owner";
				folder.allowDelete = true;
				folder.allowDownload = true;
				folder.allowAddFolders = true;
				folder.allowEdit = true;
				folder.allowShare = true;
				folder.allowUpload = true;
				folder.allowView = true;
				folder.allowLock = true;
				folder.showInvite=true;
				$scope.showInvite=true;
				folder.allowRemovingContrib = true;
			}
	//console.log(folder);
			cb();

		}, function(err) {
			//blockUI.stop();
		});
		$scope.allowFolderClick = true;
	};
/*
give permission
*/
//change allow or delete
	$scope.givePermission = function(value) {
		if (value == "deny")
			return false;
		else
			return true;
	};
/*
getLockerName
*/
$scope.getLockerName=function(document){

	var id=document.isLocked.user_id;
		User.show({
				"user_id" : id
			}, function(response) {
				//document.matchLockWithLocker = false;
				document.lockedName = response.name;
	if($rootScope.loggedIn._id==document.isLocked.user_id || document.lockedName == undefined)
	{
		document.lockerText="Unlock"
	}
	else{
		document.lockerText="Document is locked by " + document.lockedName ;
	}
			})
	
}
/*
function : initiate lock
*/
$scope.initiateLock=function(document){
	if(document.isLocked.status==true)
		document.lockImage=true;
	else 
		document.lockImage=false;

}

/*
function Name: checkIDtoLock
gets the the ID of the user to whom the document is locked to 
*/
	$scope.findlock = function(document) {
		////console.log(document.isLocked.status , document.isLocked.user_id,$rootScope.loggedIn._id)

		//check if the document object is valid. if not unlock and return.
$scope.checkForPermission(document,function(){

		if (document == undefined || document.isLocked == undefined || document.isLocked.status == undefined) {
			
	/*		document.isLocked.status=true;
			document.isLocked.user_id=$rootScope.loggedIn._id;*/
			document.isLocked.lockImage=true
			return;
		}

		if (document.allowLock){
			if (document.isLocked.status && document.isLocked.user_id==$rootScope.loggedIn._id ) {
				//modify doc to include islocked as true
				// var obj={
				// 	status:false
				// };
				document.isLocked.status = false;
			
				
				DefaultDocs.locked(document, function(data) {
					document.lockImage=false;
						var lockLog = {};
					lockLog.activitytype = activity_type.UNLOCK;
					lockLog.what = document._id;
					lockLog.location = document.s3_path;
					lockLog.status = true;
					ActivitylogService.LogActivity(lockLog);
					
				});
			} else {
				//modify doc to include islocked as false
				// var obj={
				// 	user_id:$scope.getCurrentUser()._id,
				// 	status:true
				// };

				document.isLocked.status = true;
				DefaultDocs.locked(document, function(res) {
					document.isLocked.status=true;
					document.isLocked.user_id=$rootScope.loggedIn._id;
					var unlockLog = {};
					unlockLog.activitytype = activity_type.LOCK;
					unlockLog.what = document._id;
					unlockLog.location = document.s3_path;
					unlockLog.status = true;
					ActivitylogService.LogActivity(unlockLog);
					document.lockImage=true;
					document.lockedName =$scope.loggedIn.name
				},function(err){
				notify({ message: 'Warning - Document Locked by ' +document.lockedName ,classes: 'alert-danger', templateUrl: $scope.homerTemplate});
				});
			}
		} else {
			var lockFile = document.documentName;
			//swal("You don't have persmission to lock " + lockFile);
			sweetAlert.swal("Error","You don't have persmission to lock/unlock " + lockFile,"error");
		}
	})
	};

$scope.initiateFav=function(document){
	if (document.isFavourite.status == true){
		document.favText=" Favorite"
			
		}else{
			document.favText=" Favorite"
			
		}
}


/*
function to set favourite
*/
$scope.setFavourite=function(document)
{

	if(document.isLocked.status == true && document.isLocked.user_id!=$rootScope.loggedIn._id){

        notify({ message: 'Warning - Document Locked ' ,classes: 'alert-danger', templateUrl: $scope.homerTemplate});
    }
	else{
		if (document == undefined || document.isFavourite == undefined || document.isFavourite.status == undefined) {
			document.isFavourite.status = true;
			return;
		}

		if (document.isFavourite.status) {
			//modify doc to include islocked as true
			var obj = {
				user_id : $scope.getCurrentUser()._id,
				status : false
			};
			document.isFavourite.status = false;
			DefaultDocs.favroite(document, function(data) {
				document.isFavourite.status = false;
				var unfavLog = {};
				unfavLog.activitytype = activity_type.UNFAV;
				unfavLog.what = document;
				unfavLog.location = document.s3_path;
				unfavLog.status = true;
				ActivitylogService.LogActivity(unfavLog);
				document.favImage = false
			})

			
		} else {
			//modify doc to include islocked as false
			var obj = {
				user_id : $scope.getCurrentUser()._id,
				status : true
			};
			document.isFavourite.status = true;
			DefaultDocs.favroite(document, function(res) {
				
				document.isFavourite.status = true;
				//from activity service
				var favLog = {};
				favLog.activitytype = activity_type.FAV;
				favLog.what = document;
				favLog.location = document.s3_path;
				favLog.status = true;
				ActivitylogService.LogActivity(favLog);
				document.favImage = true;
				document.favName = $scope.loggedIn.name
			});
			
		}
	}
	
}
/*FunctionName: Download
@Params:docuemnt object
Returns: URL to start download*/
$scope.download=function(document,version){
	//console.log("vers",version);
			$scope.checkForPermission(document,function(){
if(document.isLocked.user_id==$rootScope.loggedIn._id || !document.isLocked.status)
{

	if(document.allowDownload){
	var current = docsCtrlVariables.getCurrentDocument();	
	var file_id = {
				id : document._id
			};
			if (version){
				file_id.version=version;
			}
			DefaultDocs.download(file_id, function(result) {
				location.href = result.url;
			var downloadLog = {};
			downloadLog.activitytype = activity_type.DOWNLOAD;
			downloadLog.what =  document._id;
			downloadLog.location = current.parent + '/' + current.documentName;
			downloadLog.status = true;
			ActivitylogService.LogActivity(downloadLog);
			//from activity service
			$scope.downloadLog = activityFcrt.save(downloadLog, function(data) {
			});
			}, function(err) {
				notify({ message: 'Warning - Please Enter Valid Username or Password', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
       
			});
		}
		else{
			notify({ message: 'Error - You do not have permission to download '+document.documentName , classes: 'alert-danger', templateUrl: $scope.homerTemplate});

		}
	}
	else{
			notify({ message: 'Error - '+document.documentName +' is Locked' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
	}
  });
}

/*FunctionName: findIcon(document)
@Params:docuemnt object
Returns: Class for display of appropriate icon*/
	$scope.findIcon=function(document){
	/*
	first criteria if the type is a folder
	*/		var icon=docsCtrlVariables.findIcons(document)
		return icon;
			}
/*
FunctionName: multidel
will delete all ID's present in cbSeleceted
*/
$scope.multiDel = function(document,id) {
	// console.log(document);
	if(!$scope.showTrash && $scope.trashSelected.length>0){
		//two function calls max
		//$scope.setUpObject();
	var currentDoc=docsCtrlVariables.getCurrentDocument();
		var documentList=docsCtrlVariables.getDocumentInfo();
		if ($scope.cbSelected.length != 0 || $scope.trashSelected.length != 0 ) {
	sweetAlert.swal({
				title : "Warning",
				text : "Files in Trash will be deleted permanently, other files will be trashed!",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#62CB31",
				cancelButtonColor:"#F44336",
				confirmButtonText : "Delete",
				cancelButtonText : "Cancel",
				closeOnConfirm : true,
				closeOnCancel : true
			}, function(isConfirm) {
				$scope.confirmdelete = true;
				if (isConfirm) {
					$scope.checkAllItems = false;
					for (var j = 0; j < $scope.trashSelected.length; j++) {
						var obj = {
							id : $scope.trashSelected[j]
						};
						//
						DefaultDocs.permanentDel(obj, function(data) {
								//splice
								docsCtrlVariables.trashDoc(data.requestObject._id);
								var multiDelLog = {};
								multiDelLog.activitytype=activity_type.DELETE;
								multiDelLog.location=data.requestObject.s3_path;
	 						multiDelLog.what = data.requestObject;			
	 						multiDelLog.status = true;
							 ActivitylogService.LogActivity(multiDelLog);
								// DefaultDocs.show({
								// 	id : currentDoc._id
								// }, function(response) {
								// 	docsCtrlVariables.setDocumentInfo(response);
								// 	$scope.documentList = response;
								// 	$scope.list = $scope.documentInfo;
								// 	//sweetAlert.swal("Deleted!", "delete done.", "success");
								
								// });
						}, function(err) {
							/*swal(err.data);*/
						sweetAlert.swal("Error",err.data,"error");
							angular.forEach(documentList, function(document, key) {
								document.check = 'false';
							});
						});
					}
	$scope.trashSelected = [];
	// deleting normal files 
						for (var j = 0; j < $scope.cbSelected.length; j++) {
						var obj = {
							id : $scope.cbSelected[j]
						};
						//
						DefaultDocs.delete(obj, function(data) {
								var multiDelLog={};
								multiDelLog.activitytype="Trashed";
	 						multiDelLog.what = data.requestObject._id;
	 						multiDelLog.location=data.requestObject.s3_path;
	 						//multiDelLog.what = data.requestObject;			
	 						multiDelLog.status = true;
							 ActivitylogService.LogActivity(multiDelLog)
								//splice
								//docsCtrlVariables.spliceDoc(data.requestObject._id);
								// DefaultDocs.show({
								// 	id : currentDoc._id
								// }, function(response) {
								// 	docsCtrlVariables.setDocumentInfo(response);
								// 	$scope.documentList = response;
								// 	$scope.list = $scope.documentInfo;
								// 	//sweetAlert.swal("Deleted!", "delete done.", "success");
								// });
						}, function(err) {
							/*swal(err.data);*/
						sweetAlert.swal("Error",err.data,"error");
							angular.forEach(documentList, function(document, key) {
								document.check = 'false';
							});
						});
					}
					sweetAlert.swal("Deleted!", "delete done.", "success");
					$scope.cbSelected = [];
				} else {
					$scope.cbSelected = [];
					$scope.trashSelected = [];
					angular.forEach(documentList, function(document, key) {
						document.check = 'false';
					});
					sweetAlert.swal("Cancelled", "Your files/folder is safe :)", "error" );
					$scope.checkAllItems=false;
				}
			});
		} else {
			sweetAlert.swal("Cancelled", "You have not selected any folder/file", "error");

		}	
	}
	else{
		var currentDoc=docsCtrlVariables.getCurrentDocument();
		var documentList=docsCtrlVariables.getDocumentInfo();
		// console.log(currentDoc,documentList);
		$scope.trashDelete();
		};
	};

$scope.checkForWhoLocked = function(document) {
		if ($rootScope.loggedIn == document.isLocked.user_id || document.isLocked.user_id == null) {
			document.matchLockWithLocker = true;
		} else {
			User.show({
				"user_id" : document.isLocked.user_id
			}, function(response) {
				document.matchLockWithLocker = false;
				document.isLockedtoName = response.name;
			})
		}
	};

$scope.trashDelete=function(){
			var currentDoc=docsCtrlVariables.getCurrentDocument();
		var documentList=docsCtrlVariables.getDocumentInfo();
		// console.log($scope.cbSelected,currentDoc,documentList);

		if ($scope.cbSelected.length != 0) {
			// console.log($scope.lockDocName,$scope.cbSelected);
			
				// if($scope.lockDocName.isLocked.user_id==$rootScope.loggedIn._id || !$scope.lockDocName.isLocked.status){

						sweetAlert.swal({
				title : "Are you sure?",
				text : "Your files will be moved to trash!!",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#1ABC9C",
				confirmButtonText : "Delete",
				cancelButtonText : "Cancel",
				closeOnConfirm : true,
				closeOnCancel : true
			}, function(isConfirm) {

				$scope.confirmdelete = true;
				if (isConfirm) {
					$scope.checkAllItems = false;
					for (var j = 0; j < $scope.cbSelected.length; j++) {
						var obj = {
							id : $scope.cbSelected[j]
						};
						//

						DefaultDocs.delete(obj, function(data) {
								var multiDelLog={};
								multiDelLog.activitytype="Trashed";
	 						multiDelLog.what = data.requestObject._id;
	 						multiDelLog.location=data.requestObject.s3_path;
	 						//multiDelLog.what = data.requestObject;			
	 						multiDelLog.status = true;
							 ActivitylogService.LogActivity(multiDelLog)
								//splice
								docsCtrlVariables.spliceDoc(data.requestObject._id);
								// DefaultDocs.show({
								// 	id : currentDoc._id
								// }, function(response) {
								// 	docsCtrlVariables.setDocumentInfo(response);
								// 	$scope.documentList = response;
								// 	$scope.list = $scope.documentInfo;
								// 	//sweetAlert.swal("Deleted!", "delete done.", "success");
								// });
							
							

						}, function(err) {
							
							/*swal(err.data);*/
						sweetAlert.swal("Error",err.data,"error");
							angular.forEach(documentList, function(document, key) {
								// console.log(document);
								document.check = 'false';
							});
						});
					}
					sweetAlert.swal("Deleted!", "delete done.", "success");

	$scope.cbSelected = [];
				} else {
					$scope.cbSelected = [];
					angular.forEach(documentList, function(document, key) {
						// console.log(document);
						document.check = 'false';
					});
					sweetAlert.swal("Cancelled", "Your files/folder is safe :)", "error");
					$scope.checkAllItems=false;
				}
			});
							

		} else {
			sweetAlert.swal("Cancelled", "You have not selected any folder/file", "error");
		}
};

/*
functionName:deleteFolder
*/
$scope.deleteFolder=function(document){
//	// //console.log(document,document._id);
	$scope.cbSelected = [];
		$scope.cbSelected.push(document._id);
	//	// //console.log($scope.cbSelected);
		$scope.trashDelete();

};


/*
functionName: removePolicy
*/
$scope.removePolicy=function(doc){
	var current = docsCtrlVariables.getCurrentDocument();
	ShareFctry.delete_policy({
					"doc_id" : current._id,
					"policyid" : doc
				}, function(res) {
					var index = $rootScope.currentAccess.indexOf(doc);
					$rootScope.currentAccess.splice(index, 1);

				})
}
/*
function Name; createFolderRow
will set a variable true to show one row 
*/
$scope.createFolderRow=function(){
	$scope.showAddRow=true;
}
/*
funcrionName: addFolder
@Param : folder name 
return : error if folder already exists
*/

$scope.addFolder=function(newFoldername){
	blockUI.start();
	$scope.disableAddFolder=true;
	$scope.sortByWhat = ['type', 'created'];

	//$scope.currentDocForAdd=angular.copy($scope.currentDocument);
	 		 
	 		 var flags= docsCtrlVariables.addfolder(newFoldername)
	 		  $scope.disableAddFolder=flags.disableAddFolder;
	 		  $scope.showAddRow=flags.showAddRow;
	 		  $scope.folderName=flags.folderName;
	 		  
// console.log($scope.showNoFiles);
};


/*
functio to close the add folder row
*/
$scope.closeAddFolder=function(){
	$scope.showAddRow=false;
}

/*
Function name upload
@param: file names
*/
$scope.uploadFiles=function(files)//
{
	// console.log(files);
	 // blockUI.start();
	$scope.showNoFiles=false;
	if($scope.versionuploading ==true)
	{
		$scope.hiderow=files;
		$scope.uplaodwhatFile=$scope.hiderow;
	}
	else
	{
		$scope.showUploadRow=files;	
		$scope.uplaodwhatFile=$scope.showUploadRow;
	}
		docsCtrlVariables.uploadFiles($scope.uplaodwhatFile,$scope.showNoFiles,$scope.versionuploading);
		 // blockUI.stop();
		 // console.log($scope.showNoFiles);
	};
// console.log($scope.showNoFiles);


/*
Can he remove contrib
*/
$scope.canUserRemoveContrib=false;


/*
check if image is locked
*/
$scope.checkLockImage=function(doc){

	if(!doc.trash)
		{
	if(doc.isLocked.user_id==$rootScope.loggedIn._id || !doc.isLocked.status)
		return true;
	else
		return false;		
	}
	else 
	return false;
};

$scope.supplyDocumentUrl=function(doc){

	if(!doc.trash && doc.type=='file')
	{
		
			if(doc.isLocked.user_id==$rootScope.loggedIn._id || !doc.isLocked.status){
				////////console.log(doc);
				return doc.url;
			}	
		}
	};
	$scope.imageUrl=function(doc,version){
		// var file_id = {
		// 			id : doc._id
		// 		  };
		// 		if(version){
		// 			file_id.version=version;
		// 			}
		// 		DefaultDocs.viewFile(file_id, function(result) {
		// 			doc.url=result.url;
		// 		})
	};


/*
FunctionName:BreadClick
@params: bread object
will splice everything infront of it and 
*/
$scope.breadClick=function(fObject)
{
	blockUI.start();
	$scope.checkForPermission(fObject,function(){
	
	console.log(fObject,"breadcrumbs");
	$scope.checkAllItems=false;
	
	blockUI.message('Opening' + " " + fObject.documentName);
	$scope.cbSelected=[];
$scope.trashSelected=[];	
	if(fObject.root==true && fObject.documentName=="My Documents"){
		$scope.allowUpload=true;
		$scope.allowCreateFolder=true;
		$scope.allowDelete=true;
	}
	else{
		if(fObject.trash==true){
			$scope.allowUpload=false
			$scope.allowCreateFolder=false;
			
		}
		else{
		$scope.allowUpload=fObject.allowUpload;
		$scope.allowCreateFolder=fObject.allowAddFolders;
			
		}
	
	
}	$scope.showversion=false;	
	docsCtrlVariables.breadClick(fObject);
})
};

// $scope.getshowInvite=function(){
//// 	////console.log(docsCtrlVariables.getCurrentDocument().showInvite);
// 	return docsCtrlVariables.getCurrentDocument().showInvite;
// }

/*
*function to open timeline modal
*/
$scope.setTimeline=function(doc){
	timelineService.setTimeline(doc);
}


/*
*returns the present version of the file
*/
$scope.getVersion=function(doc){
	return(versionService.getVersion(doc));	
}

$scope.versionarray=[];
/*
*returns the present version of the file
*/
$scope.openVersion=function(doc){
	// console.log(doc);
	$scope.versionarray=[];
	var count=0;
	doc.version.forEach(function(val){
		// console.log(val);
		var verobj={
			version:val.number,
			versionId:val.versionId,
			created_at:val.created_at,
			documentName:doc.documentName,
			versionflag:true,
			url:doc.url,
			created_by:val.added_by,
			createdBy:val.added_by,
			s3_path:doc.s3_path,
			parent:doc.parent,
			description:doc.description,
			comments:doc.comments,
			isLocked:doc.isLocked,
			allowDownload:doc.allowDownload,
			thumbnail_version:val.thumbnail_version,
			_id:doc._id,
			document:doc
		};
		// console.log(doc.version,verobj);
		count++;
		if(count<doc.version.length){
		$scope.versionarray.push(verobj);	
		}
		
	})
	$scope.versionfilename=doc.documentName;
	$scope.showversion=true;
}

/*
*Function to make particular version as current version
*/
$scope.makeCurrentVersion=function(versionobj){
	var allowVersion=false;
if(versionobj.isLocked.status==false)
{
	allowVersion=true;
}
else if(versionobj.isLocked.user_id==$rootScope.loggedIn._id)
{
allowVersion=true;
}
else{
allowVersion=false;	
}
////console.log(versionobj.isLocked.user_id==$rootScope.loggedIn._id,allowVersion)
if(allowVersion){


	var tempobj={
		version:versionobj.version,
		id:versionobj._id
	}
		DefaultDocs.changeversion(tempobj,function(data){
				var viewLog = {};
						viewLog.activitytype = activity_type.CHANGEVERSION;
						viewLog.what = versionobj.document._id;
						viewLog.location = versionobj.document.parent;
						viewLog.status = true;
						ActivitylogService.LogActivity(viewLog);

		$scope.showversion=false;
		//$state.go("dashboard", {}, { reload: true });
		$scope.checkForPermission(docsCtrlVariables.getCurrentDocument(),function(){
			$scope.folderClick(docsCtrlVariables.getCurrentDocument());
		});
		 
		// Take it to the listing page
	},function(err){
		console.log(err);
		
	});
	}
	else{
				notify({ message: 'Document is locked '+ versionobj.isLockedtoName, classes: 'alert-danger', templateUrl: $scope.homerTemplate});

	}
};
/*
function name:checkIfImage
@param:checkIfImage(document)
*/
$scope.checkIfImage=function(document){
	if((/\.(gif|jpg|jpeg|tiff|png|ico)$/i).test(document.documentName))
		return true;
	else 
		return false;
}

/*
*function to get thumbnail url of version list
*/
$scope.getThumbnailVersion=function(doc){
	versionService.getThumbnailVersion(doc);
}


/*
*function : To get thumbnail url
*/
$scope.getThumbnail=function(doc){
	versionService.getThumbnail(doc);

}
/*
*function Add new version
*params:Doc object
*/
$scope.addNewVersion=function(file,doc){
var allowVersion=false;
if(doc.isLocked.status==false)
{
	allowVersion=true;
}
else if(doc.isLocked.user_id==$rootScope.loggedIn._id)
{
allowVersion=true;
}
else{
allowVersion=false;	
}
if(allowVersion){
if(doc.documentName==file[0].name){
	doc.versionuploadflag=true;
	$scope.versionuploading=true;
	$scope.uploadFiles(file);
}
else{
	notify({ message: 'A new Version can be uploaded only if it has same name', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
}
}
else{
		notify({ message: 'Document is locked '+ doc.isLockedtoName, classes: 'alert-danger', templateUrl: $scope.homerTemplate});

}
};
// properties
	$scope.openProperties=function(document){
		blockUI.start();
		docsCtrlVariables.setpropertyDoc(document);
		$scope.propertyDoc=document;
		var modalInstance = $uibModal.open({
            templateUrl: 'views/modal/modal_property.html',
            controller: propertyCtrl
          
                 });
	};

/*
functionName:openInviteModal
Calls the invite modal
*/
	$scope.openInviteModal=function(){
		blockUI.start();
		$rootScope.inviteforDocument=$scope.currentDocument;
		var modalInstance = $uibModal.open({
	            templateUrl: 'views/modal/modal_invite.html',
	            controller: inviteCtrl
	                 });
	};

	$scope.openModalhelp=function(){
		//blockUI.start();
				var modalInstance = $uibModal.open({
	            templateUrl: 'views/modal/modal-help.html',
	            controller: 'HelpCtrl'
	                 });
	}
/*
The invite Controller
This will open a modal
*/
function inviteCtrl ($scope,Groups,$uibModalInstance,Auth,$rootScope,policyFactory,ShareFctry,docsCtrlVariables,$moment,blockUI) {
	blockUI.stop();
	/*
	errorMessages
	*/
$scope.errorMessages=function(text){
	notify({ message: text, classes: 'alert-danger', templateUrl: $scope.homerTemplate});
};
$scope.grp=[];
$scope.G_list=[];
$scope.arrayOfgrp=[];
$scope.currentDocu=[];

  //list of alphabets
   $scope.alphabet=["ALL","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
     $scope.ok = function () {
        $uibModalInstance.close();
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.getCurrentUser = Auth.getCurrentUser;
$scope.getCurrentUser().$promise.then(function() {
		$rootScope.loggedIn = $scope.getCurrentUser();
	});

    $scope.getUsers = User.query({'count':'undefined','limit':'undefined'},function(users){
   	 users.$promise.then(function(){
      $scope.users = users;
  			});
	});
    // To get list of Groups
	Groups.getAllGroups({'count':'undefined','limit':'undefined'},function(data){
		$scope.G_list=data;
		Groups.getGroups({"id":docsCtrlVariables.getCurrentDocument()._id},function(groups){
			$scope.assignedGroups=groups;
			angular.forEach($scope.G_list,function(value){
				angular.forEach($scope.assignedGroups,function(val){
					if(val._id==value._id){
						value.ischeck=true;
						angular.forEach(val.groupinvite,function(v){
							if(v.document.indexOf(docsCtrlVariables.getCurrentDocument()._id)>-1){
								value.assignedPolicy=v.policy.policy_name;
							}
						})
					}
				})
			})

		})
	});


	// To capitalize the first letter and display(first letter)
	$scope.firstToUpperCase = function(strng) {
		var First_cap=strng.substr(0, 1).toUpperCase() + strng.substr(1);
			return First_cap;
	};

	$scope.Capital_frst = function(firstLtr) {
		if (firstLtr == undefined)
			return;
		var res = firstLtr.toUpperCase();
		var res1 = res.slice(0,1);
		return res1;
	};

//To Invite a group to contributor list 
	$scope.selectGroup= function(group,policy,index){
		//console.log("start of select group fun");
	var currentDoc=docsCtrlVariables.getCurrentDocument();
		$scope.arrayOfgrp=[currentDoc._id];
		var obj={
				id:group._id,
				policy:policy,
				document:$scope.arrayOfgrp
				};
		 if(group.ischeck==true){
		 	angular.forEach($scope.roles,function(role){
				if(role._id==policy){
					group.assignedPolicy=role.policy_name;
				}
			})		 	
			 Groups.AddGroup(obj,function(res){
			 	Groups.getGroups({"id":currentDoc._id},function(data){
			 		//console.log(data);
					$rootScope.allinvitegroups=data;
					angular.forEach($rootScope.allinvitegroups,function(value){
						angular.forEach(value.groupinvite,function(val){
							if(val.document.indexOf(currentDoc._id)>-1){
								value.policy=val.policy.policy_name;
							}
						})
					})
				});
				var grpAddLog={};
				grpAddLog.activitytype=activity_type.ADD;
	 			grpAddLog.what = res._id;			
	 			grpAddLog.status = true;
				ActivitylogService.LogActivity(grpAddLog);
			})
	    	} else{
	    		group.assignedPolicy="";
			angular.forEach($rootScope.allinvitegroups,function(value){		
					if(value._id==obj.id){
						angular.forEach(value.groupinvite,function(val){
							if(val.document.indexOf(currentDoc._id)>-1){
									obj.policy=val.policy._id;
							}
						})
					}
	    		});
		    Groups.RemoveGroupDoc(obj,function(res){
		    	Groups.getGroups({"id":currentDoc._id},function(data){
					$rootScope.allinvitegroups=data;
					angular.forEach($rootScope.allinvitegroups,function(value){
						angular.forEach(value.groupinvite,function(val){
							if(val.document.indexOf(currentDoc._id)>-1){
								value.policy=val.policy.policy_name;
								
							}
						})
					})
				});
		//var grpDelLog={};
	    //grpDelLog.activitytype=activity_type.ADD;
		//grpDelLog.location=data.requestObject.s3_path;
	 	//grpDelLog.what = data.requestObject;			
	 	//grpDelLog.status = true;
		//ActivitylogService.LogActivity(grpDelLog);
				})
	    	}	
	};
/*
show capital instead of image
*/
$scope.checkIfImageExists=function(user){
	docsCtrlVariables.checkForImage(user,user.avatar,function(){
		$scope.$apply();
	});
};
// To capitalize the first letter and display(first letter)
	$scope.Capital_frst=function(grpname){
		return GroupService.First_UpperCase(grpname);
	};
    /*
    function to search based on selection
    */
    $scope.searchReq=function(alpha){
            	if(alpha=='ALL')
            		$scope.letter=""
            	else
            		$scope.letter=alpha;
    	};
   	/*
		To get the roles
   	*/
  $scope.getRoles = policyFactory.query(function(policy) {
    $scope.roles = policy;
  	$scope.setPolicy();
  });

  /*init function for select*/
	$scope.setPolicy=function(){
		if($scope.roles != undefined){
			if($scope.roles.length > 0){
				$scope.policy = $scope.roles[0]._id;	
			}
		}
	};
  /*
	Function to get theuser and policy to be attached
  */
  $scope.changePolicy=function(user,role){
  	blockUI.start();
  	 blockUI.message("Processing..")
  	var role_id;
  	angular.forEach($scope.roles,function(value,key){
  		if(String(role)==String(value._id)){
  			role_id=value._id;
  		}
  	})
  		
  	var currentDoc=docsCtrlVariables.getCurrentDocument()
  	  	if(user.check==true){
  		if(role){
  		/*call query to add the policy to user*/
  		 var obj={
        		user_id:user._id,
       			policy_id:role,
        		document_id: currentDoc._id    
         };
         var objForRepeat={
         	/*user_id.avatar:user.avatar,
         	policy_id.policy_name:role.policy_name,
         	user_id.name:user.name */
				user_id:{},
				policy_id:{}
	         }
	    objForRepeat.user_id._id = user._id;    
        objForRepeat.user_id.name=user.name;
        objForRepeat.user_id.avatar=user.avatar;
        angular.forEach($scope.roles,function(roles,key){
        	if(roles._id==role)
        	objForRepeat.policy_id.policy_name=roles.policy_name;
        })
        /*
        add contributors
        */
        ShareFctry.save(obj,function(data){
              //push into access list here
              docsCtrlVariables.setCurrentDocument(currentDoc)
              user.assignedRole=objForRepeat.policy_id.policy_name;
              $rootScope.currentAccess.push(objForRepeat)
         
            blockUI.stop();
             // $rootScope.defaultdoc.access_list.push(createObjs)
              //$rootScope.showEmptyContribList=false;
            
          });
  		}
  		else{

  			   blockUI.stop();
  			notify({ message: "Please choose a policy to invite", classes: 'alert-danger', templateUrl: $scope.homerTemplate});
  	  	}
  	}
  	else
  	{
 		/*
 		construct the object
 		*/
 		var doc={}
 		angular.forEach($scope.roles,function(value,key){
  		if(user.assignedRole==value.policy_name){
  			role_id=value._id;
  		}
  	})
 		angular.forEach($rootScope.currentAccess,function(list,key){
						if(list.user_id._id==user._id){
							doc=list;
						}
					})
 			doc.policy_id._id=role_id;
 			ShareFctry.delete_policy({
					"doc_id" : currentDoc._id,
					"policyid" : doc
				}, function(res) {
					var spliceKey
					angular.forEach($rootScope.currentAccess,function(list,key){
						if(String(list.user_id._id)==String(user._id)){
							spliceKey=key;
						}
					})
					user.assignedRole=""
					var index = $rootScope.currentAccess.indexOf(spliceKey);
					$rootScope.currentAccess.splice(spliceKey, 1);
					  
					   blockUI.stop();
				}) 		
  	}
  };
/*
function to check if user is added in the contrib list
*/
$scope.checkIfContributor=function(user){
	angular.forEach($rootScope.currentAccess,function(access_list,key){
		if(access_list.user_id._id==user._id){
			user.check=true;
			user.assignedRole=access_list.policy_id.policy_name;
		}
	})
};

	$scope.sendTime=function(date){
		var DateFormat = $moment(date).fromNow();
		return DateFormat;
	};

}

  })
.directive('ngEnter', function(){
    return function(scope, element, attrs) {
        element.bind("keydown", function(e) {
            if(e.which === 13) {
                scope.$apply(function(){
                    scope.$eval(attrs.ngEnter, {'e': e});
                });
                e.preventDefault();
            }
        });
    };
})
.filter('startsWithLetter',function(){
    return function (items, letter) {
    	if(items){
    		var filtered = [];
        var letterMatch = new RegExp(letter, 'i');
        for (var i = 0; i < items.length; i++) {
            var item = items[i];
            if (letterMatch.test(item.name.substring(0, 1))) {
                filtered.push(item);
            }
        }
        return filtered;
    	}
       };
})
.filter('startsWithLetterN',function(){
    return function (items, letter) {
    	if(items){
    		var filtered = [];
        var letterMatch = new RegExp(letter, 'i');
        for (var i = 0; i < items.length; i++) {
            var item = items[i];
            if (letterMatch.test(item.Name.substring(0, 1))) {
                filtered.push(item);
            }
        }
        return filtered;
    	}
       };





});

/** 2Docstore
 * page: admin (user management page)
 * dev: mamatha
 * created date : 14th  july 2016
 * modified date : 20th  july 2016
 * Discription : creating the user and listing of the user 
 */
 angular.module('homer')
 .controller('userManagementCtrl', function($scope,$http,$filter,blockUI,$state,
 	$rootScope,Auth,notify,User,UserService,GlobalService,$location,DefaultDocs,
 	ActivitylogService,activityFcrt,ErrorService,NoteService,$window,$moment,
 	docsCtrlVariables,Groups,GroupService,ClientService) {
 	// console.log("inside user management")
 	/* To check logged in user is Admin or client admin*/
 	Auth.isAdmin(function(isadmin){
 		$scope.ClientUserFlag= true;
 		Auth.isClientAdmin(function(ClientAdmin){
 			if(ClientAdmin){
 				$scope.ClientUserFlag= false;
 			}
 			if (!isadmin && !ClientAdmin){
 				$state.go('common.error_two');
 			};
 		});
 	});
 	var activity_type = GlobalService.getActivitytype();
 	$scope.homerTemplate = 'views/notification/notify.html';
 	$scope.getCurrentUser = Auth.getCurrentUser;
 	$scope.EmailPtrn = /^[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})/;
 	$scope.phoneNumbr = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{3,7}$/;

 	/*To get logged in user info*/
 	$scope.getCurrentUser().$promise.then(function() {
 		$rootScope.loggedIn=$scope.getCurrentUser();
 		docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);
 		$scope.loggedIn = $scope.getCurrentUser()._id;
 		$rootScope.loggedUser = $scope.loggedIn;
 	});


  // Disabling loggedin to edit his info in user list
  $scope.checkdiableUser=function(data){
  	if(data._id==$scope.loggedIn){
  		return true;
  	} else{
  		return false;
  	}
  }; 
  /*To get userlist (existing users)*/
	// var userslist=[];
	// UserService.getUsers(function(data){
	// 	userslist=data;
	// });	
          // mamatha here          
          
	// $scope.getUserlists=function(){
	// 	return userslist;
	// };



	/*load more option actual one */
// $scope.userCount=0;
// $scope.getUserlists=User.query({'count':$scope.userCount,'limit':20},function(data){
// // console.log(data);
// });


// var Count=$scope.userCount+20;
// $scope.loaduserMore=function(){
// 	var usetlistCount=Count;
// 	var lists=User.query({'count':usetlistCount,'limit':20},function(data){
	
// 		for(var i=0;i<lists.length;i++){
// 			$scope.getUserlists.push(lists[i]);
// 		};
// 	});
// 	Count=Count+20;
// };


/*demo code */
$scope.userCount=0;
$scope.getUserlists=User.query({'count':$scope.userCount,'limit':100},function(data){
			// console.log(data);
		});

var Count=$scope.userCount+100;
$scope.loaduserMore=function(){
	var usetlistCount=Count;
	var lists=User.query({'count':usetlistCount,'limit':100},function(data){		
		for(var i=0;i<lists.length;i++){
			$scope.getUserlists.push(lists[i]);
		};
	});
	Count=Count+100;
};

/*display note in form (add user)*/
var noteuser= "";
var noteEmail= "";
var notePhone="";
var noteOrg="";
var noteAdmin="";
var noteClientAdmin="";


NoteService.getUsermanagementNotes({module:'Note',code:'N_001'},function(data){
	noteuser=data; /*Password note*/
});
$scope.getnoteuser=function(){
	return noteuser;
};
NoteService.getUsermanagementNotes({module:'Note',code:'N_002'},function(data){
	noteEmail=data; /*Enter minimum 3 alphanumeric characters.*/
});
$scope.getnoteEmail=function(){
	return noteEmail;
};
NoteService.getUsermanagementNotes({module:'Note',code:'N_006'},function(data){
	notePhone=data; /*Enter your valid contact number*/
});
$scope.getnotePhone=function(){
	return notePhone;
};
NoteService.getUsermanagementNotes({module:'Note',code:'N_016'},function(data){
	noteOrg=data; /*User will be mapped to the default organization listed*/
});
$scope.getnoteOrg=function(){
	return noteOrg;
};
NoteService.getUsermanagementNotes({module:'Note',code:'N_003'},function(data){
	noteAdmin=data; /*Administrator will have full access ...*/
});
$scope.getnoteAdmin=function(){
	return noteAdmin;
};
NoteService.getUsermanagementNotes({module:'Note',code:'N_004'},function(data){
	noteClientAdmin=data; /*Client admin has limited access, belongs to the organization*/
});
$scope.getnoteClientAdmin=function(){
	return noteClientAdmin;
};
/*display Error in form (add user)*/
var Mobile= "";
var Email="";
ErrorService.getUsermanagementErrors({module:'User',code:'U_004'},function(data){
	Mobile=data;  /*Must be a valid 9 to 13 digit contact number*/
});
$scope.getMobile=function(){
	return Mobile;
};
ErrorService.getUsermanagementErrors({module:'User',code:'U_007'},function(data){
	Email=data;  /*Enter a valid email address*/
}); 
$scope.getEmail=function(){
	return Email;
};
  // Through error message for the empty fields and disappear err after clicking on the fields
  var emptyPhoneNumber="";
  var emptyUserName="";
  var duplicateEmail="";
  var emptyName="";
  var errOrg="";
  $scope.makeEmptyEmail=function(){
  	emptyUserName="";
  };
  $scope.makeEmptyPhone=function(){
  	emptyPhoneNumber="";
  };
  $scope.duplicate_email = function(){
  	duplicateEmail="";
  };
  $scope.makeEmptyName=function(){
  	emptyName="";
  };
 // open the new page for user creation with clearing old data
 $scope.AddUser = function(){
 	$state.go('adduser');
 };
// breadcrumbs  
$scope.userBreadCrumbs=[];
$scope.userManage=function(){
	$scope.userBreadCrumbs=[];
	var b1= "User Management";
	$scope.userBreadCrumbs.push(b1);
};
// This is init function in add user page
$scope.addGroup1=function(){
		//$scope.whichPage=$state.current.name;
		var b1= "User Management";
		var b2="Add User";
		$scope.userBreadCrumbs.push(b1,b2);
	};
// click on bread to go to previous page
$scope.clickOnPage=function(page){
	if(page =="User Management")
		$state.go("userManagement");
};

// listing logo and set flag onloading and error
$scope.imageExists = function(users){
	docsCtrlVariables.checkForImage(users, users.avatar);
};

ClientService.getClientsAsync(function(data){
	
	console.log(data,"clients");
	$scope.clientOrg=data;

	var host = $location.host();
	var tenantdoc={
		name:host.split('.')[0]
	};
	$scope.hostName=host.split('.')[0];
	$scope.clientOrg.push(tenantdoc);
	console.log($scope.clientOrg);
});


// check for role and setting organization
$scope.checkRole=function(user){
 		//console.log("to set an organization",user);
 		if($scope.getCurrentUser().role=="Client Admin" || $scope.getCurrentUser().role=="user"){
 			user.organization=$scope.getCurrentUser().organization;
 			return false;
 		}
 		if($scope.getCurrentUser().role=="admin" && user.role=="admin"){
 			user.organization=$scope.getCurrentUser().clientId;
 			return false;
 		}  else
 		return true;
 	};



 	/*To set time format as (ie:- few min ago)*/
 	$scope.sendTime=function(date){
 		var DateFormat = $moment(date).fromNow();
 		return DateFormat;
 	};



 	/*to check organization*/
 	$scope.checkOrg=function(data){
 		if(data=="user"){
 			console.log("data",data,$scope.clientOrg);
 			var host = $location.host();
 			$scope.hostName=host.split('.')[0];
 			var tenantdoc={
 				name:host.split('.')[0]
 			};
 			$scope.clientOrg.push(tenantdoc);
 		}else{
 			angular.forEach($scope.clientOrg,function(value,key){

 				if($scope.hostName==value.name){
 					console.log(key)
 					$scope.clientOrg.splice(key,1);
 					console.log($scope.clientOrg);
 				}
 			})
 			
 		}
 		
 	}

//To get document folders(MyDocuments and ClientDocuments) ("clientDocs" will the org for client admin)
DefaultDocs.query(function(data){
		// console.log(data)
		$scope.defaultdocs = data;
		angular.forEach($scope.defaultdocs,function(value,key){
			// console.log(value,key);
			if(value.documentName=="Client Documents"){
		    	// console.log(value.documentName);
		    	$scope.getdefaultFolders = DefaultDocs.show({
		    		id : value._id
		    	},function(response){
		    		var host = $location.host();
		    		var tenantdoc={
		    			documentName:host.split('.')[0]
		    		};
		    		console.log(tenantdoc,response);
		    		$scope.clientDocs=response;
		    		$scope.clientDocs.push(tenantdoc);
		    		console.log($scope.clientDocs);
		    	});
		    }
		});
	});
// To capitalize the first letter and display(first letter)
$scope.Capital_frst=function(grpname){
	return GroupService.First_UpperCase(grpname);
}        
//send verificaiton mail
$scope.sendVerification=function(userObj){
	//$scope.sendVerificationnow=false;
	User.reverify(userObj,function(response){
	//$scope.sendVerificationnow=true;
	notify({ message: 'Email sent successfully', classes: 'alert-success', templateUrl: $scope.homerTemplate});
},function(err){
	// $scope.sendVerificationnow=true;
	notify({ message: 'Problem in sending email', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
}); 
};
//Check switch (toggle function)
$scope.checkSwitch=function(user){
	if(user.status==true){
		user.status=false;
		return; 
	}else{
		user.status=true;
		return;
	}
};

//status and roles( need to move this in global factory)
$scope.userstatus = [
{value: true, text: 'Active'},
{value:false, text: 'Disabled'},
];
 // $scope.userstatus=UserService.getStatus();
 $scope.groups=[
 {_id:1, text:'admin'},
 {_id:2, text:'user'},
 {_id:3, text:'Client Admin'}
 ];

 $scope.groups1=[ 
 {_id:1, text:'user'},
 {_id:2, text:'Client Admin'}
 ];
	//To show status,if set show status else not set
	$scope.showStatus = function(user) {
		var selected = [];
		selected = $filter('filter')($scope.userstatus, {value: user.status});
		return selected.length ? selected[0].text : 'Not set';
	};

  //role, if set show role else not set
  $scope.showGroup = function(user) {
  	var selected = [];
  	var grp = [];
  	if(user.role){
  		if($scope.getCurrentUser().role=='admin'){
  			selected = $filter('filter')($scope.groups, {text: user.role});
  			//console.log(selected,$scope.groups);
  		}
  		if($scope.getCurrentUser().role=='Client Admin'){
  			selected = $filter('filter')($scope.groups1, {text: user.role});
  		}	
  	}
  	return selected.length ? selected[0].text : 'Not set';
  };


  // Check if the email is duplicate
	// $scope.isNotDup=function(email){
	// 	var dup=false;
	// 	  angular.forEach(userslist,function(value,key){
	// 	     if(value.username==email){
	// 	    	dup=true;
	// 	    }else {
	// 	    	//dup=false;
	// 	    }
	// 	  });
	// 	  return dup;
	// };
	

//To check all the empty field
$scope.errorMessage=function(mod,codes){
	ErrorService.getUsermanagementErrors({module:mod,code:codes},function(data){
		if(codes=='U_012'){
			emptyName=data;
		}if(codes=='U_013'){
			emptyUserName=data;
		}if(codes=='U_005'){
			emptyPhoneNumber=data;
		}
	});	
};

	//For validation
	$scope.isValiduser=function(user){
		if(!user.name && !user.username && !user.cell_number){
			$scope.errorMessage('User','U_012');
			$scope.errorMessage('User','U_013');
			$scope.errorMessage('User','U_005');
		}
		if(user.name ==undefined){
			ErrorService.getUsermanagementErrors({module:'User',code:'U_012'},function(data){
				emptyName=data;
			});
			return false;
		}
		if(user.username == undefined){
			ErrorService.getUsermanagementErrors({module:'User',code:'U_007'},function(data){
				emptyUserName=data;
			});
			return false;
		}
		if(UserService.DuplicateUser(user.username,$scope.getUserlists)){  
			blockUI.stop(); 
			ErrorService.getUsermanagementErrors({module:'User',code:'U_017'},function(data){
				duplicateEmail=data;
				$scope.Bflag=false;
			});
			return false;
		}
		if(user.cell_number == undefined){
			ErrorService.getUsermanagementErrors({module:'User',code:'U_005'},function(data){			
				emptyPhoneNumber=data;
			});
			return false;
		}
		if(user.organization == undefined){
			ErrorService.getUsermanagementErrors({module:'User',code:'U_018'},function(data){			
				errOrg=data;
			});
			return false;
		}
		return true;
	};
	$scope.getduplicateEmail=function(){
		$scope.dupEmail=true;
		return duplicateEmail;
		blockUI.stop();
		$scope.Bflag=false;

	};
	$scope.getemptyPhoneNumber=function(){
		return emptyPhoneNumber;
	};
	$scope.getemptyUserName=function(){
		return emptyUserName;
	};
	$scope.getemptyName=function(){
		return emptyName;
	};
	$scope.errorOrg=function(){
		return errOrg;
	};

	$scope.Bflag=false;
//add user(this is the callback function for both save and add another user)
$scope.userAdd=function(user){
	console.log("add one user",user,$scope.clientDocs);
	if ($scope.Bflag) {
		return;
	}
	$scope.Bflag=true;
	if ($scope.UserForm.$valid) {
			// console.log("valid user");
			$scope.Bflag=true;
			if(user.role=="Client Admin"){
				console.log("role client",user.role,$scope.clientDocs);
				user.clientid=user.organization._id;
				user.organization=user.organization.name;
					//user.clientid=user.organization._id;
					console.log(user,user.organization,"if");
					
				}else if(user.role=="user")
				{
					user.organization=user.organization.name;
					console.log(user,user.organization,"else");
				}		
				console.log("request",user);
				$scope.submitUser(user,function(){
					$scope.Bflag=true;
					notify({ message: 'User '+ user.name +' is created', classes: 'alert-success', templateUrl: $scope.homerTemplate});
					$state.go('userManagement',{},{reload: true});
				});

			}else {
		          // console.log("not valid");
		          $scope.UserForm.submitted = true;
		          $scope.Bflag=false;
		      }
		      
		  };

		  $scope.Bflag=false;
// To add another user
$scope.oneMore=function(user){
	if ($scope.Bflag) {
		return;
	}
	$scope.Bflag=true;
		// console.log("one more user");
		if ($scope.UserForm.$valid) {
			console.log(user.organization);
			$scope.Bflag=true;
			if(user.role=="Client Admin"){
				console.log("role client",user.role,$scope.clientDocs);
				user.clientid=user.organization._id;
				user.organization=user.organization.documentName;
				console.log(user,user.organization,"if");
				
			}else if(user.role=="user")
			{
				user.organization=user.organization.documentName;
				console.log(user,user.organization,"else");
			}
			$scope.submitUser(user,function(){
				notify({ message: 'User '+ user.name +' is created', classes: 'alert-success', templateUrl: $scope.homerTemplate});
				$state.go($state.current, {}, {reload: true});
			});
		}else{
			 // console.log("not valid");
			 $scope.UserForm.submitted = true;
			 $scope.Bflag=false;
			};	 	
		};

//Creating the user
$scope.submitUser=function(user,cb){
	console.log("user ",user);
	blockUI.start();
	$scope.Bflag=true;
		// console.log("user submit",user);
		if($scope.isValiduser(user)){
			console.log("valid user",user);
	 	 //save the added user
	 	 $scope.Bflag=true;
	 	 User.save(user,function(response){
	 	 	blockUI.stop();
	 	 	var addlog = {};
	 	 	addlog.activitytype = activity_type.USERADD;
	 	 	addlog.what = user.name;
	 	 	addlog.status = true;
	 	 	ActivitylogService.LogActivity(addlog);
	 	 	cb();
	 	 },function(err){ 
	 	 	blockUI.stop();
	 	 	$scope.Bflag=false;
	 	 	cb();
	 	 });
	 	}                        	
	 };
	 
//Updating the user
$scope.updateUser = function(user){
	console.log(user,"update");
	User.update(user,function(users){
		console.log("res",users);
		if(users.type=="error"){
			console.log("error",users.message);
			notify({ message: users.message, classes: 'alert-danger', templateUrl: $scope.homerTemplate});
			$state.go('userManagement'); 
		}else{
			var updatelog = {};
			updatelog.activitytype = activity_type.USERUPDATE;
			updatelog.what = user._id;
			updatelog.status = true;
			ActivitylogService.LogActivity(updatelog);
			$state.go('userManagement');
			notify({ message: 'User '+ user.name +' Updated', classes: 'alert-success', templateUrl: $scope.homerTemplate});
		}
		
	},function(err){ 
		console.log("error",err);
		$state.go('userManagement');      
	});
};

//cancle for add user
$scope.cancel = function(user) {
    	// $scope.user.name= "";
    	// $scope.user.last_name = "";
    	// $scope.user.username = "";
    	// $scope.user.cell_number = "";
    	// $scope.user.organization = "";
    	$state.go($state.current, {}, {reload: true});
    };   
}).filter('filterOrg',function($location){
	//console.log("filterOrg");
	return function (items, letter) {
		var host = $location.host();
		var filtered = [];
		for (var i = 0; i < items.length; i++) {
			console.log("items",items);
			if(letter=="Client Admin" && items[i].documentName!=host.split('.')[0]){
				filtered.push(items[i]);
   					//console.log(filtered);
   				}
   				else if(letter=="admin" || letter=="user"){
   					filtered.push(items[i]);
   					//console.log(filtered);
   				}
   			}
   			return filtered;   			
   		};
   	}); 

/**
 * 14th july 2016
 * UserController
 * mamatha and chaithra
 * modified date : 25th oct 2016
 */
angular.module('homer')
.controller('clientManagementCtrl', function ($scope,$state,GlobalService,clientFactory,$moment,
												DocsFcrt,TenantSpace,Auth,docsCtrlVariables,User,
												$rootScope,ActivitylogService,notify,ClientService,
												ErrorService,NoteService,blockUI,Upload,
												url,TenantFactory,sweetAlert) {
$scope.homerTemplate = 'views/notification/notify.html';
	console.log(TenantSpace.size/(1024*1024*1024));
	// var tenantSpaceee = 
	Auth.isAdmin(function(isadmin) {
              console.log("isadmin",isadmin);

               if(!isadmin){
               	  $state.go('common.error_two');
           console.log("not admin");
               }             
            });

	 // webshims.setOptions('waitReady', false);
  // webshims.setOptions('forms-ext', {types: 'date'});
  // webshims.polyfill('forms forms-ext');

//Activitylog
var activity_type = GlobalService.getActivitytype();
	$scope.searchText;
	$scope.BreadCrumbs=[];
	$scope.getclientlist=[];
	$scope.clientCount=0;
	$scope.currentDate = new Date();
	// var multiplesaveFlag=true;
 	//Manage Breadcrumbs
 	var _pgManageClient= "Client Management";
 	var _pgAddClient="Add Client";
 	var _pgClientList="clientManagement";

 	//loged in user
	$scope.getCurrentUser = Auth.getCurrentUser;
	$scope.getCurrentUser().$promise.then(function() {
		$rootScope.loggedIn=$scope.getCurrentUser();
		docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);
		$scope.loggedIn = $scope.getCurrentUser()._id;
	});

 	//bread crumbs
 	$scope.clientManage=function(){
 		$scope.BreadCrumbs.push(_pgManageClient);
 	};

 	$scope.addClientcrumbs=function(){
 		$scope.BreadCrumbs.push(_pgManageClient,_pgAddClient);
	};
	//click on the bread crum to go to previous page
	$scope.clickOnPage=function(page){
		if(page ==_pgManageClient){
			$state.go(_pgClientList);
		}
	};
 	//To next page Client creation
 	$scope.AddClient = function(){
 		TenantFactory.remainingSize(function(data){
 			console.log(data.size/(1024*1024*1024));
 			if(data.size>(1024*1024*1024)){
 				$state.go('addClient');
 			}else{
 				notify({ message:'Not enough space remaining,please contact Admin' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
 		};

 		});
	};
/*to display the total number of clients */
	  ClientService.getClientsAsync(function(data){
  		$scope.clientLength=data.length;
   		console.log($scope.clientLength,data);
  		});

/*Modified for demo */

	clientFactory.query({'count':$scope.clientCount,'limit':50},function(data){
			// console.log("getclientlist");
			$scope.getclientlist=data;
		});

	$scope.loadclientMore=function(){		
		$scope.clientCount=$scope.clientCount+50;
		// console.log($scope.clientCount);
		clientFactory.query({'count':$scope.clientCount,'limit':50},function(data){
			for(var i=0;i<data.length;i++){
				$scope.getclientlist.push(data[i]);
			};
		});
	};

	$scope.sendTime=function(date){
		var DateFormat = $moment(date).fromNow();
		return DateFormat;
	};

	$scope.findSize = function(client){
       var sizeSpecs = DocsFcrt.getAllsize(client);
       	client.allotedspaceGB = sizeSpecs.allotedspace/(1024*1024*1024);
   	};

    $scope.changeSize = function(size) {
    	// console.log(size);
    	// console.log($scope.slider.value,"total",TenantSpace.size);
		$scope.remainingSize = TenantSpace.size - (size * 1024 * 1024 * 1024);
		// console.log($scope.remainingSize);
	};

	$scope.checkdiableUser=function(data){
       if(data._id==$scope.loggedIn){
          return true;
        }
       else{
          return false;
         }
    };

//If image exists
$scope.checkIfImageExists=function(user){
	docsCtrlVariables.checkForImage(user,user.logo,function(){
		$scope.$apply();
	});
};

$scope.valueToSave=function(value){
$scope.setValue=value;
}
// console.log($scope.getclientlist);
//edit and update option in listing
$scope.updateClients=function(clients){

// TenantFactory.remainingSize(function(data){
// 	console.log(data.size/(1024*1024*1024));

TenantFactory.remainingSize(function(data){
 			console.log(data.size/(1024*1024*1024));
 		});

		$scope.spaceClient=clients.alloted;
	clients.allotedspace=clients.allotedspaceGB*1024*1024*1024;
	console.log(((TenantSpace.size + parseInt($scope.spaceClient))-(clients.allotedspace))/(1024*1024*1024));
	if(clients.allotedspace > 0){
		if(((TenantSpace.size + parseInt($scope.spaceClient))-(clients.allotedspace))>0){
			
			console.log(((TenantSpace.size + parseInt($scope.spaceClient))-(clients.allotedspace))/(1024*1024*1024));
			// if((TenantSpace.size-clients.allotedspace)>0){
				clientFactory.update(clients,function(data){
					TenantFactory.remainingSize(function(dataa){
						console.log(dataa.size/(1024*1024*1024));
					})
					notify({ message:'Client is updated' , classes: 'alert-success', templateUrl: $scope.homerTemplate});
					var updateClientlog = {};
					updateClientlog.activitytype = activity_type.CLIENTUPDATE;
					updateClientlog.what = clients._id;
					updateClientlog.status = true;
					ActivitylogService.LogActivity(updateClientlog);
					$state.go('clientManagement' , {}, { reload: true } );					
				},function(err){ 
					$state.go('clientManagement' , {}, { reload: true });  
		 		});

		 		TenantSpace.size=((TenantSpace.size +parseInt($scope.spaceClient))-(clients.allotedspace));
		}
		else{
			notify({ message:'Not enough space remaining,please contact Admin' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
				 //ClientService.setclientList(function(){ });
				 console.log(TenantSpace.size/(1024*1024*1024),clients.allotedspace/(1024*1024*1024),$scope.setValue);
				 clients.allotedspaceGB=$scope.setValue;
				
		}
	}else{
		// console.log("false");
			notify({ message:'Alloted space should be greater than zero' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
	}
// });	
};

/*to delete client*/
$scope.deleteClient=function(data){
	sweetAlert.swal({
		title : "Warning",
		text : "Users, files and folders created will be moved to admin!!",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#62CB31",
		cancelButtonColor:"#F44336",
		confirmButtonText : "Delete",
		cancelButtonText : "Cancel",
		closeOnConfirm : true,
		closeOnCancel : true
	}, function(isConfirm) {
		if(isConfirm){
			//console.log("delet")
				blockUI.start();
			//console.log("to delete client",data);
			var params={
				client:data._id
			}
			console.log("params",params);
			clientFactory.deleteClient(params,function(res){
				//console.log("res",res);
				angular.forEach($scope.getclientlist,function(value,key){
					if(value==data){
						$scope.getclientlist.splice(key,1);
						sweetAlert.swal("Deleted!", "delete done.", "success");
					var deleteClientlog = {};
					deleteClientlog.activitytype = activity_type.CLIENTDELETE;
					deleteClientlog.what = params.client;
					deleteClientlog.status = true;
					ActivitylogService.LogActivity(deleteClientlog);	
					console.log("deleteclient",deleteClientlog);					
						blockUI.stop();
					}
				})		
			})
		}else{
				console.log("else");
				sweetAlert.swal("Cancelled", "client is safe :)", "error" );
			}
	})
}

//adding Client functions
	var noteClient= "";
	var noteEmail= "";
	var notePhone="";
	var StartDate="";
	var EndDate="";
	var Mobile= "";
	var Email="";
	var Space=""
	var noteFolder="";
	var noteSpace="";
	var notesubscriptionStarts="";
	var notesubscriptionEnds="";
	// var emptyPhoneNumber="";
	var duplicateEmail="";
	var duplicateFolder="";
	var emptyUserName="";
	// var emptyName="";
	// var emptyFolder="";
	// var emptysubscriptionStarts="";
	var emptysubscriptionEnds="";
	var minSpace="";
	var maxSpace="";
	var Client_name="";
	var Client_validname="";
	var Valid_Foldname="";
	$scope.sliderSpace=/^[0-9]+$/i;
	$scope.NamePtrn=/^[a-zA-Z1-9 _]{3,25}$/;
	$scope.EmailPtrn = /^[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})/;
	$scope.phoneNumbr = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{3,7}$/;
	$scope.fldrName=/^[\w. ]+$/;

	NoteService.getUsermanagementNotes({module:'Note',code:'N_005'},function(data){
		 noteClient=data;
	});
	NoteService.getUsermanagementNotes({module:'Note',code:'N_007'},function(data){
			 noteEmail=data;
	});
	NoteService.getUsermanagementNotes({module:'Note',code:'N_006'},function(data){
		 notePhone=data;
	});
	NoteService.getUsermanagementNotes({module:'Note',code:'N_008'},function(data){
		 noteFolder=data;
	});
	NoteService.getUsermanagementNotes({module:'Note',code:'N_018'},function(data){
		 notesubscriptionStarts=data;
	});
	NoteService.getUsermanagementNotes({module:'Note',code:'N_009'},function(data){
		 notesubscriptionEnds=data;
	});
	NoteService.getUsermanagementNotes({module:'Note',code:'N_017'},function(data){
		 noteSpace=data;
	});
	ErrorService.getUsermanagementErrors({module:'User',code:'U_008'},function(data){
		 Client_name=data;
	});
	ErrorService.getUsermanagementErrors({module:'User',code:'U_006'},function(data){
		 Client_validname=data;
	});
	
	ErrorService.getUsermanagementErrors({module:'User',code:'U_004'},function(data){
		 Mobile=data;
	});
	ErrorService.getUsermanagementErrors({module:'User',code:'U_007'},function(data){
		Email=data;
	});
	ErrorService.getUsermanagementErrors({module:'Client',code:'C_002'},function(data){
		Valid_Foldname=data;
	});
	ErrorService.getUsermanagementErrors({module:'Client',code:'C_001'},function(data){
		Space=data;
		// console.log(Space);
	});
	ErrorService.getUsermanagementErrors({module:'Client',code:'C_003'},function(data){
		minSpace=data;
	});
	ErrorService.getUsermanagementErrors({module:'Client',code:'C_005'},function(data){
		maxSpace=data;
	});
	$scope.getClient_name=function(){
		return Client_name;
	};
	$scope.getClient_validname=function(){
		return Client_validname;
	};
	$scope.getminSpace=function(){
		return minSpace;
	};
	$scope.getmaxSpace=function(){
		return maxSpace;
	};
	$scope.getnoteSpace=function(){
		return noteSpace;
	};
	$scope.getnotesubscriptionStarts=function(){
		return notesubscriptionStarts;
	};
	$scope.getnotesubscriptionEnds=function(){
		return notesubscriptionEnds;
	};
	$scope.getnoteFolder=function(){
		return noteFolder;
	};
	$scope.getnoteClient=function(){
		return noteClient;
	};
	$scope.getnoteEmail=function(){
		return noteEmail;
	};
	$scope.getnotePhone=function(){
		return notePhone;
	};
	$scope.getMobile=function(){
		return Mobile;
	};
	$scope.getEmail=function(){
		return Email;
	};
	$scope.getValid_Foldname=function(){
		return Valid_Foldname;
	};
	// $scope.duplicate_folder = function(){
 //    duplicateFolder="";
 //  	};
	$scope.getSpace=function(){
		return Space;
	};

// comparing start and end date
    $scope.checkErr = function(startDate,endDate){
    // console.log(startDate,endDate);
    	var curDate = new Date();
    	curDate.setHours(0,0,0,0);
        $scope.errMessage = '';
        if(new Date(startDate) >= new Date(endDate)){
        	console.log(new Date(startDate) > new Date(endDate));
        		ErrorService.getUsermanagementErrors({module:'User',code:'U_009'},function(data){
					StartDate=data;
				});
        } else{
        	StartDate="";
        }
        if(new Date(startDate) < curDate){
        	console.log(new Date(startDate) < curDate);
        	    ErrorService.getUsermanagementErrors({module:'User',code:'U_010'},function(data){
					 EndDate=data;
				});
        } else{
        	EndDate="";
        }
    };

	$scope.getStartDate=function(){
		// console.log("get start date",StartDate);
		$scope.sdate=true;
		return StartDate;
	};
	$scope.getEndDate=function(){
		// console.log("get enddate",EndDate);
		return EndDate;
	};

	//changesizee function
	$scope.sizeChange=function(){
		// console.log($scope.slider.value,"total",TenantSpace.size,TenantSpace.size/(1024 * 1024 * 1024));
		$scope.remainingSize = TenantSpace.size - ($scope.slider.value * 1024 * 1024 * 1024);
		// console.log($scope.remainingSize);
		$scope.remainSizee=TenantSpace.size/(1024 * 1024 * 1024);

		// console.log($scope.remainSizee);
	};

	//slider for space allotment
$scope.slider = {
    value: "",
    options: {
     showSelectionBar: true,
    getPointerColor: function(value) {
            if (value >= 1)
            return '#2AE02A';
        },
    getSelectionBarColor: function(value) {
          if (value >= 1)
          return '#2AE02A';
        }, 
    translate: function(value) {
      return value + ' GB';
    },
      floor: 0,
      onChange:$scope.sizeChange,
      ceil:(Math.floor(TenantSpace.size/(1024 * 1024 * 1024))),
      step: 1,
      minLimit: 1,
      maxLimit: $scope.remainingSize
    }
};

	//For validation
	$scope.isValidClient=function(client,callback){		
			//duplicate email
		ClientService.duplicate_email1(client.email,function(flag){
			// console.log(flag);
			if(flag){
				// console.log(flag);
				ErrorService.getUsermanagementErrors({module:'User',code:'U_017'},function(data){
					duplicateEmail=data;
		 			});
		 		callback(false);
			}else {
				//duplicate folder
					ClientService.duplicate_Folder(client.foldername,function(flag1){
						// console.log(flag1);
						if(flag1){
							ErrorService.getUsermanagementErrors({module:'Client',code:'C_008'},function(data){
								duplicateFolder=data;
					 		});
					 		callback(false);
						}
						else callback(true);
					});				
			}
		})
	};
 	$scope.getduplicateEmail=function(){
		return duplicateEmail;
	};
	$scope.getduplicateFolder=function(){
		return duplicateFolder;
	};
	$scope.makeEmptyEmail=function(){
	   duplicateEmail="";
	  };
	  $scope.makeEmptyfolder=function(){
	   duplicateFolder="";
	  };

//To upload client logo
$scope.uploadFiles = function(obj, cb,res){
		Upload.upload({
			url : url + 'api/clients/clientlogo',
			data : obj
		}).then(function(response) {
			blockUI.stop();
			// console.log(response,"response when logo present");
			// var addClientlog = {};
			// addClientlog.activitytype = activity_type.CLIENT;
			// addClientlog.what = response.name;
			// addClientlog.status = true;
			// ActivitylogService.LogActivity(addClientlog);
			// console.log("activitylogggggg");
		 	$scope.client={};
			$scope.slider.value="";
			cb();
		});
	};

$scope.Bflag=false;
	//creating the client
	$scope.ClientAdd=function(client){
		console.log("to add client",client);
		if ($scope.Bflag) {
        return;
    }
    $scope.Bflag=true;
		if ($scope.ClientForm.$valid){
			// $scope.ClientForm.submitted = false;
			// console.log(client);
			$scope.Bflag=true;
			$scope.saveClient(client,function(){
				$scope.Bflag=true;
				ClientService.setclientList(function(){
					$state.go('clientManagement',{},{reload: true});
				})

			});
			// $scope.Bflag=false;
		}else{
			
           // console.log("not valid",client);
            $scope.ClientForm.submitted = true;
            $scope.Bflag=false;
        }
	};

	 // $scope.ClientForm.submitted = false;
		// To add another user
		$scope.Bflag=false;
	 $scope.oneMore=function(client){

	 	if ($scope.Bflag) {
        return;
    }
	 	$scope.Bflag=true;
	 	if ($scope.ClientForm.$valid){
	 		$scope.Bflag=true;
	 		// $scope.ClientForm.submitted = false;
				$scope.saveClient(client,function(){
					ClientService.setclientList(function(){
						$state.go($state.current, {}, {reload: true});
					})
				});
		}else{
		           // console.log("not valid",client);
		            $scope.ClientForm.submitted = true;
		            $scope.Bflag=false;
		        }
	};	

$scope.dateSet=function(data){

		// console.log(date);
	return data;
};


$scope.saveClient=function(client,cb){
	console.log("save client",client,client.subscriptionStarts,client.subscriptionEnds);
	 blockUI.start();
	// var startDatee=$scope.dateSet(client.subscriptionStarts);
	// var endDatee=$scope.dateSet(client.subscriptionEnds);
	$scope.isValidClient(client,function(flag){


		console.log("dates valid",client.subscriptionEnds>client.subscriptionStarts);

		if(client.subscriptionEnds>client.subscriptionStarts){
			
			console.log("dates valid",client.subscriptionEnds>client.subscriptionStarts);
				
				client.allotedspace=$scope.slider.value*1024*1024*1024;
				$scope.remainn=($scope.remainingSize/(1024*1024*1024));
				console.log(TenantSpace.size/(1024*1024*1024));

			if (client.allotedspace >1 && (client.allotedspace <= TenantSpace.size)){	
				if(flag){
					$scope.Bflag=false;

					console.log("flag true",flag);
						clientFactory.save(client,function(res){
							// $scope.Bflag=false;
							// console.log("flag true",res);
							var obj={id:res._id,file:$scope.files};
								client.id=res._id;
								if($scope.files){ 
									// console.log("file present");
									$scope.uploadFiles(obj,cb,res);
									var addClientlog = {};
									addClientlog.activitytype = activity_type.CLIENTADD;
									addClientlog.what = res.name;
									addClientlog.status = true;
									ActivitylogService.LogActivity(addClientlog);
									// console.log(addClientlog,"activitylogggggg");
									// console.log(addClientlog);
								} else{
									blockUI.stop();
									// console.log("else condition");
							 		var addClientlog = {};
									addClientlog.activitytype = activity_type.CLIENTADD;
									addClientlog.what = res.name;
									addClientlog.status = true;
									ActivitylogService.LogActivity(addClientlog);
									// console.log(addClientlog);
							 		$scope.slider.value="";
							 		cb();
							 	}
							 	// console.log("blockui stop");
							
						},function(err){
							$scope.Bflag=false;
							console.log("error",err);
							 blockUI.stop();
						});
						// blockUI.stop();
				}else{
					$scope.Bflag=false;

					console.log("flag false");
					blockUI.stop();
				}

			}else{
				$scope.Bflag=false;
				if(client.allotedspace==0){
					$scope.ClientForm.submitted = true;
					console.log("space check",client.allotedspace,$scope.ClientForm.submitted);
					$scope.getSpace();
					  blockUI.stop();
				}else{
					$scope.Bflag=false;
					console.log("space check");
				$scope.getmaxSpace();
				 blockUI.stop();
				}
				
			}

		}else{
			$scope.Bflag=false;
			console.log("dates not valid");
			$scope.getStartDate();
			  // blockUI.stop();
		};			
	});
};

$scope.clearClient=function(data){
	if(data){
    	data={};
    	$scope.slider.value="";
    		$state.go($state.current, {}, {reload: true});
    	}
    	else{
    		$state.go($state.current, {}, {reload: true});
    	}
    };



    /*load more option*/
	// clientFactory.query({'count':$scope.clientCount,'limit':20},function(data){
	// 	// console.log("getclientlist");
	// 	$scope.getclientlist=data;
	// });
// console.log($scope.getclientlist);


/*Previous code (actual one)*/
// $scope.loadclientMore=function(){
	
// 	$scope.clientCount=$scope.clientCount+20;
// 	// console.log($scope.clientCount);
// 	clientFactory.query({'count':$scope.clientCount,'limit':20},function(data){
// 					for(var i=0;i<data.length;i++){
// 						$scope.getclientlist.push(data[i]);
// 					};
// 				});
// };

});
'use strict';
angular.module('homer').controller('rightNavCtrl', function($scope,
	$state, url, $window, $http, $q, $rootScope,$location, 
	activityFcrt,$moment,GlobalService,User,DefaultDocs,$filter,Auth,docsCtrlVariables) {
	// console.log("inside rightnav");

	// $scope.activities = new Activities();
	// $scope.activities.emptyCount();
	// $scope.activities.howMuch=0;
var howMuch=0;
//var howMuch=10;
$scope.activitylog=[];
// console.log($scope.activitylog.length,$scope.activitylog);
	$scope.activityLog=activityFcrt.query({'count':howMuch,'limit':10},function(data)
	 {
	 	  console.log(data);
		// console.log(howMuch);
  	});
	$scope.loadMore=function(){
		console.log("loading",$scope.activityLog,$scope.activityLog.length);
		var Count=10;
		var items=activityFcrt.query({'count':Count,'limit':40},function(data){
			// console.log(data,Count,data.length,items);
		for(var i=0; i<items.length; i++){
			$scope.activityLog.push(items[i]);
		// console.log($scope.activityLog,$scope.activityLog.length);
		}

		});
	};
$scope.reportPage=function(){
	// console.log("reports");
	$state.go("report");
};

var host = $location.host();
User.loginusers(function(users){
	// console.log(users);
	$scope.loggedinusers=users;
});


var ioSocket = io(url, {
	 query: "host="+host.split('.')[0],
      path: '/socket.io-client'
    });

/*
logged IN
*/
$scope.checkIfloggedIn=function(activity){
	if($scope.loggedinusers){
		var ind=$scope.loggedinusers.map(function(e) { return e.userId; }).indexOf(activity.who.user_id);
		//console.log(ind);
		if(ind>-1){
			return "LoggedIn";
		}
		else{
			return "LoggedOut";
		}		
	}
};

ioSocket.on('activitylog:save', function (item,test) {
	//console.log(item,test);
	$scope.activityLog.unshift(item);		
});

$scope.$on('$destroy', function() {
    ioSocket.removeAllListeners('activitylog:save');
  });

// var ioSocket = io('http://motomoney.2docstore.com:9000', {
// 	query: "host="+host.split('.')[0],
//       path: '/socket.io-client'
//     });


ioSocket.on('thing:save', function (item) {
	// console.log(item);
	$scope.loggedinusers.push(item);
				if(!$scope.$$phase) {
			  //$digest or $apply
			  $scope.$apply();
			}
});

ioSocket.on('thing:remove', function (item) {
	//console.log(item);
	var ind=$scope.loggedinusers.map(function(e) { return e.userId; }).indexOf(item.userId);
	$scope.loggedinusers.splice(ind,1);
				if(!$scope.$$phase) {
			  //$digest or $apply
			  $scope.$apply();
			}
});

// ioSocket.on('thing:remove', function (item) {
// 	console.log(item);

// })	

/*
Query to get the documents listed under tenant
*/
$scope.spaceInfo=function(cb){
	console.log("check");
	Auth.getTenant(function(tenant){
		//console.log("Auth",tenant) ;
	    $rootScope.tenantInfo=tenant;
	    $scope.space=0;
	   // console.log($rootScope.tenantDocs,"$rootScope.tenantDocs")
	    if($rootScope.tenantDocs==undefined){
	    	//console.log("undefined",$rootScope.tenantDocs);
	    	DefaultDocs.query(function(data){
	    		//console.log("data",data);
	    		$rootScope.tenantDocs=data;
	    		 // console.log($rootScope.tenantDocs,"tenantdocs");
				angular.forEach($rootScope.tenantDocs,function(value,key){
					//console.log(value,"value");
					$scope.space=$scope.space+value.size;
					//console.log($scope.space,"nav");
					cb($rootScope.tenantInfo,$scope.space);
				});
	   		 })

	    }else{
	    	console.log("defined",$rootScope.tenantDocs);
	    	angular.forEach($rootScope.tenantDocs,function(value,key){
				//console.log(value,"value");
				$scope.space=$scope.space+value.size;
				//console.log($scope.space,"nav");
				cb($rootScope.tenantInfo,$scope.space);
			});
	    }   
		$scope.allottedspace=$rootScope.tenantInfo.subscription.space;	
	})
}


$scope.giveWidth=function(){
	console.log("giveWidthe");
	$scope.spaceInfo(function(tenentInfo,spacevalue){
		//console.log(tenentInfo,spacevalue);
		$scope.width=($scope.space/$rootScope.tenantInfo.subscription.space)*100;
		console.log("%width",$scope.width);
	})
			
};

/*
*/
$scope.checkIfImageExists=function(user,image){
	// console.log(user,image);
	user.name=user.user_name;
	docsCtrlVariables.checkForImage(user,image);
};		
/*
Query to get the activity
*/
	var Activity = GlobalService.getActivitytype();
	console.log("activity type",Activity);

$scope.setActivity=function(){
		/*
Query to get the activity
*/

	$scope.activitylog = activityFcrt.query({
		
	}, 
	function(data) {
		$scope.ActivityLog=data;
		$scope.activitylogger = data;
		$scope.activityLength = $scope.activitylogger.length;
	});

};
		/*
		location in tooltip
		*/
		$scope.locationString=function(location)
		{

			if (location.location) {
				var str = location.location;
				var doc = str.indexOf("/");
				var n1 = str.substring(0, doc);
				var n2 = str.substring(doc);
				var n3 = n2.substring(1);
				var str1 = n3;
				var doc1 = str1.indexOf("/");
				var n4 = str1.substring(0, doc1);
				var n5 = str1.substring(doc1);
				var n6 = n5.substring(1);
				location.short=n6;
			}
		};
	/**
	*function name : get time, to give time in terms of "mins ago,days ago etc"
	@param: activity.when
	return : time in terms of moments ago
	**/
	$scope.getTime=function(date){
		var DateFormat = $moment(date).fromNow();
		return DateFormat;
	};

		/**
	*function name : getActivityType"
	@param: activity obj
	return : Activity in a senetence
	**/
	$scope.getActivitytype = function(activityMsg) {
		//console.log("activityMsg",activityMsg);
		if (activityMsg) {
			if (activityMsg.location) {
				var str = activityMsg.location;
				var doc = str.indexOf("/");
				var n1 = str.substring(0, doc);
				var n2 = str.substring(doc);
				var n3 = n2.substring(1);
				var str1 = n3;
				var doc1 = str1.indexOf("/");
				var n4 = str1.substring(0, doc1);
				var n5 = str1.substring(doc1);
				var n6 = n5.substring(1);
			}

			// To capitalize the first letter
			var firstToUpperCase = function(strng) {
				return strng.substr(0, 1).toUpperCase() + strng.substr(1);
			}
			if (activityMsg.who) {
				var strng = activityMsg.who.user_name;
				var User_name = firstToUpperCase(strng);
			} else {
				var User_name = "";
			}
			var text;
			switch (activityMsg.activitytype) {
			case Activity.LOGIN:
			case Activity.LOGOUT:
				text =  Activity.HAS + activityMsg.activitytype.bold();
				break;
			case Activity.SHARE:
			case Activity.DOWNLOAD:
			case Activity.VIEW:
			case Activity.CHANGEVERSION:
			text = activityMsg.activitytype.bold() + " " + activityMsg.what.docu_type + " " + activityMsg.what.document_name + Activity.FROM + n6;
				break;
			case Activity.UPLOAD:
			text = activityMsg.activitytype.bold() + " " + activityMsg.what.document_name.link("http://motomoneydemo.2docstore.com") + Activity.TO + n6;
				break;
			case Activity.CREATE:
				text =  activityMsg.activitytype.bold() + " " + activityMsg.what.docu_type + " " + activityMsg.what.document_name + Activity.IN + n6;
				break;
			case Activity.DELETE:
			case Activity.TRASH:
			case Activity.RESTORE:
				text = Activity.HAS + activityMsg.activitytype.bold() + " " + activityMsg.what.document_name + Activity.IN + n6;
				break;
			case Activity.ADD:
			case Activity.UPDATE:
				text =  Activity.HAS + activityMsg.activitytype.bold() + " " + activityMsg.newUser;
				break;			
			case Activity.REMIND:
				text =  Activity.HAS + activityMsg.activitytype.bold() + " " + activityMsg.what + Activity.IN + n6;
				break;
			case Activity.USERADD:
			case Activity.CLIENTUPDATE:
			case Activity.CLIENTADD:
			case Activity.USERUPDATE:
			//case Activity.CLIENTDELETE:
			text = Activity.HAS + activityMsg.activitytype.bold() + " " + activityMsg.what.document_name ; 
			 	break;
			 	case Activity.CLIENTDELETE:
			text = Activity.HAS + activityMsg.activitytype.bold() + " " + activityMsg.what ; 
			 	break;
			case Activity.LOCK:
			case Activity.UNLOCK:
			case Activity.FAV:
			case Activity.UNFAV:
				text =  Activity.HAS + activityMsg.activitytype.bold() + " " + activityMsg.what.docu_type + Activity.IN + n6;
				break;
			case Activity.PASSWORD:			
			case Activity.PROFILE:
			case Activity.PROFILEUPDATE:
				text = Activity.HAS + activityMsg.activitytype.bold() ; 
				break;
			case Activity.GROUPADD:
			case Activity.GROUPUPDATE:			
				text = Activity.HAS + activityMsg.activitytype.bold() +" " + activityMsg.what.document_name;
				break;
			case Activity.GROUPDELETE:	
				text = Activity.HAS + activityMsg.activitytype.bold() +" " + activityMsg.what;
				break;
			case Activity.COMMENT:
				text = Activity.HAS + activityMsg.activitytype.bold() + " " + activityMsg.what.docu_type + " " + activityMsg.what.document_name + Activity.IN + n6;
				break;
			case Activity.DESCRIPTION:
			 	text = Activity.HAS + activityMsg.activitytype.bold() + Activity.TO + activityMsg.what.document_name + Activity.IN + n6;
          		break; 
			default:
				text = "default text"; 
			}
			return text;
		}
	};

}).filter('bytes', function() {
	return function(bytes, precision) {
		if (bytes == 0)
			return '0KB';
		if (isNaN(parseFloat(bytes)) || !isFinite(bytes))
			return '-';
		if ( typeof precision === 'undefined')
			precision = 1;
		var units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB'],
		    number = Math.floor(Math.log(bytes) / Math.log(1024));
		return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + ' ' + units[number];
	};
}).filter('limitHtml', function() {
        return function(text, limit) {

            var changedString = String(text).replace(/<[^>]+>/gm, '');
            var length = changedString.length;

            return length > limit ? changedString.substr(0, limit - 1) : changedString;
        };
    });

angular.module('homer')

  .controller('navigationCtrl', function ($scope,$state,$http,$rootScope,md5,Auth,User,notify,DefaultDocs,$moment,ActivitylogService,GlobalService,docsCtrlVariables,UserService,$rootScope) {
$scope.shared=true;	
var activity_type = GlobalService.getActivitytype();
  	$scope.getCurrentUser = Auth.getCurrentUser;
  	//console.log($scope.getCurrentUser());
	$scope.getCurrentUser().$promise.then(function() {
		$scope.loggedIn = $scope.getCurrentUser();
		// $scope.loggedUser=$scope.getCurrentUser();
		//console.log($scope.loggedIn,$scope.loggedIn.role);
	});
	 var whatDoc=docsCtrlVariables.getWhichDocument();
	$scope.checkIfImageExists=function(user,image){
		//console.log(user);
	if(user){
		docsCtrlVariables.checkForImage(user,image);
	}
	

	//console.log(user)
	}

	$scope.mydocs=true;
	$scope.myDocuments=function(){
		if($rootScope.clientPage)
			return "something"
		if($scope.mydocs)
			return "activeClass"

		else{
		if(whatDoc=="My Documents")
			return "activeClass"
		}
	}	

	$scope.clientDocs=function(){
		
			if($rootScope.clientPage){
			
			$scope.mydocs=false;

			return "activeClass"	
			}
			
		
	}

$scope.admin=false;
	$scope.Admin=function(){
		if($scope.admin)
			// $scope.mydocs=false;
			return "activeClass"
	}

$scope.userManage=false;
$scope.userManagement=function(){
	if($scope.userManage)
		// $scope.mydocs=false;
		return "activeClass"
}

$scope.clientManage=false;
$scope.clientManagement=function(){
	if($scope.clientManage)
		// $scope.mydocs=false;
		return "activeClass"
}

$scope.redirectPage=false;
$scope.redirectPages=function(){
	if($scope.redirectPage)
		return "activeClass"		
}

$scope.report=false;
$scope.Report=function(){
	if($scope.report)
		return "activeClass"
}

	$scope.getUserListing=function(){
		UserService.setUserManagementBC('userListing');

		$state.go('userListing');
	}

	$scope.sharedDocs=function(){
		 var whatDoc=docsCtrlVariables.getWhichDocument();
		 if($scope.loggedIn){
		 			if(!whatDoc && $scope.loggedIn.role=="user")
		{
			return "activeClass";
		}
		 }

			
			if(whatDoc=="shared"){
			$scope.mydocs=false;
			return "activeClass"	
			}

		}
	


	$scope.favDocs=function(){
		if(whatDoc=="fav"){
			
			$scope.mydocs=false;

			return "activeClass"	
			}
	}


	$scope.callAdmin=function(){
		//console.log("call admin");
	};

	/*
	Function name: logout
	Logs out the user
	*/
$scope.logOut=function(){
	var logoutLog={};
	logoutLog.activitytype = "Logged-Out";
	ActivitylogService.AsyncLogActivity(logoutLog, function() {
		User.logout(function(data){
			console.log(data);
			Auth.logout()
		});
		 $state.go("common.login");
	});	
};
	$scope.clickedOnClients=function(){
		$scope.mydocs=false;
		docsCtrlVariables.setWhichDocument("My Documents","clicked");
		$state.go("clientProject", {}, { reload: true });
	};
	
	$scope.clickMyDocs=function(){
	docsCtrlVariables.setWhichDocument("My Documents","clicked");
	$rootScope.clientPage=false;
		$state.go("dashboard", {}, { reload: true });	
	};
	$scope.clickedOnShared=function(){
			docsCtrlVariables.setWhichDocument("shared","clicked");
				$rootScope.clientPage=false;
			$state.go("dashboard", {}, { reload: true });
	};
	$scope.clickedOnFav=function(){
		
			docsCtrlVariables.setWhichDocument("fav","clicked");
				$rootScope.clientPage=false;
			$state.go("dashboard", {}, { reload: true });
	};
	$scope.clickedonAdmin=function(){
		$scope.mydocs=false;
			$rootScope.clientPage=false;
		$scope.admin=true;
	};
	$scope.clickedonUser=function(){
		$scope.mydocs=false;
			$rootScope.clientPage=false;
		$scope.userManage=true;
	};
	$scope.clickedonClient=function(){
		$scope.mydocs=false;
			$rootScope.clientPage=false;
		$scope.clientManage=true;
	};
	$scope.clickedonReport=function(){
		$scope.mydocs=false;
			$rootScope.clientPage=false;
		$scope.report=true;
	};
	$scope.clickedonRedirect=function(){
		$scope.mydocs=false;
			$rootScope.clientPage=false;
		$scope.redirectPage=true;
	};

  });
	


  
  // .directive('loggedUser',function(){
  // 	return{
  // 	template:'role:{{loggedIn.role}}'
  // 	};
  // });

angular.module('homer')

  .controller('profilecntrl', function ($scope,GlobalService,url,Upload,$state,$http,$rootScope,md5,Auth,User,notify,DefaultDocs,userManagement,url,ActivitylogService,UserService,docsCtrlVariables,NoteService,ErrorService) {
  	 $scope.homerTemplate = 'views/notification/notify.html';

var activity_type = GlobalService.getActivitytype();
$scope.getCurrentUser = Auth.getCurrentUser;
	$scope.getCurrentUser().$promise.then(function() {
		$scope.loggedIn = $scope.getCurrentUser();
		// console.log($scope.loggedIn ,$scope.loggedIn.contactnumber);
		$rootScope.loggedIn=$scope.loggedIn;
		 docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);
	});

  	Auth.getTenant(function(tenant){
    $scope.tenant=tenant; 
    $rootScope.tenantInfo=$scope.tenant;     
    });

    //userlist 
    UserService.getUsers(function(data){
		// console.log(data,data);
		userslist=data;
		// console.log(userslist.contactnumber)
	});

$scope.phoneNumbr = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{3,7}$/;
$scope.errorMessagephone="";
var notePhone="";
var Mobile= "";
var firstName=" ";

/*
validate phone number
*/
$scope.checkforPhoneError=function(phone){
	// //console.log($scope.phoneNumbr.test(phone))
	if(!$scope.phoneNumbr.test(phone)){		
		$scope.errorMessagephone=$scope.getMobile();
	}
	else{
		$scope.errorMessagephone=""
	}
}

$scope.phoneNumbr = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{3,7}$/;

NoteService.getUsermanagementNotes({module:'Note',code:'N_006'},function(data){
		// //console.log(data);
		 notePhone=data;
	});

ErrorService.getUsermanagementErrors({module:'User',code:'U_004'},function(data){
		 Mobile=data;
	});
ErrorService.getUsermanagementErrors({module:'User',code:'U_006'},function(data){
		 firstName=data;
	});
$scope.getnotePhone=function(){
		return notePhone;
	};
$scope.getMobile=function(){
		return Mobile;
	};
$scope.getfirstName=function(){
	return firstName;
}
/*$scope.getCurrentUser = Auth.getCurrentUser;*/
	$scope.user_profile=$scope.getCurrentUser();
	$scope.user_profile.$promise.then(function() {
		// //console.log("user",$scope.user_profile);
		$scope.profile_info = angular.copy($scope.user_profile);
		$scope.notify = angular.copy($scope.user_profile);

	})
/*
if image exists
*/
$scope.checkIfImageExists=function(user){
	// //console.log(user,$scope.profile_info);
	// user.name=$scope.profile_info.name
	docsCtrlVariables.checkForImage($scope.user_profile,$scope.user_profile.avatar);
	// //console.log($scope.user_profile);		
	}

	$scope.click = 0;
	$scope.formData = {};
	$scope.propic={};
	$scope.billing = {};
	$scope.errorText = {};
	$scope.list = [];
 	$scope.avatarTemp = "assets/images/users.png";
	$scope.whichFlag = "Profile Info";
	$scope.errorText = GlobalService.get_error();// error msg form Global2docstoreFcrt
	$scope.note = GlobalService.getNote();// note from Global2docstoreFcrt
	$scope.checkStatus=false;



 // to upload profile pic
  
	$scope.uploadAvatar = function(file,id) {
		// //console.log("upload avatar",file,id,$scope.profile_info.avatar);
		if(file){		
		 	Upload.upload({
	            url:  url + 'api/users',
	            data: {
	            	filename:file.name,
	            	id:id,
	            	file:file
	            }
	     	}).then(function (resp) {
	        	$scope.callReload();
	            window.location.reload();
	            // //console.log(id,"success");
	   			var profileLog={};
	            profileLog.activitytype=activity_type.PROFILE;
	            // profileLog.what='Profile_Pic';
	      		ActivitylogService.LogActivity(profileLog);//from activity service
	      		// //console.log(ActivitylogService);
	         	notify({ message: 'Profile Pic Uploaded successfully', classes: 'alert-success', templateUrl: $scope.homerTemplate});
	        }, function (resp) {
	            // //console.log('Error status: ' + resp.status);
	        }, function (evt) {
	        	$scope.progressPercentage = Math.min(97, parseInt(100.0 * evt.loaded / evt.total));
	            // console.log('progress: ' + $scope.progressPercentage  + '% ' + evt.config.data.file.name);
	        });
		}
		else{
			$scope.callReload();
		}	
	};

	// to set password strength
  $scope.strength=" ";
     $scope.passStrength = function(){ 
    return $scope.strength;   
  };

 
$scope.testStrength=function(pass){
  // //console.log(pass);
  var type;
  $scope.strength = 0;
  if(pass){
    if(pass.length<6){
      $scope.strength=0;
      // //console.log($scope.strength);
    } if( /[0-9]/.test( pass ) ){
      type = 'success';
    $scope.strength=$scope.strength+1;
    // //console.log("numbers",$scope.strength);    
  } if( /[a-z]/.test( pass ) )
    {
    $scope.strength=$scope.strength+1;
    // //console.log("alpha",$scope.strength);
  
  } if( /[A-Z]/.test( pass ) ){
    $scope.strength=$scope.strength+1;
    // //console.log("caps",$scope.strength);
  
  } if( /[^A-Z-0-9]/i.test( pass ) ){
    $scope.strength=$scope.strength+1;
    // //console.log("chars",$scope.strength);   
  }
  }
  else
   $scope.strength = -1;
  // return s;
};



//Form submission

  	$scope.submitMydata = function() {
  		
		if ($scope.whichFlag == "propicture") {				
				$scope.list.push(this.propic);
				userManagement.update($scope.propic,function(data) {
				});
		}

		if ($scope.whichFlag == "Profile_Info") {
			console.log(this.profile_info,this.profile_info.name,"profileinfo");

			if(this.profile_info.name){
				//this.profile_info.cell_number=$scope.loggedIn.contactnumber;

					if(this.profile_info.cell_number){
						if($scope.loggedIn.contactnumber){
							console.log("present");
							this.profile_info.cell_number=loggedIn.contactnumber;
							console.log(this.profile_info.cell_number,"cell number");			
							$scope.list.push(this.profile_info);
			            	$scope.profile_info.files=$scope.profile_info.avatar;		
							userManagement.update($scope.profile_info,function(data) {
								console.log("updated",$scope.profile_info);
								$scope.callSuccess();
								// PROFILEUPDATE
								var proupdateLog={};
				           		proupdateLog.activitytype=activity_type.PROFILEUPDATE;
				      			ActivitylogService.LogActivity(proupdateLog);//from activity service
							});
						}else{
							console.log("absent");
							console.log(this.profile_info.cell_number,"cell number");			
							$scope.list.push(this.profile_info);
			            	$scope.profile_info.files=$scope.profile_info.avatar;		
							userManagement.update($scope.profile_info,function(data) {
								console.log("updated",$scope.profile_info);
								$scope.callSuccess();
								// PROFILEUPDATE
								var proupdateLog={};
				           		proupdateLog.activitytype=activity_type.PROFILEUPDATE;
				      			ActivitylogService.LogActivity(proupdateLog);//from activity service
							});
						}
					}
			        else{
			        	notify({ message:'Please enter a valid contact number' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
			        }
			}
			else{
				notify({ message:'Please enter a valid name' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
			}

		} 
		else if ($scope.whichFlag == "Security") {
			$scope.list.push(this.formData);				
			if($scope.formData.old_password){
				if($scope.formData.password && $scope.formData.password_c){					
					if($scope.formData.password == $scope.formData.password_c ){
							if($scope.strength<=1){
								// //console.log("here pass",$scope.strength);
								notify({ message:'Password must be strong' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
								$scope.formData="";
								$scope.strength=" ";
							}
							else{
								// //console.log("updated");
								var userObj={ }; 
								userObj._id=$scope.user_profile._id;
								userObj.oldpassword=md5.createHash($scope.formData.old_password || '');
								userObj.newpassword=md5.createHash($scope.formData.password || '')								
								userManagement.setPassword(userObj,function(data) {
									$scope.callSuccess();
									var passLog={};
				            		passLog.activitytype=activity_type.PASSWORD;
				      				ActivitylogService.LogActivity(passLog);//from activity service
				      				// //console.log(ActivitylogService);
										$scope.formData="";
								},function(err){
									$scope.error=err.data;
									
									notify({ message: 'Warning -  Passwords do  not match', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
									
									$scope.formData="";
									$scope.strength=" ";
								});
							}											
							
					}//end of both match
					else{
							
							notify({ message: 'Warning -  Passwords do  not match', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
							$scope.formData="";
							$scope.strength=" ";

					}
				}
				else{	
					notify({ message: 'Warning -  Passwords cant be empty', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
					$scope.formData="";
					$scope.strength=" ";
				}

			}//end of old pass
			else{
			
					
					 notify({ message: 'Warning -  Old password cannot be empty', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
					$scope.formData="";
					$scope.strength=" ";
				}	
		
		} else if ($scope.whichFlag == "Notification") {
			$scope.list.push(this.notify);
			userManagement.update($scope.notify,function(data) {
				$scope.callSuccess();
				var proupdateLog={};
            	proupdateLog.activitytype=activity_type.PROFILEUPDATE;
      			ActivitylogService.LogActivity(proupdateLog);//from activity service
      			// //console.log(ActivitylogService);
				$scope.formData="";
				$scope.strength=" ";
			});		
		} 		
	};

//Cancel button function
	$scope.cancelFunc=function(){
	 window.location.reload();
	}

	//Returning to profile page
	$scope.callReload=function(){
	    $state.go('profile');
	}

// update success
	$scope.callSuccess=function(){	
		notify({ message: 'Profile-updated successfully', classes: 'alert-success', templateUrl: $scope.homerTemplate});
		$scope.strength="";
	};


//to set the flags
$scope.setFlag = function(flagSet) {	
		$scope.whichFlag = "";
		if (flagSet == "pro_info") {			
			$scope.whichFlag = "Profile_Info";			
		}
		if (flagSet == "secure") {			
			$scope.whichFlag = "Security";
		}
		if (flagSet == "notify") {			
			$scope.whichFlag = "Notification";
		}if (flagSet == "propic") {			
			$scope.whichFlag = "propicture";
		} 
		else if (flagSet == "bill") {
			$scope.whichFlag = "Billing";
		}
	};

	//checking if avatar is there, if not send true
	$scope.checkForAvatar = function() {
		if($scope.profile_info){
			if ($scope.profile_info.avatar){
				return false;
			}
			else{
				  $scope.profile_info.avatar= $scope.defaultfile;
			
				 return true;
			}
	 	}
	};

	// Initial step
    $scope.step = 1;
    
	$scope.profile =  {
        show: function(number) {
            $scope.step = number;
        },
        next: function() {
            $scope.step++ ;
        },
        prev: function() {
            $scope.step-- ;
        }
    };
  	
});
angular.module('homer').controller('reportCntrl',
function($scope,$http,$filter,$state,url,$rootScope,Auth,User,
    UserService,GlobalService,$location,DefaultDocs,$location,ActivitylogService,
    activityFcrt,$moment,$timeout,$window,reportFactory,docsCtrlVariables,userManagement,UserService) {
    
    $scope.homerTemplate = 'views/notification/notify.html';
        
        User.getLoggedInUsers(function(res){
            console.log(res,"current");
        });
        
        reportFactory.reportCount(function(res){
            console.log(res);
            $scope.TotalActivity=res.TotalActivity;
            $scope.TotalLoginCount=res.TotalLoginCount;
            $scope.TotalUserCount=res.TotalUserCount;
        })

        var host = $location.host();

        var ioSocket = io(url, {
             query: "host="+host.split('.')[0],
            path: '/socket.io-client'
       });

        ioSocket.on('activitylog:save', function (item) {
            console.log(item);
            $scope.activitylog.unshift(item);    
        });

        $scope.imageExists = function(image, users) {
            // //console.log("before",image);
           var img = new Image();
               img.onload = function() {
               // //console.log("onload",image,users.avatarflag);
                 users.avatarflag = true;
                 $scope.$apply();
           }; 
               img.onerror = function() {
               // //console.log("onerror",image,users.avatarflag);
                 users.avatarflag=false;
                 $scope.$apply();
               };
           img.src = image;
        };


/* data query*/
            $scope.howMuch=0;
            // console.log($scope.howMuch);
            $scope.activityLogs=activityFcrt.query({'count':$scope.howMuch,'limit':20},function(data)
                     {
                          // console.log(data);
                        // console.log($scope.howMuch);
                      });
            var Counts=$scope.howMuch+20;
            // console.log(Counts);
            $scope.loadMore=function(){
                // console.log("here is load");
                var activityCount=Counts;
                // console.log(activityCount);
                var items=activityFcrt.query({'count':activityCount,'limit':20},function(data){
                // Counts=Counts+20;
                console.log(activityCount);
                for(var i=0;i<items.length;i++){
                    $scope.activityLogs.push(items[i]);
                    };
                    // console.log(activityCount,Counts);
                });
                Counts=Counts+20;
                // console.log(activityCount,Counts);
            };

            // To capitalize the first letter
            var firstToUpperCase = function(strng){
                // //console.log("this is strng",strng);
                return strng.substr(0, 1).toUpperCase() + strng.substr(1);
            }

            $scope.string_format=function(val){
            if(val == undefined){
                   return null;
               }
               var loc1 = val.substring(val.indexOf("/") + 1);
               var loc2 = loc1.substring(loc1.indexOf("/") + 1);
               return loc2;
              };

/**    *function name : get time, to give time in terms of "mins ago,days ago etc"
    @param: activity.when
    return : time in terms of moments ago
**/
    
            $scope.getTime=function(date){
                var DateFormat = $moment(date).fromNow();
                // //console.log(DateFormat)
                return DateFormat;
            };
            $scope.Capital_frst = function(firstLtr) {
                if (firstLtr == undefined)
                return;
                var res = firstLtr.toUpperCase();
                var res1 = res.slice(0, 1);
                return res1;
            };

/*for charts*/

           $scope.getCurrentUser = Auth.getCurrentUser;
            // console.log($scope.getCurrentUser,"$scope.getCurrentUser");
                /*To get logged in user info*/
    $scope.getCurrentUser().$promise.then(function() {
        $rootScope.loggedIn=$scope.getCurrentUser();
        docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);
        $scope.loggedIn = $scope.getCurrentUser()._id;
        $rootScope.loggedUser = $scope.loggedIn;
        console.log($rootScope.loggedIn,"$rootScope.loggedUser")
    });



$scope.showChart=false;
 $scope.valueArr=[];
  $scope.reportTitle="Reports";

$scope.loginChart=function(){
     $scope.reportTitle="Login Activities";
 $scope.isuseractivity=false;
//$scope.valueArr=[];
  /*to query for data*/
 reportFactory.query(function(data){
            console.log("reportdata",data);
            $scope.Logindata=data;
             angular.forEach($scope.Logindata,function(value,key){
                    console.log(value,key,"for loop");                   
                    var obj={};
                    obj.label=value._id.user;
                    obj.value=value.Login;
                    $scope.valueArr.push(obj);
                    $scope.showChart=true;

             })
             $scope.logindata=[{
                key:"Cumulative Return",
                values: $scope.valueArr
             }];
             $scope.activity_Data=$scope.logindata[0].values;
        console.log($scope.logindata[0].values,"$scope.logindata",$scope.activity_Data);
             
     })
}

   $scope.value2arr=[];
$scope.activityChart=function(){
     $scope.reportTitle="Activities ";   
     $scope.isuseractivity=false;
   // $scope.valueArr=[];
    reportFactory.tenantConsilidatedReport(function(data1){
        console.log("activity report",data1);
        $scope.activityData=data1;
         var keyNames = Object.keys( $scope.activityData[0]);  
                    console.log(keyNames,"keyNames")    
        angular.forEach(keyNames,function(value,key){
                    console.log(value,key,"for loop");
                     var obj={}; 
                    if(value!="_id"  && value != "Total"){
                        console.log(  value != "Total"," total")
                        obj.label=value;
                        obj.value=$scope.activityData[0][value];
                        console.log(obj,"obj label if condition");
                        $scope.value2arr.push(obj);
                         $scope.showChart=true;
                    }
             })

        $scope.logindata=[{
            key:"Cumulative Return",
            values: $scope.value2arr
        }];
        $scope.activity_Data=$scope.logindata[0].values;
        console.log($scope.logindata[0].values,"$scope.logindata",$scope.activity_Data);
    })
}


$scope.value3arr=[];
$scope.useractivityChart=function(){
     $scope.reportTitle="User Activities ";
    $scope.isuseractivity=true;
   // $scope.valueArr=[];
    console.log("useractivityChart");
    reportFactory.userReport(function(data3){
        console.log(data3,"data3");
        $scope.userActivity=data3;
        UserService.getUsers(function(res){
             console.log(res,"users",$rootScope.loggedIn);
             $scope.userList=res;
             $scope.selectedName=$rootScope.loggedIn._id;
             angular.forEach($scope.userActivity,function(value,key){
                if(value._id.user_id == $rootScope.loggedIn._id){
                     var keyNames = Object.keys(value);
                     console.log(value,"valueif",$rootScope.loggedIn,"true");                     
                     console.log(keyNames,"keyNames");
                     angular.forEach(keyNames,function(value1,key1){
                        console.log("keynames value",value1,key1);
                         if(value1!="_id" && value1 != "Total"){
                            console.log("keynames value1",value1 != "Total");
                            var obj={}; 
                            obj.label = value1;
                            obj.value = $scope.userActivity[key][value1];
                            console.log(obj,"obj.value",obj.label);
                            $scope.value3arr.push(obj);
                                $scope.showChart=true;
                            console.log( $scope.value3arr)
                        }
                     }) 
                }
             })
            
                $scope.logindata=[{
                    key:"Cumulative Return",
                    values: $scope.value3arr
                }];
                 $scope.activity_Data=$scope.logindata[0].values;
        console.log($scope.logindata[0].values,"$scope.logindata",$scope.activity_Data);
            })
    })
}
/*
*/
$scope.setNull=function(){
   // $scope.logindata=[];
    $scope.value4arr=[];
}
$scope.rc={};
$scope.value4arr=[];
$scope.selctedUser=function(user){
    //$scope.logindata=[];
    console.log($scope.value4arr);
    //$scope.value4arr=[];
                console.log("user",user);                
               // $scope.rc.api.clearElement();                
                 angular.forEach($scope.userActivity,function(value,key){
                   // console.log(value._id.user_id);
                if(value._id.user_id == user){
                     var keyNames = Object.keys(value);
                    // console.log(value,"valueif","true");                     
                    // console.log(keyNames,"keyNames");
                     angular.forEach(keyNames,function(value1,key1){

                       // console.log("keynames value",value1,key1);
                         if(value1!="_id"){
                           // console.log("keynames value1");
                            var obj={}; 
                            obj.label = value1;
                            obj.value = $scope.userActivity[key][value1];
                           // console.log(obj,"obj.value",obj.label);
                            $scope.value4arr.push(obj);
                           // console.log( $scope.value4arr);
                        }
                     }) 
                }
             })
            $scope.logindata=[{
                key:"Cumulative Return",
                values: $scope.value4arr
            }];
            $scope.activity_Data=$scope.logindata[0].values;
            console.log($scope.logindata[0].values,"$scope.logindata",$scope.activity_Data);

//                  setTimeout(function(){
//     console.log('scope api:', $scope.api);
//     $scope.api.refresh();
// },30000)
                 //console.log($scope.rc.api,"api");
               //  $scope.rc.api.update();
                // $scope.apply();
}

// $scope.data=[{
//                 key:"Cumulative Return",
//                 values: $scope.valueArr
//              }];

$scope.options = {
    chart: {
        type: 'discreteBarChart',
        height: 550,
        width: 950,

        margin : {
            top: 20,
            right: 20,
            bottom: 150,
            left: 55
        },
        x: function(d){ return d.label; },
        y: function(d){ return d.value; },
        showValues: true,
        valueFormat: function(d){
            return d3.format(',')(d);
        },
        transitionDuration: 500,
        xAxis: {
            axisLabel:'Activities' ,
            rotateLabels:-45
        },
        yAxis: {
            axisLabel: 'Count',
            axisLabelDistance: 30
        }
    }

};


/*to close the chart*/
    $scope.check=function(){
        console.log("to close chart");
        $scope.logindata=[];
        $scope.showChart=false;


    }



    // $scope.reportData=function(){

    // }
})
angular.module('homer')
.controller('commentCntrl' , function($scope,notify,GlobalService,ActivitylogService,$rootScope,docsCtrlVariables,DefaultDocs,$moment,notify){
	// console.log("inside comments control ");
	$scope.homerTemplate = 'views/notification/notify.html';
	var activity_type = GlobalService.getActivitytype();
console.log( $rootScope.documentHeadingId , $rootScope.newDoccom);


var IndivisualDoc=$rootScope.documentHeadingId;   
$scope.fileDec=IndivisualDoc.description;
$scope.commentInfo=IndivisualDoc.createdBy;
$scope.Comment=IndivisualDoc.comments;
$scope.description="";
console.log($scope.Comment);


/*for autosize our text area */
    $.fn.enlargeTextArea = function() {
        return this.each(function() {
            var el = $(this);
            var elH = el.outerHeight();
            el.css({
                overflow: "hidden"
            });

            function manageTextarea() {
                el.css({
                    height: elH,
                    overflow: "hidden"
                });
                var nH = el.get(0).scrollHeight;
                nH = nH > elH ? nH : elH;
                el.css({
                    height: nH,
                    overflow: "hidden"
                })
            }

            el.bind("keydown", function() {
                manageTextarea(this);
            }).trigger("keydown");

        })
    };

    $(function() {
        $("textarea").not("#not").enlargeTextArea()
    });



	/*comments submit*/

	//file open opertion 
    var comments=[];
    $scope.commentFlag=false;
    $scope.replyFlag=false;

   $scope.submitData=function(data){
          if(data==undefined || data==null || data.length==0){
           notify({ message: 'Please enter the comment', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
          }else{
              if(IndivisualDoc.type=="folder"){
                    var currentDoc=$rootScope.documentHeadingId;
                      console.log(currentDoc,currentDoc.version);
                    var obj = {
                         id :$rootScope.documentHeadingId._id,
                         text:data
                        };
      
                    DefaultDocs.addComment(obj,function(res){
                     console.log(res);
                    $scope.Comment=res.comments;
                    var commentLog = {};
                      commentLog.activitytype = activity_type.COMMENT;
                      commentLog.what = res._id;
                      commentLog.location = res.parent;
                      commentLog.status = true;
                      ActivitylogService.LogActivity(commentLog);
                        });
                    $scope.description="";
                     // console.log(data);
              }else{
                        var currentDoc=$rootScope.documentHeadingId;
                        console.log(currentDoc,currentDoc.version);
                       var obj = {
                          id :$rootScope.documentHeadingId._id,
                          text:data,
                          versionId:$rootScope.commentVersion.versionId,
                          versionObjectId:$rootScope.commentVersion._id
                          };
      
                      DefaultDocs.addComment(obj,function(res){
                      console.log(res);
                      $rootScope.documentHeadingId=res;
                      $rootScope.newDoccom=res;
                      docsCtrlVariables.addToDocumentInfo(res);
                      $scope.Comment=res.comments;
                      console.log($scope.Comment);
                      var commentLog = {};
            commentLog.activitytype = activity_type.COMMENT;
            commentLog.what = res._id;
            commentLog.location = res.parent;
            commentLog.status = true;
            ActivitylogService.LogActivity(commentLog);

              console.log(commentLog);

                           });
                      console.log($rootScope.documentHeadingId ,$rootScope.newDoccom);
                      $scope.description="";
                      
                    };

              };
   };



/*deletecomments*/

$scope.deleteComment=function(data2){
    var currentDoc=$rootScope.documentHeadingId;
    // console.log("delete",data2);
    var obj3={
        id:$rootScope.documentHeadingId._id,
        comment:data2._id
       }

    DefaultDocs.deleteComment(obj3,function(data){
        // console.log(data);
        var ind=$scope.Comment.map(function(e) { return e._id; }).indexOf(data2._id);
        $scope.Comment.splice(ind,1);
        // docsCtrlVariables.spliceDoc(data.requestObject._id);
          })
    };

 $scope.imageExists = function(image, users) {
  // console.log(image,users);
        var img = new Image();
        img.onload = function() {
         users.avatarflag = true;
         $scope.$apply();
     }; 
     img.onerror = function() {
        users.avatarflag=false;
        $scope.$apply();
    };
    img.src = image;
  };


$scope.Capital_frst = function(firstLtr) {
    if (firstLtr == undefined)
        return;
    var res = firstLtr.toUpperCase();
    var res1 = res.slice(0, 1);
    return res1;
};


$scope.checkIfImageExists=function(user,image){
        // console.log(user,image);
    // user.name=user.name;
    docsCtrlVariables.checkForImage(user,image);
};
$scope.modifiedDate = function(date){
  // console.log(date);
  var locdate1 = $moment(date).fromNow();
  // console.log(locdate1);
  return locdate1;
};


});
/**
 * 10th Feb 2017
 * SubscriptionController
 * Chaithra
 * modified date : 
 */
angular.module('homer')
.controller('subscriptionCtrl',function($scope,$state,$rootScope,PlanService,plansFctry){
	console.log("inside subscriptionCtrl");

	PlanService.getFreeplanObj(function(data){
		console.log("data",data);
		$scope.freeplanObj=data;
	});
	PlanService.getStandardObj(function(data1){
		console.log("data1",data1);
		$scope.stdplanObj=data1;
	});
	$scope.calcSpace=function(space){
		console.log("space",space);
		var calcspace = Math.floor(space/(1024*1024*1024));
		return calcspace;
	}
	$scope.Selected=function(){
		console.log("selected");
	}
	$scope.freePlan=function(){
		console.log("freeplan");
		$rootScope.selectedPlan="Free Trail Registration";
		$state.go('common.userregistration');
	}
	$scope.BasicPlan=function(){
		console.log("basic plan ");
		$rootScope.selectedPlan="Basic Plan Registration";
		$state.go('common.userregistration');
	}
	$scope.StandardPlan=function(){
		console.log("standard plan");
		$rootScope.selectedPlan="Standard Plan Registration";
		$state.go('common.userregistration');
	}
});

/**
 * 10th Feb 2017
 * FreePlan controller
 * Chaithra
 * modified date : 
 */
angular.module('homer')
.controller('freeplanCtrl',function($scope,$rootScope,SubscriptionFactory,$state,notify,md5){
	initialize();
	unsetclasses();
	$scope.selPlan="Free Trial Registration";
	$scope.success = true;
	$scope.showcase = "xyz"
	$scope.submitLoading = false;
	console.log("free plan controller",$rootScope.selectedPlan);
	$scope.homerTemplate = 'views/notification/notify.html';	
	$scope.submit=function(data,plan){
		unsetclasses();
		if(data.username == ""){
			$scope.uclass = "uerror";
			$scope.ulabel = true;
		}else if(data.pwd == ""){
			$scope.plabel = true;
			$scope.pclass = "perror";
		}else if(matchpassword(data)){
			$scope.cplabel = true;
			$scope.pclass = "perror";
			$scope.cpclass = "cperror";
		}else if(data.useremail == ""){
			$scope.elabel = true;
			$scope.emclass = "emerror";
		}else if(validatemail(data)){
			$scope.vallabel = true;
			$scope.emclass = "emerror";
		}else if(data.organization == ""){
			$scope.olabel = true;
			$scope.oclass = "oerror";
		}else if(data.url == ""){
			$scope.urllabel = true;
			$scope.urlclass = "uerror";
		}
		else{
			var mdpassword = md5.createHash(data.pwd || ''); 
			var obj={
				first_name:data.username,
				company_name:data.url,
				url:data.url,
				primary_email:data.useremail,
				password:mdpassword,
				Subscription_Type:$scope.selPlan
			}
			console.log(obj);
			$scope.submitLoading = true;
			SubscriptionFactory.query(obj,function(res)
			{
				console.log("hitting api",res);
				$scope.success = false;
				$scope.submitLoading = false;
				//$state.go('common.login');
				//initialize();
			},
			function(err){
				$scope.submitLoading = false;
				$scope.sameurl = true;
				$scope.urlclass = "uerror";
			});
		}
	};

	$scope.populateurl = function(org){
		if(org == ""){
			$scope.showcase = "xyz";
			$scope.data.url = "";
		}else{
			var filtervalue = org.replace(/[^a-zA-Z0-9]/g, '-');
		    $scope.data.url = filtervalue.toLowerCase();
		    $scope.showcase = $scope.data.url;
		}
	};

	$scope.populateshowcase = function(url){
		if(url == ""){
			$scope.showcase = "xyz";
		}else{
			$scope.showcase = url;
		}
		
	}

	function unsetclasses(){
		$scope.uclass = "";
		$scope.pclass = "";
		$scope.cpclass = "";
		$scope.emclass = "";
		$scope.oclass = "";
		$scope.urlclass = "";
		$scope.ulabel = false;
		$scope.plabel = false;
		$scope.cplabel = false;
		$scope.elabel = false;
		$scope.vallabel = false;
		$scope.olabel = false;
		$scope.urllabel = false;
		$scope.sameurl = false;
	}

	function matchpassword(data){
		if(data.pwd != 0){
                if(data.pwd == data.cpwd){
                    return false;
                }else{
                    return true;
                } 
        }else{
        	return true;
        }
	}

	function validatemail(data){
		var regexp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(regexp.test(data.useremail)){         
            return false;
        }else{
            return true;
        }
	}

	function initialize(){
		$scope.data = {username:"",pwd:"",cpwd:"",useremail:"",organization:"",url:""};
	}
});
angular.module('homer')
.controller('headerCtrl', function ($scope,$state,$http,$rootScope,docsCtrlVariables){
/*
function for global Search
*/
	$scope.navsearch=function(search){
		console.log(search,"search here");
		$rootScope.globalSearch=search;
		docsCtrlVariables.newbreadcrumbs();
		docsCtrlVariables.setWhichDocument("search","clicked");
		$state.go("dashboard", {}, { reload: true });
	};

	$scope.callLogs=function(){
		console.log("call logs");
		$state.go('right_sidebar')
	}


  });
angular.module('homer')
.controller('redirectpageCtrl', function($scope,$state,TenantFactory,notify,$moment,docsCtrlVariables,Auth,$rootScope,$window){
   $scope.homerTemplate = 'views/notification/notify.html';
   console.log("inside redirectpage controller");
   $scope.getCurrentUser = Auth.getCurrentUser;

   /*To get logged in user info*/
   $scope.getCurrentUser().$promise.then(function() {
      $rootScope.loggedIn=$scope.getCurrentUser();
      docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);
      $scope.loggedIn = $scope.getCurrentUser()._id;
      $rootScope.loggedUser = $scope.loggedIn;
   });

   //breadcrumbs
   $scope.userBreadCrumbs=[];
   $scope.redirectPage=function(){
      $scope.userBreadCrumbs=[];
      var b1= "Applications";
      $scope.userBreadCrumbs.push(b1);
   }

   $scope.NamePtrn=/^[a-zA-Z1-9 _]{3,25}$/;
   $scope.EmailPtrn = /^[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})/;

   $scope.infoFlag=false;
   TenantFactory.applicationList(function(data){
      console.log(data);
      $scope.domains=data;
      for(var i=0;i<data.length;i++){
         // console.log(data[i].application,"data loop");
         $scope.appList=data[i].application;
      }
      // console.log($scope.appList);
   });

   $scope.nameOfUrls=[];
   $scope.addAppflag=false;
   $scope.addApp=function(){
     $scope.addAppflag=true;
     $scope.editAppflag=false;
      // console.log("app add page");

   };

   $scope.editApp=function(data){
    $scope.addAppflag=false;
    for(var i=0;i<$scope.domains.length;i++){
      // console.log($scope.domains[i],"data loop");
      $scope.appdomain=$scope.domains[i];
   }
   $scope.editAppflag=true;
   $scope.editApplication=data;
   // for(var i =0;i<data.redirectUri.length;i++){
   //    console.log(data.redirectUri[i]);
   $scope.Uri=data.redirectUri;
   angular.forEach($scope.Uri,function(link,key){
      $scope.nameOfUrls[key]=link;
   })
   // }
   // $scope.addAppflag=true;
   // console.log($scope.editApplication);
};

$scope.updateApp=function(data){
   console.log(data,"update App",$scope.editApplication, $scope.appdomain);
   var editObj={
      id:$scope.appdomain._id,
      appId:$scope.editApplication._id,
      redirectUri:data
   }
   TenantFactory.updateapplication(editObj,function(res){
      console.log("updateduri result",res.application);
      angular.forEach(res.application,function(val){
         console.log("val",val);
         if(val._id==$scope.editApplication._id){
            // $rootScope.loginObj=val;
            console.log( val,$scope.appdomain);
            console.log(val.redirectUri[val.redirectUri.length-1]);
            var url=val.redirectUri[val.redirectUri.length-1];
             $window.location.href = "http://application.2docstore.com:3000";
         }
      })
         })
};


      $scope.formateDate = function(date){
            var locdate1 = $moment(date).fromNow();
            return locdate1;
         };

         $scope.addInfo=function(data1){
            console.log( $scope.domains);
            if($scope.dataForm.$valid){
            console.log("submit here",data1,"valid",$scope.domains);

                         angular.forEach($scope.domains,function(value){
                        console.log(value);
                        $scope.infoFlag=true;
         // $scope.information=data1;
                     var object={
                        id:value._id,
                        name:data1.name,
                        // emailId:data1.emailId,
                        description:data1.description
                     }
                     console.log(object);
         TenantFactory.createApplication(object,function(res){
            console.log(res,"result here");
            if(res.type=="error"){
               // console.log("error type");
               $scope.infoFlag=false;
               notify({ message:'Domain already exists' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
               // $scope.information
               data1={};
            }else{
               console.log(res.application[res.application.length-1],"no error",$scope.appList);
               $scope.appList=res.application;
               $scope.result=res._id;
               angular.forEach(res,function(key,value){
                  // console.log(key,value);
               })
               $scope.information = (res.application[res.application.length-1]);
               // console.log($scope.information,$scope.result);
               // $state.go("updatedPage");
            }
            
         },function(err){
            // console.log("error");
            notify({ message:'Domain already exists' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
         })

                         })
              
}else{
   // console.log("not valid");
   $scope.dataForm.submitted=true;
}   		
};

$scope.updateInfo=function(data2){
   console.log(data2,"update",$scope.information,$scope.domains);
            angular.forEach($scope.domains,function(value){
               console.log(value);
                   var obj={
                           id:'58356521b91de8d7d6c67b24',
                           appId:$scope.information._id,
                           redirectUri:data2.url
                        }
                  TenantFactory.updateapplication(obj,function(res){
                     console.log(res,"update result",data2);
                     $scope.addAppflag=false;

                      angular.forEach(res.application,function(values){
                        console.log("val",values,data2);
                        if(values.name==data2.name){
                           $rootScope.loginObj=values;
                           console.log(values.redirectUri[values.redirectUri.length-1]);
                           var url=values.redirectUri[values.redirectUri.length-1];                           
                           // $window.location.href = url;
                        }
                     })

                     $state.go($state.current, {}, {reload: true});
                           // $state.go('/login');
                        })
            })
  
   };

$scope.clearInfo=function(data1){

 // console.log("cancel");
 $state.go($state.current, {}, {reload: true});
};

});
/**
 *
 * Timeline controller
 *
 */

angular
    .module('homer')
    .controller('timelineCtrl', timelineCtrl)

function timelineCtrl($scope, $uibModalInstance,$rootScope,docsCtrlVariables,$moment,param,timelineService,Auth,notify) {
    // console.log("inside timelineCtrl",param.doc);
    $scope.timelineDoc=param.doc;
    $scope.getCurrentUser = Auth.getCurrentUser;
    $scope.repeated = timelineService.getRepeat();
    $scope.currentDate = new Date();
    $scope.updateflag=false;
$scope.timeFlag=false;
    timelineService.getTimeline(param.doc,function(value){
        // console.log(value);
    $scope.getCurrentUser = Auth.getCurrentUser;
    $scope.homerTemplate='views/notification/notify.html';
$scope.getCurrentUser().$promise.then(function() {
        $rootScope.loggedIn = $scope.getCurrentUser();
        ////////////console.log($scope.loggedIn);
    });

        if(value.when){
            // console.log(value);
            $scope.updateflag=true;
            $scope.timeline=value;
            $scope.reminder_msg=value.message;
            // console.log($scope.reminder_msg);
              if(value.repeat==2){
                // console.log(value)
                angular.forEach(value.weekDay, function(v, key){
                   angular.forEach($scope.weeks, function(val, k){
                       if(v==val.day){
                        val.check=true;
                       }
                   });
                });
                
            }
           $scope.timeline.when=new Date(value.when); 
           if(value.untilDate){
            $scope.timeline.untilDate=new Date(value.untilDate);
           }
        }
        else{
            $scope.reminder_msg='Enter the Timeline text / details here';
             $scope.timeline={};
            $scope.timeline.when=$scope.currentDate;
        }
        
    });

    $scope.weeks = [{day:'Sun',check:false},{day:'Mon',check:false},{day:'Tue',check:false},{day:'Wed',check:false},{day:'Thu',check:false},{day:'Fri',check:false},{day:'Sat',check:false}];
  
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };


    $scope.saveTimeline=function(timeline){
     console.log(timeline,"to set timeline");
       $scope.timeFlag=true;
       // console.log($scope.timeFlag);
        var arr=[];
        $scope.reminder = {
                user_id : $scope.getCurrentUser()._id,
                document_id : param.doc._id,
                message : $scope.reminder_msg,
                when : new Date(timeline.when),
                checkboxModel : timeline.checkboxModel,
                recurring : timeline.recurring,
                repeat : timeline.repeat,
                untilDate : String(timeline.untilDate),
                //weekDay : $scope.selected1
            };    
            if($scope.reminder.repeat==2){
                angular.forEach($scope.weeks, function(value, key){
                    if(value.check==true){
                        arr.push(value.day);
                    }
                });
                $scope.reminder.weekDay=arr;
            }
            // console.log($scope.reminder);    
        timelineService.saveTimeline($scope.reminder,function(cb){
            console.log("set rem",cb);
            $uibModalInstance.close();
            
        });
        
    }
        $scope.updateTimeline=function(timeline){
             console.log(timeline,"update timeline");
             console.log(timeline,timeline._id);
            $scope.timeFlag=true;
        // console.log(timeline,$scope.timeFlag);
        var arr=[];
        $scope.updatereminder = {
                _id:timeline._id,
                user_id : $scope.getCurrentUser()._id,
                document_id : param.doc._id,
                message : $scope.reminder_msg,
                when : new Date(timeline.when),
                checkboxModel : timeline.checkboxModel,
                recurring : timeline.recurring,
                repeat : timeline.repeat,
                untilDate : String(timeline.untilDate),
                //weekDay : $scope.selected1
            };  
            console.log($scope.updatereminder);
            if($scope.updatereminder.repeat==2){
                angular.forEach($scope.weeks, function(value, key){
                    if(value.check==true){
                        arr.push(value.day);
                    }
                });
                $scope.updatereminder.weekDay=arr;
            }
            else{
                $scope.updatereminder.weekDay=[];
            }
            // console.log($scope.updatereminder);    
        timelineService.updateTimeline($scope.updatereminder,function(cb){
            console.log(cb,"cb")
            $uibModalInstance.close();
        });
        // console.log($scope.updatereminder,"updated reminder");

    };    

    $scope.checkSwitch=function(timeline){
        timeline.recurring=!timeline.recurring;
    }
    $scope.cancel=function(){
        $uibModalInstance.close();
    }
    $scope.selectWeek=function(week){
        // console.log(week);
        week.check=!week.check;
    }
}
angular
   .module('homer')
   .controller('propertyCtrl', propertyCtrl)

function propertyCtrl($scope,$state,notify,
                      $uibModalInstance,activityFcrt,
                      DocstoreService,DefaultDocs,
                      $location,ActivitylogService,
                      GlobalService,docsCtrlVariables,
                      $rootScope,$moment,versionService,
                      $compile,$window,Auth,$q,$http,$filter,
                      GroupService,blockUI) {
  var activity_type = GlobalService.getActivitytype();
blockUI.stop();
// /* mamatha here for activity log*/ 

$scope.imageExists=function(users){
  console.log(user);
    docsCtrlVariables.checkForImage(users,users.avatar);
  };

// $scope.checkIfImageExists=function(user){
//   console.log(user,"image call");
//  user.name=user.user_name;
  //docsCtrlVariables.checkForImage(user,image);
// };  
/*
Query to get the documents listed under tenant
*/
$rootScope.documentHeadingId=docsCtrlVariables.getpropertyDoc();
var IndivisualDoc=$rootScope.documentHeadingId;
console.log(IndivisualDoc);
$scope.fileDec=IndivisualDoc.description;
$scope.commentInfo=IndivisualDoc.createdBy;
console.log($scope.commentInfo);
$scope.Comment=IndivisualDoc.comments;
console.log($scope.Comment);

$scope.currentDate = new Date();
Auth.getTenant(function(tenant){ 
    $rootScope.tenantInfo=tenant;$scope.space=0;
  angular.forEach($rootScope.tenantDocs,function(value,key){
    $scope.space=$scope.space+value.size;
  })
  $scope.allottedspace=$rootScope.tenantInfo.subscription.space;

    $scope.giveWidth=function(){
      $scope.width=($scope.space/$rootScope.tenantInfo.subscription.space)*100;
    }
});

  $scope.Capital_frst=function(grpname){
    return GroupService.First_UpperCase(grpname);
  };
// listing logo
  $scope.imageExists = function(image,users){
      var img = new Image();
      img.onload = function() {
        users.avatarflag = true;
       // $scope.$apply();
      }; 
      img.onerror = function(){
        users.avatarflag=false;
       // $scope.$apply();
      };
      img.src = image;
   };

$scope.property="enter the description";

$scope.save=function(a){
  $scope.property=a;
}
$scope.Remove=function(b){
  $scope.property="";
}

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };

/*
for function peroperties
*/
$scope.propertyDoc=docsCtrlVariables.getpropertyDoc();
console.log($scope.propertyDoc);
 angular.forEach($scope.propertyDoc.version, function(value, key){
  if($scope.propertyDoc.version.length == value.number){
    // console.log(value,value.number)
    $rootScope.commentVersion=value;
    // console.log($rootScope.commentVersion)
  }
 });
if($scope.propertyDoc.description){
 $scope.describ=$scope.propertyDoc.description; 
}

 angular.forEach($scope.propertyDoc.version, function(value, key){
  if($scope.propertyDoc.version.length == value.number){
    $rootScope.versionObject=value;
  }
 });

  $scope.modifiedDate = function(date){
  var locdate1 = $moment(date).fromNow();
  return locdate1;
};

$scope.Version=versionService.getVersion($scope.propertyDoc);
 $scope.formateDate = function(date){
  var locdate1 = $moment(date).fromNow();
  return locdate1;
};

$scope.findIcon=function(doc){
  var icon=docsCtrlVariables.findIcons(doc)
  return icon;
};

var location = $scope.propertyDoc.s3_path;
       location = location.substring(location.indexOf("/") + 1);
       var location1 = location;
       location1 = location1.substring(location1.indexOf("/") + 1);
        location2 = location1;
        location2 = location2.substring(0, location2.lastIndexOf("/"));
        $scope.propertyDoc.s3_path = location2;
        if ($scope.propertyDoc.imageDimension)
            $scope.showResolution = true;
        else
            $scope.showResolution = false;


$scope.versionarray=[];
/*
*returns the present version of the file
*/
$scope.openVersion=function(property_Doc){
  // console.log(doc);
  $scope.versionarray=[];
  var count=0;
  property_Doc.version.forEach(function(val){
    // console.log(val);
    var verobj={
      version:val.number,
      versionId:val.versionId,
      created_at:val.created_at,
      documentName:property_Doc.documentName,
      versionflag:true,
      url:property_Doc.url,
      created_by:val.added_by,
      createdBy:val.added_by,
      s3_path:property_Doc.s3_path,
      parent:property_Doc.parent,
      description:property_Doc.description,
      comments:property_Doc.comments,
      isLocked:property_Doc.isLocked,
      allowDownload:property_Doc.allowDownload,
      thumbnail_version:val.thumbnail_version,
      _id:property_Doc._id,
      document:property_Doc
    };
    // console.log(doc.version,verobj);
    count++;
    if(count<property_Doc.version.length){
    $scope.versionarray.push(verobj); 
    }
    
  })
  $scope.versionfilename=property_Doc.documentName;
  $scope.showversion=true;
}




  // $scope.openVersion=function(property_Doc){
  //   $scope.versionarray=[];
  //   var count=0;
  //     property_Doc.version.forEach(function(val){
  //       var verobj={
  //         version:val.number,
  //         versionId:val.versionId,
  //         created_at:val.created_at,
  //         documentName:property_Doc.documentName,
  //         versionflag:true,
  //         s3_path:property_Doc.s3_path,
  //     parent:property_Doc.parent,
  //     url:property_Doc.url,
  //     created_by:val.added_by,
  //     createdBy:val.added_by,
  //     description:property_Doc.description,
  //     comments:property_Doc.comments,
  //     isLocked:property_Doc.isLocked,
  //     allowDownload:property_Doc.allowDownload,
  //         thumbnail_version:val.thumbnail_version,
  //         _id:property_Doc._id,
  //         document:property_Doc
  //       };
  //       count++;
  //   if(count<property_Doc.version.length){
  //   $scope.versionarray.push(verobj); 
  //   }
  //       $scope.versionarray.push(verobj);
  //     })
  //   $scope.versionfilename=property_Doc.documentName;
  //   $scope.showversion=true;
  //   $scope.versionArry=$scope.versionarray;
  // };

 $scope.prop_open=function(thumb_pro){
    $scope.thumb_array=[];
      thumb_pro.version.forEach(function(val){
        var verobj={
          version:val.number,
          versionId:val.versionId,
          created_at:val.created_at,
          documentName:thumb_pro.documentName,
          versionflag:true,
          created_by:val.added_by,
          thumbnail_version:val.thumbnail_version,
          _id:thumb_pro._id,
          document:thumb_pro
        };
        $scope.thumb_array.push(verobj);
      })
    //$scope.versionfilename=thumb_pro.documentName;
    $scope.showversion=true;
    //$scope.versionArry=$scope.thumb_array;
  };

  $scope.checkIfImage=function(document){
    if((/\.(gif|jpg|jpeg|tiff|png|ico)$/i).test(document.documentName))
      return true;
    else 
      return false;
  };

  $scope.getThumbnailVersion=function(doc){
    versionService.getThumbnailVersion(doc);
  };

$scope.imageUrl=function(propertyDoc,version){
  var file_id = {
    id : propertyDoc._id
    };
    if(version){
     file_id.version=version;
   }
   DefaultDocs.viewFile(file_id, function(result) {
      propertyDoc.url=result.url;
  })
};

$scope.folderClick=function(propertyDoc,version){
  if(!propertyDoc.thumbnail_url && !$scope.checkIfImage(propertyDoc)){
            var file_id = {
              id : propertyDoc._id
            };
            if(version){
              file_id.version=version;
            }
            DefaultDocs.viewFile(file_id, function(result) {
                $rootScope.viewurl = result.url;
                $rootScope.documentHeading=propertyDoc.documentName;
                docsCtrlVariables.openFile()
                }, function(err) {
                  $scope.error = "Documenet is locked by " + propertyDoc.isLockedtoName;
                 });  
    }
};

/*
*Function to make particular version as current version
*/
$scope.makeCurrentVersion=function(versionobj){
    var allowVersion=false;
if(versionobj.isLocked.status==false)
{
  allowVersion=true;
}
else if(versionobj.isLocked.user_id==$rootScope.loggedIn._id)
{
allowVersion=true;
}
else{
allowVersion=false; 
}
if(allowVersion){

  var tempobj={
    version:versionobj.version,
    id:versionobj._id
  }
  DefaultDocs.changeversion(tempobj,function(data){
        var viewLog = {};
            viewLog.activitytype = activity_type.CHANGEVERSION;
            viewLog.what = versionobj.document._id;
            viewLog.location = versionobj.document.parent;
            viewLog.status = true;
            ActivitylogService.LogActivity(viewLog);
    $state.go('dashboard',{},{reload: true});
    $scope.showversion=false;
   
  });
}

else{
        notify({ message: 'Document is locked '+ versionobj.isLockedtoName, classes: 'alert-danger', templateUrl: $scope.homerTemplate});

  }
};

/*FunctionName: Download
@Params:docuemnt object
Returns: URL to start download*/
$scope.download=function(propertyDoc,version){
if(propertyDoc.isLocked.user_id==$rootScope.loggedIn._id || !propertyDoc.isLocked.status)
{
 if(propertyDoc.allowDownload){
  var current = docsCtrlVariables.getCurrentDocument(); 
  var file_id = {
        id : propertyDoc._id
      };
     if (version){
        file_id.version=version;
      }
      DefaultDocs.download(file_id, function(result) {
      $window.location.href = result.url;
      var downloadLog = {};
      downloadLog.activitytype = activity_type.DOWNLOAD;
      downloadLog.what =  propertyDoc._id;
      downloadLog.location = current.parent + '/' + current.documentName;
      downloadLog.status = true;
      ActivitylogService.LogActivity(downloadLog);
      //from activity service
      $scope.downloadLog = activityFcrt.save(downloadLog, function(data) {
     });
      }, function(err) {
        notify({ message: 'Warning - Please Enter Valid Username or Password', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
      });
    }
    else{
              notify({ message: 'Error - You do not have permission to download '+propertyDoc.documentName , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
    }
  }
  else{
      notify({ message: 'Error - '+propertyDoc.documentName +' is Locked' , classes: 'alert-danger', templateUrl: $scope.homerTemplate});
  }
};
  
// /* mamatha here for activity log*/ 
// $scope.checkIfImageExists=function(user,image){
//   console.log(user,image,"image call");
//   user.name=user.user_name;
//   docsCtrlVariables.checkForImage(user,image);
// };  

/* Query to get the activity */
  var Activity = GlobalService.getActivitytype();
  var Document_prop=docsCtrlVariables.getpropertyDoc();
  var prop=activityFcrt.FolderActivity(Document_prop,function(res){
    console.log(res);
    $scope.ActivityLog=res;
    console.log($scope.ActivityLog);
  });

   // To capitalize the first letter and display(first letter)
  $scope.firstToUpperCase = function(strng) {
      return GroupService.firstToUpperCase();
  };

$scope.getWho = function(activityMsg){
      if (activityMsg.who){
        var strng = activityMsg.who.user_name;
        var User_name = strng.substr(0, 1).toUpperCase() + strng.substr(1);
      } else {
        var User_name = "";
      }
      return User_name;
};


 
    /**
  *function name : getActivityType
  @param: activity obj
  return : Activity in a senetence
  **/
  $scope.getActivitytype = function(activityMsg){
    if (activityMsg){
      var text;
      switch (activityMsg.activitytype) {
        case Activity.CREATE:
        case Activity.LOCK:
        case Activity.UNLOCK:
        case Activity.FAV:
        case Activity.UNFAV:       
        case Activity.SHARE:
        case Activity.DELETE:
        case Activity.DOWNLOAD:
        case Activity.VIEW:
        case Activity.TRASH:
        case Activity.RESTORE:
        case Activity.UPLOAD:     
          text =  Activity.HAS + activityMsg.activitytype.bold() + Activity.THIS + activityMsg.what.docu_type;
          break;
        case Activity.CHANGEVERSION:
        case Activity.DESCRIPTION:
        case Activity.COMMENT:
          text = Activity.HAS + activityMsg.activitytype.bold() + Activity.TO + Activity.THIS + activityMsg.what.docu_type;
          break;      
        case Activity.REMIND:
          text = Activity.HAS + activityMsg.activitytype.bold();
          break;
        default:
          text = "default text";
      }
      return text;
     }
 };

console.log($scope.propertyDoc);
  $scope.saveForm = function(data) {
    console.log(data,$scope.propertyDoc);
    $scope.propertyDoc.description=data;
    DefaultDocs.modify($scope.propertyDoc,function(res){
        var DescribLog = {};
          DescribLog.activitytype = activity_type.DESCRIPTION;
          DescribLog.what = $scope.propertyDoc;
          DescribLog.location = $scope.propertyDoc.parent;
          DescribLog.status = true;
          ActivitylogService.LogActivity(DescribLog);
          console.log(DescribLog);
    });
  };
  


// /* mamatha here for activity log*/ 

}

angular.module('homer')
.controller('projectClientCtrl', function ($scope,$state,blockUI,$http,$rootScope,md5,Auth,
  User,notify,DefaultDocs,$moment,ShareFctry,sweetAlert,uploadService,$uibModal,
  GlobalService,ActivitylogService,activityFcrt,docsCtrlVariables,versionService,
  ClientService,DocsFcrt,UserService,$rootScope,clientFactory,sweetAlert) {
$rootScope.clientPage=true;
	
  UserService.getUsers(function(data){
		$scope.userslist=data;
    console.log($scope.userslist);
	});	

	$scope.clientList=[];
  $scope.getCurrentUser = Auth.getCurrentUser;
  $scope.getCurrentUser().$promise.then(function() {
    $rootScope.loggedIn = $scope.getCurrentUser();
    // $scope.loggedUser=$scope.getCurrentUser();
    ////console.log($scope.loggedIn,$scope.loggedIn.role);
    docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);

      });
  /*
    get the list of clients
    */ 
  ClientService.getClientsAsync(function(data){
  	$scope.clientLength=data.length;
   console.log($scope.clientLength);
  });


  /*to get limited client*/
  $scope.clientCount=0;

clientFactory.query({'count':$scope.clientCount,'limit':50},function(data){
      // console.log("getclientlist");
      $scope.clientList=data;
      console.log($scope.clientList);     
    });

$scope.loadclientMore=function(){   
    $scope.clientCount=$scope.clientCount+50;
    // console.log($scope.clientCount);
    clientFactory.query({'count':$scope.clientCount,'limit':50},function(data){
      for(var i=0;i<data.length;i++){
        $scope.clientList.push(data[i]);
      };
    });
  };

  // $scope.imageExists=function(users){
  // 	docsCtrlVariables.checkForImage(users,users.avatar);
  // 	// console.log(users)
  // }


   /*
   size calculations
   */

   $scope.getSize=function(client){
    var sizeSpecs = DocsFcrt.getAllsize(client);
   // console.log(client)
   }

/*
add clients
*/
$scope.addClient=function(){
	$state.go("clientManagement")
	}
  
$scope.checkIfImageExists=function(user,image){
	docsCtrlVariables.checkForImage(user,image);
};

/*
edit
*/
$scope.clickedOnView=function(document){
	$rootScope.projectViewDoc=document;
		docsCtrlVariables.setWhichDocument("ProjectView","clicked");
		$state.go("dashboard", {}, { reload: true });
}
/*
nav
*/
$scope.editClients=function(){
	$state.go('clientManagement')
}	

/*to delete client*/
  $scope.deleteClient=function(data){
    console.log("to delete client",data);
    sweetAlert.swal({
      title : "Warning",
      text : "Users, files and folders created will be moved to admin!!",
      type : "warning",
      showCancelButton : true,
      confirmButtonColor : "#62CB31",
      cancelButtonColor:"#F44336",
      confirmButtonText : "Delete",
      cancelButtonText : "Cancel",
      closeOnConfirm : true,
      closeOnCancel : true
    }, function(isConfirm) {
          if(isConfirm){
                var params={
                    client:data._id
                }
                clientFactory.deleteClient(params,function(res){
                  console.log("res",res);
                  angular.forEach($scope.clientList,function(value,key){
                    if(value==data){
                      $scope.clientList.splice(key,1);
                      sweetAlert.swal("Deleted!", "delete done.", "success");
                      blockUI.stop();
                    }
                  })
                })  
          }else{
            console.log("else");
            sweetAlert.swal("Cancelled", "client is safe :)", "error" );
          }
    })
  }

})



/** 2Docstore
 * page: admin (user group page)
 * dev: mamatha
 * created date : 16th  August 2016
 * modified date :  23-Sept-2016   Pradeep T,  Modify the code structure
 * Discription : creating the user group and listing of the groups 
 */
'use strict';
 angular.module('homer')
 .controller('usergroupctrl',function(
 	$scope,$state,notify,
 	Groups,GroupService,$uibModal,$rootScope,docsCtrlVariables,
 	$filter,$moment,Auth,GlobalService,ActivitylogService) 
 {
 	$scope.getCurrentUser = Auth.getCurrentUser;
  $scope.getCurrentUser().$promise.then(function() {
    $rootScope.loggedIn = $scope.getCurrentUser();
    // $scope.loggedUser=$scope.getCurrentUser();
    ////console.log($scope.loggedIn,$scope.loggedIn.role);
    docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);

      });
 	
 	//scope variables used in this controller in addition to inheritated variables
 	$scope.homerTemplate = 'views/notification/notify.html';
 	$scope.selectedList=[];
 	$scope.selectUserList=[];
 	$scope.BreadCrumbs=[];
 	//Manage Breadcrumbs
 	var _pgManageGroup= "Manage Groups";
 	var _pgAddGroup="Add Group";
 	var _pgAddUsers="Add Users";
	var _pgGroupList="GroupList";
	var activity_type = GlobalService.getActivitytype();
	
	//console.log($state.current.name, "group",$rootScope.currentGroup);
	// if($rootScope.currentGroup==undefined){
	// 	$state.go(_pgGroupList);
	// };

	if($state.current.name=="InviteGroupUser" && $rootScope.currentGroup===undefined){
		$scope.BreadCrumbs=[];
		$state.go(_pgGroupList);
	}
 	//bread crumbs
 	$scope.ManageGroup=function(){
 		$scope.BreadCrumbs.push(_pgManageGroup);
 	};
	//create group page
	$scope.addGroup1=function(){
		$scope.BreadCrumbs.push(_pgManageGroup,_pgAddGroup);
	};
	//add user to group
	$scope.addUserGroup=function(){
		$scope.BreadCrumbs.push(_pgManageGroup,_pgAddUsers);
	};    
//click on the bread crum to go to previous page
	$scope.clickOnPage=function(page){
		if(page ==_pgManageGroup){
			$rootScope.currentGroup=undefined;
			$state.go(_pgGroupList);
		}
	};

	//list of Groups 
	
	/*with load more option (actualone)*/
	// $scope.groupCount=0;
	// $scope.groupslist=Groups.getAllGroups({'count':$scope.groupCount,'limit':8},function(groups){
	// 	// =groups;
	// 	// console.log(groups);
	// });

	// var Count=$scope.groupCount+8;
	// $scope.loadgroupMore=function(){
	// 	var grouplistCount=Count;
	// 	var lists=Groups.getAllGroups({'count':grouplistCount,'limit':8},function(data){
			
	// 		for(var i=0;i<lists.length;i++){
	// 			$scope.groupslist.push(lists[i]);
	// 		};
	// 	});
	// 	Count=Count+8;
	// };
     
	
/*Demo code */
$scope.groupCount=0;
	$scope.groupslist=Groups.getAllGroups({'count':$scope.groupCount,'limit':100},function(){
		// =groups;
		 //console.log("groups",groups);
	});

	var Count=$scope.groupCount+100;
	$scope.loadgroupMore=function(){
		var grouplistCount=Count;
		var lists=Groups.getAllGroups({'count':grouplistCount,'limit':100},function(){
			
			for(var i=0;i<lists.length;i++){
				$scope.groupslist.push(lists[i]);
			}
		});
		Count=Count+100;
	};


	//To make first letter as Uppercase and slice the 1st letter (groupservice.js)
	$scope.Capital_frst=function(grpname){
		return GroupService.First_UpperCase(grpname);
	};


	//this is the 2nd screan in creat group page
	$scope.AddGroup=function(){
		$state.go('UserGroup');
	};

//select multiple check to delete
	$scope.checkAll = function(select ,filtered){
		//console.log(select,filtered);
		if(!filtered)
		//var group="group";
		GroupService.UserRemoveAll(select,filtered,"group");
	};

//select individual check to delete
	$scope.addOrRemove=function(users,index,isSelect){
		//console.log(users,index,isSelect);
		$scope.selGroupis=users;
		if(!isSelect)
			$scope.selected=false;
		GroupService.addOrRemoveUser(users,index,$scope.selectedList);
	};
	
	//both multiple and individual delete group
	$scope.DeleteGroup=function(){
	//	console.log("delete group");         
		var groupList=GroupService.getList();
		var ListOfGroups=GroupService.getGroupList();
		//console.log(groupList,ListOfGroups);
			if(groupList.length>=1){
				Groups.removeGroup(groupList,function(){
					angular.forEach(groupList,function(val,k){
						//console.log("forloop",val,k,res);
						var ind=$scope.groupslist.map(function(e) { return e._id; }).indexOf(val);
						var i=ListOfGroups.map(function(e) { return e._id; }).indexOf(val);
						$scope.groupslist.splice(ind,1);
						ListOfGroups.splice(i,1);
						 //console.log("else condition",i);
						 //console.log($scope.selGroupis._id==val,"condition",$scope.selGroupis.Name,$scope.selGroupis)
				 		var deleteGrouplog = {};
						deleteGrouplog.activitytype = activity_type.GROUPDELETE;
						deleteGrouplog.what = $scope.selGroupis.Name;
						deleteGrouplog.status = true;
						//console.log("activity",deleteGrouplog);
						ActivitylogService.LogActivity(deleteGrouplog);
					});
				notify({ 
					message: 'Group is deleted',
					classes: 'alert-success',
					templateUrl: $scope.homerTemplate
					});
				$state.go('GroupList',{},{reload: true});
				});
				GroupService.deleteList();
		}else {
			notify({ 
				message: 'No group is selected',
				classes: 'alert-danger',
				templateUrl: $scope.homerTemplate
				});
		}
	};
	
	//this is the 2nd screen in creat group page
	$scope.AddGroup=function(){
		$state.go('UserGroup');
	};
	//keypress function in search
	$scope.valueSearch=function(){
		$scope.selectedList=[];
		angular.forEach($scope.groupslist,function(val, k){
			val.isChecked=false;
		});
		GroupService.deleteList();
		$scope.selected=false;
	};

//To add group
	 $scope.groupAdd=function(grp){
	 	 console.log(grp)
	 	if ($scope.GroupForm.$valid){
	 		// console.log("valid");
	 	// console.log(GroupService.DuplicateGroup(grp.Name,$scope.groupslist));
	 		if(!GroupService.DuplicateGroup(grp.Name,$scope.groupslist)){
	 			GroupService.creatGroup(grp,function(){
					notify({ message: 'Group is created', classes: 'alert-success', templateUrl: $scope.homerTemplate});
					
					$state.go('GroupList',{},{reload: true});
				});
	 		}else{
	 		notify({ message: 'Duplicate Group name', classes: 'alert-danger', templateUrl: $scope.homerTemplate});	
	 		}
	 	}else{
	 		// console.log("not valid");
	 		$scope.GroupForm.submitted = true;
	 	}
	};

// To add another group
	 $scope.oneMoregroup=function(grp){

	 		if(!GroupService.DuplicateGroup(grp.Name,$scope.groupslist)){
	 	GroupService.creatGroup(grp,function(){
			notify({ message: 'Group is created', classes: 'alert-success', templateUrl: $scope.homerTemplate});
			$state.go($state.current, {}, {reload: true});
		});
		}else{
			notify({ message: 'Duplicate Group name', classes: 'alert-danger', templateUrl: $scope.homerTemplate});	
	 	}
	 };
	 
	 	// update group
	$scope.groupUpdate=function(grp){
		// console.log(grp);
		GroupService.updateGroup(grp,function(){
			notify({ message: 'Group is updated', classes: 'alert-success', templateUrl: $scope.homerTemplate});
					//$state.go('GroupList',{},{reload: true});
		});
	};

	$scope.viewGroup=function(group){
		// console.log(group);
		$rootScope.currentGroup=group;
		$rootScope.users_list=group.user;
		$state.go('InviteGroupUser');
	};

	$scope.addUsersToGroup=function(){
		var modalInstance = $uibModal.open({
	        templateUrl: 'views/modal/modal_usergroup.html',
	        size:'lg',
	        controller:'userSelectCtrl'
	        });
	};

	$scope.imageExists=function(users){
		docsCtrlVariables.checkForImage(users,users.avatar);
	};

  $scope.userstatus = [
    {value: true, text: 'Active'},
    {value:false, text: 'Disabled'}
  ];
  //To show status,if set show status else not set
	$scope.showStatus = function(user){
    	var selected = [];
    	selected = $filter('filter')($scope.userstatus, {value: user.status});
    	return selected.length ? selected[0].text : 'Not set';
  	};

//select multiple check to delete
	$scope.UserRemoveAll = function(select ,filtered){
		GroupService.UserRemoveAll(select,filtered,$scope.selectUserList);
	};

//select indivisual check to delete
	$scope.addOrRemoveUser=function(users,index,isSelect){
		if(!isSelect)
			$scope.select=false;
		GroupService.addOrRemoveUser(users,index,$scope.selectUserList);
	};
//both multiple and individual delete users from group
	$scope.DeleteUsers=function(){
			var ListOfUsers=GroupService.getList();
			var obj={
					user_id:ListOfUsers,
					group_id:$rootScope.currentGroup._id
				};
			if(ListOfUsers.length>=1){
				Groups.removeUsers(obj,function(){
					angular.forEach(ListOfUsers,function(val, k){
						var ind=$rootScope.users_list.map(function(e) { return e._id; }).indexOf(val);
						$rootScope.users_list.splice(ind,1);
					});
					$scope.select=false;
					notify({ message: 'Users deleted', classes: 'alert-success', templateUrl: $scope.homerTemplate});
					$state.go($state.current, {}, {reload: true});
				});	
				GroupService.deleteList();
			 }else{
				notify({ 
					message: 'No user is selected',
					classes: 'alert-danger',
					templateUrl: $scope.homerTemplate
					});
			}
	};

	$scope.sendTime=function(date){
		var DateFormat = $moment(date).fromNow();
		return DateFormat;
	};

$scope.cancelGroup=function(){
// console.log(data);
$state.go($state.current, {}, {reload: true});
};
	
});
angular
    .module('homer')
    .controller('userSelectCtrl',
    function($scope,$http,$filter,$uibModalInstance,
    	UserService,notify,
    	docsCtrlVariables,GroupService,
    	$moment,$rootScope,Groups){
    		
	$scope.homerTemplate = 'views/notification/notify.html';
	$scope.UserList=[];
	$scope.getuserslist=[];
	GroupService.deleteList();
	
		// to get users
	UserService.getUsers(function(data){
		$scope.tempuser=data;
		$scope.getuserslist=angular.copy($scope.tempuser);
		 angular.forEach($scope.tempuser,function(value,key){
		 	angular.forEach($rootScope.users_list,function(val,k){
		 		if(String(value._id)==String(val._id)){
		 		 	var ind=$scope.getuserslist.map(function(e) { return e._id; }).indexOf(value._id)
		 		 		$scope.getuserslist.splice(ind,1);
		 		 	}
		 		 });
		 })
	});	
	
	// if image exists
	$scope.imageExists=function(users){
		// console.log(users);
		docsCtrlVariables.checkForImage(users,users.avatar );
	};
	//to make first letter as capital
	$scope.Capital_frst=function(grpname){
		return GroupService.First_UpperCase(grpname);
	};	
	//time in moment.js
    $scope.sendTime=function(date){
		var DateFormat = $moment(date).fromNow();
		return DateFormat;
	};
	//select multiple check to delete
	$scope.All_user = function(select ,filtered){
		// console.log(select ,filtered,$scope.UserList);
		GroupService.UserRemoveAll(select,filtered,$scope.UserList);
	};
	//select indivisual check to delete
	$scope.selectuser=function(users,index,isSelect){
		if(!isSelect)
			$scope.selected=false;
		GroupService.addOrRemoveUser(users,index,$scope.UserList);
	};
	//add users to the group
	$scope.saveuser=function(){
		var groupList=GroupService.getList();
				angular.forEach($scope.getuserslist,function(value){
					angular.forEach(groupList,function(val){
						// console.log(value._id,val);
							if(String(value._id)==val){
								// console.log("users getting pushed",value.name);
								$rootScope.users_list.push(value);
								value.isChecked=false;
							}
					})
				})
				GroupService.deleteList();
		var obj={
				id:$rootScope.currentGroup._id,
				user:groupList
				};
			if(groupList.length>=1){
				Groups.addUser(obj,function(res){
					notify({ message: 'Users added to the group', classes: 'alert-success', templateUrl: $scope.homerTemplate});
				})
				$scope.UserList=[];
				$uibModalInstance.close();
			}else{
				notify({ message: 'Please select an user ', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
			}
	};
	
	$scope.cancel=function(){
		$scope.UserList=[]
		$uibModalInstance.close();
	}
	
	
	
});
angular.module('homer')

  .controller('registerCntrl', function ($scope,$state,ActivitylogService,GlobalService,$http,$rootScope,md5,Auth,User,notify,DefaultDocs,userManagement,docsCtrlVariables) {
     $scope.homerTemplate = 'views/notification/notify.html';
     console.log("inside register page");
     var activity_type = GlobalService.getActivitytype();

Auth.getTenant(function(tenant){
    $scope.tenant=tenant;
  //$rootScope.domainDetails=tenant;
    $rootScope.tenantInfo=$scope.tenant;
    });


$scope.user = {};
  // $scope.servicePassword = "";



  //to verify user 
var verifyUser={
  token:$state.params.id
  };

  // to set password strength
$scope.strength="";

$scope.passStrength = function(){
    return $scope.strength;
  };

 
$scope.testStrength=function(pass){
  // console.log(pass);
  var type;
  $scope.strength = 0;
  if(pass){
    if(pass.length<6){
      $scope.strength=0;
      } if( /[0-9]/.test( pass ) ){
        type = 'success';
        $scope.strength=$scope.strength+1;
        // console.log("numbers");
      } if( /[a-z]/.test( pass ) )
          {
            $scope.strength=$scope.strength+1;
            // console.log("alpha");  
           } if( /[A-Z]/.test( pass ) ){
               $scope.strength=$scope.strength+1;
              // console.log("caps");
             } if( /[^A-Z-0-9]/i.test( pass ) ){
                  $scope.strength=$scope.strength+1;
                  // console.log("chars");
                }
  }
  else{
    $scope.strength = -1;
  // return s;
  }
};


userManagement.verify(verifyUser,function(response){
  console.log(response.email,response,"response");
$scope.username=response.email;

},function(err){
  // console.log("error");
  $state.go('common.error_one');
});



$scope.register=function(){

  if($scope.strength==1 && $scope.password1 !== undefined && $scope.password2 !== undefined){
  
 notify({ message: 'Warning -  Passwords must be strong ', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
$scope.strength="";
$scope.password1 = "";
 $scope.password2 = "";
}else{
  if ($scope.password1 !== undefined && $scope.password2 !== undefined) {
          // console.log("inside condition");
 if ($scope.password1.length != 0 && $scope.password1.length != 0 && $scope.strength>=2) {
if ($scope.password1 == $scope.password2){
$scope.user.password1 = md5.createHash($scope.password1 || '');
 $scope.user.email=$scope.username;
User.changePassword($scope.user,function(data){
  var loginobj={
                  username:$scope.username,
                  password:$scope.user.password1
                };
var userObj = {};
 Auth.login(loginobj,$scope.tenant.clientSecret)
 .then(function(userr){

      $scope.email = "";
      $scope.oldPassword = "";
      $scope.password1 = "";
      $scope.password2 = "";  
      $scope.strength=""; 
 Auth.isLoggedInAsync(function(loggedIn) {
            $scope.loginButton=true;
            var loginLog={};
            loginLog.activitytype=activity_type.LOGIN;
            ActivitylogService.LogActivity(loginLog);//from activity service
          });
      $scope.getCurrentUser = Auth.getCurrentUser;
      $scope.getCurrentUser().$promise.then(function() {
    $rootScope.loggedIn = $scope.getCurrentUser();
    // console.log($rootScope.loggedIn);
    docsCtrlVariables.checkForImage($rootScope.loggedIn,$rootScope.loggedIn.avatar);
    $state.go('dashboard');
  });
notify({ message: 'success - successfully logged in  ', classes: 'alert-success', templateUrl: $scope.homerTemplate});
            var passLog={};
            passLog.activitytype=activity_type.PASSWORD;
            ActivitylogService.LogActivity(passLog);//from activity service
            // console.log(passLog);
 },
 function(err) {
        notify({ message: 'Warning - Please Enter Valid Username ', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
        });

})

}else{
  $scope.errorMessage="";
              notify({ message: 'Warning - Passwords donot match', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
              //alertify.error("New passwords don't match");
              $scope.email = "";
              $scope.oldPassword = "";
              $scope.password1 = "";
              $scope.password2 = "";
              $scope.strength="";
}
 }else{
   $scope.errorMessage=""
              notify({ message: 'Warning - Please enter the password', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
         
          $scope.email = "";
          $scope.oldPassword = "";
          $scope.password1 = "";
          $scope.password2 = "";
          $scope.strength="";
        };
}else{
$scope.errorMessage=""
              notify({ message: 'Warning - Passwords Fields can not be empty', classes: 'alert-danger', templateUrl: $scope.homerTemplate});
         
          $scope.email = "";
          $scope.oldPassword = "";
          $scope.password1 = "";
          $scope.password2 = "";
          $scope.strength="";
}
};
};

$scope.cancel=function(){
  // console.log("cancelled");
  $scope.password1 = "";
  $scope.password2 = "";
};



   });
angular.module('homer')
.controller('basicplanCtrl',function($scope){
	console.log("inside basic plan ctrl");
})
 angular.module('homer')

    .controller('GuestCtrl', function ($scope,$state,$http,$rootScope,docsCtrlVariables,DefaultDocs) {
    	console.log("GuestCtrl",$state.params.id);
    	
        DefaultDocs.getGuestDocs({"token":$state.params.id},function(res){
            $rootScope.documentHeadingId=res.fileobj;
            $rootScope.documentHeading=res.fileobj.documentName;
            $rootScope.isguestuser=true;
            console.log("res",res);
            if($scope.checkIfImage(res.fileobj)){

                $rootScope.isImage=true;
                $rootScope.imageUrl=res.url;
                console.log($rootScope.imageUrl);
            }
            else
            {
               if(((/\.(pdf)$/i).test(res.fileobj.documentName))){
                console.log("pdf");
                $rootScope.viewurl=res.url;
                console.log(res.url)
                }else{
                    console.log("not pdf and images",res.url);
                    $rootScope.viewurl="https://view.officeapps.live.com/op/view.aspx?src="+res.url;
                    console.log($rootScope.viewurl);
                };
            }
                   
            docsCtrlVariables.guestopenFile();

        })

        $scope.checkIfImage=function(document){
            if((/\.(gif|jpg|jpeg|tiff|png|ico)$/i).test(document.documentName))
                return true;
            else 
                return false;
        }

        
    	

    });
 angular.module('homer')

    .controller('GuestInviteCtrl', function ($scope,$uibModalInstance,$state,$http,$rootScope,DefaultDocs,notify,$location) {
    	//console.log("guestInviteCtrl");
        var homerTemplate=  'views/notification/notify.html';
        var host = $location.host();
        $scope.emailList=[];
        $scope.add=function(name){
            $scope.emailList.push(name);
            console.log($scope.emailList);
            $scope.name="";
        }
        $scope.delete=function(index){
            //console.log($scope.emailList,index);
            $scope.emailList.splice(index,1);
           // console.log($scope.emailList,index);
        }
    	$scope.share=function(){
            //console.log("name",name);
            var reqobj={
                "email":$scope.emailList,
                "file_id":$rootScope.guestdoc._id,
                "url":"https://"+host.split('.')[0]+".2docstore.com/#/common/guest",
            };
            if($rootScope.guestVersion){
                reqobj.versionId=$rootScope.guestVersion
            }
            console.log("req obj",reqobj);
            DefaultDocs.inviteGuest(reqobj,function(res){
                console.log(res);
                if(res.type!="error"){
                    notify({ message: 'Success- File has been shared through email', classes: 'alert-success', templateUrl: homerTemplate});
                    $uibModalInstance.dismiss('cancel');
                }
                else{
                    notify({ message: res.message, classes: 'alert-danger', templateUrl: homerTemplate});
                    $uibModalInstance.dismiss('cancel');
                }
            })
        }

        $scope.cancel = function () {
            console.log($uibModalInstance);
            $uibModalInstance.dismiss('cancel');
        };

    });
angular.module('homer')
.controller('HelpCtrl',function($scope){
	console.log("inside help ctrl");
})
/**
 * HOMER - Responsive Admin Theme
 * Copyright 2015 Webapplayers.com
 *
 */

angular
    .module('homer')
    .directive('touchSpin', touchSpin)


/**
 * touchSpin - Directive for Bootstrap TouchSpin
 */
function touchSpin() {
    return {
        restrict: 'A',
        scope: {
            spinOptions: '=',
        },
        link: function (scope, element, attrs) {
            scope.$watch(scope.spinOptions, function(){
                render();
            });
            var render = function () {
                $(element).TouchSpin(scope.spinOptions);
            };
        }
    }
};

angular
    .module('homer')
    .directive('versionForm', versionForm)



function versionForm(){
    return{
          templateUrl:'views/versionForm.html',
          restrict:'E',
          // scope:{
          //   data:'='

          // },
          link:function($scope,$element,$attrs){

          }
  }
};
angular
    .module('homer')
    .directive('autoSaveForm', autoSaveForm)


function autoSaveForm($timeout){
     return {
    require: ['^form'],
    link: function($scope, $element, $attrs, $ctrls) {
      
      var $formCtrl = $ctrls[0];
      var savePromise = null;
      var expression = $attrs.autoSaveForm || 'true';
      
      $scope.$watch(function() {
        
        if($formCtrl.$valid && $formCtrl.$dirty) {
          
          if(savePromise) {
            $timeout.cancel(savePromise);
          }
          
          savePromise = $timeout(function() {
            
            savePromise = null;
            
            // Still valid?
            
            if($formCtrl.$valid) {
              
              if($scope.$eval(expression) !== false) {
                console.log('Form data persisted -- setting prestine flag');
                $formCtrl.$setPristine();  
              }
            
            }
            
          }, 500);
        }
        
      });
    }
  };
};






angular.module('homer')
    .directive('comments', function comments(){
	    return{
		          templateUrl:'views/comments.html',
		          controller:'commentCntrl',
		          restrict:'E',
		          link:function($scope,$element,$attrs){

		          }
		  	  }
		});
function Auth($location, $rootScope, $http, $cookieStore,$q,url,User) {
   
   var currentUser = {};
   var tenantInfo;
    var currentDomain={};
    if($cookieStore.get('token')) {
      currentUser = User.get();
    }

    return {

      /**
       * Authenticate user and save token
       *
       * @param  {Object}   user     - login info
       * @param  {Function} callback - optional
       * @return {Promise}
       */
      login: function(user,secret, callback) {
       //console.log(user,secret,$rootScope.domainsInfo);
        var cb = callback || angular.noop;
        var deferred = $q.defer();
        var host = $location.host();
        console.log("host in login",host.split('.')[0],host);
          $http.post('https://revampapi.2docstore.com/oauth/token', {
          //$http.post('http://localhost:9000/oauth/token', {

          client_id:host.split('.')[0],
          client_secret:secret,
          username: user.username,
          password: user.password,
          grant_type : 'password'
          //redirecturl:  "http://tricons.2docstore.com:3000/#/dashboard"
        }).
        success(function(data) {
          $cookieStore.put('token', data.access_token);
          console.log("got token",data);
          currentUser = User.get(function(res){
            ////console.log(res);
            return cb(data);
          });
          deferred.resolve(data);
          
        }).
        error(function(err) {
          //////console.log(err);
          this.logout();
          deferred.reject(err);
          return cb(err);
        }.bind(this));

        return deferred.promise;
      },

      getTenant : function(cb)
      {
        var host = $location.host();
       console.log("host in login",host,host.split('.')[0]);
        $http.post(url+'api/Domains/gettenant', {

          clientId:host.split('.')[0]

        }).success(function(tenant){
          tenantInfo=tenant;
          cb(tenantInfo);
        })
      },

      getTenantSync: function(cb)
      {
        cb(tenantInfo);
      },

      /**
       * Delete access token and user info
       *
       * @param  {Function}
       */
      logout: function() {
        $cookieStore.remove('token');
        //$rootScope.defaultdoc=undefined;
       // console.log($cookieStore.get('token'));
        currentUser = {};
      },

      /**
       * Create a new user
       *
       * @param  {Object}   user     - user info
       * @param  {Function} callback - optional
       * @return {Promise}
       */
      createUser: function(user, callback) {
        var cb = callback || angular.noop;
        return User.save(user,
          function(data) {
            $cookieStore.put('token', data.token);
            currentUser = User.get();
            return cb(user);
          },
          function(err) {
            this.logout();
            return cb(err);
          }.bind(this)).$promise;
      },

     
      /**
       * Change password
       *
       * @param  {String}   oldPassword
       * @param  {String}   newPassword
       * @param  {Function} callback    - optional
       * @return {Promise}
       */
      changePassword: function(oldPassword, newPassword, callback) {
        var cb = callback || angular.noop;

        return User.changePassword({ id: currentUser._id }, {
          oldPassword: oldPassword,
          newPassword: newPassword
        }, function(user) {
          return cb(user);
        }, function(err) {
          return cb(err);
        }).$promise;
      },

      /**
       * Gets all available info on authenticated user
       *
       * @return {Object} user
       */
      getCurrentUser: function() {
      //  currentUser.$promise.then(function() {
          return currentUser;
//})        
      },

      /**
       * Check if a user is logged in
       *
       * @return {Boolean}
       */
      isLoggedIn: function() {

        return currentUser.hasOwnProperty('role');
      },

      /**
       * Waits for currentUser to resolve before checking if user is logged in
       */
      isLoggedInAsync: function(cb) {
       //  ////console.log("sdf");
          if(currentUser.hasOwnProperty('$promise')) {
          currentUser.$promise.then(function() {
           // ////console.log("currentUser");
            cb(true);
          }).catch(function() {
            cb(false);
          });
        } else if(currentUser.hasOwnProperty('role')) {
          cb(true);
        } else {
          cb(false);
        }
      },

      /**
       * Check if a user is an admin
       * @TODO Add comments on when it will throw error. 
       * @return {Boolean}
       */
      isAdmin: function(callback) {
        currentUser.$promise.then(function(data) {
          if(data && data.role){
            ////console.log("data",data.role);
            callback(data.role === 'admin');
          }    
        });
      },
      /* 
       checking if the user is ClientAdmin *** dev:(mamatha)
       * */
		isClientAdmin: function(callback) {
        currentUser.$promise.then(function(data) {
          if(data && data.role){
            ////console.log("data",data.role);
            callback(data.role === 'Client Admin');
          }    
        });
      },
      /**
       * Get auth token
       */
      getToken: function() {
        return $cookieStore.get('token');
      },
      checkDomain:function(callback){
        console.log("check domain function");
        var host = $location.host();
        console.log("host",host);
        currentDomain = User.checkDomain({"domain":host.split('.')[0]},function(res){
          console.log(res);
          $rootScope.domainsInfo=res; 
          console.log("domain info in service",$rootScope.domainsInfo);        
          // return res;
          });

         if(currentDomain.hasOwnProperty('$promise')) {
          currentDomain.$promise.then(function() {
            console.log("currentDomain",currentDomain);
            callback(currentDomain.status);
          });
        }else {
          callback(false);
        }
       console.log("currentDomain",currentDomain);
        $rootScope.domainsNames=currentDomain;
          console.log($rootScope.domainsNames);
      }
    };
};

/**
 * Pass function into module
 */
angular
    .module('homer')
    .factory('Auth', Auth)
function note($resource,$rootScope,url) {
	//var restcalls=GlobalService.getRestVar();
	  return $resource(url+'api/notes/:id', {id: '@_id'},
    {
      query: {
      	//isArray:true,
        method:'POST'      
      }

    })
}
angular
    .module('homer')
    .factory('Note', note)
function User($resource,$rootScope,url,GlobalService) {
  var restcalls=GlobalService.getRestVar();
    return $resource(url+'api/users/:id/:count/:limit', {id: '@_id'},
    {
      changePassword: {
        //isArray:true,
        method:restcalls.post,
        params: {
          id:'saveUser'
        }
      },
      get: {
        method: restcalls.get,
        params: {
          id:'me'
        }
      },
      show: {
        method: restcalls.post,
        params:{
          id:'getuser'
        }
  },
      checkDomain:{
        method:restcalls.post,
        params:{
          id:'domain'
        }
      },
      logout:{
        method:restcalls.post,
        params:{
          id:'logout'
        }
      },
      loginusers:{
        method:restcalls.post,
        isArray:true,
        params:{
          id:'loginusers'
        }
      },          
       update: {
          method: restcalls.post,
          params:{
            id:'modify'
          }
        },
       query:{
        method: restcalls.get,
        isArray: true,
        params:{
          count:'@count',
          limit:'@limit'
        }
       },
       save:{
        method: restcalls.post,
        params:{
          id:'createIamuser'
        }
       },
       verify:{
        method:restcalls.post,
        params: {
          id:'validatetoken'
        }
       },
      reverify:{
        method:restcalls.post,
        params: {
          id:'reverification'
        }
       },
       forgotPassword:{
        method:restcalls.post,
        params:{
          id:'forgotPassword'
        }
       },
       setPassword:{
        method:restcalls.post,
        params:{
          id:'setPassword'
        }
       },
       getLoggedInUsers:{
        method:restcalls.get,
        isArray: true,
        params:{
          id:'getLoggedInUsers'
        }
       }       
    });
}

angular
    .module('homer')
    .factory('User', User)
function ErrorFcrt($resource,$rootScope,url,GlobalService) {
	var restcalls=GlobalService.getRestVar();
	  return $resource(url+'api/errors/:id', {id: '@_id'},
    {
     query:{
        method: restcalls.post
       // isArray: true
       }
    });
}

angular
    .module('homer')
    .factory('ErrorFcrt', ErrorFcrt);

    
function activityFcrt($resource, $rootScope,url,GlobalService) {
	var restcalls=GlobalService.getRestVar();
	return $resource(url + 'api/activitylogs/:id', {id : '@_id'}, 
	{
		save : {
			method : restcalls.post
		},
		query : {
			isArray:true,
			method : restcalls.put,
			ignoreLoadingBar: true
		},
		FolderActivity :{
			isArray:true,
			method : restcalls.post,
			params:{
	   	 		id:'toGetFolderActivityLog'
	   	 	}
		}
	});
}

angular
    .module('homer')
    .factory('activityFcrt', activityFcrt)
 function authInterceptor ($rootScope, $q, $cookieStore, $location) {
    return {
      // Add authorization token to headers
      request: function (config) {
        // console.log("request",$cookieStore.get('token'));
        config.headers = config.headers || {};
        if ($cookieStore.get('token')) {
          config.headers.Authorization = 'Bearer ' + $cookieStore.get('token');
        }
        return config;
      },

      // Intercept 401s and redirect you to login
      responseError: function(response) {
        if(response.status === 401) {
          $location.path('/');
          // remove any stale tokens
          $cookieStore.remove('token');
          return $q.reject(response);
        }
        else {
          return $q.reject(response);
        }
      }
    };
  
  }
 angular
    .module('homer')
    .factory('authInterceptor', authInterceptor)
function DefaultDocs($resource,$rootScope,url,GlobalService ) {
  	var restcalls=GlobalService.getRestVar();
     //AngularJS will instantiate a singleton by calling "new" on this function
  		return $resource(url+'api/Documents/:id/:skip/:limit',  { id:  '@_id' },
    {
	   	 query: {
	   	 	isArray:true,
	        method: restcalls.get
	   	},
	   	 show:{
	   	 	isArray:true,
	   	 	method:restcalls.get
	   	},
	   	getGuestDocs:{
	   	 	//isArray:true,
	   	 	method:restcalls.post,
	   	 	params:{
	   	 		id:'getLink'
	   	 	}
	   	},
	   	 save:{
	   	 	//isArray:true,
	   	 	method:restcalls.put
	   	},
	   	  download:{
	   	 	method:restcalls.post,
	   	 	params:{
	   	 		id:'downloads'
	   	 	}
	   	},
	   	 viewFile:{
	   	 	method:restcalls.post,
	   	 	params:{
	   	 		id:'viewFile'
	   	 	}
	   	},
	   	 delete:{
	   	 	method:restcalls.delete
	   	},
	   	viewversionthumbnail:{
	   	 	method:restcalls.post,
	   	 	params:{
	   	 		id:'thumbnailurl'
	   	 	}
	   	},	   	
	   	 getSize:{
	   	 	method:restcalls.post,
	   	 	params:{
	   	 		id:'getSize'
	   	 	}
	   	},
	   	 listUserDocs:{
	   	 	isArray:true,
	   	 	method:restcalls.post,
	   	 	params:{
	   	 		id:'listUserDocs'
	   	 	}
	   	},
	   	 modify:{
	   	 	method:restcalls.post,
	   	 	params:{
	   	 		id:'modify'
	   	 	}
	   	},	   	
	   	put_policy:{
	   		method: restcalls.put,
	   		params:{
	   			id:'putpolicy'
	   		}
	   	},	   		   	
	   	favroite:{
	   		method: restcalls.post,
	   		params:{
	   			id:'favroite'
	   		}
	   	},
	   	locked:{
	   		method: restcalls.post,
	   		params:{
	   			id:'lock'
	   		}
	   	},
	   	listFavs:{
	   		isArray:true,
	   		method:restcalls.post,
	   		params:{
	   			id:'listFavs'
	   		}
	   	},
	   	getshareddocs:{
	   		isArray:true,
	   		method:restcalls.post, 
	   		params:{
	   			id:'getshareddocs'
	   		}
	   	},
	   	getUsedSpace:{
	   		method:'POST',
	   		params:{
	   			id:'toGetClientDocsize'
	   		},
	   		ignoreLoadingBar: true
	   	},
	   	sharedCount:{
	   		method:'POST',
	   		params:{
	   			id:'sharedCount'
	   		},
	   		ignoreLoadingBar: true
	   	},
	   	favouriteCount:{
	   		method:'POST',
	   		params:{
	   			id:'favouriteCount'
	   		},
	   		ignoreLoadingBar: true
	   	},
	   	delete_policy:{
	   		method:'POST',
	   		params:{
	   			id:'deleteinlinepolicy'
	   		}
	   	},
	   	changeversion:{
	   		method:'POST',
	   		params:{
	   			id:'changeversion'
	   		}
	   	},
	   	permanentDel:{

               method:'POST',
               params:{
                   id:'delete'
               }
         },
        restore:{
	   		method:restcalls.post,
	   		params:{
	   			id:'restore'
	   		}
	   	},
	   	addComment:{
	   		method:restcalls.post,
	   		//isArray:true,
	   		params:{
	   			id:'addComment'
	   		}
	   	},
	   	deleteComment:{
	   		method:restcalls.post,
	   		params:{
	   			id:'deletecomment'
	   		}
	   	},
	   	inviteGuest:{
	   		method:restcalls.post,
	   		params:{
	   			id:'createSharedLink'
	   		}
	   	}

	});

}
angular
.module('homer')
.factory('DefaultDocs',DefaultDocs)
function DocsFcrt(DefaultDocs,DocstoreService,GlobalService){
	var restcalls=GlobalService.getRestVar();
	var messagesList = {
		instructionMessage : "Documents related to clients/projects are organized below. For easy identification, associated logo has been displayed. To change or add logo, use option to upload logo or drag and drop logo at the cards below.",
		noClientMessage : "There are no clients/projects folder. <a href='client_management'>Click</a> here to add."
	};


	return {
		//Return Client instruction message
		instructionContent : function() {
			return messagesList.instructionMessage;
		},
		//Return No client or project message
		NoClientMessage : function() {
			return messagesList.noClientMessage;
		},
		checkLength : function(clients) {
			if (clients.length == 0)
				return true;
			else
				return false;
		},
		getAllsize : function(client) {
			//console.log(client,"to get size");
			DefaultDocs.getUsedSpace({
				"_id" : client.documentobj._id
			}, function(size) {
				//console.log("size",size,size.filesize);
				var used = size.filesize;
				var alloted =  client.allotedspace;
				var percentage = DocstoreService.findPercentage(used,alloted);
				// console.log(alloted  - used,used);
				//console.log(percentage,"percentage");
				if(percentage < 1){
					client.percentage = 0;	
				}
				else{
					client.percentage = percentage;
				}				
				client.usedSpace = used;
				client.alloted = alloted ;
				client.remaining = alloted  - used;

			});
			return client;	
		},		
		setProgressElements:function(){
			var progressElement={};
			progressElement.min=0;
			progressElement.low=50;
			progressElement.high=80;
			progressElement.max=100;
			progressElement.optimum=40;
			return progressElement;
		},
		sendWwarning:function(client){
			var warningLimit=85;
			if(client.percentage>=warningLimit)
			return true;
			else
			return false;
		},
		//check if you want to display on Iframe or colorbox
		checkFileType:function(fileName) {
			if ((/\.(gif|jpg|jpeg|tiff|png|ico)$/i).test(fileName)) {
			////console.log("this is a image");
			return false;
			} else {
				////console.log("this is not an image");
				return true;
			}
		}

	};
}

angular
    .module('homer')
    .factory('DocsFcrt', DocsFcrt)
function userManagement($resource,$rootScope,url,GlobalService){
	var restcalls=GlobalService.getRestVar();
	return $resource(url+'api/users/:id',  { id:  '@_id' },
		{
       update: {

          method: restcalls.post,
          params:{
            id:'modify'
          }
        },

       query:{
        method: restcalls.get,
        isArray: true
       },
       save:{
        method: restcalls.post,
        params:{
          id:'createIamuser'
        }
       },
       verify:{
        method:restcalls.post,
        params: {
          id:'validatetoken'
        }
       },
      reverify:{
        method:restcalls.post,
        params: {
          id:'reverification'
        }
       },
       forgotPassword:{
        method:restcalls.post,
        params:{
          id:'forgotPassword'
        }
       },
       setPassword:{
        method:restcalls.post,
        params:{
          id:'setPassword'
        }
       }
  });
}

angular
    .module('homer')
    .factory('userManagement', userManagement)
function clientFactory($resource,$rootScope,url,GlobalService){
		var restcalls=GlobalService.getRestVar();
	return $resource(url+'api/clients/:id/:count/:limit',  { id:  '@_id' },
    {	   	
	   	 query:{
	   	 	method: restcalls.get,
	   	 	isArray:true,
	   	 	params:{
	   	 		count:'@count',
	   	 		limit:'@limit'
	   	 	}
	   	 },
	   	 show:{
	   	 	isArray:true,
	   	 	method:restcalls.get
	   	 },
	   	 save:{
	   	 	method:restcalls.post
	   	 },
	   	 update:{
	   	 	method:restcalls.put
	   	 },
	   	 deleteClient:{
	   	 	method:restcalls.post,
	   	 	params:{
	   	 		id:'deleteClient'
	   	 	}
	   	 }
	   	
	});
}
angular
    .module('homer')
    .factory('clientFactory', clientFactory)
function ShareFctry($resource,$rootScope,url,GlobalService ){
 	var restcalls=GlobalService.getRestVar();
 	return $resource(url + 'api/sharedobjects/:id', {
		id : '@_id'
	}, {
		save : {
			method : restcalls.post
		},
		get:{
			isArray:true,
			method:restcalls.post,
			params:{
				id:'getshareobj'
			}
		},
		delete_policy:{
	   		method:restcalls.post,
	   		params:{
	   			id:'delete'
	   		}
	   	}
	   });
	}
	angular
    .module('homer')
    .factory('ShareFctry', ShareFctry)
function policyFactory($resource,$rootScope,url,GlobalService) {
var restcalls=GlobalService.getRestVar();
     //AngularJS will instantiate a singleton by calling "new" on this function
      return $resource(url+'api/policies/:id',  { id:  '@_id' },
    {
      
       query:{
        method: restcalls.get,
        isArray: true
       }
      
  })
}
angular
    .module('homer')
    .factory('policyFactory', policyFactory);
function TenantFactory($resource,$rootScope,url,GlobalService){
		var restcalls=GlobalService.getRestVar();
//AngularJS will instantiate a singleton by calling "new" on this function
	return $resource(url+'api/Domains/:id',  { id:  '@_id' },
    {	   	
	   	query:{
        method: restcalls.post,
        params:{
          id:'gettenant'
        }
       },
       remainingSize:{
        method:restcalls.post,
        params:{
          id:'remainingSize'
        }
       },
       createApplication:{
        method:restcalls.post,
        params:{
          id:'createApplication'
        }
       },
       updateapplication:{
        method:restcalls.post,
        params:{
          id:'updateapplication'
        }
       },
       applicationList:{
        isArray:true,
        method:restcalls.post,
        params:{
          id:'applicationList'
        }
       }

	});
}
angular
    .module('homer')
    .factory('TenantFactory', TenantFactory)

   
      
/**
 * 13th Feb 2017
 * SubscriptionFactory
 * Chaithra
 * modified date : 
 */
function SubscriptionFactory($resource,url,GlobalService){
	var restcalls=GlobalService.getRestVar();
	return $resource(url+'api/tenants/:id',  { id:  '@_id' },
	{
		query:
		{
			method:restcalls.post
		}
	})
}
angular
    .module('homer')
    .factory('SubscriptionFactory', SubscriptionFactory)
/**
 * 13th Feb 2017
 * registrationFctry
 * Chaithra
 * modified date : 
 */
function registrationFctry($resource,url){
	return $resource(url+'api/tenantReg/:id',  { id:  '@_id' },
	{
		query:
		{
			method:'POST'
		}
	})
}
angular
    .module('homer')
    .factory('registrationFctry', registrationFctry)
/**
 * 13th Feb 2017
 * PlansFactory
 * Chaithra
 * modified date : 
 */
 function plansFctry($resource,url,GlobalService){
 	var restcalls=GlobalService.getRestVar();
 	return  $resource(url+'api/plans/:id',  { id:  '@_id' },
 	{
 		query:{
 			method:restcalls.get,
 			isArray:true,
 			params:{
	          id:'getList'
	        }
 		}
 	})
 }

 angular
 .module('homer')
 .factory('plansFctry',plansFctry)

function globalSearchFcrt ($resource, $rootScope, url) {
	return $resource(url + 'api/globalsearchs/:id', {
		id : '@_id'
	}, {
		search : {
			method : 'POST',
			isArray : true
		}
	});
}
angular
    .module('homer')
    .factory('globalSearchFcrt', globalSearchFcrt)
'use strict';

angular.module('homer')
  .service('GlobalService', function (Upload,url) {
 	   
   
 	var space = {MIN:'Value cant be less than 5',INT:'Please enter integers only',MAX:'Value can not be more than 1000 GB'}; 
	var no_data = "No Data to display";
    var footer_text = "<span>Copyright <i class='fa fa-copyright'></i> 2016 2Docstore All rights reserved.</span>";
    var restCalls = { get:'GET', put:'PUT', post:'POST', delete:'DELETE' };
    var note = {NOTE_PW:'Password should contain Alphanumeric with atleast one UPPERCASE,one lowercase and special character(ex:Trinetra@1)',
			    USER:'Minimum 3 alphanumeric characters.',
			    NOTE_ORG:'User will be mapped to the default organization listed.',
			    NOTE_EMAIL:'Enter valid email ID for user authentication.',
			    REG_USER:'Administrator will have full access whereas User will be having limited access.',
			    CLIENTADMIN:'Client Admin has limited access,belongs to the organisation',
			    CLIENT:'Minimum 3 alphanumeric characters allowed for client name.',
			    MOBILE:'Enter 10 digit mobile number.',
			    C_EMAIL:'Enter valid email ID for client authentication.',
			    FOLDER:'Enter meaningful folder name for creating folder in client documents.',
			    SUB_UNTIL:"Chose the Subscription end date.",
			    REMINDER_NOTE:'Enter meaningful text to be remind.',
			    WHEN:'Chose the start date for the Timeline.',
			    REPEAT:'Please select how often you want be remind.',
                NEVER:'There is no end date for never.',
                UNTIL:'Chose the end date for the Timeline.',
                message: 'Enter the Timeline text / details here'};
                
    // var regex = {PHONE:'/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/'};
			    
    var error_type ={PW_ERR_PTRN:'Must contain one lower case,uppercase letter and one special character.',
		   			 PW_MIN_LNG:'Password must be between 6 and 20 characters.',PW_NOMATCH:'Passwords do not match.',
		   			 UN_SHRT:'Name should contains more than 3 characters',UN_LNG:'Name should contains less than 20 characters',
		   			 MOBILE:'Must be a valid 10 digit Contact number',VALID_NUM:'Enter a valid phone number',
		   			 VALID_NAME:'Please enter valid name' ,VALID_EMAIL:'Enter a valid email.',VALID_DATE:'Enter a valid date.',
		   			 REQ:'This field is required.',E_DATE:'End Date should be greater than start date',S_DATE:'Start Date should be greater than current date.'};

    var activity_type = {LOGIN:'Logged-In',LOGOUT:'Logged-Out',CREATE:'Created',
        SHARE:'Shared',USERADD:'Added user',USERUPDATE:'Updated user',
   		UPLOAD:'Uploaded',DOWNLOAD:'Downloaded',DELETE:'Deleted',MUL_DELETE:'Deleted',
        RESTORE:'Restored',TRASH:'Trashed',VIEW:'Viewed',CHANGEVERSION:'Changed version',
   		ADD:'Added',CLIENTADD:'Added client',CLIENTUPDATE:'Updated client',IS:' is ',
        HAS:' has ',FROM:' from ',IN:' in ',TO:' to ',
  		REMIND:'set Timeline',LOCK:'locked',UNLOCK:'unlocked',FAV:'favorited',
        UNFAV:'removed favourite',UPDATE:'updated',PASSWORD:'Changed Password',
        PROFILE:'Uploaded Profile_Pic',PROFILEUPDATE:'Updated Profile',
        GROUPADD:'Created group',GROUPUPDATE:'Updated group',THIS:' this ',
        DESCRIPTION :' updated description',COMMENT : 'Added Comment'
        ,GROUPDELETE:'Deleted group',CLIENTDELETE:'Deleted client'};
	var action ={DOC1:'My Documents',DOC2:'Client Documents',ACTIVE:'Active',
        DISABLE:'Disable',ADMIN:'Admin',
		CLIENT_ADMIN:'Client Admin',NOT_SET:'Not set'};
                
    this.getText = function(){    
        return footer_text;
    };

    this.get_error = function(){    
        return error_type;
    };
    
    this.getNote = function(){    
        return note;
    };

    this.getspace = function(){    
        return space;
    };

    this.getRestVar=function(){
    	return restCalls;
    };
       
    this.getActivitytype = function(){
        return activity_type;
 	 };
 	 
 	this.getNodata = function(){   
        return no_data;
    };
    
    this.getRegex = function(){    
        return regex;
    };
    this.getAction = function(){    
        return action;
    };
   

});

  

'use strict';

angular.module('homer')
.service('docsCtrlVariables', function ($http,$rootScope,Auth,activityFcrt,DefaultDocs,notify,$uibModal,uploadService,GlobalService,ShareFctry,ActivitylogService,$moment,globalSearchFcrt,versionService,socket,blockUI,Groups) {


 var activity_type = GlobalService.getActivitytype();
 var homerTemplate=  'views/notification/notify.html';
 var documentInfo=[];
 var currentdocument={};
 var breadcrumbs=[];
 var access_list={};
 var whichDocument;
 var clicks="";
 var propertyDoc={};
 var skip=0;
 var limit=10;

 this.resetCount=function(){
  this.skip=0;
 }
 this.setSkipValues=function(value){
  skip=(value * limit)-limit ;
 }
 this.getSkipValue=function(){
  return skip;
 }
 this.setpropertyDoc=function(doc){
   propertyDoc=doc;
 }

 this.getpropertyDoc=function(){
  return propertyDoc;
}

this.initWhichDocument=function(){
  whichDocument="";
}


this.spliceDoc=function(docid){
  var ind=documentInfo.map(function(e) { return e._id; }).indexOf(docid);
  documentInfo[ind].trash=true;
  documentInfo[ind].check=false;
      ////console.log(documentInfo);
    }

    this.restoreDoc=function(docid){
      var ind=documentInfo.map(function(e) { return e._id; }).indexOf(docid);
      documentInfo[ind].trash=false;
      documentInfo[ind].check=false;
    }    

    this.trashDoc=function(docid){
      var ind=documentInfo.map(function(e) { return e._id; }).indexOf(docid);
      documentInfo.splice(ind,1);
    }


    this.checkForImage=function(user,image,cb){
      // console.log(user,image);
      cb= cb || angular.noop;
      if(user){
        if(user.name){
          var res1 = user.name.slice(0, 1);
      var res = res1.toUpperCase();
      user.capital=res;
      /*
      image load
      */
      var img = new Image ;
      img.onload = function(){
        user.avatarflag = true;
        cb();
      //$scope.$apply();
    }; 
    img.onerror = function() {
     user.avatarflag=false;
     cb();
     // $scope.$apply();
   };
   img.src = image;
        }  
 }
}

this.setWhichDocument=function(document,click){
  skip=0;
  whichDocument=document;
  clicks=click;
};

this.addToDocumentInfo=function(document){
  console.log("start of addToDocumentInfo",document,documentInfo);
  var ind = documentInfo.map(function(e) { return e._id; }).indexOf(document._id);
  console.log("start",documentInfo);
  console.log(ind);
  if(ind>-1){
console.log("before splice",documentInfo);
    documentInfo.splice(ind,1);
    console.log("after splice",documentInfo);
    documentInfo.push(document);
        console.log("after push addToDocumentInfo",documentInfo);
  }
  else{
    console.log("after addToDocumentInfo",documentInfo);
    documentInfo.push(document);
  }
  
};

    this.addDocumentSocket=function(document,cb){
      console.log(currentdocument.s3_path,document.parent);
      if(currentdocument.s3_path==document.parent){
        checkForPermission(document,function(){
          var ind=documentInfo.map(function(e) { return e.s3_path; }).indexOf(document.s3_path);
         // console.log(ind);
          if(ind>-1){
            //documentInfo[ind]=document;
            documentInfo.splice(ind,1);
            documentInfo.push(document);
            console.log("update",ind);
            cb("update");
          }
          else{
            documentInfo.push(document);
            //console.log(documentInfo);
            cb("create");
          }
                
        })
      }      
    };

    this.delDocumentSocket=function(document,cb){
      if(currentdocument.s3_path==document.parent){
        var ind=documentInfo.map(function(e) { return e.s3_path; }).indexOf(document.s3_path);
        documentInfo.splice(ind,1);
        cb();
      }  
    };

    this.getWhichDocument=function(){
      return whichDocument;
    };

this.getbreadcrumbs=function(){
  return breadcrumbs;

};
this.addAccessList=function(value){
  //    currentdocument.access_list.push(value);
};
this.newbreadcrumbs=function(){
  breadcrumbs=[];
};
this.getAccessList=function(){
  access_list=currentdocument.access_list;
  return access_list;
}
this.getDocumentInfo=function(){
  return documentInfo;

}
this.setDocumentInfo=function(document){
  documentInfo=document;
}
this.updateDocumentInfo=function(){
  angular.forEach(documentInfo,function(doc,key){
    doc.check=false;
  })
}
this.setCurrentDocument=function(document){
// console.log(document);
  currentdocument=document;
};
  	/*
	get the current document
 */
 this.getCurrentDocument=function(document){
  // console.log(document)
  return currentdocument;
}
  	/*
		query to get the documents(MY docs or client)

   */
   this.getDocs = function(docName,cb){
     // var start = $moment().get('millisecond');
     blockUI.start()
     Auth.isAdmin(function(isadmin) {
      var role=isadmin;
      if(clicks !="clicked"){
        if(role)
        {
          docName="My Documents";
        } 
        else
        {
          docName="shared";
        }
      }

      if(docName=="shared"){
        documentInfo=[];
        var getshared = DefaultDocs.getshareddocs(function(shared) {
         documentInfo  = shared;
         angular.forEach(documentInfo,function(doc,key){

           checkForPermission(doc,function(){
                        //documentInfo.push(res);
                      })
         })
          // var end = $moment().get('millisecond');
          blockUI.stop();
          cb(docName);
        });


      }
      else if(docName=="fav"){
        documentInfo=[];
        var getfav = DefaultDocs.listFavs(function(favs) {
                var end = $moment().get('millisecond');
           documentInfo = favs;  
           blockUI.stop();
              /* angular.forEach(documentInfo,function(doc,key){
                 checkForPermission(doc,function(){
                        //documentInfo.push(res);
                      })
                    })*/


                  });
      }
      else if(docName=="search"){
        documentInfo=[];
        var getsearch = globalSearchFcrt.search({
          val : $rootScope.globalSearch
        }, function(res) {

            //var end = $moment().get('millisecond');

            documentInfo = res;
            blockUI.stop();
         /*        angular.forEach(documentInfo,function(doc,key){
                 checkForPermission(doc,function(){
                        //documentInfo.push(res);
                      })
                    })*/
                  })
      }
      else if(docName=="ProjectView"){
        documentInfo=[];
        breadcrumbs=[];
        currentdocument={};
        DefaultDocs.query(function(data) {

          var defaultdocs = data;
          angular.forEach(defaultdocs,function(documents,key){
            if(documents.documentName=="Client Documents"){
              console.log("Client Documents",documents);
              breadcrumbs.push(documents);
                  //currentdocument=documents;

                  DefaultDocs.show({
                    id : documents._id
                  }, function(response) {
                    //$scope.documentList=response;
                    DefaultDocs.show({
                      id : $rootScope.projectViewDoc._id
                    }, function(resp) {
                      breadcrumbs.push($rootScope.projectViewDoc);
                    //$scope.documentList=response;
                    currentdocument=$rootScope.projectViewDoc;
                    documentInfo=resp;
                    blockUI.stop();
                           //    var end = $moment().get('millisecond');

             /* angular.forEach(documentInfo,function(doc,key){
                 checkForPermission(doc,function(){

                       //console.log(doc);
                        //documentInfo.push(res);
                      })
            })
            */
            cb();
          });
                  });

                }
              })
        })
      }

      else{
        blockUI.message('Opening My Documents..'); 
        documentInfo=[];
        breadcrumbs=[];
        currentdocument={};
        var response1 = DefaultDocs.query(function(data) {

          var defaultdocs = data;
          angular.forEach(defaultdocs,function(documents,key){
            if(documents.documentName==docName){


              /*set Breadcrumbs here*/
              //$rootScope.breadcrumbs.push($scope.defaultdocs[i]);
              breadcrumbs.push(documents);
              //$scope.currentDocument=$scope.defaultdocs[i];
              currentdocument=documents;

              var response2=DefaultDocs.show({
                id : documents._id,
                skip:skip,
                limit:limit
              }, function(response) {

                   // $scope.documentList=response;
                   documentInfo=response;
                   blockUI.stop();
            //          angular.forEach(documentInfo,function(doc,key){
               /*  checkForPermission(doc,function(){
                     
                        //documentInfo.push(res);
                      })*/
            // })
                    // socket.syncUpdates('Document', documentInfo,function(event,item,array){

                    // });
                    var end = $moment().get('millisecond');
                    // //console.log("Time taken :",end-start);
                    //socket.syncUpdates('Document', documentInfo);
                  });
            }
          })

        });

      }
    });

}
  	 /*
  	 for length of documentInfo
      */
    this.giveLengthtrash=function(){
      var count =0
      angular.forEach(documentInfo, function(value, key){
        if(value.trash!=true){
          count++
        }
      });
      var lengthDocs=documentInfo.length;
      return count;
    }
         /*
     for length of documentInfo
       */
     this.giveLength=function(){
      var lengthDocs=documentInfo.length;
      return lengthDocs;
    }
  	/*
  	folder click

  	*/
  	this.folderClick=function(document){
      var start = $moment().get('millisecond');
      // //console.log(document,"image in service");

      //
      checkForPermission(document,function(){
                //  //console.log(doc)
                       // //console.log(res);
                        //documentInfo.push(res);
                      })
      blockUI.start();
      blockUI.message('Opening '+ ' ' + document.documentName.slice(0, 10) + '..');
      DefaultDocs.show({
        id : document._id,
        skip:skip,
        limit:limit
      }, function(response) {

       blockUI.stop();
       while(documentInfo.length > 0){
        documentInfo.pop();
      }
      breadcrumbs.push(document);				
      currentdocument = document;
      documentInfo = response;
      angular.forEach(documentInfo,function(doc,key){
       checkForPermission(doc,function(){
                //  //console.log(doc)
                       // //console.log(res);
                        //documentInfo.push(res);
                      })

     })

          //  //console.log(documentInfo);
          var end = $moment().get('millisecond');


        }, function(err) {
            console.log(err)
            blockUI.stop();
            notify({ message: 'Error - Folder locked', classes: 'alert-danger', templateUrl: homerTemplate});
          });

    }
    this.openFile=function(){
      blockUI.stop();
              //open modal
      //        $rootScope.allowMoadlOpen=false;
      var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modal_comment.html',
        controller: ModalInstanceCtrl,
        size:'lg'
      })
    }

    this.guestopenFile=function(){
      blockUI.stop();
              //open modal
      //        $rootScope.allowMoadlOpen=false;
      var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/modal_comment.html',
        controller: ModalInstanceCtrl,
         backdrop  : 'static',
   keyboard  : false,
        size:'lg'
      })
    }
    

    this.uploadFiles=function(files,showNoFiles,versionuploading){
          
      angular.forEach(files, function(file) {
        uploadService.file_upload(file, currentdocument, function(response) {
        //documentInfo.push(response);

        checkForPermission(response,function(){
          var ind=documentInfo.map(function(e) { return e._id; }).indexOf(response._id);

          if(ind>-1){
            documentInfo[ind]=response; 
            var getdocs = DefaultDocs.show({
              id : currentdocument._id
            }, function(res) {
              console.log(res);
              documentInfo=res;
            })
          }
          else{
            //documentInfo.push(response);
           // versionService.getThumbnail(response);
         }
       })


        // if (response.status == 500) {
        //  files = [];
        // /*error here*/
        // } else {
        //   //$scope.documentList= response;
        //   documentInfo=response;
        //   //$scope.mylength = $scope.isEmptyData(response.length);
        //   file.progress = 100;
        //   versionuploading=false;
        //   showNoFiles=false;
        // }
      });
      });
    };


// to cancel upload
// this.cancel=function(file){
// uploadService.cancel_upload();
// };
    /*
    to set bread and views
    */
    this.breadClick=function(document){
      var indexval = breadcrumbs.indexOf(document);
      breadcrumbs.splice(indexval + 1);
      documentInfo=[];
      DefaultDocs.show({
        id : document._id
      }, function(response){
        documentInfo = response;

        currentdocument = document;
        blockUI.stop();
    /*    angular.forEach(documentInfo,function(doc,key){
         checkForPermission(doc,function(){
                       //console.log(doc);
                        //documentInfo.push(res);
                      })
       })*/
      });
    };

  /*
  Adding a new folder
  */
  this.addfolder=function(newFoldername,showAddRow,disableAddFolder){

    var flagsObj={};
    var regex = /^[\w. ]+$/;
    var notExists = this.docDuplicates(newFoldername);
    if (newFoldername != undefined && regex.test(newFoldername) && newFoldername.length <= 25) {
      if (notExists) {
       blockUI.stop();

      //  notify({ message: 'Success - Please wait while the Folder is being created', classes: 'alert-success', templateUrl: homerTemplate});
          /* flagsObj.showAddRow=true;
             flagsObj.disableAddFolder=true;
             newFoldername=newFoldername;*/
console.log(newFoldername);
             currentdocument.name = newFoldername;
             blockUI.start();
             blockUI.message('Creating Folder...');
             DefaultDocs.save({
              "id" : currentdocument._id,
              "s3_path" : currentdocument.s3_path,
              "parent" : currentdocument.parent,
              "documentName" : currentdocument.documentName,
              "name" : newFoldername
            }, function(res) {
      console.log(res);
          var addfolderlog = {};
          var id = res._id;
          var addfolderlog = {};
          var id = res._id;
          addfolderlog.activitytype = activity_type.CREATE;
          addfolderlog.what = id;
          addfolderlog.location = currentdocument.parent + '/' + currentdocument.documentName;
          addfolderlog.status = true;
          ActivitylogService.LogActivity(addfolderlog);
          //console.log(documentInfo);
          // checkForPermission(res,function(){
          //              // console.log(res);
          //               documentInfo.push(res);

          //             })
          //documentInfo.push(res);
          // socket.syncUpdates('Document',documentInfo,function(event,item,array){

          //             console.log(event,item,array);
          //             //documentInfo=array;
          //             checkForPermission(item,function(){
          //              // console.log(res);
          //               //documentInfo.push(res);
          //             })
          // });

         // console.log(documentInfo);
         showAddRow=false;
         disableAddFolder=false;
         newFoldername="";
         flagsObj.disableAddFolder=true;
         flagsObj.showAddRow=false;
         blockUI.stop();
         notify({ message: 'Success- Folder successfully created', classes: 'alert-success', templateUrl: homerTemplate});          

          // var getdocs = DefaultDocs.show({
          //   id : currentdocument._id
          // }, function(response) {
          // documentInfo=response
          //   showAddRow=false;
          //   disableAddFolder=false;
          //   newFoldername="";
          //     flagsObj.disableAddFolder=true;
          //     flagsObj.showAddRow=false;
          //      notify({ message: 'Success- Folder successfully created', classes: 'alert-success', templateUrl: homerTemplate});
          // });
        });

           } else {
            blockUI.stop();
            notify({ message: 'Error - Folder name already exists', classes: 'alert-danger', templateUrl: homerTemplate});
            flagsObj.showAddRow="true"
            flagsObj.newFoldername="";
            flagsObj.disableAddFolder=false; 
          }
        } else {
          blockUI.stop();
          notify({ message: 'Enter valid folder name', classes: 'alert-danger', templateUrl: homerTemplate});
          flagsObj.showAddRow="true"
          flagsObj.newFoldername="";
          flagsObj.disableAddFolder=false;
        } 

        return flagsObj; 
      }
      
      this.docDuplicates = function(newFolder) {

        var notExists = true;
        angular.forEach(documentInfo,function(document,key){
         if (document.type == "folder" && document.documentName == newFolder) {
          notExists = false;
        }
      })


        return notExists;
      };

      this.findIcons=function(document){
        if(document.type=="folder"){
          return "pe-7s-folder"
        }
        else if((/\.(gif|jpg|jpeg|tiff|png|ico)$/i).test(document.documentName)){
          return "fa fa-file-picture-o";
        }
        else if ((/\.(docx|doc)$/i).test(document.documentName)) {

          return "fa fa-file-word-o";
        } else if ((/\.(xlsx|xls|csv)$/i).test(document.documentName)) {

          return "fa fa-file-excel-o";;
        } else if ((/\.(ppt|pptx)$/i).test(document.documentName)) {

          return "fa fa-file-powerpoint-o";
        }
        else if ((/\.(pdf)$/i).test(document.documentName)) {
          return "fa fa-file-pdf-o"
        }
        else {
          return "pe-7s-file";
        }

      }

      var checkForPermission = function(folder,cb) {
    //blockUI.start();

    //$scope.allowFolderClick = false;
    var docIndex;
    var flag = true;

    folder.access_list = [];
    Groups.getdocument({
      "document" : folder
    }, function(response) {
      if (response.length > 0) {
        angular.forEach(response, function(user, key) {
          var obj = {};
          angular.forEach(user.policies, function(policy, key) {
            if (policy.document.indexOf(folder._id) > -1) {
              obj.policy_id = policy.policy;
              obj.user_id = user.user;
              folder.access_list.push(obj);
              $rootScope.showEmptyContribList = false;
              if ($rootScope.loggedIn._id == user.user._id) {
                flag = false;
                folder.policyName = policy.policy.policy_name;
                folder.allowDelete = givePermission(policy.policy.permissions.delete)
                folder.allowDownload = givePermission(policy.policy.permissions.download);
                folder.allowAddFolders = givePermission(policy.policy.permissions.add_folders);
                folder.allowEdit = givePermission(policy.policy.permissions.edit);
                folder.allowShare = givePermission(policy.policy.permissions.share);
                folder.allowUpload = givePermission(policy.policy.permissions.upload);
                folder.allowView = givePermission(policy.policy.permissions.view);
                if (folder.policyName == "Owner" || folder.policyName == "Co-owner" || folder.policyName == "Publisher") {
                  folder.allowLock = true;
                  folder.allowRemovingContrib = true;
                  folder.showInvite=true;
                  $rootScope.showInvite=true;

                } else {
                  folder.allowLock = false;
                  folder.allowRemovingContrib = false;
                  folder.showInvite=false;
                  $rootScope.showInvite=false;
                  
                } 
              }
            }

          })
        })
        if (flag == true) {
          //set as owner if admin
          folder.policyName = "Owner";
          //
          folder.allowDelete = true;
          folder.allowDownload = true;
          folder.allowAddFolders = true;
          folder.allowEdit = true;
          folder.allowShare = true;
          folder.allowUpload = true;
          folder.allowView = true;
          folder.allowLock = true;
          folder.allowRemovingContrib = true;
          folder.showInvite=true
          $rootScope.showInvite=true;
        }

      } else {
        folder.policyName = "Owner";
        folder.allowDelete = true;
        folder.allowDownload = true;
        folder.allowAddFolders = true;
        folder.allowEdit = true;
        folder.allowShare = true;
        folder.allowUpload = true;
        folder.allowView = true;
        folder.allowLock = true;
        folder.showInvite=true;
        $rootScope.showInvite=true;
        folder.allowRemovingContrib = true;
      }
      cb();
    }, function(err) {
      //blockUI.stop();
    });
    //console.log(folder)
    $rootScope.allowFolderClick = true;
  };

  var givePermission = function(value) {
    if (value == "deny")
      return false;
    else
      return true;
  }
})

angular.module('homer')
  .service('uploadService', function (Upload,url,DefaultDocs,ActivitylogService,GlobalService) {
    var activity_type = GlobalService.getActivitytype();
  	this.file_upload=function(file1,currentDoc,callback) {
                console.log("parameter",file1);
                    file1.upload = Upload.upload({
                            url : url + 'api/Documents',
                            data : {
                                filename : file1.name,
                                s3_path : currentDoc.s3_path,
                                parent : currentDoc.parent,
                                id:currentDoc._id,
                                foldername : currentDoc.documentName,
                                //as_user:"582d565ea2339920d3ca5cb6",
                                size:file1.size,
                                file : file1
                            }
                        });
                        file1.upload.then(function(response) {
                                console.log("parameter",response);
                                callback(response.data);
                                var uploadLog = {};
                                    var uploadLog = {};
                                file1.progress=100;
                                    uploadLog.activitytype = activity_type.UPLOAD;
                                uploadLog.what = response.data._id;
                                // console.log(uploadLog.what);
                                uploadLog.location = currentDoc.parent + '/' + currentDoc.documentName;
                                uploadLog.status = true;
                                ActivitylogService.LogActivity(uploadLog);
                                // console.log(file1.progress);
                               // console.log(docsCtrlVariables.getWhichDocument());
                            // var getdocs = DefaultDocs.show({
                            //     id : currentDoc._id
                            // }, function(res) {
                            //     // console.log("parameter",response);
                            //     var uploadLog = {};
                            //     file1.progress=100;
                            //     uploadLog.activitytype = activity_type.UPLOAD;
                            //     uploadLog.what = response.data._id;
                            //     // console.log(uploadLog.what);
                            //     uploadLog.location = currentDoc.parent + '/' + currentDoc.documentName;
                            //     uploadLog.status = true;
                            //     ActivitylogService.LogActivity(uploadLog);
                            //     callback(res);
                            // });
                            file1.result = response.data;
                            },function(response) {
                                // console.log("parameter1",response);
                                callback(response);
                            }, function(evt) {
                                // console.log("event",evt);
                                file1.progress = Math.min(97, parseInt(100.0 * evt.loaded / evt.total));
                            // console.log(file1.progress);
                            });
            };

            // this.cancel_upload=function(file1){
            //     file1.upload.abort(); 
            // };
  })
angular.module('homer')
  .service('ErrorService', function ($http,$rootScope,ErrorFcrt) {
  		this.getUsermanagementErrors=function(data,cb){
  			ErrorFcrt.query(data,function(res){
				cb(res.errormsg);
			});
  		}
  })
angular.module('homer')
  .service('NoteService', function ($http,$rootScope,Note) {
  		this.getUsermanagementNotes=function(data,cb){
  			Note.query(data,function(res){
				cb(res.note);
			});
  		}
  })
angular.module('homer')
  .service('versionService', function (DefaultDocs,$rootScope) {

this.getVersion = function(doc){
	if(doc.type=='file'){
		if(doc.version){
			// console.log(doc.version);
			if(doc.version.length>1){
			doc.versionflag=true;
			// console.log(doc.documentName,doc.version.length,doc.version);
			return(doc.version.length);

			}
		}
		
	}

};

this.getThumbnailVersion=function(doc){
		if((/\.(gif|jpg|jpeg|tiff|png|ico)$/i).test(doc.documentName))
		{
				var file_id = {
							id : doc._id,
							version:doc.version
						};
						DefaultDocs.viewversionthumbnail(file_id, function(result) {
							// console.log(result);
							doc.thumbnail_url=result.url;
						})
		}
}

this.getThumbnail=function(doc){
	if(doc.thumbnail_url){
	var file_id = {
		id : doc._id,
	};
	DefaultDocs.viewversionthumbnail(file_id, function(result) {
		console.log(result);
		doc.thumbnail_url=result.url;
	})		
	}
}

this.setShowversionFlag=function(){
	
}



  });
angular.module('homer')
  .service('timelineService', function (DefaultDocs,$uibModal,timelineFctry) {


  	    var repeatlist = [
    	{ "id": 1, "repeat": " Daily " },
        { "id": 2, "repeat": " Weekly " },
        { "id": 3, "repeat": " Monthly " },
        { "id": 4, "repeat": " Yearly " }
    ];

this.setTimeline=function(doc){

    var modalInstance = $uibModal.open({
        templateUrl: 'views/modal/timeline.html',
        controller: timelineCtrl,
        size:'md',
        resolve:{
        	param:function(){
        		return{'doc':doc};
        	}
        }          
    })
}

this.getTimeline=function(doc,cb){
	var obj = {
			"_id" : doc._id
		};
		timelineFctry.query(obj, function(value) {
			// console.log(value);
				cb(value);
		});
        // console.log(obj);
}

this.saveTimeline=function(timeline,cb){
     console.log(timeline);
	timelineFctry.save(timeline,function(res){
		console.log(res,"set timeline servs");
		cb(res);
	})
}
this.updateTimeline=function(timeline,cb){
     console.log(timeline,"update timeline service");
	timelineFctry.update(timeline,function(res){
		console.log(res,"rsult");
		cb(res);
	})
}

this.validateTimeline=function(timeline){

}

this.getRepeat=function(){
	return repeatlist;
}

  });
angular.module('homer')
  .service('UserService', function ($http,$rootScope,User,userManagement) {
   	var listOfUsers=User.query(function(res){ 
    });
    
  	this.getUsers=function(cb){
  		User.query({'count':'undefined','limit':'undefined'},function(res){
				cb(res);
			});
  	};
   
    this.DuplicateUser=function(grp,userslist){
    var dup=false;
            angular.forEach(userslist,function(value,key){
               if(value.username==grp){
                dup=true;
              }
            });
          return dup
  };
  
  // $scope.groups=[
  //   {_id:1, text:'admin'},
  //   {_id:2, text:'user'},
  //   {_id:3, text:'Client Admin'}
  // ];  
    // var status= [
    //     {value: true, text: 'Active'},
    //     {value:false, text: 'Disabled'},
    // ];
    
    // this.getStatus = function(){
    //   return status;
    // }
    // var usermanagementBreadCrumbs = [];
    // this.initusermanagementBreadCrumbs=function(param){
    //   if(param=="userListing"){
    //   usermanagementBreadCrumbs.push("User Listing");
    //   }
    //   else{
    //     usermanagementBreadCrumbs.push("User Listing");
    //     usermanagementBreadCrumbs.push("Add User");
    //   }

    // }
    // this.getUserManagentBC=function(){
    //   //console.log(usermanagementBreadCrumbs)
    //     return usermanagementBreadCrumbs;
    // }
    // this.setUserManagementBC=function(param){
    //   usermanagementBreadCrumbs.push(param);
    //   // //console.log(usermanagementBreadCrumbs)
    // }
  
  });
/* global io */
'use strict';

angular.module('homer')
  .factory('socket', function() {

    // socket.io now auto-configures its connection when we ommit a connection url
    //var ioSocket = io.connect('http://tricons.2docstore.com:9000/socket.io-client');



    // var host = $location.host();
    // var ioSocket = io('https://mamatha.2docstore.com:9000', {
    //  //   // Send auth token on connection, you will need to DI the Auth service above
    // 'query': 'token=' + host.split('.')[0],

    // 'reconnection': true,
    // 'reconnectionDelay': 1000,
    // 'reconnectionDelayMax' : 5000,
    // 'reconnectionAttempts': 5,
    //  path: '/socket.io-client'

    // });

    // var socket = socketFactory({
    //   ioSocket: ioSocket
    // });

 return {
      // socket: socket,

      // /**
      //  * Register listeners to sync an array with updates on a model
      //  *
      //  * Takes the array we want to sync, the model name that socket updates are sent from,
      //  * and an optional callback function after new items are updated.
      //  *
      //  * @param {String} modelName
      //  * @param {Array} array
      //  * @param {Function} cb
      //  */
      // syncUpdates: function (modelName, array, cb) {
      //   cb = cb || angular.noop;


      //   /**
      //    * Syncs item creation/updates on 'model:save'
      //    */
      //   socket.on(modelName + ':save', function (item) {
      //    console.log("item here",item,array);
      //     var oldItem = _.find(array, {_id: item._id});
      //     var index = array.indexOf(oldItem);
      //     var event = 'created';
      //     // replace oldItem if it exists
      //     // otherwise just add item to the collection
      //     if (oldItem) {
      //       array.splice(index, 1, item);
      //       event = 'updated';
      //     } else {
      //      array.push(item);
      //    }

      //     cb(event, item, array);
      //   });

      //   /**
      //    * Syncs removed items on 'model:remove'
      //    */
      //   socket.on(modelName + ':remove', function (item) {
      //     console.log(item);
      //     var event = 'deleted';
      //     _.remove(array, {_id: item._id});
      //     cb(event, item, array);
      //   });

      //   socket.on(modelName + ':update', function (item) {
      //    console.log(item);
      //   });
      // },

      // /**
      //  * Removes listeners for a models updates on the socket
      //  *
      //  * @param modelName
      //  */
      // unsyncUpdates: function (modelName) {
      //   socket.removeAllListeners(modelName + ':save');
      //   socket.removeAllListeners(modelName + ':remove');
      // }
    };
  });

'use strict';

angular.module('homer')
  .service('ActivitylogService', function ($http,$rootScope,Auth,activityFcrt) {
 	var getCurrentUser=Auth.getCurrentUser;
 
  this.LogActivity = function(obj){
  	    //console.log("This is activity log service",obj);    
    var reminderObject = {
			who		: getCurrentUser()._id,
			/*browser	: deviceDetector.browser,
			device  : deviceDetector.device,*/
			level 	: 1
		};
		
    obj.who=getCurrentUser()._id;
    activityFcrt.save(obj, function(data) {
    	obj={};
		//console.log(data);	
		//callback();
	});
	
  };
  
  this.AsyncLogActivity = function(obj,callback){  	
   // console.log("This is activity log service",obj);
    obj.who=getCurrentUser()._id;
    //console.log("activity log service",obj.who);
   /* obj.browser=deviceDetector.browser;
    obj.device=deviceDetector.device;
    */
    var reminderObject = {
			who		: getCurrentUser()._id,
			/*browser	: deviceDetector.browser,
			device  : deviceDetector.device,
*/			level 	: 1
		};
		
    activityFcrt.save(obj, function(data) {
    	obj={};
		//console.log(data);	
		callback();
	});
	
  };
  
});
  
  

angular.module('homer')
  .service('ClientService', function ($http,$rootScope,clientFactory,DefaultDocs,User) {
  	
    
var clientList= clientFactory.query(function(data){
  // console.log(data,data.length,data.created_at);
})
 
 this.getClients=function(){
  return clientList;
 };

  this.getClientsAsync=function(cb){
    clientFactory.query({'count':'undefined','limit':'undefined'},function(data){
        cb(data);
    })
 };

 this.setclientList=function(cb){
  clientFactory.query({'count':'undefined','limit':'undefined'},function(data){
    clientList=data;
    cb();
  })
 }

 this.duplicate_Folder=function(folderName,cb){
  	//console.log(folderName);
  	var duplicate=false;
   		var response1 = DefaultDocs.query(function(data) {
          var defaultdocs = data;
         angular.forEach(defaultdocs,function(documents,key){
            if(documents.documentName=="Client Documents"){
               var response2=DefaultDocs.show({
                  id : documents._id
                  }, function(response) {
                  	var listOfClients=response;
                  		angular.forEach(listOfClients,function(value,key){
							if (value.type == "folder" && value.documentName == folderName) {
										duplicate = true;
									}
								
                  		});
                  		cb(duplicate);
                  });
               }
            })

        });

   };

 this.duplicate_email1=function(email,cb){
    var dup=false;
      var response2 = User.query({'count':'undefined','limit':'undefined'}, function(data) {
         angular.forEach(data,function(val,key){
              if(val.username==email){
                   dup=true;
                }      
          });
           cb(dup);
        });

      };


  });



  
angular.module('homer')
.service('DocstoreService',function($location, $rootScope, $http, User, $cookieStore, $q, url, DefaultDocs,activityFcrt, Upload, ActivitylogService,GlobalService){
var documents = [];
	var activity_type = GlobalService.getActivitytype();

//var description = DefaultDocs.modify();

this.getDescription=function(){
	console.log(description);
	return description;
};


	var mydocs=300000;
var clientDocs=40000;
this.getMydocsSize=function(){
	return mydocs;
};
this.getClientdocsSize=function(){
	return clientDocs;
};

var rootImagesPath=[
{
	type: "client",
	path: "assets/images/clientImage.png"
},
{
	type: "MyDocs",
	path:"assets/images/mydocs.png"
},
{
	type:"Shared",
	path:"assets/images/favouriteImage.png"
},
{
	type:"Fav",
	path:"assets/images/sharedImage.png"
},
{
	type:"search",
	path:"assets/images/search.png"
}
];
//function to get rootimage path
this.getRootDisplayImage=function(Rtype){
	var imgLoc=undefined;
	angular.forEach(rootImagesPath,function(value,key){
		if(value.type==Rtype)
		 imgLoc=value.path;
	});
	return imgLoc;
};
//function to get location
this.getdocImage=function(fType){
	var imgLocation = undefined;

	angular.forEach(docImage, function(value, key){
		if(value.type==fType)
		{
			imgLocation = value.location;
		}
	});

	return imgLocation;
	//console.log(imgLocation);
};
	// this.getdocumentPage = function() {
		// return documentPage;
	// };

	this.getcontribInfo = function() {
		return contribInfo;
	};
	var  src="assets/images/picjumbo.com_HNCK4153_resize.jpg";
	var  src1="assets/images/batman.jpg";
	var  src2="assets/images/images.jpg";
	var  src3="assets/images/lady.jpg";
  
	  this.getactivityLog = function(){
    	ActivityFcrt.query(function(data){
    		//console.log(data);
    		return data;
    	})	;
    }; 
	
	//Factory for document need these for calculations
	this.findPercentage=function(x,y){
		//console.log("to cal perc",x,y);
	var y=(x/y)*100;
	//console.log("perc is",y);
	return y;
	};
	this.gbToBytes=function(value){
		var mb=1024;
		var kb=1024;
		var bytes=1024;
		var byteConverted=(value*mb*kb*bytes);
		return byteConverted;
	};
})
angular.module('homer')
  .service('GroupService', function ($http,$rootScope,Groups,ActivitylogService,GlobalService,ClientService) {
  	var groupslist=Groups.getAllGroups();
	var activity_type = GlobalService.getActivitytype();
  	// listing logo
	this.imageExists = function(image,users){
	    var img = new Image();
	    img.onload = function() {
	      users.avatarflag = true;
	    }; 
	    img.onerror = function(){
	      users.avatarflag=false;
	    };
	    img.src = image;
	 };	

//To avoid the duplicate groups to be created
	this.DuplicateGroup=function(grp,list){
		console.log(grp,list);
		var dup=false;
		  angular.forEach(list,function(value,key){
		     if(value.Name==grp){
		    	dup=true;
		    }else {
		    }
		  });
		  return dup;
	};
//To make first letter as Uppercase and slice the 1st letter
	this.First_UpperCase = function(firstLtr) {
		if (firstLtr == undefined)
			return;
		var res = firstLtr.toUpperCase();
		var res1 = res.slice(0,1);
		return res1;
	};
		
/*
Select All
*/
var list=[];
		this.UserRemoveAll = function(select ,filtered,value){
			console.log(select ,filtered);
			if(select){
				list=[];
				angular.forEach(filtered, function(val, k){
					val.isChecked=true;
					console.log(val.isChecked);
					list.push(val._id);
				})
			}else{
				angular.forEach(filtered, function(val, k){
					val.isChecked=false;
					list=[];
				})	
			}		
		};
		

// indivisual check
	this.addOrRemoveUser = function(user,index){
	    if(user.isChecked){
	    	list.push(user._id);
	    }else{
	    	user.select=false;
	        var i=list.indexOf(user._id);
	        list.splice(i,1); 
	    }   	
	};
	
	// to delete
	this.deleteList=function(){
		list=[];
	}
	
	this.getList=function(){
		return list;
	}

	this.getGroupList=function(){
		return groupslist;
	}
	
//To make first letter as Uppercase in a group name 
	this.firstToUpperCase = function(strng){
	    var First_cap=strng.substr(0, 1).toUpperCase() + strng.substr(1);
	      return First_cap;
	};
	
	// creating the group
	this.creatGroup=function(group,cb){
		console.log("group",group,$rootScope.loggedIn);
		ClientService.getClientsAsync(function(data){
			//console.log("clients",data);
			console.log(data,$rootScope.loggedIn.organization);
			if($rootScope.loggedIn.role=="admin"){
				//group.clientid=value._id;
					//console.log("groupif",group);
					Groups.create(group,function(data){
						//console.log(data);
						var addgroup = {};
						addgroup.activitytype = activity_type.GROUPADD;
						addgroup.what = data._id;
						addgroup.status = true;
						ActivitylogService.LogActivity(addgroup);
						console.log(addgroup);
						cb();
					},function(err){ 
						cb();
					})
			}
			else{
				angular.forEach(data,function(value,key){
				if(value.name==$rootScope.loggedIn.organization)
				{	
					//console.log("Match")
					group.clientid=value._id;
					//console.log("group else",group);
					Groups.create(group,function(data){
						//console.log(data);
						var addgroup = {};
						addgroup.activitytype = activity_type.GROUPADD;
						addgroup.what = data._id;
						addgroup.status = true;
						ActivitylogService.LogActivity(addgroup);
						console.log(addgroup);
						cb();
					},function(err){ 
						cb();
					})
				}

			})
			}
			
		})
		
	};

		
	//update group
   this.updateGroup = function(user,cb){
	   Groups.Update(user,function(users){
	   		var groupUpdate = {};
			groupUpdate.activitytype = activity_type.GROUPUPDATE;
			groupUpdate.what = users._id;
			groupUpdate.status = true;
			ActivitylogService.LogActivity(groupUpdate);
			console.log(groupUpdate);
			cb();
		   	},function(err){
					cb();    
			});
   };

	
	
	
  })
angular.module('homer')
.service('PlanService',function(plansFctry){
	console.log("plans service");
	// var obj= plansFctry.query(function(data){
	// 	console.log(data);
	// })

	// this.getFreeplanObj=function(cb){
	// 	cb(obj) ;
	// }

	this.getFreeplanObj=function(cb){
		plansFctry.query(function(data){
			console.log("inside getFree",data)
			cb(data[0]);
			// return data;
		});
	}
	this.getStandardObj=function(cb){
		plansFctry.query(function(data){
			console.log("inside getFree",data)
			cb(data[1]);
		});
	}
})
function Groups($resource,$rootScope,url,GlobalService) {
	var restcalls=GlobalService.getRestVar();
	  return $resource(url+'api/groups/:id/:count/:limit', {id: '@_id'},
    {
      create:{
        method: restcalls.post,
        params:{
          id:'creategroup'
        }
      },
      getAllGroups:{
        method: restcalls.get,
        isArray:true,
        params:
        {
          id:'listGroup',
          count:'@count',
          limit:'@limit'
        }  
      },
      addUser:{
        method: restcalls.post,
        params:{
          id:'addUserToGroup'
        }
      },
      Update:{
        method: restcalls.post,
        params:{
          id:'update'
        }
      } ,
      removeGroup:{
        method: restcalls.post,
        isArray:true,
        params:{
          id:'deleteGroup'
        }
      },
      removeUsers:{
        method: restcalls.post,
        //isArray:true,
        params:{
          id:'deleteUsersFromGroup'
        }
      },
      removeUser:{
        method: restcalls.post,
        params:{
          id:'deleteUserFromGroup'
        }
      },
       AddGroup:{
        method: restcalls.post,
        params:{
          id:'addDocumentToGroup'
        }
      },  
       RemoveGroupDoc:{
        method: restcalls.post,
        params:{
          id:'deleteDocument'
        }
      },
      getGroups:{
        method: restcalls.post,
        isArray:true,
        params:{
          id:'getGroups'
        }
      },
       removeDocGroups:{
        method: restcalls.post,
        isArray:true,
        params:{
          id:'deleteDocument'
        }
      },
       getdocument:{
        method: restcalls.post,
        isArray:true,
        params:{
          id:'getdocument'
        }
      }

    });
}

angular
    .module('homer')
    .factory('Groups', Groups)
function timelineFctry($resource,$rootScope,url,GlobalService){
		var restcalls=GlobalService.getRestVar();
    return $resource(url + 'api/reminders/:id', {id : '@_id'}, 
		{
		save : {
			method : restcalls.post,
			params:{
	   			id:'createreminder'
	   		}
		},
		query : {
			method : restcalls.post,
			params:{
	   			id:'getreminder'
	   		}
		},
		update : {
			method : restcalls.post,
			params:{
	   			id:'modify'
	   		}
		}
		
		
	});
}
angular
    .module('homer')
    .factory('timelineFctry', timelineFctry)
function reportFactory($resource,url,GlobalService){
	var restcalls=GlobalService.getRestVar();
	return $resource(url+'api/reports/:id/:count/:limit',  { id:  '@_id' },
    {	
    	query:{
    		method:restcalls.post,
    		isArray:true,
    		params:{
    			id:'tenantLoginLogoutReport'
    		}
    	},
    	tenantConsilidatedReport:{
    		method:restcalls.post,
    		isArray:true,
    		params:{
    			id:'tenantConsilidatedReport'
    		}
    	},
    	userReport:{
    		method:restcalls.post,
    		isArray:true,
    		params:{
    			id:'userReport'
    		}
    	},
        reportCount:{
            method:restcalls.post,
            params:{
                id:'count'
            }
        }

    });
}
angular
	.module ('homer')
	.factory('reportFactory',reportFactory)
angular.module('homer')
  .controller('apiDocumentationCtrl', function ($scope,$state,$http,$rootScope,$location,md5,Auth,User,notify,DefaultDocs,GlobalService,ActivitylogService,docsCtrlVariables,blockUI,$window,$anchorScroll) {
    $scope.popupHeight = $window.innerHeight;
    console.log($scope.popupHeight)
    

})



